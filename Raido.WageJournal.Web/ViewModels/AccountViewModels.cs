﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Raido.WageJournal.Web.ViewModels
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(ResourceType = typeof(Localization.AccountViewModels), Name = "EmailLabel")]
        public string Email { get; set; }

        [Required]
        [Display(ResourceType = typeof(Localization.AccountViewModels), Name = "CompanyNameLabel")]
        public string CompanyName { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(ResourceType = typeof(Localization.AccountViewModels), Name = "CodeLabel")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(ResourceType = typeof(Localization.AccountViewModels), Name = "RememberBrowserLabel")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(ResourceType = typeof(Localization.AccountViewModels), Name = "EmailLabel")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(ResourceType = typeof(Localization.AccountViewModels), Name = "EmailLabel")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Localization.AccountViewModels), Name = "PasswordLabel")]
        public string Password { get; set; }

        [Display(ResourceType = typeof(Localization.AccountViewModels), Name = "RememberMeLabel")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "EmailLabel", ResourceType = typeof(Localization.AccountViewModels))]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceType = typeof(Localization.ErrorMessages), ErrorMessageResourceName = "PasswordLengthErrorMessage", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Localization.AccountViewModels), Name = "PasswordLabel")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Localization.AccountViewModels), Name = "ConfirmPasswordLabel")]
        [Compare("Password", ErrorMessageResourceType = typeof(Localization.ErrorMessages), ErrorMessageResourceName = "PasswordConfirmationErrorMessage")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "CompanyNameLabel", ResourceType = typeof(Localization.AccountViewModels))]
        public string CompanyName { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(ResourceType = typeof(Localization.AccountViewModels), Name = "EmailLabel")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceType = typeof(Localization.ErrorMessages), ErrorMessageResourceName = "PasswordLengthErrorMessage", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessageResourceType = typeof(Localization.ErrorMessages), ErrorMessageResourceName = "PasswordConfirmationErrorMessage")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(ResourceType = typeof(Localization.AccountViewModels), Name = "EmailLabel")]
        public string Email { get; set; }
    }
}
