﻿using System.Web.Mvc;

namespace Raido.WageJournal.Web.Extensions
{
    public static class HtmlExtensions
    {
        public static bool IsDebug(this HtmlHelper htmlHelper)
        {
            #if DEBUG
                return true;
            #else
                return false;
            #endif
        }
    }
}