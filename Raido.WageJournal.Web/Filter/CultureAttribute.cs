﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Raido.WageJournal.Web.Filter
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class CultureAttribute : ActionFilterAttribute
    {
        private const string DefaultCulture = "en-US";

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // TODO: Get user's preferred language from user setting or from browser language priority
            System.Globalization.CultureInfo userCulture = new System.Globalization.CultureInfo(DefaultCulture);
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                // TODO: Get user culture from claim
                var firstLocale = System.Security.Claims.ClaimsPrincipal.Current.FindAll(System.Security.Claims.ClaimTypes.Locality).Select(c => c.Value).FirstOrDefault() ?? DefaultCulture;

                try
                {
                    userCulture = new System.Globalization.CultureInfo(firstLocale);
                }
                catch (System.Globalization.CultureNotFoundException)
                {
                    userCulture = new System.Globalization.CultureInfo(DefaultCulture);
                }
            }
            else
            {
                string[] userLanguagePreferences = filterContext.HttpContext.Request.UserLanguages;

                // loop through each language setting to see if there's anything we provide for
                bool found = false;
                int count = 0;

                // find exact matches of cultures we support
                while (!found && userLanguagePreferences != null && count < userLanguagePreferences.Length)
                {
                    // Note that some browsers includes a ranking of the language therefore we need to split by the semi-colon
                    string cultureAndRank = userLanguagePreferences[count];
                    string cultureName = string.IsNullOrEmpty(cultureAndRank) ? string.Empty : cultureAndRank.Split(';')[0];
                    if (cultureName.IndexOf("en-us", StringComparison.InvariantCultureIgnoreCase) != -1)
                    {
                        userCulture = new System.Globalization.CultureInfo("en-US");
                        found = true;
                    }
                    else if (cultureName.IndexOf("en-gb", StringComparison.InvariantCultureIgnoreCase) != -1)
                    {
                        userCulture = new System.Globalization.CultureInfo("en-GB");
                        found = true;
                    }
                    else if (cultureName.IndexOf("zh-tw", StringComparison.InvariantCultureIgnoreCase) != -1)
                    {
                        userCulture = new System.Globalization.CultureInfo("zh-TW");
                        found = true;
                    }

                    count++;
                }


                // if we find a base culture, use default 
                count = 0;
                while (!found && userLanguagePreferences != null && count < userLanguagePreferences.Length)
                {
                    // Note that some browsers includes a ranking of the language therefore we need to split by the semi-colon
                    string cultureAndRank = userLanguagePreferences[count];
                    string cultureName = string.IsNullOrEmpty(cultureAndRank) ? string.Empty : cultureAndRank.Split(';')[0];
                    if (cultureName.IndexOf("en", StringComparison.InvariantCultureIgnoreCase) != -1)
                    {
                        userCulture = new System.Globalization.CultureInfo("en-US");
                        found = true;
                    }
                    else if (cultureName.IndexOf("zh", StringComparison.InvariantCultureIgnoreCase) != -1)
                    {
                        userCulture = new System.Globalization.CultureInfo("zh-TW");
                        found = true;
                    }

                    count++;
                }

                // if still nothing, default to english
                if (!found)
                {
                    userCulture = new System.Globalization.CultureInfo(DefaultCulture);
                }
            }

            System.Threading.Thread.CurrentThread.CurrentCulture = userCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = userCulture;
        }
    }
}