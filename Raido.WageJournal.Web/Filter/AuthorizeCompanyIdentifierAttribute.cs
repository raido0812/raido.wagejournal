﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Raido.WageJournal.DataAccess.Repositories.Interfaces;

namespace Raido.WageJournal.Web.Filter
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class AuthorizeCompanyIdentifierAttribute : ActionFilterAttribute
    {
        private ICompanyRepository _companyRepository;
        private string _companyIdentifierKey;
        public AuthorizeCompanyIdentifierAttribute(string companyIdentifierKey = null)
        {
            this._companyIdentifierKey = companyIdentifierKey;
            this._companyRepository = DependencyResolver.Current.GetService<ICompanyRepository>();
        }

        public new async void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (string.IsNullOrWhiteSpace(this._companyIdentifierKey))
            {
                throw new HttpException(500, Localization.CompanyIdentifierAttribute.AttributeNotSetupCorrectlyMessage);
            }

            var routeValue = filterContext.RouteData.Values[this._companyIdentifierKey];

            Guid identifier = Guid.Empty;
            if (routeValue != null &&
                Guid.TryParse(routeValue.ToString(), out identifier))
            {
                bool routeCompanyExists = await this._companyRepository.CompanyExistsAsync(identifier);

                if (!routeCompanyExists)
                {
                    throw new HttpException(403, Localization.CompanyIdentifierAttribute.UnauthorizedCompanyIdentifierMessage);
                }

                var currentCompanyIdentifier = await this._companyRepository.GetLastViewedCompanyIdentifierAsync();
                if (currentCompanyIdentifier != identifier && currentCompanyIdentifier != Guid.Empty)
                {
                    await this._companyRepository.UpdateLastViewedCompanyIdentifierAsync(identifier);
                }
            }
            else
            {
                throw new HttpException(403, Localization.CompanyIdentifierAttribute.UnknownCompanyIdentifierMessage);
            }

            base.OnActionExecuting(filterContext);
        }
    }
}