﻿/*
 * For API Compression check out http://benfoster.io/blog/aspnet-web-api-compression
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Raido.WageJournal.Extensions;

namespace Raido.WageJournal.Web.Filter
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class CompressionAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            var request = filterContext.HttpContext.Request;
            IList<AcceptEncodingDetails> acceptedEncoding = (request.Headers["Accept-Encoding"] ?? string.Empty).Split(',').Select(enc => new AcceptEncodingDetails(enc)).ToList();
            HttpResponseBase response = filterContext.HttpContext.Response;

            if (acceptedEncoding != null &&
                acceptedEncoding.Count != 0 &&
                response.Filter != null &&
                filterContext.Exception == null &&
                (
                    response.ContentType.IndexOf("json", StringComparison.CurrentCultureIgnoreCase) != -1 ||
                    response.ContentType.IndexOf("text", StringComparison.CurrentCultureIgnoreCase) != -1))
            {
                // As per RFC2616.14.3:
                // Ignores encodings with quality == 0
                // If multiple content-codings are acceptable, then the acceptable content-coding with the highest non-zero qvalue is preferred.
                var encoding = (from enc in acceptedEncoding
                                let quality = enc.Quality ?? 1.0m
                                where quality > 0
                                orderby quality descending
                                select enc).FirstOrDefault();
                if (encoding == null)
                {
                    return;
                }
                else if (encoding.Name.IndexOf("gzip", StringComparison.CurrentCultureIgnoreCase) != -1)
                {
                    response.Filter = new GZipStream(response.Filter, CompressionLevel.Fastest);
                    response.AppendHeader("Content-Encoding", "gzip");
                }
                else if (encoding.Name.IndexOf("deflate", StringComparison.CurrentCultureIgnoreCase) != -1)
                {
                    response.Filter = new DeflateStream(response.Filter, CompressionLevel.Fastest);
                    response.AppendHeader("Content-Encoding", "deflate");
                }

                // Allow proxy servers to cache encoded and unencoded versions separately
                response.AppendHeader("Vary", "Content-Encoding");
            }
        }

        private class AcceptEncodingDetails
        {
            internal string Name { get; set; }
            internal decimal? Quality { get; set; }
            public AcceptEncodingDetails(string rawHeader)
            {
                this.Name = rawHeader;
                this.Quality = null;
                string[] encodingParts = rawHeader.Split(new char[] { ';' });
                if (encodingParts != null && encodingParts.Length > 1)
                {
                    this.Name = encodingParts[0];
                    string quality = encodingParts[1];
                    if (quality.IndexOf("=", StringComparison.CurrentCultureIgnoreCase) != -1)
                    {
                        string[] qualityParts = quality.Split(new char[] { '=' });
                        this.Quality = qualityParts.Last().AsNullableDecimal();
                    }
                }
            }
        }
    }
}