﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    using Raido.WageJournal.Web;
    
    #line 2 "..\..\Areas\Companies\Views\Companies\Partial\DropdownMenuWithPreselect.cshtml"
    using Raido.WageJournal.Web.Areas.Companies.Localization;
    
    #line default
    #line hidden
    
    #line 1 "..\..\Areas\Companies\Views\Companies\Partial\DropdownMenuWithPreselect.cshtml"
    using Raido.WageJournal.Web.Areas.Companies.ViewModel;
    
    #line default
    #line hidden
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Areas/Companies/Views/Companies/Partial/DropdownMenuWithPreselect.cshtml")]
    public partial class _Areas_Companies_Views_Companies_Partial_DropdownMenuWithPreselect_cshtml : System.Web.Mvc.WebViewPage<CompanyMenuViewModel>
    {
        public _Areas_Companies_Views_Companies_Partial_DropdownMenuWithPreselect_cshtml()
        {
        }
        public override void Execute()
        {
            
            #line 5 "..\..\Areas\Companies\Views\Companies\Partial\DropdownMenuWithPreselect.cshtml"
  
    string firstCompanyName = LocalizedText.NoCompanyName;
    if (Model != null && Model.AllUserCompanies.Count > 0 &&
        Model.AllUserCompanies.Any(c => c.Identifier == Model.CurrentCompanyIdentifier))
    {
        firstCompanyName = Model.AllUserCompanies.First(c => c.Identifier == Model.CurrentCompanyIdentifier).TradeName;
    }

    var otherCompaniesExceptCurrent = Model.AllUserCompanies.Where(uc => uc.Identifier != Model.CurrentCompanyIdentifier).ToList();

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n<li");

WriteLiteral(" class=\"dropdown\"");

WriteLiteral(">\r\n    <a");

WriteLiteral(" class=\"dropdown-toggle\"");

WriteLiteral(" aria-expanded=\"true\"");

WriteLiteral(" role=\"button\"");

WriteLiteral(" data-toggle=\"dropdown\"");

WriteLiteral(" href=\"#\"");

WriteLiteral(">");

            
            #line 17 "..\..\Areas\Companies\Views\Companies\Partial\DropdownMenuWithPreselect.cshtml"
                                                                                             Write(firstCompanyName);

            
            #line default
            #line hidden
WriteLiteral(" <span");

WriteLiteral(" class=\"caret\"");

WriteLiteral("></span></a>\r\n    <ul");

WriteLiteral(" class=\"dropdown-menu\"");

WriteLiteral(" role=\"menu\"");

WriteLiteral(">\r\n");

            
            #line 19 "..\..\Areas\Companies\Views\Companies\Partial\DropdownMenuWithPreselect.cshtml"
        
            
            #line default
            #line hidden
            
            #line 19 "..\..\Areas\Companies\Views\Companies\Partial\DropdownMenuWithPreselect.cshtml"
         foreach (var company in otherCompaniesExceptCurrent)
        {

            
            #line default
            #line hidden
WriteLiteral("            <li>\r\n");

WriteLiteral("                ");

            
            #line 22 "..\..\Areas\Companies\Views\Companies\Partial\DropdownMenuWithPreselect.cshtml"
           Write(Html.ActionLink(company.TradeName + (!company.IsActive ? "(" + LocalizedText.InactiveCompanyText + ")" : string.Empty), MVC.Companies.Employees.Index(company.Identifier)));

            
            #line default
            #line hidden
WriteLiteral("\r\n            </li>\r\n");

            
            #line 24 "..\..\Areas\Companies\Views\Companies\Partial\DropdownMenuWithPreselect.cshtml"
        }

            
            #line default
            #line hidden
WriteLiteral("        <li");

WriteLiteral(" class=\"divider\"");

WriteLiteral("></li>\r\n        <li>\r\n            <div");

WriteLiteral(" class=\"col-xs-12\"");

WriteLiteral(">\r\n                <a");

WriteLiteral(" href=\"#\"");

WriteLiteral(" class=\"btn btn-default btn-block\"");

WriteLiteral(" data-toggle=\"modal\"");

WriteLiteral(" data-target=\"#addCompanyModal\"");

WriteLiteral(">");

            
            #line 28 "..\..\Areas\Companies\Views\Companies\Partial\DropdownMenuWithPreselect.cshtml"
                                                                                                            Write(LocalizedText.AddNewCompanyButtonText);

            
            #line default
            #line hidden
WriteLiteral("</a>\r\n            </div>\r\n        </li>\r\n    </ul>\r\n</li>");

        }
    }
}
#pragma warning restore 1591
