﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    
    #line 1 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
    using System.Collections.Generic;
    
    #line default
    #line hidden
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    
    #line 4 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
    using LocalizedText = Raido.WageJournal.Web.Areas.Companies.Localization.LocalizedText;
    
    #line default
    #line hidden
    
    #line 2 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
    using Raido.WageJournal.Contracts.ComplexTransferObjects;
    
    #line default
    #line hidden
    
    #line 3 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
    using Raido.WageJournal.Extensions;
    
    #line default
    #line hidden
    using Raido.WageJournal.Web;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Areas/Companies/Views/Companies/Details.cshtml")]
    public partial class _Areas_Companies_Views_Companies_Details_cshtml : System.Web.Mvc.WebViewPage<CompanyAndSettings>
    {
        public _Areas_Companies_Views_Companies_Details_cshtml()
        {
        }
        public override void Execute()
        {
            
            #line 6 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
  
    ViewBag.Title = string.Format(LocalizedText.CompanyDetailsTitle, Model.Company.TradeName);

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n\r\n<div");

WriteLiteral(" class=\"panel-heading\"");

WriteLiteral(">\r\n    <h2>");

            
            #line 12 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
    Write(ViewBag.Title + " " + (!Model.Company.IsActive ? "(" + LocalizedText.InactiveCompanyText + ")" : string.Empty));

            
            #line default
            #line hidden
WriteLiteral("</h2>\r\n</div>\r\n<div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n");

            
            #line 15 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
    
            
            #line default
            #line hidden
            
            #line 15 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
     using (Html.BeginForm(MVC.Companies.Companies.Details(), FormMethod.Post, new { id = "CompanyDetailsForm" }))
    {

            
            #line default
            #line hidden
WriteLiteral("        <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"col-xs-12 col-sm-6 col-lg-4\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n");

WriteLiteral("                    ");

            
            #line 20 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
               Write(Html.LabelFor(model => model.Company.FullName, new { data_identifier = Model.SaturdayOvertimeMultiplier.Identifier }));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("                    ");

            
            #line 21 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
               Write(Html.TextBoxFor(model => model.Company.FullName, new { required = "required", @class = "form-control" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("                    ");

            
            #line 22 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
               Write(Html.ValidationMessageFor(model => model.Company.FullName));

            
            #line default
            #line hidden
WriteLiteral("\r\n                </div>\r\n                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n");

WriteLiteral("                    ");

            
            #line 25 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
               Write(Html.LabelFor(model => model.Company.TradeName));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("                    ");

            
            #line 26 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
               Write(Html.TextBoxFor(model => model.Company.TradeName, new { required = "required", @class = "form-control" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("                    ");

            
            #line 27 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
               Write(Html.ValidationMessageFor(model => model.Company.TradeName));

            
            #line default
            #line hidden
WriteLiteral("\r\n                </div>\r\n                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n                    <label");

WriteLiteral(" class=\"policy-type\"");

WriteLiteral(">");

            
            #line 30 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
                                          Write(Model.AttendanceBonusAmount.PolicyType.GetDisplayName(typeof(Raido.WageJournal.Contracts.Enumerations.Localization.PolicyType)));

            
            #line default
            #line hidden
WriteLiteral("</label>\r\n");

WriteLiteral("                    ");

            
            #line 31 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
               Write(Html.TextBoxFor(model => model.AttendanceBonusAmount.Value, new { type = "number", step = "any", placeholder = 0.ToString("F2"), @class = "form-control", data_identifier = Model.AttendanceBonusAmount.Identifier }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                </div>\r\n                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n");

WriteLiteral("                    ");

            
            #line 34 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
               Write(Html.LabelFor(model => model.Company.IsActive));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("                    ");

            
            #line 35 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
               Write(Html.CheckBoxFor(model => model.Company.IsActive));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("                    ");

            
            #line 36 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
               Write(Html.ValidationMessageFor(model => model.Company.IsActive));

            
            #line default
            #line hidden
WriteLiteral("\r\n                </div>\r\n            </div>\r\n            <div");

WriteLiteral(" class=\"col-xs-12 col-sm-6 col-lg-4\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n                    <label");

WriteLiteral(" class=\"policy-type\"");

WriteLiteral(">");

            
            #line 41 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
                                          Write(Model.SaturdayOvertimeMultiplier.PolicyType.GetDisplayName(typeof(Raido.WageJournal.Contracts.Enumerations.Localization.PolicyType)));

            
            #line default
            #line hidden
WriteLiteral("</label>\r\n");

WriteLiteral("                    ");

            
            #line 42 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
               Write(Html.TextBoxFor(model => model.SaturdayOvertimeMultiplier.Value, new { type = "number", step = "any", placeholder = 0.ToString("F2"), @class = "form-control", data_identifier = Model.SaturdayOvertimeMultiplier.Identifier }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                </div>\r\n                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n                    <label");

WriteLiteral(" class=\"policy-type\"");

WriteLiteral(">");

            
            #line 45 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
                                          Write(Model.SundayOvertimeMultiplier.PolicyType.GetDisplayName(typeof(Raido.WageJournal.Contracts.Enumerations.Localization.PolicyType)));

            
            #line default
            #line hidden
WriteLiteral("</label>\r\n");

WriteLiteral("                    ");

            
            #line 46 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
               Write(Html.TextBoxFor(model => model.SundayOvertimeMultiplier.Value, new { type = "number", step = "any", placeholder = 0.ToString("F2"), @class = "form-control", data_identifier = Model.SundayOvertimeMultiplier.Identifier }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                </div>\r\n                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n                    <label");

WriteLiteral(" class=\"policy-type\"");

WriteLiteral(">");

            
            #line 49 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
                                          Write(Model.UnemploymentInsuranceFundPercentage.PolicyType.GetDisplayName(typeof(Raido.WageJournal.Contracts.Enumerations.Localization.PolicyType)));

            
            #line default
            #line hidden
WriteLiteral("</label>\r\n");

WriteLiteral("                    ");

            
            #line 50 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
               Write(Html.TextBoxFor(model => model.UnemploymentInsuranceFundPercentage.Value, new { type = "number", step = "any", placeholder = 0.ToString("F2"), @class = "form-control", data_identifier = Model.UnemploymentInsuranceFundPercentage.Identifier }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                </div>\r\n                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n                    <label");

WriteLiteral(" class=\"policy-type\"");

WriteLiteral(">");

            
            #line 53 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
                                          Write(Model.WorkDayHours.PolicyType.GetDisplayName(typeof(Raido.WageJournal.Contracts.Enumerations.Localization.PolicyType)));

            
            #line default
            #line hidden
WriteLiteral("</label>\r\n");

WriteLiteral("                    ");

            
            #line 54 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
               Write(Html.TextBoxFor(model => model.WorkDayHours.Value, new { type = "number", step = "any", placeholder = 0.ToString("F2"), @class = "form-control", data_identifier = Model.WorkDayHours.Identifier }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                </div>\r\n            </div>\r\n        </div>\r\n");

            
            #line 58 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
    }

            
            #line default
            #line hidden
WriteLiteral("</div>\r\n");

DefineSection("scripts", () => {

WriteLiteral("\r\n");

WriteLiteral("    ");

            
            #line 61 "..\..\Areas\Companies\Views\Companies\Details.cshtml"
Write(Scripts.Render("~/bundles/company-details"));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

});

        }
    }
}
#pragma warning restore 1591
