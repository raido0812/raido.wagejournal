﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    
    #line 1 "..\..\Areas\Companies\Views\Reports\PayslipFilterPage.cshtml"
    using LocalizedText = Raido.WageJournal.Web.Areas.Companies.Localization.LocalizedText;
    
    #line default
    #line hidden
    using Raido.WageJournal.Web;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Areas/Companies/Views/Reports/PayslipFilterPage.cshtml")]
    public partial class _Areas_Companies_Views_Reports_PayslipFilterPage_cshtml : System.Web.Mvc.WebViewPage<Guid?>
    {
        public _Areas_Companies_Views_Reports_PayslipFilterPage_cshtml()
        {
        }
        public override void Execute()
        {
WriteLiteral("\r\n");

            
            #line 4 "..\..\Areas\Companies\Views\Reports\PayslipFilterPage.cshtml"
  
    ViewBag.Title = LocalizedText.PayslipFilterPageTitle;

    Dictionary<DayOfWeek, int> closestSundaySearch = new Dictionary<DayOfWeek, int>(){
        {DayOfWeek.Sunday,0},
        {DayOfWeek.Monday,-1},
        {DayOfWeek.Tuesday,-2},
        {DayOfWeek.Wednesday,-3},
        {DayOfWeek.Thursday,-4},
        {DayOfWeek.Friday,-5},
        {DayOfWeek.Saturday,-6}
    };
    DateTime today = DateTime.Today;
    DateTime closestSunday = today.AddDays(closestSundaySearch[today.DayOfWeek]);
    DateTime mondayBeforeTheSunday = closestSunday.AddDays(-6);

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n\r\n<h2>");

            
            #line 22 "..\..\Areas\Companies\Views\Reports\PayslipFilterPage.cshtml"
Write(ViewBag.Title);

            
            #line default
            #line hidden
WriteLiteral("</h2>\r\n<div>\r\n    <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"col-xs-12 col-sm-6\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n                <label");

WriteLiteral(" for=\"StartDateTextBox\"");

WriteLiteral(">");

            
            #line 27 "..\..\Areas\Companies\Views\Reports\PayslipFilterPage.cshtml"
                                         Write(LocalizedText.FromDateLabelText);

            
            #line default
            #line hidden
WriteLiteral("</label>\r\n                <input");

WriteLiteral(" name=\"StartDateTextBox\"");

WriteLiteral(" id=\"StartDateTextBox\"");

WriteLiteral(" type=\"date\"");

WriteLiteral(" class=\"form-control date-pickers\"");

WriteAttribute("value", Tuple.Create("\r\n                       value=\"", 1026), Tuple.Create("\"", 1100)
            
            #line 29 "..\..\Areas\Companies\Views\Reports\PayslipFilterPage.cshtml"
, Tuple.Create(Tuple.Create("", 1058), Tuple.Create<System.Object, System.Int32>(mondayBeforeTheSunday.ToShortDateString()
            
            #line default
            #line hidden
, 1058), false)
);

WriteAttribute("placeholder", Tuple.Create("\r\n                       placeholder=\"", 1101), Tuple.Create("\"", 1171)
            
            #line 30 "..\..\Areas\Companies\Views\Reports\PayslipFilterPage.cshtml"
, Tuple.Create(Tuple.Create("", 1139), Tuple.Create<System.Object, System.Int32>(LocalizedText.FromDateLabelText
            
            #line default
            #line hidden
, 1139), false)
);

WriteLiteral(" />\r\n            </div>\r\n        </div>\r\n        <div");

WriteLiteral(" class=\"col-xs-12 col-sm-6\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n                <label");

WriteLiteral(" for=\"EndDateTextBox\"");

WriteLiteral(">");

            
            #line 35 "..\..\Areas\Companies\Views\Reports\PayslipFilterPage.cshtml"
                                       Write(LocalizedText.ToDateLabelText);

            
            #line default
            #line hidden
WriteLiteral("</label>\r\n                <input");

WriteLiteral(" name=\"EndDateTextBox\"");

WriteLiteral(" id=\"EndDateTextBox\"");

WriteLiteral(" type=\"date\"");

WriteLiteral(" class=\"form-control date-pickers\"");

WriteAttribute("value", Tuple.Create("\r\n                       value=\"", 1487), Tuple.Create("\"", 1553)
            
            #line 37 "..\..\Areas\Companies\Views\Reports\PayslipFilterPage.cshtml"
, Tuple.Create(Tuple.Create("", 1519), Tuple.Create<System.Object, System.Int32>(closestSunday.ToShortDateString()
            
            #line default
            #line hidden
, 1519), false)
);

WriteAttribute("placeholder", Tuple.Create("\r\n                       placeholder=\"", 1554), Tuple.Create("\"", 1622)
            
            #line 38 "..\..\Areas\Companies\Views\Reports\PayslipFilterPage.cshtml"
, Tuple.Create(Tuple.Create("", 1592), Tuple.Create<System.Object, System.Int32>(LocalizedText.ToDateLabelText
            
            #line default
            #line hidden
, 1592), false)
);

WriteLiteral(" />\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div");

WriteLiteral(" class=\"text-right\"");

WriteLiteral(">\r\n        <a");

WriteLiteral(" id=\"generateReportButton\"");

WriteLiteral(" href=\"#\"");

WriteLiteral(" data-report-url=\"");

            
            #line 43 "..\..\Areas\Companies\Views\Reports\PayslipFilterPage.cshtml"
                                                          Write(Url.Action(MVC.Companies.Reports.GetPayslipPdf()));

            
            #line default
            #line hidden
WriteLiteral("\"");

WriteLiteral(" class=\"btn btn-primary\"");

WriteLiteral(">");

            
            #line 43 "..\..\Areas\Companies\Views\Reports\PayslipFilterPage.cshtml"
                                                                                                                                      Write(LocalizedText.GenerateReportButtonText);

            
            #line default
            #line hidden
WriteLiteral("</a>\r\n    </div>\r\n</div>\r\n");

DefineSection("scripts", () => {

WriteLiteral("\r\n");

WriteLiteral("    ");

            
            #line 47 "..\..\Areas\Companies\Views\Reports\PayslipFilterPage.cshtml"
Write(Scripts.Render("~/bundles/reports/payslip"));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

});

        }
    }
}
#pragma warning restore 1591
