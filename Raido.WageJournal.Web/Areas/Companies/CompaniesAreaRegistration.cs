﻿using System.Web.Mvc;

namespace Raido.WageJournal.Web.Areas.Companies
{
    public class CompaniesAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Companies";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Companies_default",
                "Companies/{companyIdentifier:guid}/{controller}/{action}/",
                new { action = "Index", controller = "Companies" }
            );
        }
    }
}