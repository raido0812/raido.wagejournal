﻿// namespacing CompanyDetail
(function (companyDetailAutoSave, $, undefined) {
    //Private Property
    var currentModel = {};
    var $TradeNameTextBox = $('#Company_TradeName');
    var $FullNameTextBox = $('#Company_FullName');
    var $IsActiveCheckbox = $('#Company_IsActive');

    var $AttendanceBonusAmountTextBox = $('#AttendanceBonusAmount_Value');
    var $SaturdayPolicyTextbox = $('#SaturdayOvertimeMultiplier_Value');
    var $SundayPolicyTextbox = $('#SundayOvertimeMultiplier_Value');
    var $UifPolicyTextbox = $('#UnemploymentInsuranceFundPercentage_Value');
    var $WorkHourPolicyTextbox = $('#WorkDayHours_Value');

    var getModelValues = function () {
        var model = {};
        model.Company = {};
        model.SaturdayOvertimeMultiplier = {};
        model.SundayOvertimeMultiplier = {};
        model.UnemploymentInsuranceFundPercentage = {};
        model.WorkDayHours = {};
        model.AttendanceBonusAmount = {};

        model.Company.Identifier = $FullNameTextBox.data('identifier');
        model.Company.TradeName = $TradeNameTextBox.val();
        model.Company.FullName = $FullNameTextBox.val();
        
        model.Company.IsActive = $IsActiveCheckbox.is(':checked');
        
        model.AttendanceBonusAmount.Value = parseFloat($AttendanceBonusAmountTextBox.val());
        model.SaturdayOvertimeMultiplier.Value = parseFloat($SaturdayPolicyTextbox.val());
        model.SundayOvertimeMultiplier.Value = parseFloat($SundayPolicyTextbox.val());
        model.UnemploymentInsuranceFundPercentage.Value = parseFloat($UifPolicyTextbox.val());
        model.WorkDayHours.Value = parseFloat($WorkHourPolicyTextbox.val());

        model.AttendanceBonusAmount.Identifier = $AttendanceBonusAmountTextBox.data('identifier');
        model.SaturdayOvertimeMultiplier.Identifier = $SaturdayPolicyTextbox.data('identifier');
        model.SundayOvertimeMultiplier.Identifier = $SundayPolicyTextbox.data('identifier');
        model.UnemploymentInsuranceFundPercentage.Identifier = $UifPolicyTextbox.data('identifier');
        model.WorkDayHours.Identifier = $WorkHourPolicyTextbox.data('identifier');
        return model;
    };

    var lastSavedModel = getModelValues();

    //Public Method
    companyDetailAutoSave.updateModel = function () {
        currentModel = getModelValues();
    };

    companyDetailAutoSave.getModel = function () { return currentModel; };

    companyDetailAutoSave.modelHasChanges = function () {
        if (!currentModel) {
            companyDetailAutoSave.updateModel();
        }

        if (lastSavedModel && currentModel) {
            return lastSavedModel.Company.Identifier !== currentModel.Company.Identifier ||
                   lastSavedModel.Company.TradeName !== currentModel.Company.TradeName ||
                   lastSavedModel.Company.FullName !== currentModel.Company.FullName ||
                   lastSavedModel.Company.IsActive !== currentModel.Company.IsActive ||
                   lastSavedModel.AttendanceBonusAmount.Value !== currentModel.AttendanceBonusAmount.Value ||
                   lastSavedModel.SaturdayOvertimeMultiplier.Value !== currentModel.SaturdayOvertimeMultiplier.Value ||
                   lastSavedModel.SundayOvertimeMultiplier.Value !== currentModel.SundayOvertimeMultiplier.Value ||
                   lastSavedModel.UnemploymentInsuranceFundPercentage.Value !== currentModel.UnemploymentInsuranceFundPercentage.Value ||
                   lastSavedModel.WorkDayHours.Value !== currentModel.WorkDayHours.Value ||
                   lastSavedModel.SaturdayOvertimeMultiplier.Identifier !== currentModel.SaturdayOvertimeMultiplier.Identifier ||
                   lastSavedModel.SundayOvertimeMultiplier.Identifier !== currentModel.SundayOvertimeMultiplier.Identifier ||
                   lastSavedModel.UnemploymentInsuranceFundPercentage.Identifier !== currentModel.UnemploymentInsuranceFundPercentage.Identifier ||
                   lastSavedModel.WorkDayHours.Identifier !== currentModel.WorkDayHours.Identifier;
        }
        else {
            return !lastSavedModel;
        }
    };

    companyDetailAutoSave.saveDetails = function (url, callback) {
        if ($('[data-val="true"]').valid()) {
            companyDetailAutoSave.updateModel();
            if (companyDetailAutoSave.modelHasChanges()) {
                
                lastSavedModel = getModelValues();
                $.ajax({
                    url: url,
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify(currentModel),
                    success: function (result) {
                        if (callback && typeof (callback) === 'function') {
                            callback(result);
                        }

                        if (result) {
                            toastr.success(document.webL10n.get("AutoSaveSuccessful"));
                        }
                        else {
                            toastr.error(document.webL10n.get("AutoSaveFailed"));
                        }
                    },
                    error: function (result) {
                        toastr.error(document.webL10n.get("AutoSaveFailed"));

                        if (callback && typeof (callback) === 'function') {
                            callback(result);
                        }
                    }
                });
            }
        }
    };

}(window.companyDetailAutoSave = window.companyDetailAutoSave || {}, jQuery));