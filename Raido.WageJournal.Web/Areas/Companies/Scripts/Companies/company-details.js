﻿$(function () {
    var $currentDocument = $(document);
    $currentDocument.on('change keyup', '#CompanyDetailsForm input', function queueSave() {
        var previousId = $currentDocument.data('company-details-auto-save-queued');
        if (previousId) {
            clearTimeout(previousId);
        }

        var timeoutId = setTimeout(function () {
                companyDetailAutoSave.saveDetails(document.location, function (result) {
                    // do nothing with result
                });
        },
        1000);

        $currentDocument.data('company-details-auto-save-queued', timeoutId);
    });
});