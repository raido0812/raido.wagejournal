﻿$(function()
{
    var timeSheetLoad = function()
    {

        var $fileUpload = $('#TimesheetFileUpload');

        var twoDecimalNumberFormatter = Globalize(window.appSettings.culture).numberFormatter({ minimumFractionDigits: 2, maximumFractionDigits: 2 });
        var zeroDecimalNumberFormatter = Globalize(window.appSettings.culture).numberFormatter({ minimumIntegerDigits: 2, maximumFractionDigits: 0 });
        var numberParser = Globalize(window.appSettings.culture).numberParser();

        var generateTimeCardEntryHeader = function()
        {
            let $logEntryHeaderDiv = $('<div class="container-fluid"/>');

            return $logEntryHeaderDiv
                    .append($('<div class="row"/>')
                        .append($('<div class="col-xs-3"/>')
                            .text(document.webL10n.get("ImportCardMachineEntryDateHeading")))
                        .append($('<div class="col-xs-2"/>')
                            .text(document.webL10n.get("ImportCardMachineEntryStartTimeHeading")))
                        .append($('<div class="col-xs-2"/>')
                            .text(document.webL10n.get("ImportCardMachineEntryEndTimeHeading")))
                        .append($('<div class="col-xs-2"/>')
                            .text(document.webL10n.get("ImportCardMachineEntryBreaksHeading")))
                        .append($('<div class="col-xs-2"/>')
                            .text(document.webL10n.get("ImportCardMachineEntryHoursHeading"))));
        };

        var formatCSharpTimeStamp = function(timestamp)
        {
            if (!timestamp) {
                return '';
            }

            var hoursFormatted = zeroDecimalNumberFormatter(timestamp.Hours || 0);
            var minutesFormatted = zeroDecimalNumberFormatter(timestamp.Minutes || 0);

            return hoursFormatted + ":" + minutesFormatted
        };

        // generate a div containing the individual time card log entry details
        var generateTimeCardEntryDisplay = function(timesheetLogEntries)
        {
            var $logEntryDiv = $('<div class="container-fluid"/>');
            $.each(timesheetLogEntries, function(index, logEntry)
            {
                var hourDifferenceFormatted = twoDecimalNumberFormatter(logEntry.HourDifference);

                $logEntryDiv
                    .append($('<div class="js-log-entry row"/>')
                        .append($('<div class="col-xs-3 js-log-entry-date"/>')
                            .data('date', logEntry.DateISO8601Display)
                            .text(logEntry.DateDisplay))
                        .append($('<div class="col-xs-2"/>')
                            .append($('<input type="text" class="form-control js-start-time-input"/>')
                                .val(formatCSharpTimeStamp(logEntry.StartTime))))
                        .append($('<div class="col-xs-2"/>')
                            .append($('<input type="text" class="form-control js-end-time-input"/>')
                                .val(formatCSharpTimeStamp(logEntry.EndTime))))
                        .append($('<div class="col-xs-2"/>')
                            .append($('<input type="text" class="form-control js-break-time-input"/>')))
                        .append($('<div class="col-xs-2 js-hours-worked-display"/>')
                            .text(hourDifferenceFormatted)));
            });

            return $logEntryDiv;
        };

        // generate a div containing the extracted employee and time log details
        var generateTimeCardEmployeeEntryDisplay = function(employeeEntry)
        {
            var $employeeDiv = $('<div class="js-employee-timesheet-result"/>')
                .append($('<div class="form-inline imported-employee-timesheet"/>')
                    .append($('<strong/>')
                        .text(document.webL10n.get(
                            'EmployeeNumberOnTimeCard',
                            'Employee Number on Time Card') + ': ' + employeeEntry.TimesheetMetaInfo.EmployeeNumber + ' '))
                    .append($('#EmployeeDropDownList')
                        .clone()
                        .addClass('js-map-to-employee-dropdown')
                        .on('change', function()
                        {
                            var $dropDown = $(this);
                            if ($dropDown.val() !== '')
                            {
                                $dropDown.addClass("has-selected")
                            }
                            else
                            {
                                $dropDown.removeClass("has-selected")
                            }
                        })))
                .append(generateTimeCardEntryHeader())
                .append(generateTimeCardEntryDisplay(employeeEntry.TimesheetLogEntries));

            return $employeeDiv;
        }

        var getTotalMinutesFromTimestampString = function(timestamp)
        {
            var split = timestamp.split(':');
            var hourPortion = split[0] || '0';
            var minutePortion = split[1] || '0';

            var hours = numberParser(hourPortion);

            var minutes = numberParser(minutePortion);

            return (hours || 0) * 60 + (minutes || 0);
        }

        // handle drop zone styling effect
        $(document).bind('dragover', function(e)
        {
            var dropZone = $('.file-drop-zone'),
                timeout = window.dropZoneTimeout;

            if (timeout) {
                clearTimeout(timeout);
            } else {
                dropZone.addClass('in');
            }
            var hoveredDropZone = $(e.target).closest(dropZone);
            if (hoveredDropZone.length > 0 &&
                !dropZone.hasClass('hover')) {
                dropZone.toggleClass('hover', hoveredDropZone.length);
            }

            window.dropZoneTimeout = setTimeout(function()
            {
                window.dropZoneTimeout = null;
                dropZone.removeClass('in hover');
            }, 100);
        }).on('change keyup', '.js-start-time-input,.js-end-time-input,.js-break-time-input', function()
        {
            var $currentRow = $(this).closest('.js-log-entry');
            var startTimeInMinutes = getTotalMinutesFromTimestampString($currentRow.find('.js-start-time-input').val());
            var endTimeInMinutes = getTotalMinutesFromTimestampString($currentRow.find('.js-end-time-input').val());
            var breakHours = numberParser($currentRow.find('.js-break-time-input').val());
            
            var hours = ((endTimeInMinutes - startTimeInMinutes) / 60) - breakHours;
            $currentRow.find('.js-hours-worked-display').text(twoDecimalNumberFormatter(hours));
        }).on('click', '.js-save-mapped-timesheet-data', function()
        {
            var saveData = [];
            $('.js-employee-timesheet-result').each(function(index, employeeTimesheetDiv)
            {
                var $employeeTimesheetDiv = $(employeeTimesheetDiv);
                var employeeIdentifier = $employeeTimesheetDiv.find('.js-map-to-employee-dropdown').val();
                if (employeeIdentifier) {
                    $employeeTimesheetDiv.find('.js-log-entry').each(function(index, logEntryDiv)
                    {
                        var $logEntryDiv = $(logEntryDiv);

                        var hours = numberParser($logEntryDiv.find('.js-hours-worked-display').text());
                        var date = $logEntryDiv.find('.js-log-entry-date').data('date');

                        if (date && hours && hours !== 0) {
                            $logEntryDiv.addClass('bg-success');
                            saveData.push({
                                EmployeeIdentifier: employeeIdentifier,
                                DateWorked: date,
                                HoursWorked: hours
                            });
                        } else {
                            $logEntryDiv.addClass('ignored-for-save bg-danger');
                        }
                    });
                }
            });

            if (!saveData || saveData.length === 0)
            {
                // highlight first dropdown
                $('.js-map-to-employee-dropdown').addClass('error');
                toastr.error(document.webL10n.get("NoEmployeesSelectedInDropDown"));
                return;
            }

            $.ajax({
                url: $('#ExtractedResults').data('save-url'),
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(saveData),
                success: function(result)
                {
                    // do nothing 

                    if (result) {
                        toastr.success(document.webL10n.get("SaveSuccessful"));
                    }
                    else {
                        toastr.error(document.webL10n.get("SaveFailed"));
                    }
                },
                error: function(result)
                {
                    toastr.error(document.webL10n.get("SaveFailed"));
                }
            });
        });

        $fileUpload.fileupload({
            url: $fileUpload.data('upload-url'),
            dataType: 'json',
            dropZone: $('.file-drop-zone'),
            acceptFileTypes: /(\.|\/)(xls|xml)$/i,
            maxFileSize: 5000000,
            done: function(e, data)
            {
                var $resultsDiv = $('#ExtractedResults').empty();
                $.each(data.result, function(index, employeeEntry)
                {
                    // interestingly i couldn't return html here to be replaced. 
                    // this callback doesn't fire at all unless the response is json
                    $resultsDiv.append(
                        generateTimeCardEmployeeEntryDisplay(
                            employeeEntry));
                });

                $resultsDiv.append($('<div class="floating-save-button-wrapper"/>')
                        .append($('<a href="#" class="js-save-mapped-timesheet-data floating-save-button btn btn-success"/>')
                            .text(document.webL10n.get('SaveMappedTimesheetData', 'Save mapped timesheet data'))));
            },
            fail: function(e, data)
            {
                console.log('fail', e, data);
            },
            progressall: function(e, data)
            {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css(
                    'width',
                    progress + '%'
                );
            }
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    };

    // if globalize is not ready, check again in 300 milliseconds
    var init = function checkForGlobalizeReadyBeforeInitialize()
    {
        if (!$(document).data('globalize-loaded')) {
            setTimeout(checkForGlobalizeReadyBeforeInitialize, 200);
        }
        else {
            timeSheetLoad();
        }
    }

    init();
});