﻿$(function () {
    var updateGenerateUrl = function () {
        // update the generate Url
        var $gerenateButton = $('#generateReportButton');
        url = $gerenateButton.data('report-url');
        var startDate = $('#StartDateTextBox').datepicker('getDate');
        var endDate = $('#EndDateTextBox').datepicker('getDate');

        url += '/?startDate=' + (startDate ? startDate.toUniversalString() : '') +
                '&endDate=' + (endDate ? endDate.toUniversalString() : '')
        $gerenateButton.attr('href', url);
    };

    $('.date-pickers')
        .datepicker()
        .on('changeDate', function () {
            updateGenerateUrl();
        });

    updateGenerateUrl();
});