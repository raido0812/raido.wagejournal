﻿// namespacing workDetailAutoSave
(function (workDetailAutoSave, $, undefined) {
    //Private Property
    var currentModel = {};
    var getModelValues = function ($row) {
        var model = {};
        
        model.HoursWorked = $row.find('#work_HoursWorked').val();
        model.DateWorked = $row.find('#Day').val();
        model.Identifier = $row.find('#work_Identifier').val() || uuid.v4();

        return model;
    };

    //Public Method
    workDetailAutoSave.updateModel = function ($row) {
        currentModel = getModelValues($row);
        return currentModel;
    };

    workDetailAutoSave.getModel = function () { return currentModel; };

    workDetailAutoSave.modelHasChanges = function ($row) {
        if (!currentModel) {
            workDetailAutoSave.updateModel($row);
        }

        var lastSavedModel = $row.data('lastSavedModel');
        if (lastSavedModel && currentModel) {
            return lastSavedModel.HoursWorked !== currentModel.HoursWorked;
        }
        else {
            return !lastSavedModel;
        }
    };

    var validate = function ($row) {
        if (!currentModel) {
            workDetailAutoSave.updateModel($row);
        }

        // custom validation conditions
        if (isNaN(parseFloat(currentModel.HoursWorked)))
        {
            $row.find('#work_HoursWorked').addClass('error');
            return false;
        }

        $row.find('#work_HoursWorked').removeClass('error');
        return $row.find('input[data-val="true"]').valid();
    };

    workDetailAutoSave.saveDetails = function (url, $row, callback) {
        if (validate($row)) {
            $row.data('lastSavedModel', currentModel);
            $.ajax({
                url: url,
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(currentModel),
                success: function (result) {
                    if (callback && typeof (callback) === 'function') {
                        callback(result);
                    }

                    if (result) {
                        toastr.success(document.webL10n.get("AutoSaveSuccessful"));
                    }
                    else {
                        toastr.error(document.webL10n.get("AutoSaveFailed"));
                    }
                },
                error: function (result) {
                    toastr.error(document.webL10n.get("AutoSaveFailed"));

                    if (callback && typeof (callback) === 'function') {
                        callback(result);
                    }
                }
            });
        }
        else {
            if (callback && typeof (callback) === 'function') {
                callback(null);
            }
        }
    };

}(window.workDetailAutoSave = window.workDetailAutoSave || {}, jQuery));