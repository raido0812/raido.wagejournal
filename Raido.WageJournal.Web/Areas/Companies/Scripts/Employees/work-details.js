﻿$(function () {
    var $currentDocument = $(document);
    
    var loadModalForAnotherDay = function ($modalBody, dayIncrement)
    {
        var currentDate = new Date($modalBody.find('#Day').val() || '');
        if (!currentDate)
        {
            return;
        }

        var previousDay = currentDate.addDays(dayIncrement).toUniversalString();
        modalLoader.loadEmployeeWorkDayModal('#editEmployeeWorkDayModal', $modalBody.data('employee-identifier'), previousDay);
    }

    $currentDocument.on('click', '.work-day-cell', function (e)
    {
        var $clickedCell = $(this);
        modalLoader.loadEmployeeWorkDayModal('#editEmployeeWorkDayModal', $clickedCell.data('employee-identifier'), $clickedCell.closest('.work-day-wrapper').data('work-date'));
        e.preventDefault();
    }).on('click', '.employee-work-date .previous-day-button', function ()
    {
        var $modalBody = $(this).closest('.modal-body');
        loadModalForAnotherDay($modalBody, -1);
    }).on('click', '.employee-work-date .next-day-button', function ()
    {
        var $modalBody = $(this).closest('.modal-body');
        loadModalForAnotherDay($modalBody, 1);
    }).on('change keyup', '.work-row input', function queueSave()
        {
        var $wordDetailRow = $(this).closest('.work-row');
        var day = $wordDetailRow.find('#Day').val();
        var autoSaveQueueKey = 'work-details-' + day + '-' + $wordDetailRow.find('#Work_Identifier').val();

        var previousId = $currentDocument.data(autoSaveQueueKey);
        if (previousId) {
            clearTimeout(previousId);
        }

        var timeoutId = setTimeout(function () {
            workDetailAutoSave.updateModel($wordDetailRow);
            if (workDetailAutoSave.modelHasChanges($wordDetailRow)) {
                var url = $wordDetailRow.data('save-url') + '';
                workDetailAutoSave.saveDetails(url, $wordDetailRow, function (result) {
                    // TODO: Raise Event to refresh the day of work with updated amount
                    $(document).trigger('WorkDayUpdated', { day })

                });
            }
        },
        1000);

        $currentDocument.data(autoSaveQueueKey, timeoutId);
    });
});