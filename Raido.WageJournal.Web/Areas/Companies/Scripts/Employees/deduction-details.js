﻿$(function () {
    var $currentDocument = $(document);

    var loadDeduction = function (guid) {
        // Get the modal, clear out the content, load an empty one
        var $deductionDetails = $('.deduction-details');
        $deductionDetails.empty();
        var url = $deductionDetails.data('details-url').replace("00000000-0000-0000-0000-000000000000", guid);
        preloader.show();
        $.ajax({
            url: url,
            type: 'GET',
            success: function (result) {
                preloader.hide();
                $deductionDetails.html(result);

                var allDateInputs = $('input[type="Date"]')
                    .attr('type', 'text')
                    .datepicker({ language: window.appSettings.culture, format: window.appSettings.bootstrapDateFormat });

                $currentDocument.trigger('deduction-form-loaded');
            }
        });
    };

    $currentDocument.on('click', '#editEmployeeDeductionModal .new-deduction.btn', function (e) {
        loadDeduction(uuid.v4());
    }).on('click', '.deduction-row', function (e) {
        var $deductionRow = $(this);
        $('.deduction-row').removeClass('selected');
        $deductionRow.addClass('selected');
        loadDeduction($deductionRow.data('identifier'));
    }).on('change keyup', '#DeductionDetailsForm input, #DeductionDetailsForm select', function queueSave() {

        var $form = $(this).closest('#DeductionDetailsForm');
        var deductionIdentifier = $form.find('#deduction_Identifier').val();
        var autoSaveQueueKey = 'deduction-details-' + deductionIdentifier;

        var previousId = $currentDocument.data(autoSaveQueueKey);
        if (previousId) {
            clearTimeout(previousId);
        }

        var timeoutId = setTimeout(function () {
                var url = $form.attr('action').replace('00000000-0000-0000-0000-000000000000', deductionIdentifier);
                deductionDetailAutoSave.saveDetails(url, $form, function (result) {
                    // do nothing with result
                    var deductionListing = $('.deduction-listing');
                    preloader.show();
                    $.ajax({
                        url: deductionListing.data('reload-url'),
                        type: 'GET',
                        success: function (result) {
                            preloader.hide();
                            deductionListing.empty();
                            deductionListing.html(result);
                            $('.deduction-row[data-identifier="' + deductionIdentifier + '"]').addClass("selected");
                        }
                    });
                });
        },
        1000);

        $currentDocument.data(autoSaveQueueKey, timeoutId);
    });
});