﻿(function (modalLoader, $, undefined)
{

    modalLoader.loadEmployeeWorkDayModal = function (modalWrapperSelector, employeeIdentifier, date)
    {
        /// <summary>Get the specified modal, clear out the content, load details for employee work day</summary>
        /// <param name="modalWrapperSelector" type="String">The jQuery selector to find the modal to which the employee work day details will be loaded</param>
        /// <param name="employeeIdentifier" type="String">Guid of the selected employee</param>
        /// <param name="date" type="String">Work Day Date to load</param>
        var $editEmployeeWorkDayModal = $(modalWrapperSelector);
        $editEmployeeWorkDayModal.find('.modal-content').empty();
        $editEmployeeWorkDayModal.modal('show');
        var url = $editEmployeeWorkDayModal
            .data('details-url')
            .replace("00000000-0000-0000-0000-000000000000", employeeIdentifier) + '?workDay=' + date;

        preloader.show();
        $.ajax({
            url: url,
            type: 'GET',
            success: function (result)
            {
                preloader.hide();
                var $result = $(result);
                $editEmployeeWorkDayModal.find('.modal-content').html($result);
                window.setTimeout(function() { $result.find('.hours-worked-textbox').focus(); }, 100);
            }
        });
    };
    
    modalLoader.loadEmployeeDeductionModal = function (modalWrapperSelector, employeeIdentifier)
    {
        /// <summary>Get the specified modal, clear out the content, load details for employee deductions</summary>
        /// <param name="modalWrapperSelector" type="String">The jQuery selector to find the modal to which the employee work day details will be loaded</param>
        /// <param name="employeeIdentifier" type="String">Guid of the selected employee</param>
        var $employeeDeductionModal = $(modalWrapperSelector);
        $employeeDeductionModal.find('.modal-content').empty();
        $employeeDeductionModal.modal('show');
        var url = $employeeDeductionModal.data('details-url').replace("00000000-0000-0000-0000-000000000000", employeeIdentifier);
        preloader.show();
        $.ajax({
            url: url,
            type: 'GET',
            success: function (result)
            {
                preloader.hide();
                $employeeDeductionModal.find('.modal-content').html(result);
            }
        });
    };


    modalLoader.loadEmployeeDetailsModal = function (modalWrapperSelector, employeeIdentifier)
    {
        /// <summary>Get the specified modal, clear out the content, load details for employee</summary>
        /// <param name="modalWrapperSelector" type="String">The jQuery selector to find the modal to which the employee work day details will be loaded</param>
        /// <param name="employeeIdentifier" type="String">Guid of the selected employee</param>
        var $editEmployeeModal = $(modalWrapperSelector);
        $editEmployeeModal.find('.modal-content').empty();
        $editEmployeeModal.modal('show');
        var url = $editEmployeeModal.data('details-url').replace("00000000-0000-0000-0000-000000000000", employeeIdentifier);
        preloader.show();
        $.ajax({
            url: url,
            type: 'GET',
            success: function (result)
            {
                preloader.hide();
                $editEmployeeModal.find('.modal-content').html(result);

                // bind datepickers as they weren't there on page load to be bound
                var allDateInputs = $('input[type="Date"]')
                    .attr('type', 'text')
                    .datepicker({ language: window.appSettings.culture, format: window.appSettings.bootstrapDateFormat });

                $(document).trigger('employee-form-loaded');
            }
        });
    };

})(window.modalLoader = window.modalLoader || {}, jQuery, null)