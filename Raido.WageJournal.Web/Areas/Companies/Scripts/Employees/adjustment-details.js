﻿$(function () {
    var $currentDocument = $(document);
    $currentDocument.on('change keyup', '.adjustment-row input', function queueSave(e) {
        var $inputChanged = $(this);
        var $adjustmentDetailRow = $inputChanged.closest('.adjustment-row');
        var day = $adjustmentDetailRow.find('#Day').val();
        var autoSaveQueueKey = 'adjustment-details-' + day + '-' + $adjustmentDetailRow.find('#Adjustment_Identifier').val();
        
        var isDescriptionChanged = $inputChanged.attr('Id').indexOf('adjustment_Description') > -1;
        if (isDescriptionChanged)
        {
            var formulaToEvaluate = $inputChanged.val();
            var formulaRegEx = /[(].*[)]/;
            var formulaSection = formulaRegEx.exec($inputChanged.val());
            if (formulaSection)
            {
                formulaToEvaluate = formulaSection[0];
            }

            formulaToEvaluate = formulaToEvaluate.replace(/x/gi, '*');

            var mathEvalResult = 0;
            try {
                mathEvalResult = math.eval(formulaToEvaluate);
            }
            catch (ex) {
                if (window.console && window.console.log && window.appSettings.isDebug)
                {
                    console.log('Unable to evaluate description as mathematical formula: ', formulaToEvaluate);
                }

                mathEvalResult = NaN;
            }
            finally {
                if (!isNaN(mathEvalResult))
                {
                    $adjustmentDetailRow.find('#adjustment_Amount').val(mathEvalResult);
                }
            }
        }

        var previousId = $currentDocument.data(autoSaveQueueKey);
        if (previousId) {
            clearTimeout(previousId);
        }

        var timeoutId = setTimeout(function () {

            adjustmentDetailAutoSave.updateModel($adjustmentDetailRow);
            if (adjustmentDetailAutoSave.modelHasChanges($adjustmentDetailRow)) {
                var url = $adjustmentDetailRow.data('save-url') + '';
                adjustmentDetailAutoSave.saveDetails(url, $adjustmentDetailRow, function (result) {
                    // Raise Event to refresh the day of work with updated amount
                    $(document).trigger('WorkDayUpdated', { day })
                });
            }
        },
        1000);

        $currentDocument.data(autoSaveQueueKey, timeoutId);
    });
});