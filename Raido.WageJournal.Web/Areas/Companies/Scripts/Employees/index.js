﻿$(function () {
    var $body = $('body'),
        $currentDocument = $(document),
        $calendarSection = $('.jq-calendar-header'),
        $workSection = $('.jq-work-list'),
        slideDatesRendered = [],
        slickSlideCount = 0,
        slickSlideLoadedCount = 0,
        todaySlideIndex = 0;

    // calculate how much each cell will take up
    var $workDayCell = $('<div class="calendar-cell" style="visibility: hidden;">&nbsp;</div>');
    $body.append($workDayCell);
    var expectedCellWidth = $workDayCell.outerWidth();
    $workDayCell.detach();
    $workDayCell = null;

    var containerWidth = $workSection.innerWidth();

    // plus 2 because we always prefetch 1 in future and 1 in past
    var daysThatCanBeRendered = Math.floor(containerWidth / expectedCellWidth) + 2;

    var responsiveSettings = [
          {
              breakpoint: 4000,
              settings: {
                  slidesToShow: 8,
                  slidesToScroll: 1,
                  swipeToSlide: true
              }
          },
          {
              breakpoint: 1921,
              settings: {
                  slidesToShow: 5,
                  slidesToScroll: 1,
                  swipeToSlide: true
              }
          },
          {
              breakpoint: 1201,
              settings: {
                  slidesToShow: 3,
                  slidesToScroll: 1,
                  swipeToSlide: true
              }
          },
          {
              breakpoint: 993,
              settings: {
                  slidesToShow: 2,
                  slidesToScroll: 1,
                  swipeToSlide: true
              }
          },
          {
              breakpoint: 769,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  swipeToSlide: true
              }
          }];

    $calendarSection.slick({
        slidesToShow: daysThatCanBeRendered,
        slidesToScroll: 1,
        infinite: false,
        variableWidth: true,
        centerMode: true,
        focusOnSelect: false,
        dots: false,
        responsive: responsiveSettings,
        prevArrow: $('.slick-slider-wrapper .previous-day-button'),
        nextArrow: $('.slick-slider-wrapper .next-day-button'),
        asNavFor: '.jq-work-list',
        swipeToSlide: false
    });

    $workSection.slick({
        slidesToShow: daysThatCanBeRendered,
        slidesToScroll: 1,
        infinite: false,
        variableWidth: true,
        centerMode: true,
        focusOnSelect: false,
        dots: false,
        responsive: responsiveSettings,
        arrows: false,
        asNavFor: '.jq-calendar-header',
        swipeToSlide: false
    });


    var getWorkDay = function (workDateString, insertAtBegining, scrollToToday) {
        /// <param name="workDateString" type="String">The universal formatted string representation of the work day to show</param>
        /// <param name="insertAtBegining" type="Boolean">Whether or not to insert the result at begining of slides</param>
        /// <param name="scrollToToday" type="Boolean">Whether to pre select the "today" slide</param>

        $.ajaxQueue({
            url: $workSection.data('work-url') + '/' + workDateString,
            type: 'GET',
            success: function (result) {
                var workDate = new Date(workDateString);

                var calendarDay = $('<div/>')
                                    .addClass('text-center calendar-cell')
                                    .append($('<div/>')
                                        .addClass('calendar-day')
                                        .append($('<p/>')
                                            .text(window.appSettings.dayNames[workDate.getDay()]))
                                        .append($('<p/>')
                                            .text(Globalize(window.appSettings.culture).dateFormatter({ skeleton: 'Md' })(workDate))
                                            .attr('title',Globalize(window.appSettings.culture).dateFormatter({ date: 'short' })(workDate))));
                slickSlideLoadedCount++;

                $calendarSection.slick('slickAdd', calendarDay[0].outerHTML, insertAtBegining);
                $workSection.slick('slickAdd', result, insertAtBegining);

                if (scrollToToday && slickSlideLoadedCount > scrollToToday) {
                    // Manually refresh positioning of slick
                    $calendarSection.slick('slickGoTo', todaySlideIndex, false);
                }

                //if (!scrollToToday && insertAtBegining) {
                //    // Something inserted at begining, need to move to next item to keep correct selected item
                //    $calendarSection.slick('slickGoTo', $calendarSection.slick('slickCurrentSlide') + 1, false);
                //}

                if (daysThatCanBeRendered === slickSlideLoadedCount) {
                    $calendarSection.data('initial-days-loaded', true);
                }
            }
        });
    };


    $calendarSection.on('afterChange', function () {
        if (!$calendarSection.data('initial-days-loaded')) {
            return;
        }

        var currentSlideIndex = $calendarSection.slick('slickCurrentSlide');
        var slidesToShow = $calendarSection.slick('slickGetOption', 'slidesToShow');
        var slidesUntilOffGrid = Math.ceil(slidesToShow / 2);
        // if no more slides to the left, get 1 extra day for scrolling
        if (currentSlideIndex <= slidesUntilOffGrid) {
            var firstItemDate = new Date(slideDatesRendered[0]);

            todaySlideIndex++;
            var newFirstItem = [];
            newFirstItem[0] = new Date(firstItemDate.getFullYear(), firstItemDate.getMonth(), firstItemDate.getDate() - 1).toUniversalString();

            slideDatesRendered = newFirstItem.concat(slideDatesRendered);

            getWorkDay(slideDatesRendered[0], true, false);
        }

        // if no more slides to the right, get 1 extra day for scrolling
        if (currentSlideIndex >= slickSlideCount - slidesUntilOffGrid) {
            var lastItemDate = new Date(slideDatesRendered[slideDatesRendered.length - 1]);
            todaySlideIndex--;
            slideDatesRendered[slideDatesRendered.length] = new Date(lastItemDate.getFullYear(), lastItemDate.getMonth(), lastItemDate.getDate() + 1).toUniversalString();
            getWorkDay(slideDatesRendered[slideDatesRendered.length - 1], false, false);
        }
    });

    // if data is updated, refresh the day
    $currentDocument.on('WorkDayUpdated', function (e, d)
    {
        var dayIndex = -1;
        var found = false;
        var dayFindIndex = 0;
        while (!found && dayFindIndex < slideDatesRendered.length)
        {
            if (slideDatesRendered[dayFindIndex] === d.day)
            {
                found = true;
            }

            dayFindIndex++;
        }

        if (found)
        {
            // reload day
            
            $.ajaxQueue({
                url: $workSection.data('work-url') + '/' + d.day,
                type: 'GET',
                success: function (result) {
                    // remove and readd to couresel
                    $workSection.slick('slickRemove', dayFindIndex, true);
                    $workSection.slick('slickAdd', result, dayFindIndex-1, true);
                }
            });
        }

    });

    var initialLoad = function () {
        // Add the number of calendar days specified
        var currentDate = new Date();
        // Put today's date in middle
        currentDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - Math.ceil(daysThatCanBeRendered / 2));

        for (var i = 0; i < daysThatCanBeRendered; i++) {
            slideDatesRendered[i] = currentDate.toUniversalString();

            slickSlideCount++;
            if (slideDatesRendered[i] === new Date().toUniversalString()) {
                // track which slide is the "today"
                todaySlideIndex = slickSlideCount - 1;
            }

            getWorkDay(slideDatesRendered[i], false, true);
            currentDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() + 1);
        }

        // hook up the toggle for displaying all or active employees
        $('#ShowAllEmployeeCheckbox').on('change', function()
        {
            var $self = $(this);
            var $wrapper = $self.closest('.employee-work-wrapper');
            if ($self.is(':checked')) {
                $wrapper.addClass('show-all-employee');
            }
            else {
                $wrapper.removeClass('show-all-employee');
            }
        });
    };

    // if globalize is not ready, check again in 300 milliseconds
    var init = function checkForGlobalizeReadyBeforeInitialize() {
        if (!$(document).data('globalize-loaded')) {
            setTimeout(checkForGlobalizeReadyBeforeInitialize, 200);
        }
        else {
            initialLoad();
        }
    }

    init();    
});