﻿// namespacing EmployeeDetail
(function (employeeDetailAutoSave, $, undefined) {
    //Private Property
    var currentModel = {};
    var getModelValues = function () {
        var model = {};
        var $modalForm = $('#EmployeeDetailsModalForm');
        var $employee_EmployeeNumberTextBox = $modalForm.find('#employee_EmployeeNumber');
        var $employee_NameTextBox = $modalForm.find('#employee_Name');
        var $employee_EmailTextBox = $modalForm.find('#employee_Email');
        var $employee_PhoneNumberTextBox = $modalForm.find('#employee_PhoneNumber');
        var $employee_IdentifierHiddenField = $modalForm.find('#employee_Identifier');
        var $employee_CompanyIdentifierHiddenField = $modalForm.find('#employee_CompanyIdentifier');
        var $employee_IsActiveHiddenField = $modalForm.find('#employee_IsActive');
        var $employee_HourlyRateTextBox = $modalForm.find('#employee_HourlyRate');
        var $employee_EmploymentStartDate = $modalForm.find('#employee_EmploymentStartDate');
        var $employee_EmploymentEndDate = $modalForm.find('#employee_EmploymentEndDate');
        
        model.EmployeeNumber = $employee_EmployeeNumberTextBox.val();
        model.Name = $employee_NameTextBox.val();
        model.Email = $employee_EmailTextBox.val();
        model.PhoneNumber = $employee_PhoneNumberTextBox.val();
        model.Identifier = $employee_IdentifierHiddenField.val();
        model.IsActive = $employee_IsActiveHiddenField.val();
        model.EmploymentStartDate = $employee_EmploymentStartDate.val();
        model.EmploymentEndDate = $employee_EmploymentEndDate.val();

        if ($(document).data('globalize-loaded')) {
            model.HourlyRate = Globalize(window.appSettings.culture).numberParser()($employee_HourlyRateTextBox.val());
        }
        else {
            model.HourlyRate = parseFloat($employee_HourlyRateTextBox.val());
        }
        
        model.CompanyIdentifier = $employee_CompanyIdentifierHiddenField.val();

        return model;
    };

    var lastSavedModel = getModelValues();

    //Public Method
    employeeDetailAutoSave.updateModel = function () {
        currentModel = getModelValues();
    };

    employeeDetailAutoSave.getModel = function () { return currentModel; };

    employeeDetailAutoSave.modelHasChanges = function () {
        if (!currentModel) {
            employeeDetailAutoSave.updateModel();
        }

        if (lastSavedModel && currentModel) {
            return lastSavedModel.Identifier !== currentModel.Identifier ||
                   lastSavedModel.Email !== currentModel.Email ||
                   lastSavedModel.Name !== currentModel.Name ||
                   lastSavedModel.EmployeeNumber !== currentModel.EmployeeNumber ||
                   lastSavedModel.IsActive !== currentModel.IsActive ||
                   lastSavedModel.PhoneNumber !== currentModel.PhoneNumber ||
                   lastSavedModel.HourlyRate !== currentModel.HourlyRate ||
                   lastSavedModel.EmploymentStartDate !== currentModel.EmploymentStartDate ||
                   lastSavedModel.EmploymentEndDate !== currentModel.EmploymentEndDate;
        }
        else {
            return !lastSavedModel;
        }
    };

    employeeDetailAutoSave.saveDetails = function (url, callback) {
        if ($('#EmployeeDetailsModalForm').valid()) {
            employeeDetailAutoSave.updateModel();
            if (employeeDetailAutoSave.modelHasChanges()) {
                lastSavedModel = getModelValues();

                $.ajax({
                    url: url,
                    type: 'POST',
                    //contentType: 'application/json; charset=utf-8',
                    data: $('#EmployeeDetailsModalForm').serialize(),
                    success: function (result) {
                        if (callback && typeof (callback) === 'function') {
                            callback(result);
                        }

                        if (result) {
                            toastr.success(document.webL10n.get("AutoSaveSuccessful"));
                        }
                        else {
                            toastr.error(document.webL10n.get("AutoSaveFailed"));
                        }
                    },
                    error: function (result) {
                        toastr.error(document.webL10n.get("AutoSaveFailed"));

                        if (callback && typeof (callback) === 'function') {
                            callback(result);
                        }
                    }
                });
            }
        }
        else {
            if (callback && typeof (callback) === 'function') {
                callback(null);
            }
        }
    };

    $(document).on('employee-form-loaded', function () {
        lastSavedModel = getModelValues();
    });

}(window.employeeDetailAutoSave = window.employeeDetailAutoSave || {}, jQuery));