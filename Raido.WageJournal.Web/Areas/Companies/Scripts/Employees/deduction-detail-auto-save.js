﻿// namespacing EmployeeDetail
(function (deductionDetailAutoSave, $, undefined) {
    //Private Property
    var currentModel = {};
    var getModelValues = function ($modalForm) {
        var model = {};
        var $deduction_DeductionIdHiddenField = $modalForm.find('#deduction_DeductionId');
        var $deduction_CompanyIdentifierHiddenField = $modalForm.find('#deduction_CompanyIdentifier');
        var $deduction_IdentifierHiddenField = $modalForm.find('#deduction_Identifier');
        var $deduction_DescriptionTextBox = $modalForm.find('#deduction_Description');
        var $deduction_AmountTextBox = $modalForm.find('#deduction_Amount');
        var $deduction_EffectiveFromTextBox = $modalForm.find('#deduction_EffectiveFrom');
        var $deduction_EffectiveToTextBox = $modalForm.find('#deduction_EffectiveTo');
        var $deduction_CronDropdown = $modalForm.find('#deduction_Cron');
        var $deduction_IsActiveCheckbox = $modalForm.find('#deduction_IsActive');

        model.DeductionId = $deduction_DeductionIdHiddenField.val();
        model.CompanyIdentifier = $deduction_CompanyIdentifierHiddenField.val();
        model.Identifier = $deduction_IdentifierHiddenField.val();
        model.Description = $deduction_DescriptionTextBox.val();
        model.Amount = parseFloat($deduction_AmountTextBox.val());
        model.EffectiveFrom = $deduction_EffectiveFromTextBox.val();
        if ($deduction_EffectiveToTextBox.val() && $deduction_EffectiveToTextBox.val() !== '') {
            model.EffectiveTo = $deduction_EffectiveToTextBox.val();
        }
        else {
            model.EffectiveTo = null;
        }

        model.Cron = $deduction_CronDropdown.val();
        model.IsActive = $deduction_IsActiveCheckbox.is(':checked');

        return model;
    };
    
    //Public Method
    deductionDetailAutoSave.updateModel = function ($modalForm) {
        currentModel = getModelValues($modalForm);
        return currentModel;
    };

    deductionDetailAutoSave.getModel = function () { return currentModel; };

    deductionDetailAutoSave.modelHasChanges = function ($modalForm) {
        if (!currentModel) {
            deductionDetailAutoSave.updateModel($modalForm);
        }

        var lastSavedModel = $modalForm.data('lastSavedModel');
        if (lastSavedModel && currentModel) {
            return lastSavedModel.Identifier !== currentModel.Identifier ||
                   lastSavedModel.Description !== currentModel.Description ||
                   lastSavedModel.Amount !== currentModel.Amount ||
                   lastSavedModel.EffectiveFrom !== currentModel.EffectiveFrom ||
                   lastSavedModel.EffectiveTo !== currentModel.EffectiveTo ||
                   lastSavedModel.Cron !== currentModel.Cron ||
                   lastSavedModel.IsActive !== currentModel.IsActive;
        }
        else {
            return !lastSavedModel;
        }
    };

    deductionDetailAutoSave.saveDetails = function (url, $modalForm, callback) {
        if ($modalForm.valid()) {
            
            deductionDetailAutoSave.updateModel($modalForm);
            if (deductionDetailAutoSave.modelHasChanges($modalForm)) {
                $modalForm.data('lastSavedModel', getModelValues($modalForm));
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: $modalForm.serialize(),
                    success: function (result) {
                        if (callback && typeof (callback) === 'function') {
                            callback(result);
                        }

                        if (result) {
                            toastr.success(document.webL10n.get("AutoSaveSuccessful"));
                        }
                        else {
                            toastr.error(document.webL10n.get("AutoSaveFailed"));
                        }
                    },
                    error: function (result) {
                        toastr.error(document.webL10n.get("AutoSaveFailed"));

                        if (callback && typeof (callback) === 'function') {
                            callback(result);
                        }
                    }
                });
            }
        }
        else {
            if (callback && typeof (callback) === 'function') {
                callback(null);
            }
        }
    };

    $(document).on('deduction-form-loaded', function () {
        var $form = $('#DeductionDetailsForm');
        $form.data('lastSavedModel', deductionDetailAutoSave.updateModel($form));

    });

}(window.deductionDetailAutoSave = window.deductionDetailAutoSave || {}, jQuery));