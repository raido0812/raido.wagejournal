﻿$(function ()
{
    var $currentDocument = $(document);
    
    $currentDocument.on('click', '.edit-employee-button', function (e)
    {
        modalLoader.loadEmployeeDetailsModal('#editEmployeeModal', $(this).closest('.employee-name').data('guid'));
        e.preventDefault();
    }).on('click', '.edit-deduction-items-button', function (e)
    {
        modalLoader.loadEmployeeDeductionModal('#editEmployeeDeductionModal', $(this).closest('.employee-name').data('guid'));
        e.preventDefault();
    }).on('change keyup', '#EmployeeDetailsModalForm input', function queueSave()
    {
        var previousId = $currentDocument.data('employee-details-auto-save-queued');
        if (previousId)
        {
            clearTimeout(previousId);
        }

        var timeoutId = setTimeout(function ()
        {
            employeeDetailAutoSave.saveDetails($('#EmployeeDetailsModalForm').attr('action'), function (result)
            {
                // update the displayed employee name
                var savedModel = employeeDetailAutoSave.getModel();

                var displayName = savedModel.Name;
                if (savedModel.EmployeeNumber && savedModel.EmployeeNumber !== '')
                {
                    displayName += " (#" + savedModel.EmployeeNumber + ")";
                }

                var $employeeDisplay = $('.employee-name[data-guid="' + savedModel.Identifier + '"]');
                $employeeDisplay.find('.name').attr('title', displayName);
                $employeeDisplay.find('.name').text(displayName);
            });
        },
        1000);

        $currentDocument.data('employee-details-auto-save-queued', timeoutId);
    });
});