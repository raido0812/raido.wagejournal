﻿// namespacing EmployeeDetail
(function (adjustmentDetailAutoSave, $, undefined) {
    //Private Property
    var currentModel = {};
    var getModelValues = function ($row) {
        var model = {};

        model.Description = $row.find('#adjustment_Description').val();
        model.DateOfIssuance = $row.find('#Day').val();
        model.Amount = $row.find('#adjustment_Amount').val();
        model.Identifier = $row.find('#adjustment_Identifier').val() || uuid.v4();

        return model;
    };


    adjustmentDetailAutoSave.modelHasChanges = function ($row) {
        if (!currentModel) {
            adjustmentDetailAutoSave.updateModel($row);
        }

        var lastSavedModel = $row.data('lastSavedModel');
        if (lastSavedModel && currentModel) {
            return lastSavedModel.Description !== currentModel.Description ||
                lastSavedModel.DateOfIssuance !== currentModel.DateOfIssuance ||
                lastSavedModel.Amount !== currentModel.Amount ||
                lastSavedModel.Identifier !== currentModel.Identifier;
        }
        else {
            return !lastSavedModel;
        }
    };

    var validate = function ($row) {
        if (!currentModel) {
            adjustmentDetailAutoSave.updateModel($row);
        }

        // custom validation conditions
        if (!currentModel.Description || currentModel.Description === '')
        {
            $row.find('#adjustment_Description').addClass('error');
            return false;
        }

        if (isNaN(parseFloat(currentModel.Amount))) {
            $row.find('#adjustment_Amount').addClass('error');
            return false;
        }

        $row.find('#adjustment_Amount').removeClass('error');
        $row.find('#adjustment_Description').removeClass('error');
        return $row.find('input[data-val="true"]').valid();
    };

    //Public Method
    adjustmentDetailAutoSave.updateModel = function ($row) {
        currentModel = getModelValues($row);
        return currentModel;
    };

    adjustmentDetailAutoSave.getModel = function () { return currentModel; };

    adjustmentDetailAutoSave.saveDetails = function (url, $row, callback) {
        if (validate($row)) {
            $row.data('lastSavedModel', currentModel);
            $.ajax({
                url: url,
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(currentModel),
                success: function (result) {
                    if (callback && typeof (callback) === 'function') {
                        callback(result);
                    }

                    if (result) {
                        toastr.success(document.webL10n.get("AutoSaveSuccessful"));
                    }
                    else {
                        toastr.error(document.webL10n.get("AutoSaveFailed"));
                    }
                },
                error: function (result) {
                    toastr.error(document.webL10n.get("AutoSaveFailed"));

                    if (callback && typeof (callback) === 'function') {
                        callback(result);
                    }
                }
            });
        }
        else {
            if (callback && typeof (callback) === 'function') {
                callback(null);
            }
        }
    };

}(window.adjustmentDetailAutoSave = window.adjustmentDetailAutoSave || {}, jQuery));