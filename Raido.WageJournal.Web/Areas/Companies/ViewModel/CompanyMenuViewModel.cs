﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Raido.WageJournal.Web.Areas.Companies.ViewModel
{
    public class CompanyMenuViewModel
    {
        public Guid CurrentCompanyIdentifier { get; set; }
        public IList<Contracts.DataTransferObjects.Company> AllUserCompanies { get; set; }
    }
}