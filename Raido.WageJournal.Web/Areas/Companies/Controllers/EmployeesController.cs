﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Raido.WageJournal.DataAccess.Repositories.Interfaces;
using Raido.WageJournal.Logic.Services.Interfaces;
using Contracts = Raido.WageJournal.Contracts;

namespace Raido.WageJournal.Web.Areas.Companies.Controllers
{
    [RouteArea("Companies")]
    [RoutePrefix("{companyIdentifier:guid}")]
    [Authorize]
    public partial class EmployeesController : AsyncController
    {
        #region Fields
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly ICompanyRepository _companyRepository;
        private readonly IWorkRepository _workRepository;
        private readonly IAdjustmentRepository _adjustmentRepository;

        #endregion Fields

        #region Constructor
        public EmployeesController(
            IEmployeeService employeeService,
            ICompanyRepository companyRepository,
            IEmployeeRepository employeeRepository,
            IWorkRepository workRepository,
            IAdjustmentRepository adjustmentRepository)
        {
            this._companyRepository = companyRepository;
            this._employeeRepository = employeeRepository;
            this._employeeService = employeeService;
            this._workRepository = workRepository;
            this._adjustmentRepository = adjustmentRepository;
        }
        #endregion Constructor

        #region Methods

        [Route("Employees")]
        [HttpGet]
        public async virtual Task<ActionResult> Index(Guid companyIdentifier)
        {
            var allEmployees = await this._employeeService.GetAllEmployeesAsync(companyIdentifier);
            return this.View(allEmployees);
        }

        [Route("Employees/Work/", Order = 1)]
        [Route("Employees/Work/{workDay:datetime}", Order = 2)]
        [HttpGet]
        public async virtual Task<ActionResult> WorkForDay(Guid companyIdentifier, DateTime workDay)
        {
            var work = await this._employeeService.GetAllEmployeeWorkForDayAsync(companyIdentifier, workDay);
            return this.PartialView("Partial/WorkForDay", work);
        }

        [Route("Employees/{employeeIdentifier:guid}/Work/")]
        [HttpGet]
        public async virtual Task<ActionResult> EmployeeWorkForDay(Guid companyIdentifier, Guid employeeIdentifier, DateTime? workDay)
        {
            return this.PartialView("Partial/EmployeeWorkForDayDetails", await this._employeeService.GetEmployeeWorkForDayAsync(companyIdentifier, employeeIdentifier, workDay ?? DateTime.Today));
        }

        [Route("Employees/{employeeIdentifier:guid}/Work/", Order = 3)]
        [HttpPost]
        public async virtual Task<ActionResult> SaveWorkForDay(Guid companyIdentifier, Guid employeeIdentifier, Contracts.DataTransferObjects.Work workForEmployee)
        {
            if (ModelState.IsValid)
            {
                return this.Json(await this._workRepository.SaveWorkAsync(workForEmployee, employeeIdentifier, companyIdentifier));
            }

            return this.Json(false);
        }

        [Route("Employees/{employeeIdentifier:guid}/Adjustment/", Order = 3)]
        [HttpPost]
        public async virtual Task<ActionResult> SaveAdjustmentForDay(Guid companyIdentifier, Guid employeeIdentifier, Contracts.DataTransferObjects.Adjustment adjustmentForEmployee)
        {
            if (ModelState.IsValid)
            {
                return this.Json(await this._adjustmentRepository.SaveAdjustmentAsync(adjustmentForEmployee, employeeIdentifier, companyIdentifier));
            }

            return this.Json(false);
        }

        [Route("Employees/{employeeIdentifier:guid}")]
        [HttpGet]
        public async virtual Task<ActionResult> Details(Guid employeeIdentifier)
        {
            Guid companyIdentifier = Guid.Empty;
            if (!this.Request.RequestContext.RouteData.Values.ContainsKey("companyIdentifier") ||
                !Guid.TryParse(this.Request.RequestContext.RouteData.Values["companyIdentifier"].ToString(), out companyIdentifier))
            {
                companyIdentifier = await this._companyRepository.GetLastViewedCompanyIdentifierAsync();
            }

            Contracts.DataTransferObjects.Employee employee = await this._employeeRepository.GetEmployeeAsync(employeeIdentifier, companyIdentifier) ?? new Contracts.DataTransferObjects.Employee();

            return this.PartialView("Partial/Details", employee);
        }

        [Route("Employees/{employeeIdentifier:guid}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async virtual Task<ActionResult> SaveEmployee(Guid employeeIdentifier, [Bind(Include = "EmployeeId,EmployeeNumber,Name,Email,PhoneNumber,CompanyId,Identifier,IsActive,HourlyRate,EmploymentStartDate,EmploymentEndDate")] Contracts.DataTransferObjects.Employee employee)
        {
            Guid companyIdentifier = Guid.Empty;
            if (!this.Request.RequestContext.RouteData.Values.ContainsKey("companyIdentifier") ||
                !Guid.TryParse(this.Request.RequestContext.RouteData.Values["companyIdentifier"].ToString(), out companyIdentifier))
            {
                companyIdentifier = await this._companyRepository.GetLastViewedCompanyIdentifierAsync();
            }

            if (ModelState.IsValid)
            {
                bool isNew = employee.EmployeeId == default(int);

                await this._employeeRepository.SaveEmployeeAsync(employee, companyIdentifier);
                if (isNew)
                {
                    // adding new needs to refresh underlying page
                    return this.RedirectToAction(MVC.Companies.Employees.Index(companyIdentifier));
                }
                else
                {
                    // saving existing, just needs to refresh the details
                    return this.RedirectToAction(MVC.Companies.Employees.Details(employee.Identifier));
                }
            }

            return this.PartialView("Partial/Details", employee);
        }

        [Route("Employees/{employeeIdentifier:guid}")]
        [HttpDelete]
        [ValidateAntiForgeryToken]
        public async virtual Task<ActionResult> DeleteEmployee(Guid employeeIdentifier)
        {
            Guid companyIdentifier = Guid.Empty;
            if (!this.Request.RequestContext.RouteData.Values.ContainsKey("companyIdentifier") ||
                !Guid.TryParse(this.Request.RequestContext.RouteData.Values["companyIdentifier"].ToString(), out companyIdentifier))
            {
                companyIdentifier = await this._companyRepository.GetLastViewedCompanyIdentifierAsync();
            }

            await this._employeeRepository.DeleteEmployeeAsync(employeeIdentifier, companyIdentifier);
            return this.RedirectToAction(MVC.Companies.Employees.Index());
        }

        #endregion
    }
}
