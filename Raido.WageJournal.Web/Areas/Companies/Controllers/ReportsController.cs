﻿using Raido.WageJournal.Logic.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using LocalizedText = Raido.WageJournal.Web.Areas.Companies.Localization.LocalizedText;

namespace Raido.WageJournal.Web.Areas.Companies.Controllers
{
    [RouteArea("Companies")]
    [RoutePrefix("{companyIdentifier:guid}")]
    [Authorize]
    public partial class ReportsController : AsyncController
    {
        #region Fields

        private readonly IWageSummaryExportService _wageSummaryExportService;
        private readonly IPayslipExportService _payslipExportService;
        private readonly IAttendanceSummaryExportService _attendanceSummaryExportService;

        #endregion Fields

        #region Constructor

        public ReportsController(
            IWageSummaryExportService wageSummaryExportService,
            IPayslipExportService payslipExportService,
            IAttendanceSummaryExportService attendanceSummaryExportService)
        {
            this._wageSummaryExportService = wageSummaryExportService;
            this._payslipExportService = payslipExportService;
            this._attendanceSummaryExportService = attendanceSummaryExportService;
        }

        #endregion Constructor

        #region Methods

        [Route("Reports/WageSummary")]
        [HttpGet]
        public virtual ActionResult WageSummaryFilterPage(Guid companyIdentifier)
        {
            return this.View(companyIdentifier);
        }

        [Route("Reports/WageSummary/Pdf")]
        [HttpGet]
        public async virtual Task<ActionResult> GetWageSummaryPdf(DateTime startDate, DateTime endDate)
        {
            var currentCompanyIdentifier = Guid.Empty;
            if (!this.Request.RequestContext.RouteData.Values.ContainsKey("companyIdentifier") ||
                !Guid.TryParse(this.Request.RequestContext.RouteData.Values["companyIdentifier"].ToString(), out currentCompanyIdentifier))
            {
                currentCompanyIdentifier = Guid.Empty;
            }


            byte[] fileBytes = await this._wageSummaryExportService.GenerateWageSummaryPdfAsync(currentCompanyIdentifier, startDate, endDate, new Uri(Server.MapPath("~/fonts/Lato/"), UriKind.Absolute), "Lato Semibold", "Lato Semibold");

            return this.File(fileBytes, "application/pdf", string.Format(LocalizedText.WageSummaryReportFileName, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd")));
        }

        [Route("Reports/Payslips")]
        [HttpGet]
        public virtual ActionResult PayslipFilterPage(Guid companyIdentifier)
        {
            return this.View(companyIdentifier);
        }

        [Route("Reports/Payslips/Pdf")]
        [HttpGet]
        public async virtual Task<ActionResult> GetPayslipPdf(DateTime startDate, DateTime endDate)
        {
            var currentCompanyIdentifier = Guid.Empty;
            if (!this.Request.RequestContext.RouteData.Values.ContainsKey("companyIdentifier") ||
                !Guid.TryParse(this.Request.RequestContext.RouteData.Values["companyIdentifier"].ToString(), out currentCompanyIdentifier))
            {
                currentCompanyIdentifier = Guid.Empty;
            }

            byte[] fileBytes = await this._payslipExportService.GeneratePayslipPdfAsync(currentCompanyIdentifier, startDate, endDate, new Uri(Server.MapPath("~/fonts/Lato/"), UriKind.Absolute), "Lato Semibold", "Lato Semibold");

            return this.File(fileBytes, "application/pdf", string.Format(LocalizedText.PayslipReportFileName, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd")));
        }

        [Route("Reports/AttendanceSummary")]
        [HttpGet]
        public virtual ActionResult AttendanceSummaryFilterPage(Guid companyIdentifier)
        {
            return this.View(companyIdentifier);
        }

        [Route("Reports/AttendanceSummary/Pdf")]
        [HttpGet]
        public async virtual Task<ActionResult> GetAttendanceSummaryPdf(DateTime startDate, DateTime endDate)
        {
            var currentCompanyIdentifier = Guid.Empty;
            if (!this.Request.RequestContext.RouteData.Values.ContainsKey("companyIdentifier") ||
                !Guid.TryParse(this.Request.RequestContext.RouteData.Values["companyIdentifier"].ToString(), out currentCompanyIdentifier))
            {
                currentCompanyIdentifier = Guid.Empty;
            }

            byte[] fileBytes = await this._attendanceSummaryExportService.GenerateAttendanceSummaryPdfAsync(currentCompanyIdentifier, startDate, endDate, new Uri(Server.MapPath("~/fonts/Lato/"), UriKind.Absolute), "Lato Semibold", "Lato Semibold");

            return this.File(fileBytes, "application/pdf", string.Format(LocalizedText.AttendanceReportFileName, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd")));
        }

        #endregion Methods
    }
}