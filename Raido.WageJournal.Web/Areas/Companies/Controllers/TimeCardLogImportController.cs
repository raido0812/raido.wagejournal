﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Raido.WageJournal.Contracts.ComplexTransferObjects;
using Raido.WageJournal.Contracts.DataTransferObjects;
using Raido.WageJournal.DataAccess.Repositories.Interfaces;
using Raido.WageJournal.Logic.Services.Interfaces;

namespace Raido.WageJournal.Web.Areas.Companies.Controllers
{
    [RouteArea("Companies")]
    [RoutePrefix("{companyIdentifier:guid}")]
    [Authorize]
    public partial class TimeCardLogImportController : Controller
    {
        private readonly IEmployeeService _employeeService;
        private readonly ITimeSheetImporter _timeSheetImporter;
        private readonly IWorkRepository _workRepository;

        public TimeCardLogImportController(
            ITimeSheetImporter timeSheetImporter,
            IEmployeeService employeeService,
            IWorkRepository workRepository)
        {
            _employeeService = employeeService;
            _timeSheetImporter = timeSheetImporter;
            _workRepository = workRepository;
        }

        [Route("Import")]
        [HttpGet]
        public virtual async Task<ActionResult> Index(Guid companyIdentifier)
        {
            var allEmployees = await this._employeeService.GetAllEmployeesAsync(companyIdentifier);
            return View(allEmployees);
        }

        [Route("Import")]
        [HttpPost]
        public virtual async Task<JsonResult> UploadFile()
        {
            IEnumerable<TimesheetRecord> results = null;
            if (Request.Files?.Count > 0)
            {
                results = await _timeSheetImporter.ExtractTimesheetRecords(Request.Files[0].InputStream);
            }

            return Json(results);
        }

        [Route("TimesheetResults")]
        [HttpPost]
        public virtual async Task<JsonResult> SaveTimesheetImportResults(Guid companyIdentifier, IEnumerable<Work> importedWork)
        {
            foreach (var work in importedWork)
            {
                await _workRepository.SaveWorkAsync(work, work.EmployeeIdentifier, companyIdentifier);
            }

            return Json(true);
        }
    }
}