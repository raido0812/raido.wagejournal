﻿using Raido.WageJournal.DataAccess.Repositories.Interfaces;
using Raido.WageJournal.Logic.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Raido.WageJournal.Contracts.SearchFilters;

namespace Raido.WageJournal.Web.Areas.Companies.Controllers
{
    [RouteArea("Companies")]
    [RoutePrefix("{companyIdentifier:guid}")]
    [Authorize]
    public partial class DeductionsController : AsyncController
    {
        #region Fields
        private readonly IDeductionService _deductionService;
        private readonly IDeductionRepository _deductionRepository;
        private readonly ICompanyRepository _companyRepository;
        #endregion Fields

        #region Constructor
        public DeductionsController(ICompanyRepository companyRepository, IDeductionRepository deductionRepository, IDeductionService deductionService)
        {
            this._companyRepository = companyRepository;
            this._deductionRepository = deductionRepository;
            this._deductionService = deductionService;
        }
        #endregion Constructor

        #region Methods

        [Route("Employees/{employeeIdentifier:guid}/Deductions/")]
        [HttpGet]
        public async virtual Task<ActionResult> DeductionListing(Guid employeeIdentifier, Guid companyIdentifier)
        {
            var allDeductions = await this._deductionService.GetDeductionsAsync(null, employeeIdentifier, companyIdentifier);
            return this.PartialView("Partial/DeductionListing", allDeductions);
        }

        [Route("Employees/{employeeIdentifier:guid}/Deductions/listing")]
        [HttpGet]
        public async virtual Task<ActionResult> DeductionListingItems(Guid employeeIdentifier, Guid companyIdentifier)
        {
            var allDeductions = await this._deductionRepository.GetDeductionsAsync(new DeductionSearchOptions { EmployeeIdentifier = employeeIdentifier }, companyIdentifier);
            return this.PartialView("Partial/DeductionListingItems", allDeductions);
        }

        [Route("Employees/{employeeIdentifier:guid}/Deductions/{deductionIdentifier:guid}")]
        [HttpGet]
        public async virtual Task<ActionResult> Details(Guid employeeIdentifier, Guid deductionIdentifier)
        {
            Guid companyIdentifier = Guid.Empty;
            if (!this.Request.RequestContext.RouteData.Values.ContainsKey("companyIdentifier") ||
                !Guid.TryParse(this.Request.RequestContext.RouteData.Values["companyIdentifier"].ToString(), out companyIdentifier))
            {
                companyIdentifier = await this._companyRepository.GetLastViewedCompanyIdentifierAsync();
            }

            Contracts.DataTransferObjects.Deduction deduction = await this._deductionRepository.GetDeductionAsync(deductionIdentifier, employeeIdentifier, companyIdentifier) ?? new Contracts.DataTransferObjects.Deduction();
            deduction.EmployeeIdentifier = employeeIdentifier;
            deduction.CompanyIdentifier = companyIdentifier;
            return this.PartialView("Partial/Details", deduction);
        }

        [Route("Employees/{employeeIdentifier:guid}/Deductions")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async virtual Task<ActionResult> SaveDeduction(Guid employeeIdentifier, Guid deductionIdentifier, [Bind(Include = "Description,Cron,Amount,EffectiveFrom,EffectiveTo,Identifier,IsActive")] Contracts.DataTransferObjects.Deduction deduction)
        {
            Guid companyIdentifier = Guid.Empty;
            if (!this.Request.RequestContext.RouteData.Values.ContainsKey("companyIdentifier") ||
                !Guid.TryParse(this.Request.RequestContext.RouteData.Values["companyIdentifier"].ToString(), out companyIdentifier))
            {
                companyIdentifier = await this._companyRepository.GetLastViewedCompanyIdentifierAsync();
            }

            if (ModelState.IsValid)
            {
                bool isNew = deduction.DeductionId == default(int);

                await this._deductionRepository.SaveDeductionAsync(deduction, employeeIdentifier, companyIdentifier);
                if (isNew)
                {
                    // adding new needs to refresh underlying page
                    return this.RedirectToAction(MVC.Companies.Deductions.DeductionListing(employeeIdentifier, companyIdentifier));
                }
                else
                {
                    // saving existing, just needs to refresh the details
                    return this.RedirectToAction(MVC.Companies.Deductions.Details(employeeIdentifier, deduction.Identifier));
                }
            }

            return this.PartialView("Partial/Details", deduction);
        }

        [Route("Employees/{employeeIdentifier:guid}/Deductions/{deductionIdentifier:guid}")]
        [HttpDelete]
        [ValidateAntiForgeryToken]
        public async virtual Task<ActionResult> DeleteDeduction(Guid employeeIdentifier, Guid deductionIdentifier)
        {
            Guid companyIdentifier = Guid.Empty;
            if (!this.Request.RequestContext.RouteData.Values.ContainsKey("companyIdentifier") ||
                !Guid.TryParse(this.Request.RequestContext.RouteData.Values["companyIdentifier"].ToString(), out companyIdentifier))
            {
                companyIdentifier = await this._companyRepository.GetLastViewedCompanyIdentifierAsync();
            }

            await this._deductionRepository.DeleteDeductionAsync(deductionIdentifier, employeeIdentifier, companyIdentifier);
            return this.RedirectToAction(MVC.Companies.Deductions.DeductionListing());
        }

        #endregion
    }
}