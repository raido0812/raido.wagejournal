﻿using Raido.WageJournal.DataAccess.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using Raido.WageJournal.Web.Areas.Companies.ViewModel;
using Raido.WageJournal.Web.Filter;
using Raido.WageJournal.Logic.Services.Interfaces;

namespace Raido.WageJournal.Web.Areas.Companies.Controllers
{
    [Authorize]
    [RouteArea("Companies")]
    [RoutePrefix("")]
    [AuthorizeCompanyIdentifier("companyIdentifier")]
    public partial class CompaniesController : AsyncController
    {
        #region Fields

        private readonly ICompanyService _companyService;
        private readonly ICompanyRepository _companyRepository;

        #endregion

        #region Constructor
        public CompaniesController(ICompanyRepository companyRepository, ICompanyService companyService)
        {
            this._companyRepository = companyRepository;
            this._companyService = companyService;
        }
        #endregion

        #region Methods
        [Route("menu")]
        public virtual ActionResult DropdownMenuWithPreselect()
        {
            return Task.Run(async () => {
                Guid identifier = Guid.Empty;
                if (!this.Request.RequestContext.RouteData.Values.ContainsKey("companyIdentifier") ||
                    !Guid.TryParse(this.Request.RequestContext.RouteData.Values["companyIdentifier"].ToString(), out identifier))
                {
                    identifier = await this._companyRepository.GetLastViewedCompanyIdentifierAsync();
                }

                CompanyMenuViewModel viewModel = new CompanyMenuViewModel();
                viewModel.AllUserCompanies = (await this._companyRepository.GetCompaniesForCurrentUserAsync()).OrderBy(item => item.Identifier == identifier ? 0 : 1).ToList();
                viewModel.CurrentCompanyIdentifier = identifier;
                return this.PartialView("Partial/DropdownMenuWithPreselect", viewModel);
            }).Result;
        }

        [HttpGet]
        [Route("{companyIdentifier:Guid}/details")]
        public async virtual Task<ActionResult> Details(Guid companyIdentifier)
        {
            var viewModel = await this._companyService.GetCompanyDetailsAsync(companyIdentifier);
            return this.View(viewModel);
        }

        [HttpPost]
        [Route("{companyIdentifier:Guid}/details")]
        public async virtual Task<JsonResult> SaveCompanyDetails(Raido.WageJournal.Contracts.ComplexTransferObjects.CompanyAndSettings companyDetails, Guid companyIdentifier)
        {
            if (companyDetails != null && companyDetails.Company != null)
            {
                companyDetails.Company.Identifier = companyIdentifier;
            }

            if (ModelState.IsValid)
            {
                return this.Json(await this._companyService.SaveCompanyDetailsAsync(companyDetails));
            }

            return this.Json(false);
        }

        [Route("create")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async virtual Task<ActionResult> AddCompany(Contracts.DataTransferObjects.Company newCompany)
        {
            var companyIdentifier = await this._companyRepository.AddCompanyForCurrentUserAsync(newCompany.Identifier, newCompany.FullName);

            return this.RedirectToAction(MVC.Companies.Companies.Details(companyIdentifier));
        }
        #endregion
    }
}