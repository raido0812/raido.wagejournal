﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Raido.WageJournal.Web.Views.Localization.Manage {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Index {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Index() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Raido.WageJournal.Web.Views.Localization.Manage.Index", typeof(Index).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change your password.
        /// </summary>
        public static string ChangePasswordActionText {
            get {
                return ResourceManager.GetString("ChangePasswordActionText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change your account settings.
        /// </summary>
        public static string ChangeSettingsHeading {
            get {
                return ResourceManager.GetString("ChangeSettingsHeading", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create.
        /// </summary>
        public static string CreatePasswordActionText {
            get {
                return ResourceManager.GetString("CreatePasswordActionText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Disable.
        /// </summary>
        public static string DisableButtonText {
            get {
                return ResourceManager.GetString("DisableButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enable.
        /// </summary>
        public static string EnableButtonText {
            get {
                return ResourceManager.GetString("EnableButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to External Logins:.
        /// </summary>
        public static string ExternalLoginLabel {
            get {
                return ResourceManager.GetString("ExternalLoginLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Language.
        /// </summary>
        public static string LanguageSettingLabel {
            get {
                return ResourceManager.GetString("LanguageSettingLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Manage.
        /// </summary>
        public static string ManageExternalLoginActionText {
            get {
                return ResourceManager.GetString("ManageExternalLoginActionText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Passeword:.
        /// </summary>
        public static string PasswordLabel {
            get {
                return ResourceManager.GetString("PasswordLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Two-Factor Authentication:.
        /// </summary>
        public static string TwoFactorAuthLabel {
            get {
                return ResourceManager.GetString("TwoFactorAuthLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There are no two-factor authentication providers configured. See &lt;a href=&quot;http://go.microsoft.com/fwlink/?LinkId=403804&quot;&gt;this article&lt;/a&gt;
        ///                for details on setting up this ASP.NET application to support two-factor authentication..
        /// </summary>
        public static string TwoFactorAuthNotSetUpNotice {
            get {
                return ResourceManager.GetString("TwoFactorAuthNotSetUpNotice", resourceCulture);
            }
        }
    }
}
