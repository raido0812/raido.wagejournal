﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Raido.WageJournal.Web.Views.Localization.Shared {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class LocalizedText {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal LocalizedText() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Raido.WageJournal.Web.Views.Localization.Shared.LocalizedText", typeof(LocalizedText).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Company.
        /// </summary>
        public static string AddCompanyModalButtonText {
            get {
                return ResourceManager.GetString("AddCompanyModalButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Company.
        /// </summary>
        public static string AddCompanyModalHeading {
            get {
                return ResourceManager.GetString("AddCompanyModalHeading", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Raido&apos;s Wage Journal.
        /// </summary>
        public static string ApplicationName {
            get {
                return ResourceManager.GetString("ApplicationName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Attendance Summary.
        /// </summary>
        public static string AttendanceSummaryReportMenuItemText {
            get {
                return ResourceManager.GetString("AttendanceSummaryReportMenuItemText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Menu.
        /// </summary>
        public static string BurgerIconText {
            get {
                return ResourceManager.GetString("BurgerIconText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        public static string CancelButtonText {
            get {
                return ResourceManager.GetString("CancelButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Close.
        /// </summary>
        public static string CloseButtonText {
            get {
                return ResourceManager.GetString("CloseButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Companies.
        /// </summary>
        public static string CompaniesMenuItem {
            get {
                return ResourceManager.GetString("CompaniesMenuItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company Details.
        /// </summary>
        public static string CompanyDetailsMenuItemText {
            get {
                return ResourceManager.GetString("CompanyDetailsMenuItemText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company Name.
        /// </summary>
        public static string CompanyNameTextboxPlaceholder {
            get {
                return ResourceManager.GetString("CompanyNameTextboxPlaceholder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Employees.
        /// </summary>
        public static string EmployeesMenuItemText {
            get {
                return ResourceManager.GetString("EmployeesMenuItemText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error.
        /// </summary>
        public static string ErrorPageHeading {
            get {
                return ResourceManager.GetString("ErrorPageHeading", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An error occurred while processing your request..
        /// </summary>
        public static string ErrorPageMessage {
            get {
                return ResourceManager.GetString("ErrorPageMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Home.
        /// </summary>
        public static string HomeMenuItem {
            get {
                return ResourceManager.GetString("HomeMenuItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Import Time Card Log.
        /// </summary>
        public static string ImportTimesheetMenuItemText {
            get {
                return ResourceManager.GetString("ImportTimesheetMenuItemText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Loading.
        /// </summary>
        public static string LoadingMessage {
            get {
                return ResourceManager.GetString("LoadingMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Locked out.
        /// </summary>
        public static string LockedOutHeading {
            get {
                return ResourceManager.GetString("LockedOutHeading", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This account has been locked out, please try again later..
        /// </summary>
        public static string LockedOutMessage {
            get {
                return ResourceManager.GetString("LockedOutMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Log on.
        /// </summary>
        public static string LogInButtonText {
            get {
                return ResourceManager.GetString("LogInButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Log off.
        /// </summary>
        public static string LogOffButtonText {
            get {
                return ResourceManager.GetString("LogOffButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Manage.
        /// </summary>
        public static string ManageMenuItem {
            get {
                return ResourceManager.GetString("ManageMenuItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Monthly on 15th.
        /// </summary>
        public static string MonthlyFifteenthDay {
            get {
                return ResourceManager.GetString("MonthlyFifteenthDay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Monthly on 1st.
        /// </summary>
        public static string MonthlyFirstDay {
            get {
                return ResourceManager.GetString("MonthlyFirstDay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Monthly on 28th.
        /// </summary>
        public static string MonthlyTwentyEighthDay {
            get {
                return ResourceManager.GetString("MonthlyTwentyEighthDay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add New.
        /// </summary>
        public static string NewItemButtonText {
            get {
                return ResourceManager.GetString("NewItemButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Payslips.
        /// </summary>
        public static string PayslipReportMenuItemText {
            get {
                return ResourceManager.GetString("PayslipReportMenuItemText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Register.
        /// </summary>
        public static string RegisterButtonText {
            get {
                return ResourceManager.GetString("RegisterButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reports.
        /// </summary>
        public static string ReportsMenuItemText {
            get {
                return ResourceManager.GetString("ReportsMenuItemText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save.
        /// </summary>
        public static string SaveButtonText {
            get {
                return ResourceManager.GetString("SaveButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit Profile.
        /// </summary>
        public static string UserProfileButtonText {
            get {
                return ResourceManager.GetString("UserProfileButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Wage Summary.
        /// </summary>
        public static string WageSummaryReportMenuItemText {
            get {
                return ResourceManager.GetString("WageSummaryReportMenuItemText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Weekly on Friday.
        /// </summary>
        public static string WeeklyFriday {
            get {
                return ResourceManager.GetString("WeeklyFriday", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Weekly on Monday.
        /// </summary>
        public static string WeeklyMonday {
            get {
                return ResourceManager.GetString("WeeklyMonday", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Weekly on Saturday.
        /// </summary>
        public static string WeeklySaturday {
            get {
                return ResourceManager.GetString("WeeklySaturday", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Weekly on Sunday.
        /// </summary>
        public static string WeeklySunday {
            get {
                return ResourceManager.GetString("WeeklySunday", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Weekly on Thursday.
        /// </summary>
        public static string WeeklyThursday {
            get {
                return ResourceManager.GetString("WeeklyThursday", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Weekly on Tuesday.
        /// </summary>
        public static string WeeklyTuesday {
            get {
                return ResourceManager.GetString("WeeklyTuesday", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Weekly on Wednesday.
        /// </summary>
        public static string WeeklyWednesday {
            get {
                return ResourceManager.GetString("WeeklyWednesday", resourceCulture);
            }
        }
    }
}
