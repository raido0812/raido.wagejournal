﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    
    #line 1 "..\..\Views\Account\Login.cshtml"
    using LocalizedText = Raido.WageJournal.Web.Views.Localization.Account.Login;
    
    #line default
    #line hidden
    using Raido.WageJournal.Web;
    
    #line 2 "..\..\Views\Account\Login.cshtml"
    using Raido.WageJournal.Web.ViewModels;
    
    #line default
    #line hidden
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Account/Login.cshtml")]
    public partial class _Views_Account_Login_cshtml : System.Web.Mvc.WebViewPage<LoginViewModel>
    {
        public _Views_Account_Login_cshtml()
        {
        }
        public override void Execute()
        {
            
            #line 4 "..\..\Views\Account\Login.cshtml"
  
    ViewBag.Title = LocalizedText.PageTitle;

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n<h2>");

            
            #line 8 "..\..\Views\Account\Login.cshtml"
Write(ViewBag.Title);

            
            #line default
            #line hidden
WriteLiteral(".</h2>\r\n<div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"col-md-8\"");

WriteLiteral(">\r\n        <section");

WriteLiteral(" id=\"loginForm\"");

WriteLiteral(">\r\n");

            
            #line 12 "..\..\Views\Account\Login.cshtml"
            
            
            #line default
            #line hidden
            
            #line 12 "..\..\Views\Account\Login.cshtml"
             using (Html.BeginForm(MVC.Account.Login((string)ViewBag.ReturnUrl), FormMethod.Post, new { @class = "form-horizontal", role = "form" }))
            {
                
            
            #line default
            #line hidden
            
            #line 14 "..\..\Views\Account\Login.cshtml"
           Write(Html.AntiForgeryToken());

            
            #line default
            #line hidden
            
            #line 14 "..\..\Views\Account\Login.cshtml"
                                        

            
            #line default
            #line hidden
WriteLiteral("                <h4>");

            
            #line 15 "..\..\Views\Account\Login.cshtml"
               Write(LocalizedText.LocalAccountLoginHeading);

            
            #line default
            #line hidden
WriteLiteral("</h4>\r\n");

WriteLiteral("                <hr />\r\n");

            
            #line 17 "..\..\Views\Account\Login.cshtml"
                
            
            #line default
            #line hidden
            
            #line 17 "..\..\Views\Account\Login.cshtml"
           Write(Html.ValidationSummary(true, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
            
            #line 17 "..\..\Views\Account\Login.cshtml"
                                                                                 

            
            #line default
            #line hidden
WriteLiteral("                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"col-md-2\"");

WriteLiteral(">\r\n");

WriteLiteral("                        ");

            
            #line 20 "..\..\Views\Account\Login.cshtml"
                   Write(Html.LabelFor(m => m.Email));

            
            #line default
            #line hidden
WriteLiteral("\r\n                    </div>\r\n                    <div");

WriteLiteral(" class=\"col-md-10\"");

WriteLiteral(">\r\n");

WriteLiteral("                        ");

            
            #line 23 "..\..\Views\Account\Login.cshtml"
                   Write(Html.TextBoxFor(m => m.Email, new { @class = "form-control" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("                        ");

            
            #line 24 "..\..\Views\Account\Login.cshtml"
                   Write(Html.ValidationMessageFor(m => m.Email, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                    </div>\r\n                </div>\r\n");

WriteLiteral("                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"col-md-2\"");

WriteLiteral(">\r\n");

WriteLiteral("                        ");

            
            #line 29 "..\..\Views\Account\Login.cshtml"
                   Write(Html.LabelFor(m => m.Password));

            
            #line default
            #line hidden
WriteLiteral("\r\n                    </div>\r\n                    <div");

WriteLiteral(" class=\"col-md-10\"");

WriteLiteral(">\r\n");

WriteLiteral("                        ");

            
            #line 32 "..\..\Views\Account\Login.cshtml"
                   Write(Html.PasswordFor(m => m.Password, new { @class = "form-control" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("                        ");

            
            #line 33 "..\..\Views\Account\Login.cshtml"
                   Write(Html.ValidationMessageFor(m => m.Password, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                    </div>\r\n                </div>\r\n");

WriteLiteral("                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"col-md-12 text-right\"");

WriteLiteral(">\r\n");

WriteLiteral("                        ");

            
            #line 38 "..\..\Views\Account\Login.cshtml"
                   Write(Html.LabelFor(m => m.RememberMe, new { @class = "inline" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("                        ");

            
            #line 39 "..\..\Views\Account\Login.cshtml"
                   Write(Html.CheckBoxFor(m => m.RememberMe));

            
            #line default
            #line hidden
WriteLiteral("\r\n                    </div>\r\n                </div>\r\n");

WriteLiteral("                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"col-md-12 text-right\"");

WriteLiteral(">\r\n                        <input");

WriteLiteral(" type=\"submit\"");

WriteAttribute("value", Tuple.Create(" value=\"", 2048), Tuple.Create("\"", 2086)
            
            #line 44 "..\..\Views\Account\Login.cshtml"
, Tuple.Create(Tuple.Create("", 2056), Tuple.Create<System.Object, System.Int32>(LocalizedText.LoginButtonText
            
            #line default
            #line hidden
, 2056), false)
);

WriteLiteral(" class=\"btn btn-default\"");

WriteLiteral(" />\r\n                    </div>\r\n                </div>\r\n");

WriteLiteral("                <p");

WriteLiteral(" class=\"text-right\"");

WriteLiteral(">\r\n");

WriteLiteral("                    ");

            
            #line 48 "..\..\Views\Account\Login.cshtml"
               Write(Html.ActionLink(LocalizedText.RegisterAccountActionText, MVC.Account.Register()));

            
            #line default
            #line hidden
WriteLiteral("\r\n                </p>\r\n");

            
            #line 50 "..\..\Views\Account\Login.cshtml"
                
            
            #line default
            #line hidden
            
            #line 53 "..\..\Views\Account\Login.cshtml"
                          
            }

            
            #line default
            #line hidden
WriteLiteral("        </section>\r\n    </div>\r\n    <div");

WriteLiteral(" class=\"col-md-4\"");

WriteLiteral(">\r\n        <section");

WriteLiteral(" id=\"socialLoginForm\"");

WriteLiteral(">\r\n");

WriteLiteral("            ");

            
            #line 59 "..\..\Views\Account\Login.cshtml"
       Write(Html.Partial("_ExternalLoginsListPartial", new ExternalLoginListViewModel { ReturnUrl = ViewBag.ReturnUrl }));

            
            #line default
            #line hidden
WriteLiteral("\r\n        </section>\r\n    </div>\r\n</div>\r\n\r\n");

DefineSection("Scripts", () => {

WriteLiteral("\r\n");

});

        }
    }
}
#pragma warning restore 1591
