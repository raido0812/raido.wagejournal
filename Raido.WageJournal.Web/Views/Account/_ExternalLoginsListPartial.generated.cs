﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    
    #line 1 "..\..\Views\Account\_ExternalLoginsListPartial.cshtml"
    using LocalizedText = Raido.WageJournal.Web.Views.Localization.Account._ExternalLoginsListPartial;
    
    #line default
    #line hidden
    
    #line 4 "..\..\Views\Account\_ExternalLoginsListPartial.cshtml"
    using Microsoft.Owin.Security;
    
    #line default
    #line hidden
    using Raido.WageJournal.Web;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Account/_ExternalLoginsListPartial.cshtml")]
    public partial class _Views_Account__ExternalLoginsListPartial_cshtml : System.Web.Mvc.WebViewPage<Raido.WageJournal.Web.ViewModels.ExternalLoginListViewModel>
    {
        public _Views_Account__ExternalLoginsListPartial_cshtml()
        {
        }
        public override void Execute()
        {
WriteLiteral("\r\n");

WriteLiteral("\r\n<h4>");

            
            #line 6 "..\..\Views\Account\_ExternalLoginsListPartial.cshtml"
Write(LocalizedText.OtherLoginServiceHeading);

            
            #line default
            #line hidden
WriteLiteral(" </h4>\r\n<hr />\r\n");

            
            #line 8 "..\..\Views\Account\_ExternalLoginsListPartial.cshtml"
  
    var loginProviders = Context.GetOwinContext().Authentication.GetExternalAuthenticationTypes();
    if (loginProviders.Count() == 0)
    {

            
            #line default
            #line hidden
WriteLiteral("        <div>\r\n            <p>\r\n");

WriteLiteral("                ");

            
            #line 14 "..\..\Views\Account\_ExternalLoginsListPartial.cshtml"
            Write(new MvcHtmlString(LocalizedText.NoExternalAuthServicesNotice));

            
            #line default
            #line hidden
WriteLiteral("\r\n            </p>\r\n        </div>\r\n");

            
            #line 17 "..\..\Views\Account\_ExternalLoginsListPartial.cshtml"
    }
    else
    {
        using (Html.BeginForm(MVC.Account.ExternalLogin(null, Model.ReturnUrl), FormMethod.Post))
        {
            
            
            #line default
            #line hidden
            
            #line 22 "..\..\Views\Account\_ExternalLoginsListPartial.cshtml"
       Write(Html.AntiForgeryToken());

            
            #line default
            #line hidden
            
            #line 22 "..\..\Views\Account\_ExternalLoginsListPartial.cshtml"
                                    

            
            #line default
            #line hidden
WriteLiteral("            <div");

WriteLiteral(" id=\"socialLoginList\"");

WriteLiteral(">\r\n                <p>\r\n");

            
            #line 25 "..\..\Views\Account\_ExternalLoginsListPartial.cshtml"
                    
            
            #line default
            #line hidden
            
            #line 25 "..\..\Views\Account\_ExternalLoginsListPartial.cshtml"
                     foreach (AuthenticationDescription p in loginProviders)
                    {

            
            #line default
            #line hidden
WriteLiteral("                        <button");

WriteLiteral(" type=\"submit\"");

WriteLiteral(" class=\"btn btn-default\"");

WriteAttribute("id", Tuple.Create(" id=\"", 963), Tuple.Create("\"", 989)
            
            #line 27 "..\..\Views\Account\_ExternalLoginsListPartial.cshtml"
, Tuple.Create(Tuple.Create("", 968), Tuple.Create<System.Object, System.Int32>(p.AuthenticationType
            
            #line default
            #line hidden
, 968), false)
);

WriteLiteral(" name=\"provider\"");

WriteAttribute("value", Tuple.Create(" value=\"", 1006), Tuple.Create("\"", 1035)
            
            #line 27 "..\..\Views\Account\_ExternalLoginsListPartial.cshtml"
                                        , Tuple.Create(Tuple.Create("", 1014), Tuple.Create<System.Object, System.Int32>(p.AuthenticationType
            
            #line default
            #line hidden
, 1014), false)
);

WriteAttribute("title", Tuple.Create(" title=\"", 1036), Tuple.Create("\"", 1109)
            
            #line 27 "..\..\Views\Account\_ExternalLoginsListPartial.cshtml"
                                                                      , Tuple.Create(Tuple.Create("", 1044), Tuple.Create<System.Object, System.Int32>(string.Format(LocalizedText.SubmitButtonTooltipText, @p.Caption)
            
            #line default
            #line hidden
, 1044), false)
);

WriteLiteral(">");

            
            #line 27 "..\..\Views\Account\_ExternalLoginsListPartial.cshtml"
                                                                                                                                                                                                                    Write(p.AuthenticationType);

            
            #line default
            #line hidden
WriteLiteral("</button>\r\n");

            
            #line 28 "..\..\Views\Account\_ExternalLoginsListPartial.cshtml"
                    }

            
            #line default
            #line hidden
WriteLiteral("                </p>\r\n            </div>\r\n");

            
            #line 31 "..\..\Views\Account\_ExternalLoginsListPartial.cshtml"
        }
    }

            
            #line default
            #line hidden
WriteLiteral("\r\n");

        }
    }
}
#pragma warning restore 1591
