// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments and CLS compliance
// 0108: suppress "Foo hides inherited member Foo. Use the new keyword if hiding was intended." when a controller and its abstract parent are both processed
// 0114: suppress "Foo.BarController.Baz()' hides inherited member 'Qux.BarController.Baz()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword." when an action (with an argument) overrides an action in a parent controller
#pragma warning disable 1591, 3008, 3009, 0108, 0114
#region T4MVC

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using T4MVC;
namespace Raido.WageJournal.Web.Areas.Companies.Controllers
{
    public partial class ReportsController
    {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected ReportsController(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(Task<ActionResult> taskResult)
        {
            return RedirectToAction(taskResult.Result);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoutePermanent(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(Task<ActionResult> taskResult)
        {
            return RedirectToActionPermanent(taskResult.Result);
        }

        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult WageSummaryFilterPage()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.WageSummaryFilterPage);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Threading.Tasks.Task<System.Web.Mvc.ActionResult> GetWageSummaryPdf()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.GetWageSummaryPdf);
            return System.Threading.Tasks.Task.FromResult(callInfo as System.Web.Mvc.ActionResult);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult PayslipFilterPage()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.PayslipFilterPage);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Threading.Tasks.Task<System.Web.Mvc.ActionResult> GetPayslipPdf()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.GetPayslipPdf);
            return System.Threading.Tasks.Task.FromResult(callInfo as System.Web.Mvc.ActionResult);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult AttendanceSummaryFilterPage()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.AttendanceSummaryFilterPage);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Threading.Tasks.Task<System.Web.Mvc.ActionResult> GetAttendanceSummaryPdf()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.GetAttendanceSummaryPdf);
            return System.Threading.Tasks.Task.FromResult(callInfo as System.Web.Mvc.ActionResult);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ReportsController Actions { get { return MVC.Companies.Reports; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "Companies";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "Reports";
        [GeneratedCode("T4MVC", "2.0")]
        public const string NameConst = "Reports";
        [GeneratedCode("T4MVC", "2.0")]
        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass
        {
            public readonly string WageSummaryFilterPage = "WageSummaryFilterPage";
            public readonly string GetWageSummaryPdf = "GetWageSummaryPdf";
            public readonly string PayslipFilterPage = "PayslipFilterPage";
            public readonly string GetPayslipPdf = "GetPayslipPdf";
            public readonly string AttendanceSummaryFilterPage = "AttendanceSummaryFilterPage";
            public readonly string GetAttendanceSummaryPdf = "GetAttendanceSummaryPdf";
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNameConstants
        {
            public const string WageSummaryFilterPage = "WageSummaryFilterPage";
            public const string GetWageSummaryPdf = "GetWageSummaryPdf";
            public const string PayslipFilterPage = "PayslipFilterPage";
            public const string GetPayslipPdf = "GetPayslipPdf";
            public const string AttendanceSummaryFilterPage = "AttendanceSummaryFilterPage";
            public const string GetAttendanceSummaryPdf = "GetAttendanceSummaryPdf";
        }


        static readonly ActionParamsClass_WageSummaryFilterPage s_params_WageSummaryFilterPage = new ActionParamsClass_WageSummaryFilterPage();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_WageSummaryFilterPage WageSummaryFilterPageParams { get { return s_params_WageSummaryFilterPage; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_WageSummaryFilterPage
        {
            public readonly string companyIdentifier = "companyIdentifier";
        }
        static readonly ActionParamsClass_GetWageSummaryPdf s_params_GetWageSummaryPdf = new ActionParamsClass_GetWageSummaryPdf();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_GetWageSummaryPdf GetWageSummaryPdfParams { get { return s_params_GetWageSummaryPdf; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_GetWageSummaryPdf
        {
            public readonly string startDate = "startDate";
            public readonly string endDate = "endDate";
        }
        static readonly ActionParamsClass_PayslipFilterPage s_params_PayslipFilterPage = new ActionParamsClass_PayslipFilterPage();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_PayslipFilterPage PayslipFilterPageParams { get { return s_params_PayslipFilterPage; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_PayslipFilterPage
        {
            public readonly string companyIdentifier = "companyIdentifier";
        }
        static readonly ActionParamsClass_GetPayslipPdf s_params_GetPayslipPdf = new ActionParamsClass_GetPayslipPdf();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_GetPayslipPdf GetPayslipPdfParams { get { return s_params_GetPayslipPdf; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_GetPayslipPdf
        {
            public readonly string startDate = "startDate";
            public readonly string endDate = "endDate";
        }
        static readonly ActionParamsClass_AttendanceSummaryFilterPage s_params_AttendanceSummaryFilterPage = new ActionParamsClass_AttendanceSummaryFilterPage();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_AttendanceSummaryFilterPage AttendanceSummaryFilterPageParams { get { return s_params_AttendanceSummaryFilterPage; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_AttendanceSummaryFilterPage
        {
            public readonly string companyIdentifier = "companyIdentifier";
        }
        static readonly ActionParamsClass_GetAttendanceSummaryPdf s_params_GetAttendanceSummaryPdf = new ActionParamsClass_GetAttendanceSummaryPdf();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_GetAttendanceSummaryPdf GetAttendanceSummaryPdfParams { get { return s_params_GetAttendanceSummaryPdf; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_GetAttendanceSummaryPdf
        {
            public readonly string startDate = "startDate";
            public readonly string endDate = "endDate";
        }
        static readonly ViewsClass s_views = new ViewsClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewsClass Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewsClass
        {
            static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
            public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
            public class _ViewNamesClass
            {
                public readonly string AttendanceSummaryFilterPage = "AttendanceSummaryFilterPage";
                public readonly string PayslipFilterPage = "PayslipFilterPage";
                public readonly string WageSummaryFilterPage = "WageSummaryFilterPage";
            }
            public readonly string AttendanceSummaryFilterPage = "~/Areas/Companies/Views/Reports/AttendanceSummaryFilterPage.cshtml";
            public readonly string PayslipFilterPage = "~/Areas/Companies/Views/Reports/PayslipFilterPage.cshtml";
            public readonly string WageSummaryFilterPage = "~/Areas/Companies/Views/Reports/WageSummaryFilterPage.cshtml";
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public partial class T4MVC_ReportsController : Raido.WageJournal.Web.Areas.Companies.Controllers.ReportsController
    {
        public T4MVC_ReportsController() : base(Dummy.Instance) { }

        [NonAction]
        partial void WageSummaryFilterPageOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, System.Guid companyIdentifier);

        [NonAction]
        public override System.Web.Mvc.ActionResult WageSummaryFilterPage(System.Guid companyIdentifier)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.WageSummaryFilterPage);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "companyIdentifier", companyIdentifier);
            WageSummaryFilterPageOverride(callInfo, companyIdentifier);
            return callInfo;
        }

        [NonAction]
        partial void GetWageSummaryPdfOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, System.DateTime startDate, System.DateTime endDate);

        [NonAction]
        public override System.Threading.Tasks.Task<System.Web.Mvc.ActionResult> GetWageSummaryPdf(System.DateTime startDate, System.DateTime endDate)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.GetWageSummaryPdf);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "startDate", startDate);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "endDate", endDate);
            GetWageSummaryPdfOverride(callInfo, startDate, endDate);
            return System.Threading.Tasks.Task.FromResult(callInfo as System.Web.Mvc.ActionResult);
        }

        [NonAction]
        partial void PayslipFilterPageOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, System.Guid companyIdentifier);

        [NonAction]
        public override System.Web.Mvc.ActionResult PayslipFilterPage(System.Guid companyIdentifier)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.PayslipFilterPage);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "companyIdentifier", companyIdentifier);
            PayslipFilterPageOverride(callInfo, companyIdentifier);
            return callInfo;
        }

        [NonAction]
        partial void GetPayslipPdfOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, System.DateTime startDate, System.DateTime endDate);

        [NonAction]
        public override System.Threading.Tasks.Task<System.Web.Mvc.ActionResult> GetPayslipPdf(System.DateTime startDate, System.DateTime endDate)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.GetPayslipPdf);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "startDate", startDate);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "endDate", endDate);
            GetPayslipPdfOverride(callInfo, startDate, endDate);
            return System.Threading.Tasks.Task.FromResult(callInfo as System.Web.Mvc.ActionResult);
        }

        [NonAction]
        partial void AttendanceSummaryFilterPageOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, System.Guid companyIdentifier);

        [NonAction]
        public override System.Web.Mvc.ActionResult AttendanceSummaryFilterPage(System.Guid companyIdentifier)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.AttendanceSummaryFilterPage);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "companyIdentifier", companyIdentifier);
            AttendanceSummaryFilterPageOverride(callInfo, companyIdentifier);
            return callInfo;
        }

        [NonAction]
        partial void GetAttendanceSummaryPdfOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, System.DateTime startDate, System.DateTime endDate);

        [NonAction]
        public override System.Threading.Tasks.Task<System.Web.Mvc.ActionResult> GetAttendanceSummaryPdf(System.DateTime startDate, System.DateTime endDate)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.GetAttendanceSummaryPdf);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "startDate", startDate);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "endDate", endDate);
            GetAttendanceSummaryPdfOverride(callInfo, startDate, endDate);
            return System.Threading.Tasks.Task.FromResult(callInfo as System.Web.Mvc.ActionResult);
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591, 3008, 3009, 0108, 0114
