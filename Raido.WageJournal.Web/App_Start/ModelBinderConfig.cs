﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Raido.WageJournal.Web.CustomModelBinders;

namespace Raido.WageJournal.Web.App_Start
{
    public static class ModelBinderConfig
    {
        public static void SetupBindings()
        {
            ModelBinders.Binders.Add(typeof(decimal?), new DecimalModelBinder());
        }
    }
}