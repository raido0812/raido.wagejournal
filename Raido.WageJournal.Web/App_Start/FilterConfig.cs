﻿using System.Web;
using System.Web.Mvc;
using Raido.WageJournal.Web.Filter;

namespace Raido.WageJournal.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new CompressionAttribute());
            filters.Add(new HandleErrorAttribute());
            filters.Add(new CultureAttribute());
            filters.Add(new System.Web.Mvc.AuthorizeAttribute());
            filters.Add(new RequireHttpsAttribute());
        }
    }
}
