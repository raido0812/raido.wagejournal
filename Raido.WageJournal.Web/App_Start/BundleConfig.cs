﻿using System.Web;
using System.Web.Optimization;

namespace Raido.WageJournal.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/prerequisites")
                .Include(
                    "~/Scripts/modernizr-*",
                    "~/Scripts/jquery-{version}.js",
                    "~/Scripts/jquery.validate.js",
                    "~/Scripts/jquery.validate.unobtrusive.js",
                    "~/Scripts/gnarf/jquery.ajaxQueue.js",
                    "~/Scripts/date-extension.js",
                    "~/Scripts/bootstrap.js",
                    "~/Scripts/bootstrap-datepicker.js",
                    "~/Scripts/locales/*.js",
                    "~/Scripts/respond.js",
                    "~/Scripts/toastr.js",
                    "~/Scripts/l10n.js",
                    "~/Scripts/cldr.js",
                    "~/scripts/cldr/event.js",
                    "~/scripts/cldr/supplemental.js",
                    "~/scripts/cldr/unresolved.js",
                    "~/Scripts/globalize.js",
                    "~/Scripts/globalize/number.js",
                    "~/Scripts/globalize/currency.js",
                    "~/Scripts/globalize/date.js",
                    "~/Scripts/preloader.js",
                    "~/Scripts/site-init.js"));

            bundles.Add(new StyleBundle("~/content/site").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-datepicker3.css",
                      "~/Content/toastr.css",
                      "~/Content/site.css",
                      "~/Content/bootstrap-override.css"));

            bundles.Add(new ScriptBundle("~/bundles/company-details")
                .Include("~/Areas/Companies/Scripts/Companies/company-detail-auto-save.js")
                .Include("~/Areas/Companies/Scripts/Companies/company-details.js"));

            bundles.Add(new StyleBundle("~/content/employee/work")
                .Include("~/Content/slick.css")
                .Include("~/Content/custom-toggle.css")
                .Include("~/Areas/Companies/Styles/Employees/deduction.css")
                .Include("~/Areas/Companies/Styles/Employees/work-for-day.css"));

            bundles.Add(new ScriptBundle("~/bundles/employee/work")
                .Include("~/Scripts/slick.js")
                .Include("~/Scripts/uuid.js")
                .Include("~/Scripts/math.min.js")
                .Include("~/Areas/Companies/Scripts/Employees/modal-loader.js")
                .Include("~/Areas/Companies/Scripts/Employees/index.js")
                .Include("~/Areas/Companies/Scripts/Employees/deduction-detail-auto-save.js")
                .Include("~/Areas/Companies/Scripts/Employees/deduction-details.js")
                .Include("~/Areas/Companies/Scripts/Employees/employee-detail-auto-save.js")
                .Include("~/Areas/Companies/Scripts/Employees/employee-details.js")
                .Include("~/Areas/Companies/Scripts/Employees/adjustment-detail-auto-save.js")
                .Include("~/Areas/Companies/Scripts/Employees/adjustment-details.js")
                .Include("~/Areas/Companies/Scripts/Employees/work-detail-auto-save.js")
                .Include("~/Areas/Companies/Scripts/Employees/work-details.js"));

            bundles.Add(new ScriptBundle("~/bundles/reports/wage-summary")
                .Include("~/Areas/Companies/Scripts/Reports/wage-summary.js"));
            bundles.Add(new ScriptBundle("~/bundles/reports/payslip")
                .Include("~/Areas/Companies/Scripts/Reports/payslip.js"));
            bundles.Add(new ScriptBundle("~/bundles/reports/attendance-summary")
                .Include("~/Areas/Companies/Scripts/Reports/attendance-summary.js"));

            bundles.Add(new ScriptBundle("~/bundles/manage")
                .Include("~/Scripts/Manage/change-culture.js"));

            bundles.Add(new ScriptBundle("~/bundles/import-timesheet")
                .Include("~/Scripts/jquery.ui.widget.js")
                .Include("~/Scripts/blueimp-file-upload/jquery.fileupload.js")
                .Include("~/Scripts/blueimp-file-upload/jquery.fileupload-process.js")
                .Include("~/Scripts/blueimp-file-upload/jquery.fileupload-validate.js")
                .Include("~/Areas/Companies/Scripts/TimeCardLogImport/import-timesheet.js"));
            
            bundles.Add(new StyleBundle("~/content/import-timesheet")
                .Include("~/Content/file-drop-zone.css")
                .Include("~/Areas/Companies/Styles/TimeCardLogImport/import-timesheet.css"));
        }
    }
}
