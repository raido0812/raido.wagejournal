﻿using Autofac;
using Autofac.Integration.Mvc;
using Raido.WageJournal.DataAccess.Security;
using Raido.WageJournal.DataAccess.Repositories.Interfaces;
using Raido.WageJournal.Web.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Raido.WageJournal.Logic.Services.Interfaces;
using System.Runtime.Caching;

namespace Raido.WageJournal.Web.App_Start
{
    public static class InversionOfControlConfig
    {
        public static void RegisterComponents()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterFilterProvider(); // Enable property injection on filters.

            // Register individual components
            builder.Register(x => new CustomPrincipalProvider()).As<ICustomPrincipalProvider>().InstancePerRequest();

            builder.Register(x => MemoryCache.Default).As<ObjectCache>().SingleInstance();
            
            // Scan an assembly for components
            builder.RegisterAssemblyTypes(Assembly.GetAssembly(typeof(ICompanyRepository)))
                                       .Where(t =>
                                                t.Namespace != null &&
                                                t.Namespace.StartsWith("Raido.WageJournal.DataAccess.Repositories", StringComparison.Ordinal))
                                       .AsImplementedInterfaces()
                                       .InstancePerRequest();

            // Scan an assembly for components
            builder.RegisterAssemblyTypes(Assembly.GetAssembly(typeof(ICompanyService)))
                                       .Where(t =>
                                                t.Namespace != null &&
                                                t.Namespace.StartsWith("Raido.WageJournal.Logic.Services", StringComparison.Ordinal))
                                       .AsImplementedInterfaces()
                                       .InstancePerRequest();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}