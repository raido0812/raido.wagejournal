using Raido.WageJournal.Web;
using WebActivatorEx;

[assembly: PostApplicationStartMethod(typeof(ElmahInitializer), "Initialize")]

namespace Raido.WageJournal.Web
{
    using Elmah.SqlServer.EFInitializer;

    public static class ElmahInitializer
    {
        public static void Initialize()
        {
            using (var context = new ElmahContext())
            {
                context.Database.Initialize(true);
            }
        }
    }
}