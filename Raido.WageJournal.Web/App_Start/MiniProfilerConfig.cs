﻿using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Raido.WageJournal.Extensions;
using Raido.WageJournal.Web.HttpModules;
using StackExchange.Profiling;
using StackExchange.Profiling.EntityFramework6;
using StackExchange.Profiling.Mvc;


[assembly: WebActivatorEx.PreApplicationStartMethod(
    typeof(Raido.WageJournal.Web.App_Start.MiniProfilerConfig), "PreStart")]

[assembly: WebActivatorEx.PostApplicationStartMethod(
    typeof(Raido.WageJournal.Web.App_Start.MiniProfilerConfig), "PostStart")]

namespace Raido.WageJournal.Web.App_Start
{
    public static class MiniProfilerConfig
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "PreStart", Justification = "Correct Casing")]
        public static void PreStart()
        {
            if (System.Configuration.ConfigurationManager.AppSettings["ShowMiniProfiler"].AsNullableBoolean() ?? false)
            {
                MiniProfiler.Settings.SqlFormatter = new StackExchange.Profiling.SqlFormatters.SqlServerFormatter();

                // some things should never be seen
                var ignored = MiniProfiler.Settings.IgnoredPaths.ToList();

                ignored.Add("/bundles/");
                ignored.Add("/Styles/");
                ignored.Add("/Content/");
                ignored.Add("/Scripts/");
                ignored.Add("/Images/");
                ignored.Add(".js");
                ignored.Add("/__browserLink/");

                MiniProfiler.Settings.IgnoredPaths = ignored.ToArray();

                MiniProfilerEF6.Initialize();

                //Make sure the MiniProfiler handles BeginRequest and EndRequest
                HttpApplication.RegisterModule(typeof(MiniProfilerStartupModule));

                //Setup profiler for Controllers via a Global ActionFilter
                GlobalFilters.Filters.Add(new ProfilingActionFilter());

                // MiniProfiler.Settings.Results_Authorize = (request) =>
                // {
                // you should implement this if you need to restrict visibility of profiling on a per request basis 
                //     return !DisableProfilingResults; 
                // };

                //  the list of all sessions in the store is restricted by default, you must return true to alllow it
                // MiniProfiler.Settings.Results_List_Authorize = (request) =>
                // {a
                // you may implement this if you need to restrict visibility of profiling lists on a per request basis 
                //return true; // all requests are kosher
                // };
            }
        }

        public static void PostStart()
        {
            if (System.Configuration.ConfigurationManager.AppSettings["ShowMiniProfiler"].AsNullableBoolean() ?? false)
            {
                // Intercept ViewEngines to profile all partial views and regular views.
                // If you prefer to insert your profiling blocks manually you can comment this out
                var copy = ViewEngines.Engines.ToList();
                ViewEngines.Engines.Clear();
                foreach (var item in copy)
                {
                    ViewEngines.Engines.Add(new ProfilingViewEngine(item));
                }
            }
        }
    }
}