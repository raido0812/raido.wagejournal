// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments and CLS compliance
// 0108: suppress "Foo hides inherited member Foo. Use the new keyword if hiding was intended." when a controller and its abstract parent are both processed
// 0114: suppress "Foo.BarController.Baz()' hides inherited member 'Qux.BarController.Baz()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword." when an action (with an argument) overrides an action in a parent controller
#pragma warning disable 1591, 3008, 3009, 0108, 0114
#region T4MVC

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using T4MVC;
namespace Raido.WageJournal.Web.Areas.Companies.Controllers
{
    public partial class CompaniesController
    {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected CompaniesController(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(Task<ActionResult> taskResult)
        {
            return RedirectToAction(taskResult.Result);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoutePermanent(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(Task<ActionResult> taskResult)
        {
            return RedirectToActionPermanent(taskResult.Result);
        }

        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Threading.Tasks.Task<System.Web.Mvc.ActionResult> Details()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Details);
            return System.Threading.Tasks.Task.FromResult(callInfo as System.Web.Mvc.ActionResult);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Threading.Tasks.Task<System.Web.Mvc.JsonResult> SaveCompanyDetails()
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.SaveCompanyDetails);
            return System.Threading.Tasks.Task.FromResult(callInfo as System.Web.Mvc.JsonResult);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Threading.Tasks.Task<System.Web.Mvc.ActionResult> AddCompany()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.AddCompany);
            return System.Threading.Tasks.Task.FromResult(callInfo as System.Web.Mvc.ActionResult);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public CompaniesController Actions { get { return MVC.Companies.Companies; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "Companies";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "Companies";
        [GeneratedCode("T4MVC", "2.0")]
        public const string NameConst = "Companies";
        [GeneratedCode("T4MVC", "2.0")]
        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass
        {
            public readonly string DropdownMenuWithPreselect = "DropdownMenuWithPreselect";
            public readonly string Details = "Details";
            public readonly string SaveCompanyDetails = "SaveCompanyDetails";
            public readonly string AddCompany = "AddCompany";
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNameConstants
        {
            public const string DropdownMenuWithPreselect = "DropdownMenuWithPreselect";
            public const string Details = "Details";
            public const string SaveCompanyDetails = "SaveCompanyDetails";
            public const string AddCompany = "AddCompany";
        }


        static readonly ActionParamsClass_Details s_params_Details = new ActionParamsClass_Details();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Details DetailsParams { get { return s_params_Details; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Details
        {
            public readonly string companyIdentifier = "companyIdentifier";
        }
        static readonly ActionParamsClass_SaveCompanyDetails s_params_SaveCompanyDetails = new ActionParamsClass_SaveCompanyDetails();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_SaveCompanyDetails SaveCompanyDetailsParams { get { return s_params_SaveCompanyDetails; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_SaveCompanyDetails
        {
            public readonly string companyDetails = "companyDetails";
            public readonly string companyIdentifier = "companyIdentifier";
        }
        static readonly ActionParamsClass_AddCompany s_params_AddCompany = new ActionParamsClass_AddCompany();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_AddCompany AddCompanyParams { get { return s_params_AddCompany; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_AddCompany
        {
            public readonly string newCompany = "newCompany";
        }
        static readonly ViewsClass s_views = new ViewsClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewsClass Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewsClass
        {
            static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
            public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
            public class _ViewNamesClass
            {
                public readonly string Details = "Details";
            }
            public readonly string Details = "~/Areas/Companies/Views/Companies/Details.cshtml";
            static readonly _PartialClass s_Partial = new _PartialClass();
            public _PartialClass Partial { get { return s_Partial; } }
            [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
            public partial class _PartialClass
            {
                static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
                public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
                public class _ViewNamesClass
                {
                    public readonly string DropdownMenuWithPreselect = "DropdownMenuWithPreselect";
                }
                public readonly string DropdownMenuWithPreselect = "~/Areas/Companies/Views/Companies/Partial/DropdownMenuWithPreselect.cshtml";
            }
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public partial class T4MVC_CompaniesController : Raido.WageJournal.Web.Areas.Companies.Controllers.CompaniesController
    {
        public T4MVC_CompaniesController() : base(Dummy.Instance) { }

        [NonAction]
        partial void DropdownMenuWithPreselectOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult DropdownMenuWithPreselect()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.DropdownMenuWithPreselect);
            DropdownMenuWithPreselectOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void DetailsOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, System.Guid companyIdentifier);

        [NonAction]
        public override System.Threading.Tasks.Task<System.Web.Mvc.ActionResult> Details(System.Guid companyIdentifier)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Details);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "companyIdentifier", companyIdentifier);
            DetailsOverride(callInfo, companyIdentifier);
            return System.Threading.Tasks.Task.FromResult(callInfo as System.Web.Mvc.ActionResult);
        }

        [NonAction]
        partial void SaveCompanyDetailsOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, Raido.WageJournal.Contracts.ComplexTransferObjects.CompanyAndSettings companyDetails, System.Guid companyIdentifier);

        [NonAction]
        public override System.Threading.Tasks.Task<System.Web.Mvc.JsonResult> SaveCompanyDetails(Raido.WageJournal.Contracts.ComplexTransferObjects.CompanyAndSettings companyDetails, System.Guid companyIdentifier)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.SaveCompanyDetails);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "companyDetails", companyDetails);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "companyIdentifier", companyIdentifier);
            SaveCompanyDetailsOverride(callInfo, companyDetails, companyIdentifier);
            return System.Threading.Tasks.Task.FromResult(callInfo as System.Web.Mvc.JsonResult);
        }

        [NonAction]
        partial void AddCompanyOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, Raido.WageJournal.Contracts.DataTransferObjects.Company newCompany);

        [NonAction]
        public override System.Threading.Tasks.Task<System.Web.Mvc.ActionResult> AddCompany(Raido.WageJournal.Contracts.DataTransferObjects.Company newCompany)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.AddCompany);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "newCompany", newCompany);
            AddCompanyOverride(callInfo, newCompany);
            return System.Threading.Tasks.Task.FromResult(callInfo as System.Web.Mvc.ActionResult);
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591, 3008, 3009, 0108, 0114
