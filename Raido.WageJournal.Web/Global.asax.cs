﻿using Raido.WageJournal.Web.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Raido.WageJournal.DataAccess.MappingConfiguration;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Raido.WageJournal.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            InversionOfControlConfig.RegisterComponents();
            AutoMapperConfiguration.RegisterAllMappings();
            ModelBinderConfig.SetupBindings();

        }
    }
}
