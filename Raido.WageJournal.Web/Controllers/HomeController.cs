﻿using Raido.WageJournal.DataAccess.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;

namespace Raido.WageJournal.Web.Controllers
{
    [RequireHttps]
    public partial class HomeController : AsyncController
    {
        private ICompanyRepository _companyRepository;
        public HomeController(ICompanyRepository companyRepository)
        {
            this._companyRepository = companyRepository;
        }

        public async virtual Task<ActionResult> Index()
        {
            if (this.User.Identity.IsAuthenticated)
            {
                return this.RedirectToAction(MVC.Companies.Companies.Details(await this._companyRepository.GetLastViewedCompanyIdentifierAsync()));
            }
            else
            {
                return this.RedirectToAction(MVC.Account.Login());
            }
        }
    }
}