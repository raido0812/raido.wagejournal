﻿using System.Configuration;
using System.Web;
using Raido.WageJournal.Extensions;
using StackExchange.Profiling;

namespace Raido.WageJournal.Web.HttpModules
{
    public class MiniProfilerStartupModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.BeginRequest += (sender, e) =>
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ShowMiniProfiler"].AsNullableBoolean() ?? false)
                {
                    MiniProfiler.Start();
                }
            };

            context.EndRequest += (sender, e) =>
            {
                MiniProfiler.Stop();
            };
        }

        public void Dispose()
        {
        }
    }
}