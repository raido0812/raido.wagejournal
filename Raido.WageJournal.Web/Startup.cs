﻿using Microsoft.Owin;
using Owin;
using System.Data.Entity;

[assembly: OwinStartupAttribute(typeof(Raido.WageJournal.Web.Startup))]
namespace Raido.WageJournal.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<WageJournal.DataAccess.DataContext.WageDbContext, WageJournal.DataAccess.Migrations.Configuration>());
            ConfigureAuth(app);
        }
    }
}
