﻿(function (preloader, $, undefined) {
    var $preloaderWrapper = $('.preloader-wrapper');

    preloader.show = function () {
        $preloaderWrapper.removeClass('hidden');
    };

    preloader.hide = function () {
        $preloaderWrapper.addClass('hidden');
    };
}(window.preloader = window.preloader || {}, jQuery));