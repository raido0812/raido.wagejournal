﻿$(function () {

    $(document).on('change', '#CultureDropDown', function () {
        var $dropdown = $(this);
        $.ajax({
            type: 'POST',
            url: $dropdown.data('change-url'),
            contentType: 'application/json; charset=utf-8',
            data: '{ "cultureCode" : "' + $dropdown.val() + '" }',
            success: function (result) {
                if (result) {
                    toastr.success(document.webL10n.get("AutoSaveSuccessful"));
                }
                else {
                    toastr.error(document.webL10n.get("AutoSaveFailed"));
                }
            },
            error: function (result) {
                toastr.error(document.webL10n.get("AutoSaveFailed"));
            }
        });
    });

});