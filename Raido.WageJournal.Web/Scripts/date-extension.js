﻿(function(window, undefined)
{
    if (!window.Date.prototype.toUniversalString)
    {
        window.Date.prototype.toUniversalString = function ()
        {
            /// <summary>Converts to yyyy-MM-dd format string</summary>
            var result = '' + this.getFullYear() ;
        
            if (this.getMonth() > 8) {
                result += '-' + (this.getMonth() + 1);
            }
            else {
                result += '-0' + (this.getMonth() + 1);
            }

            if (this.getDate() > 9) {
                result += '-' + this.getDate();
            }
            else {
                result += '-0' + this.getDate();
            }

            return result;
        };

        window.Date.prototype.addDays = function(days)
        {
            /// <summary>Gets a new date after adding the specified days</summary>
            /// <param name="days" type="Number">Number of days to Add</param>
            var dat = new Date(this.valueOf());
            dat.setDate(dat.getDate() + days);
            return dat;
        };
    }
})(window);