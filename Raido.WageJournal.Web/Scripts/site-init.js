﻿$(function()
{
    var $currentDocument = $(document);
    $currentDocument.on('click', 'a.submit', function(e)
    {
        var $clickedButton = $(this);
        var $formToSubmit = $clickedButton.closest('form');
        if ($formToSubmit.length !== 0) {
            e.preventDefault();
            $formToSubmit.submit();
        }
    });

    if (document.webL10n) {
        document.webL10n.ready(function()
        {
            document.webL10n.setLanguage(window.appSettings.culture, function()
            {
                //initialize jquery validate messages in local language
                $.validator.messages.required = document.webL10n.get("jQueryValidateMessage_Required");
                $.validator.messages.remote = document.webL10n.get("jQueryValidateMessage_Remote");
                $.validator.messages.email = document.webL10n.get("jQueryValidateMessage_Email");
                $.validator.messages.url = document.webL10n.get("jQueryValidateMessage_Url");
                $.validator.messages.date = document.webL10n.get("jQueryValidateMessage_Date");
                $.validator.messages.dateISO = document.webL10n.get("jQueryValidateMessage_DateISO");
                $.validator.messages.number = document.webL10n.get("jQueryValidateMessage_Number");
                $.validator.messages.digits = document.webL10n.get("jQueryValidateMessage_Digits");
                $.validator.messages.equalTo = document.webL10n.get("jQueryValidateMessage_EqualTo");
                $.validator.messages.maxlength = $.validator.format(document.webL10n.get("jQueryValidateMessage_Maxlength"));
                $.validator.messages.minlength = $.validator.format(document.webL10n.get("jQueryValidateMessage_Minlength"));
                $.validator.messages.rangelength = $.validator.format(document.webL10n.get("jQueryValidateMessage_Rangelength"));
                $.validator.messages.range = $.validator.format(document.webL10n.get("jQueryValidateMessage_Range"));
                $.validator.messages.max = $.validator.format(document.webL10n.get("jQueryValidateMessage_Max"));
                $.validator.messages.min = $.validator.format(document.webL10n.get("jQueryValidateMessage_Min"));
                $.validator.messages.step = $.validator.format(document.webL10n.get("jQueryValidateMessage_Step"));
            });
        });
    }

    $currentDocument.on('submit', 'form', function(e)
    {
        var $form = $(this);
        if (!$form.valid()) {
            e.cancel();
            e.preventDefault();
        }
    });

    $.fn.datepicker.defaults.autoclose = true;
    $.fn.datepicker.defaults.format = window.appSettings.bootstrapDateFormat;
    $.fn.datepicker.defaults.orientation = 'top left';
    $.fn.datepicker.defaults.todayBtn = 'linked';
    $.fn.datepicker.defaults.todayHighlight = false;

    var allDateInputs = $('input[type="Date"]')
        .attr('type', 'text')
        .datepicker({ language: window.appSettings.culture, format: window.appSettings.bootstrapDateFormat });



    $.validator.setDefaults({
        highlight: function(element, errorClass, validClass)
        {
            if (element.type === 'radio') {
                this.findByName(element.name).addClass(errorClass).removeClass(validClass);
            } else {
                $(element).addClass(errorClass).removeClass(validClass);
                $(element).closest('.control-group').removeClass('success').addClass('error');
            }
        },
        unhighlight: function(element, errorClass, validClass)
        {
            if (element.type === 'radio') {
                this.findByName(element.name).removeClass(errorClass).addClass(validClass);
            } else {
                $(element).removeClass(errorClass).addClass(validClass);
                $(element).closest('.control-group').removeClass('error').addClass('success');
            }
        }
    });


    // Used to normalize json result just incase the mime type for json is not set correctly on web server
    var normalizeJsonResults = function()
    {
        // Normalize $.get results, we only need the JSON, not the request statuses.
        return [].slice.apply(arguments, [0]).map(function(result)
        {
            return result[0];
        });
    }

    // load up the culture config files for number and date formatting via ajax
    $.when(
      $.getJSON(window.appSettings.rootUrl + 'Scripts/cldr/supplemental/likelySubtags.json'),
      $.getJSON(window.appSettings.rootUrl + 'Scripts/cldr/supplemental/numberingSystems.json'),
      $.getJSON(window.appSettings.rootUrl + 'Scripts/cldr/supplemental/timeData.json'),
      $.getJSON(window.appSettings.rootUrl + 'Scripts/cldr/supplemental/weekData.json'),
      $.getJSON(window.appSettings.rootUrl + 'Scripts/cldr/' + window.appSettings.culture + '/numbers.json'),
      $.getJSON(window.appSettings.rootUrl + 'Scripts/cldr/' + window.appSettings.culture + '/ca-gregorian.json'),
      $.getJSON(window.appSettings.rootUrl + 'Scripts/cldr/' + window.appSettings.culture + '/timeZoneNames.json')
    )
    .then(normalizeJsonResults)
    .then(Globalize.load)
    .then(function()
    {
        // Set the globalize locale
        Globalize.locale(window.appSettings.culture);

        $(document).data('globalize-loaded', true);
    });
});