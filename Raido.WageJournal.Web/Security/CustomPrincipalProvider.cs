﻿using System.Linq;
using System.Security.Claims;
using Raido.WageJournal.DataAccess.Security;
namespace Raido.WageJournal.Web.Security
{
    public class CustomPrincipalProvider : ICustomPrincipalProvider
    {
        private string _currentUserID = string.Empty;

        public ClaimsPrincipal GetCurrentUser()
        {
            return ClaimsPrincipal.Current;
        }

        public string GetCurrentUserId()
        {
            if (string.IsNullOrWhiteSpace(this._currentUserID) &&
                this.GetCurrentUser() != null &&
                this.GetCurrentUser().Claims.Any(c => c.Type == ClaimTypes.NameIdentifier))
            {
                this._currentUserID = this.GetCurrentUser().Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;
            }

            return this._currentUserID;
        }
    }
}