// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments and CLS compliance
// 0108: suppress "Foo hides inherited member Foo. Use the new keyword if hiding was intended." when a controller and its abstract parent are both processed
// 0114: suppress "Foo.BarController.Baz()' hides inherited member 'Qux.BarController.Baz()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword." when an action (with an argument) overrides an action in a parent controller
#pragma warning disable 1591, 3008, 3009, 0108, 0114
#region T4MVC

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using T4MVC;
namespace T4MVC
{
    public class LocalizationController
    {

        static readonly ViewsClass s_views = new ViewsClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewsClass Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewsClass
        {
            static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
            public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
            public class _ViewNamesClass
            {
            }
            static readonly _AccountClass s_Account = new _AccountClass();
            public _AccountClass Account { get { return s_Account; } }
            [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
            public partial class _AccountClass
            {
                static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
                public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
                public class _ViewNamesClass
                {
                    public readonly string _ExternalLoginsListPartial = "_ExternalLoginsListPartial";
                    public readonly string _ExternalLoginsListPartial_zh_tw = "_ExternalLoginsListPartial.zh-tw";
                    public readonly string ConfirmEmail = "ConfirmEmail";
                    public readonly string ConfirmEmail_zh_tw = "ConfirmEmail.zh-tw";
                    public readonly string ExternalLoginConfirmation = "ExternalLoginConfirmation";
                    public readonly string ExternalLoginConfirmation_zh_tw = "ExternalLoginConfirmation.zh-tw";
                    public readonly string ExternalLoginFailure = "ExternalLoginFailure";
                    public readonly string ExternalLoginFailure_zh_tw = "ExternalLoginFailure.zh-tw";
                    public readonly string ForgotPassword = "ForgotPassword";
                    public readonly string ForgotPassword_zh_tw = "ForgotPassword.zh-tw";
                    public readonly string ForgotPasswordConfirmation = "ForgotPasswordConfirmation";
                    public readonly string ForgotPasswordConfirmation_zh_tw = "ForgotPasswordConfirmation.zh-tw";
                    public readonly string Login = "Login";
                    public readonly string Login_zh_tw = "Login.zh-tw";
                    public readonly string Register = "Register";
                    public readonly string Register_zh_tw = "Register.zh-tw";
                    public readonly string ResetPassword = "ResetPassword";
                    public readonly string ResetPassword_zh_tw = "ResetPassword.zh-tw";
                    public readonly string ResetPasswordConfirmation = "ResetPasswordConfirmation";
                    public readonly string ResetPasswordConfirmation_zh_tw = "ResetPasswordConfirmation.zh-tw";
                    public readonly string SendCode = "SendCode";
                    public readonly string SendCode_zh_tw = "SendCode.zh-tw";
                    public readonly string VerifyCode = "VerifyCode";
                    public readonly string VerifyCode_zh_tw = "VerifyCode.zh-tw";
                }
                public readonly string _ExternalLoginsListPartial = "~/Views/Localization/Account/_ExternalLoginsListPartial.resx";
                public readonly string _ExternalLoginsListPartial_zh_tw = "~/Views/Localization/Account/_ExternalLoginsListPartial.zh-tw.resx";
                public readonly string ConfirmEmail = "~/Views/Localization/Account/ConfirmEmail.resx";
                public readonly string ConfirmEmail_zh_tw = "~/Views/Localization/Account/ConfirmEmail.zh-tw.resx";
                public readonly string ExternalLoginConfirmation = "~/Views/Localization/Account/ExternalLoginConfirmation.resx";
                public readonly string ExternalLoginConfirmation_zh_tw = "~/Views/Localization/Account/ExternalLoginConfirmation.zh-tw.resx";
                public readonly string ExternalLoginFailure = "~/Views/Localization/Account/ExternalLoginFailure.resx";
                public readonly string ExternalLoginFailure_zh_tw = "~/Views/Localization/Account/ExternalLoginFailure.zh-tw.resx";
                public readonly string ForgotPassword = "~/Views/Localization/Account/ForgotPassword.resx";
                public readonly string ForgotPassword_zh_tw = "~/Views/Localization/Account/ForgotPassword.zh-tw.resx";
                public readonly string ForgotPasswordConfirmation = "~/Views/Localization/Account/ForgotPasswordConfirmation.resx";
                public readonly string ForgotPasswordConfirmation_zh_tw = "~/Views/Localization/Account/ForgotPasswordConfirmation.zh-tw.resx";
                public readonly string Login = "~/Views/Localization/Account/Login.resx";
                public readonly string Login_zh_tw = "~/Views/Localization/Account/Login.zh-tw.resx";
                public readonly string Register = "~/Views/Localization/Account/Register.resx";
                public readonly string Register_zh_tw = "~/Views/Localization/Account/Register.zh-tw.resx";
                public readonly string ResetPassword = "~/Views/Localization/Account/ResetPassword.resx";
                public readonly string ResetPassword_zh_tw = "~/Views/Localization/Account/ResetPassword.zh-tw.resx";
                public readonly string ResetPasswordConfirmation = "~/Views/Localization/Account/ResetPasswordConfirmation.resx";
                public readonly string ResetPasswordConfirmation_zh_tw = "~/Views/Localization/Account/ResetPasswordConfirmation.zh-tw.resx";
                public readonly string SendCode = "~/Views/Localization/Account/SendCode.resx";
                public readonly string SendCode_zh_tw = "~/Views/Localization/Account/SendCode.zh-tw.resx";
                public readonly string VerifyCode = "~/Views/Localization/Account/VerifyCode.resx";
                public readonly string VerifyCode_zh_tw = "~/Views/Localization/Account/VerifyCode.zh-tw.resx";
            }
            static readonly _HomeClass s_Home = new _HomeClass();
            public _HomeClass Home { get { return s_Home; } }
            [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
            public partial class _HomeClass
            {
                static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
                public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
                public class _ViewNamesClass
                {
                }
            }
            static readonly _ManageClass s_Manage = new _ManageClass();
            public _ManageClass Manage { get { return s_Manage; } }
            [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
            public partial class _ManageClass
            {
                static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
                public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
                public class _ViewNamesClass
                {
                    public readonly string AddPhoneNumber = "AddPhoneNumber";
                    public readonly string AddPhoneNumber_zh_tw = "AddPhoneNumber.zh-tw";
                    public readonly string ChangePassword = "ChangePassword";
                    public readonly string ChangePassword_zh_tw = "ChangePassword.zh-tw";
                    public readonly string Index = "Index";
                    public readonly string Index_zh_tw = "Index.zh-tw";
                    public readonly string ManageLogins = "ManageLogins";
                    public readonly string ManageLogins_zh_tw = "ManageLogins.zh-tw";
                    public readonly string SetPassword = "SetPassword";
                    public readonly string SetPassword_zh_tw = "SetPassword.zh-tw";
                    public readonly string VerifyPhoneNumber = "VerifyPhoneNumber";
                    public readonly string VerifyPhoneNumber_zh_tw = "VerifyPhoneNumber.zh-tw";
                }
                public readonly string AddPhoneNumber = "~/Views/Localization/Manage/AddPhoneNumber.resx";
                public readonly string AddPhoneNumber_zh_tw = "~/Views/Localization/Manage/AddPhoneNumber.zh-tw.resx";
                public readonly string ChangePassword = "~/Views/Localization/Manage/ChangePassword.resx";
                public readonly string ChangePassword_zh_tw = "~/Views/Localization/Manage/ChangePassword.zh-tw.resx";
                public readonly string Index = "~/Views/Localization/Manage/Index.resx";
                public readonly string Index_zh_tw = "~/Views/Localization/Manage/Index.zh-tw.resx";
                public readonly string ManageLogins = "~/Views/Localization/Manage/ManageLogins.resx";
                public readonly string ManageLogins_zh_tw = "~/Views/Localization/Manage/ManageLogins.zh-tw.resx";
                public readonly string SetPassword = "~/Views/Localization/Manage/SetPassword.resx";
                public readonly string SetPassword_zh_tw = "~/Views/Localization/Manage/SetPassword.zh-tw.resx";
                public readonly string VerifyPhoneNumber = "~/Views/Localization/Manage/VerifyPhoneNumber.resx";
                public readonly string VerifyPhoneNumber_zh_tw = "~/Views/Localization/Manage/VerifyPhoneNumber.zh-tw.resx";
            }
            static readonly _SharedClass s_Shared = new _SharedClass();
            public _SharedClass Shared { get { return s_Shared; } }
            [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
            public partial class _SharedClass
            {
                static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
                public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
                public class _ViewNamesClass
                {
                    public readonly string LocalizedText = "LocalizedText";
                    public readonly string LocalizedText_zh_tw = "LocalizedText.zh-tw";
                }
                public readonly string LocalizedText = "~/Views/Localization/Shared/LocalizedText.resx";
                public readonly string LocalizedText_zh_tw = "~/Views/Localization/Shared/LocalizedText.zh-tw.resx";
            }
        }
    }

}

#endregion T4MVC
#pragma warning restore 1591, 3008, 3009, 0108, 0114
