# Summary #
This project arose as a need to replace a family member's Excel spreadsheet for tracking employee wage.

### Is there a working version hosted somewhere? ###
Yes. I have currently hosted the app on Azure https://raidowagejournal.azurewebsites.net


### What tech does it use? ###
* MVC5
* Entity Framework 6 - Code-First approach
* Autofac as the IoC
* Twitter Bootstrap as UI Layout Framework

### What does it do? ###
The main focus of this application is to capture employee work hours for a company and be able to generate simple weekly summary and payslips
from the hours and rates captured.

### Prerequisites ###
* VS2013 Update 4
* Web Essentials for VS2013
* IISExpress
* Sql Server Express

### How do I get set up? ###
* Install Prerequisites
* Pull Latest
* Build Solution
* This project uses Entity Framework 6 - Code First, therefore the database would automatically be deployed to SQLExpress on startup
* Register an account on the application, and you should be good to go

### Contribution guidelines ###
* Currently no contributions needed.

### Who do I talk to? ###
* Repo owner or admin