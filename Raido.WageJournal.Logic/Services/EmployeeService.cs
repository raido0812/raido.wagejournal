﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Raido.WageJournal.Contracts.ComplexTransferObjects;
using Raido.WageJournal.Contracts.SearchFilters;
using Raido.WageJournal.DataAccess.Repositories.Interfaces;
using Raido.WageJournal.Logic.Services.Interfaces;

namespace Raido.WageJournal.Logic.Services
{
    public class EmployeeService : IEmployeeService
    {
        #region Fields
        private readonly ICompanyRepository _companyRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IWorkRepository _workRepository;
        private readonly IAdjustmentRepository _adjustmentRepository;
        private readonly IPolicyRepository _policyRepository;
        #endregion Fields

        #region Constructors
        public EmployeeService(
            ICompanyRepository companyRepository,
            IEmployeeRepository employeeRepository,
            IWorkRepository workRepository,
            IAdjustmentRepository adjustmentRepository,
            IPolicyRepository policyRepository)
        {
            this._companyRepository = companyRepository;
            this._employeeRepository = employeeRepository;
            this._workRepository = workRepository;
            this._adjustmentRepository = adjustmentRepository;
            this._policyRepository = policyRepository;
        }
        #endregion Constructors

        #region Methods

        public async Task<CompanyAndEmployeeWork> GetAllEmployeeWorkForDayAsync(Guid companyIdentifier, DateTime workDay)
        {
            CompanyAndEmployeeWork companyAndEmployeeWork = new CompanyAndEmployeeWork();

            var existingWork = await this._workRepository.GetWorkAsync(new WorkSearchOptions { PeriodStart = workDay, PeriodEnd = workDay }, companyIdentifier);
            var existingAdjustments = await this._adjustmentRepository.GetAdjustmentsAsync(new AdjustmentSearchOptions { FromDate = workDay, ToDate = workDay }, companyIdentifier);
            var allPolicies = await this._policyRepository.GetPoliciesAsync(null, companyIdentifier);
            var employees = await this._employeeRepository.GetEmployeesAsync(null, companyIdentifier);
            companyAndEmployeeWork.EmployeeWork = (from employee in employees
                                                   let work = (from w in existingWork where w.EmployeeId == employee.EmployeeId select w).FirstOrDefault()
                                                   let adjustment = (from a in existingAdjustments where a.EmployeeIdentifier == employee.Identifier select a).FirstOrDefault()
                                                   select new EmployeeWorkDay()
                                                   {
                                                       Employee = employee,
                                                       Work = work,
                                                       Adjustment = adjustment,
                                                       Day = workDay
                                                   }).ToList();
            companyAndEmployeeWork.CompanyIdentifier = companyIdentifier;

            companyAndEmployeeWork.SaturdayOvertimeMultiplier = allPolicies.FirstOrDefault(p => p.PolicyType == Contracts.Enumerations.PolicyType.SaturdayOvertimeMultiplier) ??
                new Contracts.DataTransferObjects.Policy()
                {
                    IsActive = true,
                    Identifier = Guid.NewGuid(),
                    PolicyType = Contracts.Enumerations.PolicyType.SaturdayOvertimeMultiplier,
                    Value = 1
                };
            companyAndEmployeeWork.SundayOvertimeMultiplier = allPolicies.FirstOrDefault(p => p.PolicyType == Contracts.Enumerations.PolicyType.SundayOvertimeMultiplier) ??
                new Contracts.DataTransferObjects.Policy()
                {
                    IsActive = true,
                    Identifier = Guid.NewGuid(),
                    PolicyType = Contracts.Enumerations.PolicyType.SundayOvertimeMultiplier,
                    Value = 1
                };

            return companyAndEmployeeWork;
        }

        public async Task<CompanyAndEmployees> GetAllEmployeesAsync(Guid companyIdentifier)
        {
            CompanyAndEmployees companyAndEmployees = new CompanyAndEmployees();
            companyAndEmployees.Employees = await this._employeeRepository.GetEmployeesAsync(null, companyIdentifier);
            companyAndEmployees.Company = await this._companyRepository.GetCompanyForCurrentUserAsync(companyIdentifier);

            return companyAndEmployees;
        }

        public async Task<EmployeeWorkDay> GetEmployeeWorkForDayAsync(Guid companyIdentifier, Guid employeeIdentifier, DateTime workDay)
        {
            var allPolicies = await this._policyRepository.GetPoliciesAsync(null, companyIdentifier);
            EmployeeWorkDay employeeWorkDay = new EmployeeWorkDay();
            employeeWorkDay.Day = workDay;
            employeeWorkDay.Employee = await this._employeeRepository.GetEmployeeAsync(employeeIdentifier, companyIdentifier);
            employeeWorkDay.Adjustment = (await this._adjustmentRepository.GetAdjustmentsAsync(new AdjustmentSearchOptions { EmployeeIdentifier = employeeIdentifier, FromDate = workDay, ToDate = workDay }, companyIdentifier)).FirstOrDefault();
            employeeWorkDay.Work = (await this._workRepository.GetWorkAsync(new WorkSearchOptions { EmployeeIdentifier = employeeIdentifier, PeriodStart = workDay, PeriodEnd = workDay }, companyIdentifier)).FirstOrDefault();

            employeeWorkDay.SaturdayOvertimeMultiplier = allPolicies.FirstOrDefault(p => p.PolicyType == Contracts.Enumerations.PolicyType.SaturdayOvertimeMultiplier) ??
                new Contracts.DataTransferObjects.Policy()
                {
                    IsActive = true,
                    Identifier = Guid.NewGuid(),
                    PolicyType = Contracts.Enumerations.PolicyType.SaturdayOvertimeMultiplier,
                    Value = 1
                };
            employeeWorkDay.SundayOvertimeMultiplier = allPolicies.FirstOrDefault(p => p.PolicyType == Contracts.Enumerations.PolicyType.SundayOvertimeMultiplier) ??
                new Contracts.DataTransferObjects.Policy()
                {
                    IsActive = true,
                    Identifier = Guid.NewGuid(),
                    PolicyType = Contracts.Enumerations.PolicyType.SundayOvertimeMultiplier,
                    Value = 1
                };
            return employeeWorkDay;
        }

        #endregion Methods
    }
}
