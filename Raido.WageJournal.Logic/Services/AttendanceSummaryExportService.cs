﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using Raido.WageJournal.Contracts.SearchFilters;
using Raido.WageJournal.DataAccess.Repositories.Interfaces;
using Raido.WageJournal.Extensions;
using Raido.WageJournal.Logic.Extensions;
using Raido.WageJournal.Logic.Localization;
using Raido.WageJournal.Logic.Services.Interfaces;
using Raido.WageJournal.Contracts.DataTransferObjects;

namespace Raido.WageJournal.Logic.Services
{
    internal class AttendanceSummaryExportService : IAttendanceSummaryExportService
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly IWorkRepository _workRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IPolicyRepository _policyRepository;
        public AttendanceSummaryExportService(
            ICompanyRepository companyRepository,
            IWorkRepository workRepository,
            IEmployeeRepository employeeRepository,
            IPolicyRepository policyRepository)
        {
            this._companyRepository = companyRepository;
            this._workRepository = workRepository;
            this._employeeRepository = employeeRepository;
            this._policyRepository = policyRepository;
        }

        public async Task<byte[]> GenerateAttendanceSummaryPdfAsync(Guid companyIdentifier, DateTime startDate, DateTime endDate, Uri fontUri, string fontFamily, string fontName)
        {
            byte[] result = null;
            using (MemoryStream inMemoryFile = new MemoryStream())
            {
                int temp = 0;
                Company company = await this._companyRepository.GetCompanyForCurrentUserAsync(companyIdentifier);
                var allCompanyPolicy = await this._policyRepository.GetPoliciesAsync(null, companyIdentifier);
                var allWork = await this._workRepository.GetWorkAsync(new WorkSearchOptions { PeriodStart = startDate, PeriodEnd = endDate }, companyIdentifier);
                var allEmployee = (await this._employeeRepository.GetEmployeesAsync(null, companyIdentifier))
                    .Where(e =>
                        (!e.EmploymentStartDate.HasValue || e.EmploymentStartDate.Value <= endDate) &&
                        (!e.EmploymentEndDate.HasValue || e.EmploymentEndDate.Value >= startDate))
                    .OrderBy(e =>
                        int.TryParse(e.EmployeeNumber, out temp) ? int.Parse(e.EmployeeNumber) : 0)
                    .ThenBy(e => e.Name).ToList();
                decimal periodPercentOfYear = (int)Math.Round(endDate.Subtract(startDate).TotalDays / new DateTime(DateTime.Today.Year,12,1).Subtract(new DateTime(DateTime.Today.Year,1,1)).TotalDays,0, MidpointRounding.AwayFromZero);
                var daysCount = startDate.DaysCountUntil(endDate);
                
                int totalWeekdays = daysCount[DayOfWeek.Monday] + daysCount[DayOfWeek.Tuesday] + daysCount[DayOfWeek.Wednesday] + daysCount[DayOfWeek.Thursday] + daysCount[DayOfWeek.Friday];

                // Create a new PDF document
                Document document = new Document();
                document.Info.Title = LocalizedText.AttendanceSummaryPdfInfoTitle;

                PdfDocumentRenderer renderer = new PdfDocumentRenderer(true, PdfSharp.Pdf.PdfFontEmbedding.Always);
                renderer.Document = document;

                // setup up styles such as normal, heading1 etc.
                document.DefineStyles(fontUri, fontFamily, fontName);

                Unit oneCentimetre = new Unit(1, UnitType.Centimeter);

                // create a section on the document to hold content
                // and set the page setup for the section
                Section documentSection = document.AddSection();
                documentSection.PageSetup.Orientation = Orientation.Portrait;

                documentSection.PageSetup.TopMargin = oneCentimetre;
                documentSection.PageSetup.RightMargin = oneCentimetre;
                documentSection.PageSetup.BottomMargin = oneCentimetre;
                documentSection.PageSetup.LeftMargin = oneCentimetre;

                // Add report Title
                Paragraph reportTitleParagraph = documentSection.AddParagraph();
                reportTitleParagraph.Format.Alignment = ParagraphAlignment.Center;
                reportTitleParagraph.Style = "Heading1";
                reportTitleParagraph.AddText(string.Format(LocalizedText.AttendanceSummaryPdfReportTitle, company.FullName));

                // Add the filter criterias onto the report
                TextFrame reportParameterFrame = documentSection.AddTextFrame();
                reportParameterFrame.Left = ShapePosition.Left;
                reportParameterFrame.Top = new Unit(1, UnitType.Centimeter);
                reportParameterFrame.RelativeHorizontal = RelativeHorizontal.Margin;
                reportParameterFrame.RelativeVertical = RelativeVertical.Margin;
                reportParameterFrame.Height = new Unit(2, UnitType.Centimeter);
                reportParameterFrame.Width = new Unit(7, UnitType.Centimeter);
                Paragraph fromDateFilter = reportParameterFrame.AddParagraph(string.Format(LocalizedText.FromDateFilterLabel, startDate.ToShortDateString()));
                Paragraph toDateFilter = reportParameterFrame.AddParagraph(string.Format(LocalizedText.ToDateFilterLabel, endDate.ToShortDateString()));
                Paragraph weekdayFilter = reportParameterFrame.AddParagraph(string.Format(LocalizedText.TotalWeekdaysFilterLabel, totalWeekdays.ToString("F0")));

                //use an empty paragraph to space the table correctly
                Paragraph spacerParagraph = documentSection.AddParagraph();
                spacerParagraph.Format.SpaceBefore = new Unit(2, UnitType.Centimeter);

                // Add a table to hold the summary results
                Table employeeWageTable = documentSection.AddTable();
                employeeWageTable.Format.Font.Name = fontName;
                employeeWageTable.Borders.Color = Colors.DarkGray;
                employeeWageTable.Borders.Width = new Unit(0.1, UnitType.Millimeter);
                employeeWageTable.Rows.LeftIndent = 0;
                employeeWageTable.Rows.Height = new Unit(0.5, UnitType.Centimeter);
                employeeWageTable.Rows.VerticalAlignment = VerticalAlignment.Center;
                employeeWageTable.Format.Font.Size = new Unit(10, UnitType.Point);

                // Before you can add a row, you must define the columns
                Policy attendanceBonusPolicy = allCompanyPolicy.Where(p => p.PolicyType == Contracts.Enumerations.PolicyType.AttendanceBonusAmount).FirstOrDefault();
                SetupAttendanceTableHeadings(employeeWageTable);
                RenderEmployeeAttendanceDetailsForPeriod(
                    company,
                    allWork,
                    allEmployee,
                    periodPercentOfYear * (attendanceBonusPolicy == null ? 0 : attendanceBonusPolicy.Value),
                    employeeWageTable,
                    totalWeekdays);

                // Save the document...
                renderer.RenderDocument();
                renderer.PdfDocument.Save(inMemoryFile);
                result = inMemoryFile.ToArray();
            }

            return result;
        }

        private static void SetupAttendanceTableHeadings(Table employeeWageTable)
        {
            employeeWageTable.AddColumn(new Unit(1.21, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            employeeWageTable.AddColumn(new Unit(3.3, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            employeeWageTable.AddColumn(new Unit(3.3, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            employeeWageTable.AddColumn(new Unit(3.3, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            employeeWageTable.AddColumn(new Unit(3.3, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            employeeWageTable.AddColumn(new Unit(3.3, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;

            // Create the header of the table
            Row row = employeeWageTable.AddRow();
            row.HeadingFormat = true;

            row.Cells[0].AddParagraph("#");
            row.Cells[1].AddParagraph(LocalizedText.AttendanceSummaryNameTableHeading);
            row.Cells[2].AddParagraph(LocalizedText.AttendanceSummaryWeekDaysWorkedTableHeading);
            row.Cells[3].AddParagraph(LocalizedText.AttendanceSummaryPercentageWorkedWeekDaysTableHeading);
            row.Cells[4].AddParagraph(LocalizedText.AttendanceSummaryProRataBonusTableHeading);
            row.Cells[5].AddParagraph(LocalizedText.AttendanceSummarySignatureTableHeading);
        }

        private static void RenderEmployeeAttendanceDetailsForPeriod(
            Company company,
            IList<Work> allWork,
            IList<Employee> allEmployee,
            decimal attendanceBonusAmount,
            Table employeeWageTable,
            int totalWeekdays)
        {
            // Render each of employee on wage table
            for (int i = 0; i < allEmployee.Count; i++)
            {
                Row row = employeeWageTable.AddRow();
                row.HeadingFormat = true;

                decimal weekdaysWorked = (from work in allWork
                                          where
                                              work.EmployeeIdentifier == allEmployee[i].Identifier &&
                                              (work.HoursWorked ?? 0) > 0 &&
                                              (
                                                  work.DateWorked.DayOfWeek == DayOfWeek.Monday ||
                                                  work.DateWorked.DayOfWeek == DayOfWeek.Tuesday ||
                                                  work.DateWorked.DayOfWeek == DayOfWeek.Wednesday ||
                                                  work.DateWorked.DayOfWeek == DayOfWeek.Thursday ||
                                                  work.DateWorked.DayOfWeek == DayOfWeek.Friday
                                              )
                                          select 1).Count();

                decimal percentageWeekdaysWorked = totalWeekdays != 0 ? weekdaysWorked / totalWeekdays : 0.0m;

                row.Cells[0].AddParagraph(allEmployee[i].EmployeeNumber);
                row.Cells[1].AddParagraph(allEmployee[i].Name);
                row.Cells[2].AddParagraph(weekdaysWorked.ToString("F0")).Format.Alignment = ParagraphAlignment.Right;
                row.Cells[3].AddParagraph(percentageWeekdaysWorked.ToString("P0")).Format.Alignment = ParagraphAlignment.Right;
                row.Cells[4].AddParagraph((percentageWeekdaysWorked * attendanceBonusAmount).ToString("F2")).Format.Alignment = ParagraphAlignment.Right;
                row.Cells[5].AddParagraph(string.Empty);
            }
        }
    }
}
