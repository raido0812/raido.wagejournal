﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Raido.WageJournal.Contracts.ComplexTransferObjects;
using Raido.WageJournal.Contracts.DataTransferObjects;
using Raido.WageJournal.Logic.Services.Interfaces;

namespace Raido.WageJournal.Logic.Services
{
    public class TimeSheetImporter : ITimeSheetImporter
    {
        private XmlNamespaceManager xmlNamespaceManager;
        public TimeSheetImporter()
        {
            xmlNamespaceManager = new XmlNamespaceManager(new NameTable());
            xmlNamespaceManager.AddNamespace("ss", "urn:schemas-microsoft-com:office:spreadsheet");
        }

        public async Task<IEnumerable<TimesheetRecord>> ExtractTimesheetRecords(Stream input)
        {
            var result = new List<TimesheetRecord>();
            var raw = string.Empty;
            using (var streamReader = new StreamReader(input, Encoding.GetEncoding("GB2312")))
            {
                raw = await streamReader.ReadToEndAsync();
            }

            if (!raw.StartsWith("<?xml"))
            {
                return result;
            }

            var xmlDocument = XDocument.Parse(raw);

            var navigator = xmlDocument.CreateNavigator();
            var firstTableInSheet1 = XPathExpression.Compile("/ss:Workbook/ss:Worksheet[@ss:Name = 'Sheet1']/ss:Table/ss:Row", xmlNamespaceManager);
            var rows = navigator.Select(firstTableInSheet1);

            TimesheetMetaInfo employeeMetaInfo = null;
            IList<TimesheetLogEntry> employeeLogEntries = null;
             
            // extra data out of rows
            foreach (XPathNavigator row in rows)
            {
                var cells = row.SelectChildren(XPathNodeType.Element);
                var cellEnumerator = cells.GetEnumerator();
                while (cellEnumerator.MoveNext())
                {
                    // extra data and interpret content
                    var data = ((XPathNavigator)cellEnumerator.Current)
                                    .SelectSingleNode("ss:Data", xmlNamespaceManager)
                                    ?.InnerXml
                                    ?.Trim();

                    // skip cells without data
                    if (string.IsNullOrWhiteSpace(data))
                    {
                        continue;
                    }

                    if (data.StartsWith("工号：", StringComparison.InvariantCultureIgnoreCase))
                    {
                        employeeMetaInfo = new TimesheetMetaInfo();
                        employeeMetaInfo.EmployeeNumber = data.Split('：').LastOrDefault();
                        employeeLogEntries = new List<TimesheetLogEntry>();
                    }

                    DateTime tempDateTime;
                    if (data.StartsWith("日期：", StringComparison.InvariantCultureIgnoreCase))
                    {
                        var dateRangeText = data.Split('：')?.LastOrDefault().Split('～');
                        if (DateTime.TryParseExact(
                            dateRangeText.FirstOrDefault(), 
                            "yy.MM.dd", 
                            CultureInfo.GetCultureInfo("zh-cn"), 
                            DateTimeStyles.AssumeUniversal, 
                            out tempDateTime))
                        {
                            employeeMetaInfo.DateRangeStart = tempDateTime;
                        }
                        if (DateTime.TryParseExact(
                            dateRangeText.LastOrDefault(),
                            "yy.MM.dd",
                            CultureInfo.GetCultureInfo("zh-cn"),
                            DateTimeStyles.AssumeUniversal,
                            out tempDateTime))
                        {
                            employeeMetaInfo.DateRangeEnd = tempDateTime;
                        }

                        employeeLogEntries = new List<TimesheetLogEntry>();
                    }

                    // date formatted as MM.dd
                    if (data.Length == 5 &&
                        data[2] == '.')
                    {
                        var entry = new TimesheetLogEntry();
                        var enrichedDateText = (employeeMetaInfo.DateRangeStart ?? employeeMetaInfo.DateRangeEnd ?? DateTime.UtcNow).ToString("yy") + $".{data}";
                        if (DateTime.TryParseExact(
                            enrichedDateText,
                            "yy.MM.dd",
                            CultureInfo.GetCultureInfo("zh-cn"),
                            DateTimeStyles.AssumeUniversal,
                            out tempDateTime))
                        {
                            entry.Date = tempDateTime;
                        }

                        TimeSpan tempTimeSpan;
                        
                        // format is date, day of week then 3 pairs of start and end time

                        // try to get the start and end time from next cells
                        if (cellEnumerator.MoveNext())
                        {
                            var startTimesForDate = new List<TimeSpan>();
                            var endTimesForDate = new List<TimeSpan>();

                            for (var i = 0; i < 6; i++)
                            {
                                if (cellEnumerator.MoveNext())
                                {
                                    var timeText = ((XPathNavigator)cellEnumerator.Current)
                                            .SelectSingleNode("ss:Data", xmlNamespaceManager)
                                            ?.InnerXml
                                            ?.Trim();
                                    if (TimeSpan.TryParse(timeText, out tempTimeSpan))
                                    {
                                        if (i % 2 == 0)
                                        {
                                            startTimesForDate.Add(tempTimeSpan);
                                        }
                                        else
                                        {
                                            endTimesForDate.Add(tempTimeSpan);
                                        }
                                    }
                                }
                            }

                            if (startTimesForDate.Any())
                            {
                                entry.StartTime = startTimesForDate.Min();
                            }

                            if (endTimesForDate.Any())
                            {
                                entry.EndTime = endTimesForDate.Max();
                            }

                            if (entry.Date.HasValue &&
                                (entry.StartTime.HasValue || entry.EndTime.HasValue))
                            {
                                employeeLogEntries.Add(entry);
                            }
                        }
                    }

                    if (data.StartsWith("员工签字", StringComparison.InvariantCultureIgnoreCase))
                    {
                        // end of current employee,
                        result.Add(new TimesheetRecord
                        {
                            TimesheetMetaInfo = employeeMetaInfo,
                            TimesheetLogEntries = employeeLogEntries.OrderBy(e => e.Date)
                        });
                    }
                }
            }

            return result;
        }
    }
}
