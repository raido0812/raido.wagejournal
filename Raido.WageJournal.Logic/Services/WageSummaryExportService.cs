﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using Raido.WageJournal.Contracts.SearchFilters;
using Raido.WageJournal.DataAccess.Repositories.Interfaces;
using Raido.WageJournal.Logic.Extensions;
using Raido.WageJournal.Logic.Localization;
using Raido.WageJournal.Logic.Services.Interfaces;
using Raido.WageJournal.Contracts.DataTransferObjects;
namespace Raido.WageJournal.Logic.Services
{
    internal class WageSummaryExportService : IWageSummaryExportService
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly IWorkRepository _workRepository;
        private readonly IAdjustmentRepository _adjustmentRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IPolicyRepository _policyRepository;
        private readonly IDeductionRepository _deductionRepository;

        public WageSummaryExportService(
            ICompanyRepository companyRepository,
            IWorkRepository workRepository,
            IAdjustmentRepository adjustmentRepository,
            IEmployeeRepository employeeRepository,
            IPolicyRepository policyRepository,
            IDeductionRepository deductionRepository)
        {
            this._companyRepository = companyRepository;
            this._workRepository = workRepository;
            this._adjustmentRepository = adjustmentRepository;
            this._employeeRepository = employeeRepository;
            this._policyRepository = policyRepository;
            this._deductionRepository = deductionRepository;
        }

        public async Task<byte[]> GenerateWageSummaryPdfAsync(Guid companyIdentifier, DateTime startDate, DateTime endDate, Uri fontUri, string fontFamily, string fontName)
        {
            byte[] result = null;
            using (MemoryStream inMemoryFile = new MemoryStream())
            {
                int temp = 0;
                Company company = await this._companyRepository.GetCompanyForCurrentUserAsync(companyIdentifier);
                var allWork = await this._workRepository.GetWorkAsync(new WorkSearchOptions { PeriodStart = startDate, PeriodEnd = endDate }, companyIdentifier);
                var allEmployee = (await this._employeeRepository.GetEmployeesAsync(null, companyIdentifier))
                    .Where(e =>
                        (!e.EmploymentStartDate.HasValue || e.EmploymentStartDate.Value <= endDate) &&
                        (!e.EmploymentEndDate.HasValue || e.EmploymentEndDate.Value >= startDate))
                    .OrderBy(e =>
                        int.TryParse(e.EmployeeNumber, out temp) ? int.Parse(e.EmployeeNumber) : 0)
                    .ThenBy(e => e.Name).ToList();

                var allAdjustment = await this._adjustmentRepository.GetAdjustmentsAsync(new AdjustmentSearchOptions { FromDate = startDate, ToDate = endDate }, companyIdentifier);
                var allCompanyPolicy = await this._policyRepository.GetPoliciesAsync(null, companyIdentifier);
                var allDeduction = await this._deductionRepository.GetDeductionsAsync(null, companyIdentifier);

                // Create a new PDF document
                Document document = new Document();
                document.Info.Title = LocalizedText.WageSummaryPdfInfoTitle;

                PdfDocumentRenderer renderer = new PdfDocumentRenderer(true, PdfSharp.Pdf.PdfFontEmbedding.Always);
                renderer.Document = document;

                // setup up styles such as normal, heading1 etc.
                document.DefineStyles(fontUri, fontFamily, fontName);

                Unit oneCentimetre = new Unit(1, UnitType.Centimeter);

                // create a section on the document to hold content
                // and set the page setup for the section
                Section documentSection = document.AddSection();
                documentSection.PageSetup.Orientation = Orientation.Landscape;

                documentSection.PageSetup.TopMargin = oneCentimetre;
                documentSection.PageSetup.RightMargin = oneCentimetre;
                documentSection.PageSetup.BottomMargin = oneCentimetre;
                documentSection.PageSetup.LeftMargin = oneCentimetre;

                // Add report Title
                Paragraph reportTitleParagraph = documentSection.AddParagraph();
                reportTitleParagraph.Format.Alignment = ParagraphAlignment.Center;
                reportTitleParagraph.Style = "Heading1";
                reportTitleParagraph.AddText(string.Format(LocalizedText.WageSummaryPdfReportTitle, company.FullName));

                // Add the filter criterias onto the report
                TextFrame reportParameterFrame = documentSection.AddTextFrame();
                reportParameterFrame.Left = ShapePosition.Left;
                reportParameterFrame.Top = new Unit(1, UnitType.Centimeter);
                reportParameterFrame.RelativeHorizontal = RelativeHorizontal.Margin;
                reportParameterFrame.RelativeVertical = RelativeVertical.Margin;
                reportParameterFrame.Height = new Unit(2, UnitType.Centimeter);
                reportParameterFrame.Width = new Unit(7, UnitType.Centimeter);
                Paragraph fromDateFilter = reportParameterFrame.AddParagraph(string.Format(LocalizedText.FromDateFilterLabel, startDate.ToShortDateString()));
                Paragraph toDateFilter = reportParameterFrame.AddParagraph(string.Format(LocalizedText.ToDateFilterLabel, endDate.ToShortDateString()));

                //use an empty paragraph to space the table correctly
                Paragraph spacerParagraph = documentSection.AddParagraph();
                spacerParagraph.Format.SpaceBefore = new Unit(1.5, UnitType.Centimeter);

                // Add a table to hold the summary results
                Table employeeWageTable = documentSection.AddTable();
                employeeWageTable.Format.Font.Name = fontName;
                employeeWageTable.Borders.Color = Colors.DarkGray;
                employeeWageTable.Borders.Width = new Unit(0.1, UnitType.Millimeter);
                employeeWageTable.Rows.LeftIndent = 0;
                employeeWageTable.Rows.Height = new Unit(0.5, UnitType.Centimeter);
                employeeWageTable.Rows.VerticalAlignment = VerticalAlignment.Center;
                employeeWageTable.Format.Font.Size = new Unit(10, UnitType.Point);

                // Before you can add a row, you must define the columns
                SetupWageTableHeadings(employeeWageTable);

                decimal totalGrossWage = 0;
                decimal totalDeductions = 0;
                decimal totalUif = 0;
                decimal totalAdjustment = 0;
                decimal totalWage = 0;
                RenderEmployeeWageDetailsForPeriod(
                    allWork,
                    allEmployee,
                    allAdjustment,
                    allDeduction,
                    allCompanyPolicy,
                    employeeWageTable,
                    ref totalGrossWage,
                    ref totalDeductions,
                    ref totalUif,
                    ref totalAdjustment,
                    ref totalWage);

                RenderTotalWageTable(documentSection, totalGrossWage, totalDeductions, totalUif, totalAdjustment, totalWage);

                // Save the document...
                renderer.RenderDocument();
                renderer.PdfDocument.Save(inMemoryFile);
                result = inMemoryFile.ToArray();
            }

            return result;
        }

        private static void RenderEmployeeWageDetailsForPeriod(
            IList<Work> allWork,
            IList<Employee> allEmployee,
            IList<Adjustment> allAdjustment,
            IList<Deduction> allDeduction,
            IList<Policy> allCompanyPolicy,
            Table employeeWageTable,
            ref decimal totalGrossWage,
            ref decimal totalDeductions,
            ref decimal totalUif,
            ref decimal totalAdjustment,
            ref decimal totalWage)
        {
            decimal saturdayMultiplier = (from policy in allCompanyPolicy
                                          where
                                              policy.PolicyType == Contracts.Enumerations.PolicyType.SaturdayOvertimeMultiplier
                                          select
                                              policy.Value).FirstOrDefault();
            decimal sundayMultiplier = (from policy in allCompanyPolicy
                                        where
                                            policy.PolicyType == Contracts.Enumerations.PolicyType.SundayOvertimeMultiplier
                                        select
                                            policy.Value).FirstOrDefault();
            decimal uifMultiplier = (from policy in allCompanyPolicy
                                     where
                                         policy.PolicyType == Contracts.Enumerations.PolicyType.UnemploymentInsuranceFundPercentage
                                     select
                                         policy.Value).FirstOrDefault() / 100;

            // Render each of employee on wage table
            for (int i = 0; i < allEmployee.Count; i++)
            {
                Row row = employeeWageTable.AddRow();
                row.HeadingFormat = true;
                var hourlyRate = (allEmployee[i].HourlyRate ?? 0);
                var workdayHours = allWork.Where(w =>
                    w.EmployeeIdentifier == allEmployee[i].Identifier &&
                    w.DateWorked.DayOfWeek != DayOfWeek.Saturday &&
                    w.DateWorked.DayOfWeek != DayOfWeek.Sunday).Sum(w => w.HoursWorked ?? 0);
                var saturdayHours = allWork.Where(w =>
                    w.EmployeeIdentifier == allEmployee[i].Identifier &&
                    w.DateWorked.DayOfWeek == DayOfWeek.Saturday).Sum(w => w.HoursWorked ?? 0);
                var sundayHours = allWork.Where(w =>
                    w.EmployeeIdentifier == allEmployee[i].Identifier &&
                    w.DateWorked.DayOfWeek == DayOfWeek.Sunday).Sum(w => w.HoursWorked ?? 0);
                var deductions = allDeduction.Where(d =>
                    d.EmployeeIdentifier == allEmployee[i].Identifier).Sum(d => d.Amount);

                var adjustmentForPeriod = allAdjustment.Where(a => a.EmployeeIdentifier == allEmployee[i].Identifier).Sum(a => a.Amount ?? 0);

                var grossWage = (workdayHours * hourlyRate) +
                    (saturdayHours * hourlyRate * saturdayMultiplier) +
                    (sundayHours * hourlyRate * sundayMultiplier) +
                    adjustmentForPeriod -
                    deductions;

                totalGrossWage += grossWage;
                totalDeductions += deductions;
                totalUif += grossWage * uifMultiplier;
                totalAdjustment += adjustmentForPeriod;
                totalWage += grossWage - (grossWage * uifMultiplier);

                row.Cells[0].AddParagraph(allEmployee[i].EmployeeNumber ?? string.Empty);
                row.Cells[1].AddParagraph(allEmployee[i].Name ?? string.Empty);
                row.Cells[2].AddParagraph(workdayHours.ToString("F2")).Format.Alignment = ParagraphAlignment.Right;
                row.Cells[3].AddParagraph(hourlyRate.ToString("F2")).Format.Alignment = ParagraphAlignment.Right;
                row.Cells[4].AddParagraph(saturdayHours.ToString("F2")).Format.Alignment = ParagraphAlignment.Right;
                row.Cells[5].AddParagraph((hourlyRate * saturdayMultiplier).ToString("F2")).Format.Alignment = ParagraphAlignment.Right;
                row.Cells[6].AddParagraph(sundayHours.ToString("F2")).Format.Alignment = ParagraphAlignment.Right;
                row.Cells[7].AddParagraph((hourlyRate * sundayMultiplier).ToString("F2")).Format.Alignment = ParagraphAlignment.Right;
                row.Cells[8].AddParagraph(deductions.ToString("F2")).Format.Alignment = ParagraphAlignment.Right;
                row.Cells[9].AddParagraph(adjustmentForPeriod.ToString("F2")).Format.Alignment = ParagraphAlignment.Right;
                row.Cells[10].AddParagraph(grossWage.ToString("F2")).Format.Alignment = ParagraphAlignment.Right;
                row.Cells[11].AddParagraph((grossWage * uifMultiplier).ToString("F2")).Format.Alignment = ParagraphAlignment.Right;
                row.Cells[12].AddParagraph((grossWage - (grossWage * uifMultiplier)).ToString("F2")).Format.Alignment = ParagraphAlignment.Right;
                row.Cells[13].AddParagraph(string.Empty);
            }
        }

        private static void SetupWageTableHeadings(Table employeeWageTable)
        {
            employeeWageTable.AddColumn(new Unit(1.21, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            employeeWageTable.AddColumn(new Unit(2, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            employeeWageTable.AddColumn(new Unit(2, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            employeeWageTable.AddColumn(new Unit(2, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            employeeWageTable.AddColumn(new Unit(2, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            employeeWageTable.AddColumn(new Unit(2, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            employeeWageTable.AddColumn(new Unit(2, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            employeeWageTable.AddColumn(new Unit(2, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            employeeWageTable.AddColumn(new Unit(2, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            employeeWageTable.AddColumn(new Unit(2, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            employeeWageTable.AddColumn(new Unit(2, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            employeeWageTable.AddColumn(new Unit(2, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            employeeWageTable.AddColumn(new Unit(2.2, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            employeeWageTable.AddColumn(new Unit(2.2, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;

            // Create the header of the table
            Row row = employeeWageTable.AddRow();
            row.HeadingFormat = true;

            row.Cells[0].AddParagraph("#");
            row.Cells[1].AddParagraph(LocalizedText.WageSummaryNameTableHeading);
            row.Cells[2].AddParagraph(LocalizedText.WageSummaryHoursWorkedTableHeading);
            row.Cells[3].AddParagraph(LocalizedText.WageSummaryHourRateTableHeading);
            row.Cells[4].AddParagraph(LocalizedText.WageSummarySaturdayWorkedTableHeading);
            row.Cells[5].AddParagraph(LocalizedText.WageSummarySaturdayRateTableHeading);
            row.Cells[6].AddParagraph(LocalizedText.WageSummarySundayWorkedTableHeading);
            row.Cells[7].AddParagraph(LocalizedText.WageSummarySundayRateTableHeading);
            row.Cells[8].AddParagraph(LocalizedText.WageSummaryDeductionsTableHeading);
            row.Cells[9].AddParagraph(LocalizedText.WageSummaryAdjustmentTableHeading);
            row.Cells[10].AddParagraph(LocalizedText.WageSummaryGrossWageTableHeading);
            row.Cells[11].AddParagraph(LocalizedText.WageSummaryUIFTableHeading);
            row.Cells[12].AddParagraph(LocalizedText.WageSummaryTotalWageTableHeading);
            row.Cells[13].AddParagraph(LocalizedText.WageSummarySignatureTableHeading);
        }

        private static void RenderTotalWageTable(Section documentSection, decimal totalGrossWage, decimal totalDeductions, decimal totalUif, decimal totalAdjustment, decimal totalWage)
        {
            // render totals
            TextFrame totalWageFrame = documentSection.AddTextFrame();
            totalWageFrame.Left = ShapePosition.Right;
            totalWageFrame.Top = new Unit(1, UnitType.Centimeter);
            totalWageFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            totalWageFrame.RelativeVertical = RelativeVertical.Margin;
            totalWageFrame.Height = new Unit(2, UnitType.Centimeter);
            totalWageFrame.Width = new Unit(14.5, UnitType.Centimeter);

            var totalWageTable = totalWageFrame.AddTable();
            totalWageTable.Borders.Color = Colors.DarkGray;
            totalWageTable.Borders.Width = new Unit(0.1, UnitType.Millimeter);
            totalWageTable.Rows.LeftIndent = 0;
            totalWageTable.Rows.Height = new Unit(0.75, UnitType.Centimeter);
            totalWageTable.Rows.VerticalAlignment = VerticalAlignment.Center;
            totalWageTable.Format.Font.Size = new Unit(10, UnitType.Point);
            totalWageTable.AddColumn(new Unit(2.0, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            totalWageTable.AddColumn(new Unit(2.0, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            totalWageTable.AddColumn(new Unit(2.0, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            totalWageTable.AddColumn(new Unit(2.0, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            totalWageTable.AddColumn(new Unit(2.0, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;
            totalWageTable.AddColumn(new Unit(4.4, UnitType.Centimeter)).Format.Alignment = ParagraphAlignment.Center;

            Row heading = totalWageTable.AddRow();
            heading.Cells[0].AddParagraph(string.Empty);
            heading.Cells[1].AddParagraph(LocalizedText.WageSummaryDeductionsTableHeading);
            heading.Cells[2].AddParagraph(LocalizedText.WageSummaryAdjustmentTableHeading);
            heading.Cells[3].AddParagraph(LocalizedText.WageSummaryGrossWageTableHeading);
            heading.Cells[4].AddParagraph(LocalizedText.WageSummaryUIFTableHeading);
            heading.Cells[5].AddParagraph(LocalizedText.WageSummaryTotalWageTableHeading);

            Row totalRow = totalWageTable.AddRow();
            totalRow.Cells[0].AddParagraph(LocalizedText.WageSummaryTotalRowHeading);
            totalRow.Cells[1].AddParagraph(totalDeductions.ToString("F2")).Format.Alignment = ParagraphAlignment.Right;
            totalRow.Cells[2].AddParagraph(totalAdjustment.ToString("F2")).Format.Alignment = ParagraphAlignment.Right;
            totalRow.Cells[3].AddParagraph(totalGrossWage.ToString("F2")).Format.Alignment = ParagraphAlignment.Right;
            totalRow.Cells[4].AddParagraph(totalUif.ToString("F2")).Format.Alignment = ParagraphAlignment.Right;
            totalRow.Cells[5].AddParagraph(totalWage.ToString("F2")).Format.Alignment = ParagraphAlignment.Right;
        }
    }
}
