﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using Raido.WageJournal.Contracts.SearchFilters;
using Raido.WageJournal.DataAccess.Repositories.Interfaces;
using Raido.WageJournal.Logic.Extensions;
using Raido.WageJournal.Logic.Localization;
using Raido.WageJournal.Logic.Services.Interfaces;
using Raido.WageJournal.Contracts.DataTransferObjects;
namespace Raido.WageJournal.Logic.Services
{
    internal class PayslipExportService : IPayslipExportService
    {
        #region Fields
        private readonly ICompanyRepository _companyRepository;
        private readonly IWorkRepository _workRepository;
        private readonly IAdjustmentRepository _adjustmentRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IPolicyRepository _policyRepository;
        private readonly IDeductionRepository _deductionRepository;
        #endregion Fields

        #region Constructors
        public PayslipExportService(
            ICompanyRepository companyRepository,
            IWorkRepository workRepository,
            IAdjustmentRepository adjustmentRepository,
            IEmployeeRepository employeeRepository,
            IPolicyRepository policyRepository,
            IDeductionRepository deductionRepository)
        {
            this._companyRepository = companyRepository;
            this._workRepository = workRepository;
            this._adjustmentRepository = adjustmentRepository;
            this._employeeRepository = employeeRepository;
            this._policyRepository = policyRepository;
            this._deductionRepository = deductionRepository;
        }
        #endregion Constructors

        #region Methods

        public async Task<byte[]> GeneratePayslipPdfAsync(Guid companyIdentifier, DateTime startDate, DateTime endDate, Uri fontUri, string fontFamily, string fontName)
        {
            byte[] result = null;
            using (MemoryStream inMemoryFile = new MemoryStream())
            {
                int temp = 0;
                Company company = await this._companyRepository.GetCompanyForCurrentUserAsync(companyIdentifier);
                var allWork = await this._workRepository.GetWorkAsync(new WorkSearchOptions { PeriodStart = startDate, PeriodEnd = endDate }, companyIdentifier);
                var allEmployee = (await this._employeeRepository.GetEmployeesAsync(null, companyIdentifier))
                    .Where(e =>
                        (!e.EmploymentStartDate.HasValue || e.EmploymentStartDate.Value <= endDate) &&
                        (!e.EmploymentEndDate.HasValue || e.EmploymentEndDate.Value >= startDate))
                    .OrderBy(e =>
                        int.TryParse(e.EmployeeNumber, out temp) ? int.Parse(e.EmployeeNumber) : 0)
                    .ThenBy(e => e.Name).ToList();
                var allAdjustment = await this._adjustmentRepository.GetAdjustmentsAsync(new AdjustmentSearchOptions { FromDate = startDate, ToDate = endDate }, companyIdentifier);
                var allCompanyPolicy = await this._policyRepository.GetPoliciesAsync(null, companyIdentifier);
                var allDeduction = await this._deductionRepository.GetDeductionsAsync(null, companyIdentifier);



                decimal saturdayMultiplier = (from policy in allCompanyPolicy
                                              where
                                                  policy.PolicyType == Contracts.Enumerations.PolicyType.SaturdayOvertimeMultiplier
                                              select
                                                  policy.Value).FirstOrDefault();
                decimal sundayMultiplier = (from policy in allCompanyPolicy
                                            where
                                                policy.PolicyType == Contracts.Enumerations.PolicyType.SundayOvertimeMultiplier
                                            select
                                                policy.Value).FirstOrDefault();
                decimal uifMultiplier = (from policy in allCompanyPolicy
                                         where
                                             policy.PolicyType == Contracts.Enumerations.PolicyType.UnemploymentInsuranceFundPercentage
                                         select
                                             policy.Value).FirstOrDefault() / 100;

                // Create a new PDF document
                Document document = new Document();
                document.Info.Title = LocalizedText.WageSummaryPdfInfoTitle;
                PdfDocumentRenderer renderer = new PdfDocumentRenderer(true, PdfSharp.Pdf.PdfFontEmbedding.Always);
                renderer.Document = document;

                // setup up styles such as normal, heading1 etc.
                document.DefineStyles(fontUri, fontFamily, fontName);
                Unit oneCentimetre = new Unit(1, UnitType.Centimeter);

                // create a section on the document to hold content
                // and set the page setup for the section
                Section documentSection = document.AddSection();
                documentSection.PageSetup.Orientation = Orientation.Portrait;

                documentSection.PageSetup.TopMargin = oneCentimetre;
                documentSection.PageSetup.RightMargin = oneCentimetre;
                documentSection.PageSetup.BottomMargin = oneCentimetre;
                documentSection.PageSetup.LeftMargin = oneCentimetre;

                // Render payslips for all active employees
                for (int i = 0; i < allEmployee.Count; i++)
                {
                    // render employee
                    RenderEmployeePayslip(
                        startDate,
                        endDate,
                        company,
                        allWork,
                        allAdjustment,
                        allDeduction,
                        saturdayMultiplier,
                        sundayMultiplier,
                        uifMultiplier,
                        documentSection,
                        i,
                        allEmployee[i],
                        allEmployee[i].HourlyRate ?? 0);

                    if (i % 6 == 5)
                    {
                        documentSection = document.AddSection();
                        documentSection.PageSetup.Orientation = Orientation.Portrait;
                        documentSection.PageSetup.TopMargin = oneCentimetre;
                        documentSection.PageSetup.RightMargin = oneCentimetre;
                        documentSection.PageSetup.BottomMargin = oneCentimetre;
                        documentSection.PageSetup.LeftMargin = oneCentimetre;
                    }
                }

                // Save the document...
                renderer.RenderDocument();
                renderer.PdfDocument.Save(inMemoryFile);
                result = inMemoryFile.ToArray();
            }

            return result;
        }

        private static void RenderEmployeePayslip(
            DateTime startDate,
            DateTime endDate,
            Company company,
            IList<Work> allWork,
            IList<Adjustment> allAdjustment,
            IList<Deduction> allDeduction,
            decimal saturdayMultiplier,
            decimal sundayMultiplier,
            decimal uifMultiplier,
            Section documentSection,
            int renderCount,
            Employee currentEmployee,
            decimal currentEmployeeHourlyRate)
        {
            TextFrame employeePayslipFrame = documentSection.AddTextFrame();
            if (renderCount % 2 == 0)
            {
                employeePayslipFrame.Left = ShapePosition.Left;
            }
            else
            {
                employeePayslipFrame.Left = ShapePosition.Right;
            }

            if (renderCount % 6 <= 1)
            {
                employeePayslipFrame.Top = new Unit(1, UnitType.Centimeter);
            }
            else if (renderCount % 6 <= 3)
            {
                employeePayslipFrame.Top = new Unit(10.5, UnitType.Centimeter);
            }
            else
            {
                employeePayslipFrame.Top = new Unit(20, UnitType.Centimeter);
            }

            employeePayslipFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            employeePayslipFrame.RelativeVertical = RelativeVertical.Page;
            employeePayslipFrame.Height = new Unit(10, UnitType.Centimeter);
            employeePayslipFrame.Width = new Unit(8.75, UnitType.Centimeter);

            employeePayslipFrame.AddParagraph(company.FullName).Format.Font.Bold = true;
            var employeeDetailsTable = employeePayslipFrame.AddTable();
            employeeDetailsTable.Style = "Table";
            employeeDetailsTable.Borders.Color = Colors.Transparent;
            employeeDetailsTable.Rows.LeftIndent = 0;
            employeeDetailsTable.Rows.Height = new Unit(0.75, UnitType.Centimeter);
            employeeDetailsTable.Rows.VerticalAlignment = VerticalAlignment.Center;
            employeeDetailsTable.Format.Font.Size = new Unit(10, UnitType.Point);
            Unit frameQuarterWidth = new Unit(employeePayslipFrame.Width.Centimeter / 4, UnitType.Centimeter);
            employeeDetailsTable.AddColumn(frameQuarterWidth).Format.Alignment = ParagraphAlignment.Left;
            employeeDetailsTable.AddColumn(frameQuarterWidth).Format.Alignment = ParagraphAlignment.Left;
            employeeDetailsTable.AddColumn(frameQuarterWidth).Format.Alignment = ParagraphAlignment.Right;
            employeeDetailsTable.AddColumn(frameQuarterWidth).Format.Alignment = ParagraphAlignment.Left;

            Row detailRow1 = employeeDetailsTable.AddRow();
            detailRow1.Cells[0].AddParagraph(LocalizedText.PayslipEmployeeNumberLabel);
            detailRow1.Cells[1].AddParagraph(currentEmployee.EmployeeNumber ?? string.Empty);
            detailRow1.Cells[2].AddParagraph(LocalizedText.PayslipFromDateLabel);
            detailRow1.Cells[3].AddParagraph(startDate.ToShortDateString());

            Row detailRow2 = employeeDetailsTable.AddRow();
            detailRow2.Cells[0].AddParagraph(LocalizedText.PayslipEmployeeNameLabel);
            detailRow2.Cells[1].AddParagraph(currentEmployee.Name ?? string.Empty);
            detailRow2.Cells[2].AddParagraph(LocalizedText.PayslipToDateLabel);
            detailRow2.Cells[3].AddParagraph(endDate.ToShortDateString());

            //spacer line
            employeePayslipFrame.AddParagraph();

            var wageForPeriodTable = employeePayslipFrame.AddTable();
            wageForPeriodTable.Style = "Table";
            wageForPeriodTable.Rows.LeftIndent = 0;
            wageForPeriodTable.Rows.Height = new Unit(0.65, UnitType.Centimeter);
            wageForPeriodTable.Rows.VerticalAlignment = VerticalAlignment.Center;
            wageForPeriodTable.Format.Font.Size = new Unit(10, UnitType.Point);
            wageForPeriodTable.AddColumn(frameQuarterWidth).Format.Alignment = ParagraphAlignment.Right;
            wageForPeriodTable.AddColumn(frameQuarterWidth).Format.Alignment = ParagraphAlignment.Right;
            wageForPeriodTable.AddColumn(frameQuarterWidth).Format.Alignment = ParagraphAlignment.Right;
            wageForPeriodTable.AddColumn(frameQuarterWidth).Format.Alignment = ParagraphAlignment.Right;

            AddPayslipTableHeading(wageForPeriodTable);

            decimal hoursForWeekday = (from work in allWork
                                       where
                                           work.EmployeeIdentifier == currentEmployee.Identifier &&
                                           work.DateWorked.DayOfWeek != DayOfWeek.Saturday &&
                                           work.DateWorked.DayOfWeek != DayOfWeek.Sunday
                                       select
                                           work.HoursWorked ?? 0).Sum();
            decimal hoursForSaturday = (from work in allWork
                                        where
                                            work.EmployeeIdentifier == currentEmployee.Identifier &&
                                            work.DateWorked.DayOfWeek == DayOfWeek.Saturday
                                        select
                                            work.HoursWorked ?? 0).Sum();
            decimal hoursForSunday = (from work in allWork
                                      where
                                          work.EmployeeIdentifier == currentEmployee.Identifier &&
                                          work.DateWorked.DayOfWeek == DayOfWeek.Sunday
                                      select
                                          work.HoursWorked ?? 0).Sum();

            decimal totalAdjustment = (from adj in allAdjustment
                                       where
                                           adj.EmployeeIdentifier == currentEmployee.Identifier
                                       select
                                           adj.Amount ?? 0).Sum();

            decimal totalDeductions = (from deduction in allDeduction
                                       where
                                            deduction.EmployeeIdentifier == currentEmployee.Identifier &&
                                            !string.IsNullOrWhiteSpace(deduction.Cron)
                                       select
                                           deduction.Amount * NCrontab.CrontabSchedule.Parse(deduction.Cron).GetNextOccurrences(startDate, endDate).Count()).Sum();

            decimal grossWage = (hoursForWeekday * currentEmployeeHourlyRate) +
                (hoursForSaturday * currentEmployeeHourlyRate * saturdayMultiplier) +
                (hoursForSunday * currentEmployeeHourlyRate * sundayMultiplier) +
                totalAdjustment -
                totalDeductions;

            AddPayslipWeekdayRow(currentEmployeeHourlyRate, wageForPeriodTable, hoursForWeekday);

            AddPayslipSaturdayRow(saturdayMultiplier, currentEmployeeHourlyRate, wageForPeriodTable, hoursForSaturday);

            AddPayslipSundayRow(sundayMultiplier, currentEmployeeHourlyRate, wageForPeriodTable, hoursForSunday);

            AddPayslipDeductionsRow(wageForPeriodTable, totalDeductions);

            AddPayslipAdjustmentsRow(wageForPeriodTable, totalAdjustment);

            AddPayslipGrossWagesRow(wageForPeriodTable, grossWage);

            AddPayslipUnemploymentInsuranceFundRow(uifMultiplier, wageForPeriodTable, grossWage);

            AddPayslipNettWageRow(uifMultiplier, wageForPeriodTable, grossWage);
        }

        private static void AddPayslipNettWageRow(decimal uifMultiplier, Table wageForPeriodTable, decimal grossWage)
        {
            Row totalWage = wageForPeriodTable.AddRow();
            totalWage.Cells[0].AddParagraph();
            totalWage.Cells[1].AddParagraph();
            totalWage.Cells[2].AddParagraph(LocalizedText.PayslipTotalWageRowHeading);
            totalWage.Cells[3].AddParagraph((grossWage - (grossWage * uifMultiplier)).ToString("F2"));
            totalWage.Cells[3].Borders.Color = Colors.DarkGray;
            totalWage.Cells[3].Borders.Width = new Unit(0.1, UnitType.Millimeter);
        }

        private static void AddPayslipUnemploymentInsuranceFundRow(decimal uifMultiplier, Table wageForPeriodTable, decimal grossWage)
        {
            Row unemploymentInsuranceFund = wageForPeriodTable.AddRow();
            unemploymentInsuranceFund.Cells[0].AddParagraph();
            unemploymentInsuranceFund.Cells[1].AddParagraph();
            unemploymentInsuranceFund.Cells[2].AddParagraph(LocalizedText.PayslipUifRowHeading);
            unemploymentInsuranceFund.Cells[3].AddParagraph((grossWage * uifMultiplier).ToString("F2"));
            unemploymentInsuranceFund.Cells[3].Borders.Color = Colors.DarkGray;
            unemploymentInsuranceFund.Cells[3].Borders.Width = new Unit(0.1, UnitType.Millimeter);
        }

        private static void AddPayslipGrossWagesRow(Table wageForPeriodTable, decimal grossWage)
        {
            Row grossWageRow = wageForPeriodTable.AddRow();
            grossWageRow.Cells[0].AddParagraph();
            grossWageRow.Cells[1].AddParagraph();
            grossWageRow.Cells[2].AddParagraph(LocalizedText.PayslipGrossWageRowHeading);
            grossWageRow.Cells[3].AddParagraph(grossWage.ToString("F2"));
            grossWageRow.Cells[3].Borders.Color = Colors.DarkGray;
            grossWageRow.Cells[3].Borders.Width = new Unit(0.1, UnitType.Millimeter);
        }

        private static void AddPayslipDeductionsRow(Table wageForPeriodTable, decimal totalDeductions)
        {
            Row adjustments = wageForPeriodTable.AddRow();
            adjustments.Cells[0].AddParagraph();
            adjustments.Cells[1].AddParagraph();
            adjustments.Cells[2].AddParagraph(LocalizedText.PayslipDeductionRowHeading);
            adjustments.Cells[3].AddParagraph(totalDeductions.ToString("F2"));
            adjustments.Cells[3].Borders.Color = Colors.DarkGray;
            adjustments.Cells[3].Borders.Width = new Unit(0.1, UnitType.Millimeter);
        }

        private static void AddPayslipAdjustmentsRow(Table wageForPeriodTable, decimal totalAdjustment)
        {
            Row adjustments = wageForPeriodTable.AddRow();
            adjustments.Cells[0].AddParagraph();
            adjustments.Cells[1].AddParagraph();
            adjustments.Cells[2].AddParagraph(LocalizedText.PayslipAdjustmentRowHeading);
            adjustments.Cells[3].AddParagraph(totalAdjustment.ToString("F2"));
            adjustments.Cells[3].Borders.Color = Colors.DarkGray;
            adjustments.Cells[3].Borders.Width = new Unit(0.1, UnitType.Millimeter);
        }

        private static void AddPayslipSundayRow(decimal sundayMultiplier, decimal currentEmployeeHourlyRate, Table wageForPeriodTable, decimal hoursForSunday)
        {
            Row sundayWork = wageForPeriodTable.AddRow();
            sundayWork.Cells[0].AddParagraph(LocalizedText.PayslipSundayRowHeading);
            sundayWork.Cells[1].AddParagraph(hoursForSunday.ToString("F2"));
            sundayWork.Cells[1].Borders.Color = Colors.DarkGray;
            sundayWork.Cells[1].Borders.Width = new Unit(0.1, UnitType.Millimeter);
            sundayWork.Cells[2].AddParagraph((currentEmployeeHourlyRate * sundayMultiplier).ToString("F2"));
            sundayWork.Cells[2].Borders.Color = Colors.DarkGray;
            sundayWork.Cells[2].Borders.Width = new Unit(0.1, UnitType.Millimeter);
            sundayWork.Cells[3].AddParagraph((hoursForSunday * currentEmployeeHourlyRate * sundayMultiplier).ToString("F2"));
            sundayWork.Cells[3].Borders.Color = Colors.DarkGray;
            sundayWork.Cells[3].Borders.Width = new Unit(0.1, UnitType.Millimeter);
        }

        private static void AddPayslipSaturdayRow(decimal saturdayMultiplier, decimal currentEmployeeHourlyRate, Table wageForPeriodTable, decimal hoursForSaturday)
        {
            Row saturdayWork = wageForPeriodTable.AddRow();
            saturdayWork.Cells[0].AddParagraph(LocalizedText.PayslipSaturdayRowHeading);
            saturdayWork.Cells[1].AddParagraph(hoursForSaturday.ToString("F2"));
            saturdayWork.Cells[1].Borders.Color = Colors.DarkGray;
            saturdayWork.Cells[1].Borders.Width = new Unit(0.1, UnitType.Millimeter);
            saturdayWork.Cells[2].AddParagraph((currentEmployeeHourlyRate * saturdayMultiplier).ToString("F2"));
            saturdayWork.Cells[2].Borders.Color = Colors.DarkGray;
            saturdayWork.Cells[2].Borders.Width = new Unit(0.1, UnitType.Millimeter);
            saturdayWork.Cells[3].AddParagraph((hoursForSaturday * currentEmployeeHourlyRate * saturdayMultiplier).ToString("F2"));
            saturdayWork.Cells[3].Borders.Color = Colors.DarkGray;
            saturdayWork.Cells[3].Borders.Width = new Unit(0.1, UnitType.Millimeter);
        }

        private static void AddPayslipWeekdayRow(decimal currentEmployeeHourlyRate, Table wageForPeriodTable, decimal hoursForWeekday)
        {
            Row weekdayWork = wageForPeriodTable.AddRow();
            weekdayWork.Cells[0].AddParagraph(LocalizedText.PayslipWeekdayRowHeading);
            weekdayWork.Cells[1].AddParagraph(hoursForWeekday.ToString("F2"));
            weekdayWork.Cells[1].Borders.Color = Colors.DarkGray;
            weekdayWork.Cells[1].Borders.Width = new Unit(0.1, UnitType.Millimeter);
            weekdayWork.Cells[2].AddParagraph(currentEmployeeHourlyRate.ToString("F2"));
            weekdayWork.Cells[2].Borders.Color = Colors.DarkGray;
            weekdayWork.Cells[2].Borders.Width = new Unit(0.1, UnitType.Millimeter);
            weekdayWork.Cells[3].AddParagraph((hoursForWeekday * currentEmployeeHourlyRate).ToString("F2"));
            weekdayWork.Cells[3].Borders.Color = Colors.DarkGray;
            weekdayWork.Cells[3].Borders.Width = new Unit(0.1, UnitType.Millimeter);
        }

        private static void AddPayslipTableHeading(Table wageForPeriodTable)
        {
            Row heading = wageForPeriodTable.AddRow();
            heading.Cells[0].AddParagraph(string.Empty);
            heading.Cells[1].AddParagraph(LocalizedText.PayslipHoursTableHeading).Format.Alignment = ParagraphAlignment.Center;
            heading.Cells[1].Borders.Color = Colors.DarkGray;
            heading.Cells[1].Borders.Width = new Unit(0.1, UnitType.Millimeter);
            heading.Cells[2].AddParagraph(LocalizedText.PayslipRateHeading).Format.Alignment = ParagraphAlignment.Center;
            heading.Cells[2].Borders.Color = Colors.DarkGray;
            heading.Cells[2].Borders.Width = new Unit(0.1, UnitType.Millimeter);
            heading.Cells[3].AddParagraph(LocalizedText.PayslipAmountTableHeading).Format.Alignment = ParagraphAlignment.Center;
            heading.Cells[3].Borders.Color = Colors.DarkGray;
            heading.Cells[3].Borders.Width = new Unit(0.1, UnitType.Millimeter);
        }
        #endregion Methods
    }
}
