﻿using System;
using System.Threading.Tasks;
using Raido.WageJournal.Contracts.ComplexTransferObjects;

namespace Raido.WageJournal.Logic.Services.Interfaces
{
    public interface ICompanyService
    {
        Task<CompanyAndSettings> GetCompanyDetailsAsync(Guid companyIdentifier);

        Task<CompanyAndSettings> SaveCompanyDetailsAsync(CompanyAndSettings companyDetails);
    }
}
