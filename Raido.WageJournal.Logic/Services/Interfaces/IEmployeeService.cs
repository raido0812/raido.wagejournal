﻿using System;
using System.Threading.Tasks;
using Raido.WageJournal.Contracts.ComplexTransferObjects;
namespace Raido.WageJournal.Logic.Services.Interfaces
{
    public interface IEmployeeService
    {
        Task<CompanyAndEmployeeWork> GetAllEmployeeWorkForDayAsync(Guid companyIdentifier, DateTime workDay);

        Task<EmployeeWorkDay> GetEmployeeWorkForDayAsync(Guid companyIdentifier, Guid employeeIdentifier, DateTime workDay);
        
        Task<CompanyAndEmployees> GetAllEmployeesAsync(Guid companyIdentifier);
    }
}
