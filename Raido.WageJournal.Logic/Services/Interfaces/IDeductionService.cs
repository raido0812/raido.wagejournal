﻿using System;
using System.Threading.Tasks;
using Raido.WageJournal.Contracts.ComplexTransferObjects;
namespace Raido.WageJournal.Logic.Services.Interfaces
{
    public interface IDeductionService
    {
        Task<EmployeeAndDeductions> GetDeductionsAsync(Guid? deductionIdentifier, Guid employeeIdentifier, Guid companyIdentifier);
    }
}
