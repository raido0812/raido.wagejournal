﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Logic.Services.Interfaces
{
    public interface IAttendanceSummaryExportService
    {
        Task<byte[]> GenerateAttendanceSummaryPdfAsync(Guid companyIdentifier, DateTime startDate, DateTime endDate, Uri fontUri, string fontFamily, string fontName);
    }
}
