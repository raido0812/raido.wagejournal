﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Raido.WageJournal.Contracts.ComplexTransferObjects;

namespace Raido.WageJournal.Logic.Services.Interfaces
{
    public interface ITimeSheetImporter
    {
        Task<IEnumerable<TimesheetRecord>> ExtractTimesheetRecords(Stream input);
    }
}
