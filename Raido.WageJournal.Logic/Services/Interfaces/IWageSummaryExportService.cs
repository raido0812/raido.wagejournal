﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Logic.Services.Interfaces
{
    public interface IWageSummaryExportService
    {
        Task<byte[]> GenerateWageSummaryPdfAsync(Guid companyIdentifier, DateTime startDate, DateTime endDate, Uri fontUri, string fontFamily, string fontName);
    }
}
