﻿using Raido.WageJournal.DataAccess.Repositories.Interfaces;
using Raido.WageJournal.Logic.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raido.WageJournal.Contracts.SearchFilters;

namespace Raido.WageJournal.Logic.Services
{
    internal class DeductionService : IDeductionService
    {
        #region Fields
        private readonly IDeductionRepository _deductionRepository;
        private readonly IEmployeeRepository _employeeRepository;
        #endregion Fields

        #region Constructors
        public DeductionService(IDeductionRepository deductionRepository, IEmployeeRepository employeeRepository)
        {
            this._deductionRepository = deductionRepository;
            this._employeeRepository = employeeRepository;
        }
        #endregion Constructors

        #region Methods

        public async Task<Contracts.ComplexTransferObjects.EmployeeAndDeductions> GetDeductionsAsync(Guid? deductionIdentifier, Guid employeeIdentifier, Guid companyIdentifier)
        {
            Contracts.ComplexTransferObjects.EmployeeAndDeductions employeeAndDeductions = new Contracts.ComplexTransferObjects.EmployeeAndDeductions();

            employeeAndDeductions.Deductions = await this._deductionRepository.GetDeductionsAsync(new DeductionSearchOptions { DeductionIdentifier = deductionIdentifier, EmployeeIdentifier = employeeIdentifier }, companyIdentifier);
            employeeAndDeductions.Employee = await this._employeeRepository.GetEmployeeAsync(employeeIdentifier, companyIdentifier);
            return employeeAndDeductions;
        }
        #endregion Methods
    }
}
