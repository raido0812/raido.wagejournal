﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Raido.WageJournal.DataAccess.Repositories.Interfaces;
using Raido.WageJournal.Logic.Services.Interfaces;
using DataTransferObjects = Raido.WageJournal.Contracts.DataTransferObjects;
namespace Raido.WageJournal.Logic.Services
{
    internal class CompanyService : ICompanyService
    {
        #region Fields
        private readonly ICompanyRepository _companyRepository;
        private readonly IPolicyRepository _policyRepository;
        #endregion Fields

        #region Constructors
        public CompanyService(ICompanyRepository companyRepository, IPolicyRepository policyRepository)
        {
            this._companyRepository = companyRepository;
            this._policyRepository = policyRepository;
        }
        #endregion Constructors

        #region Methods

        public async Task<Contracts.ComplexTransferObjects.CompanyAndSettings> GetCompanyDetailsAsync(Guid companyIdentifier)
        {
            Contracts.ComplexTransferObjects.CompanyAndSettings companyAndSettings = new Contracts.ComplexTransferObjects.CompanyAndSettings();
            companyAndSettings.Company = await this._companyRepository.GetCompanyForCurrentUserAsync(companyIdentifier);
            var allPolicies = await this._policyRepository.GetPoliciesAsync(null, companyIdentifier);

            companyAndSettings.SaturdayOvertimeMultiplier = ExistingOrNewInstance(allPolicies, Contracts.Enumerations.PolicyType.SaturdayOvertimeMultiplier, companyAndSettings.Company.CompanyId);
            companyAndSettings.SundayOvertimeMultiplier = ExistingOrNewInstance(allPolicies, Contracts.Enumerations.PolicyType.SundayOvertimeMultiplier, companyAndSettings.Company.CompanyId);
            companyAndSettings.UnemploymentInsuranceFundPercentage = ExistingOrNewInstance(allPolicies, Contracts.Enumerations.PolicyType.UnemploymentInsuranceFundPercentage, companyAndSettings.Company.CompanyId);
            companyAndSettings.WorkDayHours = ExistingOrNewInstance(allPolicies, Contracts.Enumerations.PolicyType.WorkDayHours, companyAndSettings.Company.CompanyId);
            companyAndSettings.AttendanceBonusAmount = ExistingOrNewInstance(allPolicies, Contracts.Enumerations.PolicyType.AttendanceBonusAmount, companyAndSettings.Company.CompanyId);
            return companyAndSettings;
        }

        public async Task<Contracts.ComplexTransferObjects.CompanyAndSettings> SaveCompanyDetailsAsync(Contracts.ComplexTransferObjects.CompanyAndSettings companyDetails)
        {
            Contracts.ComplexTransferObjects.CompanyAndSettings updatedCompanyAndSettings = new Contracts.ComplexTransferObjects.CompanyAndSettings();

            companyDetails.SaturdayOvertimeMultiplier.PolicyType = Contracts.Enumerations.PolicyType.SaturdayOvertimeMultiplier;
            companyDetails.SundayOvertimeMultiplier.PolicyType = Contracts.Enumerations.PolicyType.SundayOvertimeMultiplier;
            companyDetails.UnemploymentInsuranceFundPercentage.PolicyType = Contracts.Enumerations.PolicyType.UnemploymentInsuranceFundPercentage;
            companyDetails.WorkDayHours.PolicyType = Contracts.Enumerations.PolicyType.WorkDayHours;
            companyDetails.AttendanceBonusAmount.PolicyType = Contracts.Enumerations.PolicyType.AttendanceBonusAmount;

            updatedCompanyAndSettings.Company = await this._companyRepository.SaveCompanyAsync(companyDetails.Company);
            updatedCompanyAndSettings.SaturdayOvertimeMultiplier = await this._policyRepository.SavePolicyAsync(companyDetails.SaturdayOvertimeMultiplier, companyDetails.Company.Identifier);
            updatedCompanyAndSettings.SundayOvertimeMultiplier = await this._policyRepository.SavePolicyAsync(companyDetails.SundayOvertimeMultiplier, companyDetails.Company.Identifier);
            updatedCompanyAndSettings.UnemploymentInsuranceFundPercentage = await this._policyRepository.SavePolicyAsync(companyDetails.UnemploymentInsuranceFundPercentage, companyDetails.Company.Identifier);
            updatedCompanyAndSettings.WorkDayHours = await this._policyRepository.SavePolicyAsync(companyDetails.WorkDayHours, companyDetails.Company.Identifier);
            updatedCompanyAndSettings.AttendanceBonusAmount = await this._policyRepository.SavePolicyAsync(companyDetails.AttendanceBonusAmount, companyDetails.Company.Identifier);

            return updatedCompanyAndSettings;
        }

        private static DataTransferObjects.Policy ExistingOrNewInstance(IList<DataTransferObjects.Policy> allPolicies, Contracts.Enumerations.PolicyType policyType, int companyId)
        {
            return allPolicies.FirstOrDefault(p => p.PolicyType == policyType) ??
                new DataTransferObjects.Policy() { CompanyId = companyId, PolicyType = policyType };
        }

        #endregion Methods
    }
}
