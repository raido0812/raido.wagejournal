﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MigraDoc.DocumentObjectModel;

namespace Raido.WageJournal.Logic.Extensions
{
    public static class MigraDocumentExtension
    {
        public static Document DefineStyles(this Document document, Uri fontUri, string fontFamily, string fontName)
        {
            try
            {
                PdfSharp.Drawing.XPrivateFontCollection.Global.Add(fontUri, "./#" + fontFamily);
            }
            catch (System.ArgumentException)
            {
                // do nothing if font already added
            }

            // Get the predefined style Normal.
            Style style = document.Styles["Normal"];
            // Because all styles are derived from Normal, the next line changes the 
            // font of the whole document. Or, more exactly, it changes the font of
            // all styles and paragraphs that do not redefine the font.
            style.Font.Name = fontName;
            style.Font.Size = 10;

            // Heading1 to Heading9 are predefined styles with an outline level. An outline level
            // other than OutlineLevel.BodyText automatically creates the outline (or bookmarks) 
            // in PDF.
            style = document.Styles["Heading1"];
            style.Font.Size = 14;
            style.Font.Bold = true;
            style.ParagraphFormat.PageBreakBefore = true;
            style.ParagraphFormat.SpaceAfter = 6;

            style = document.Styles["Heading2"];
            style.Font.Size = 12;
            style.Font.Bold = true;
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 2;

            style = document.Styles["Heading3"];
            style.Font.Size = 12;
            style.Font.Bold = false;
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 2;

            //// Create a new style called TextBox based on style Normal
            //style = document.Styles.AddStyle("TextBox", "Normal");
            //style.ParagraphFormat.Alignment = ParagraphAlignment.Justify;
            //style.ParagraphFormat.Borders.Width = 2.5;
            //style.ParagraphFormat.Borders.Distance = "3pt";

            //// Create a new style called TOC based on style Normal
            //style = document.Styles.AddStyle("TOC", "Normal");
            //style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right, TabLeader.Dots);
            //style.ParagraphFormat.Font.Color = Colors.Blue;

            return document;
        }
    }
}
