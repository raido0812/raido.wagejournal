--/****** Script for SelectTopNRows command from SSMS  ******/
--SELECT TOP (1000)
--	Employee.Name,
--	Employee.EmployeeNumber,
--	 [Work].[WorkId]
--    ,[Work].[HoursWorked]
--    ,[Work].[DateWorked]
--    ,[Work].[EmployeeId]
--    ,[Work].[Identifier]
--    ,[Work].[Version]
--    ,[Work].[CreatedDate]
--    ,[Work].[ModifiedDate]
--    ,[Work].[DeletedDate]
--    ,[Work].[IsActive]
--    ,[Work].[IsDeleted]
--FROM
--	[dbo].[Work]
--	INNER JOIN Employee ON
--		Work.EmployeeId = Employee.EmployeeId
--WHERE
--	DateWorked BETWEEN '2018-07-09' AND '2018-07-15' AND
--	EmployeeNumber IN ('19','20','32','41','50')
--ORDER BY
--	Employee.Name,
--	Work.DateWorked

DECLARE @DuplicateWorkLogged TABLE ([Name] NVARCHAR(255), EmployeeNumber NVARCHAR(50),EmployeeId INT, DateWorked DATETIME, HoursWorked DECIMAL(18,2), WorkId INT, ModifiedDate DATETIME, ModifiedDescRanking INT, IdAscRanking INT)
INSERT INTO @DuplicateWorkLogged([Name], EmployeeNumber, EmployeeId, DateWorked, HoursWorked, WorkId, ModifiedDate, ModifiedDescRanking, IdAscRanking)
SELECT
	Dup.Name,
	Dup.EmployeeNumber,
	Dup.EmployeeId,
	Dup.DateWorked,
	Work.HoursWorked,
	Work.WorkId,
	Work.ModifiedDate,
	ROW_NUMBER() OVER (PARTITION BY Dup.EmployeeId, Dup.DateWorked ORDER BY Work.ModifiedDate DESC),
	ROW_NUMBER() OVER (PARTITION BY Dup.EmployeeId, Dup.DateWorked ORDER BY Work.WorkId ASC)
FROM
	(SELECT 
		Employee.Name,
		Employee.EmployeeNumber,
		[Work].[EmployeeId],
		Work.DateWorked,
		COUNT([Work].[WorkId]) as NumberEntriesForDate
	FROM
		[dbo].[Work]
		INNER JOIN Employee ON
			Work.EmployeeId = Employee.EmployeeId
	GROUP BY
		Employee.Name,
		Employee.EmployeeNumber,
		[Work].[EmployeeId],
		Work.DateWorked
	HAVING
		COUNT([Work].[WorkId]) > 1) as Dup
	INNER JOIN Work ON
		Dup.EmployeeId = Work.EmployeeId AND
		Dup.DateWorked = Work.DateWorked

SELECT
	*
FROM
	@DuplicateWorkLogged
ORDER BY
	Name,
	DateWorked,
	WorkId
