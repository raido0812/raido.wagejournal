SELECT
	C.CompanyId,
	E.[Name],
	E.EmployeeNumber,
	ROW_NUMBER() OVER(PARTITION BY C.CompanyId,E.EmployeeNumber,E.[Name] ORDER BY E.EmployeeId) as EmployeeRank,
	E.EmployeeId,
	E.EmploymentStartDate,
	E.EmploymentEndDate
FROM
	AspNetUsers as U
	INNER JOIN CompanyUser as CU ON
		CU.UserId = U.Id
	INNER JOIN Company as C ON
		C.CompanyId = CU.CompanyId
	INNER JOIN Employee as E ON
		E.CompanyId = C.CompanyId
WHERE
	U.Email = 'hlkplk@gmail.com'
ORDER BY
	C.CompanyId,
	E.EmployeeNumber,
	E.[Name]


SELECT
	C.CompanyId,
	C.FullName,
	E.EmployeeId,
	E.EmployeeNumber,
	E.[Name],
	E.EmploymentStartDate,
	E.EmploymentEndDate,
	W.WorkId,
	W.DateWorked,
	W.HoursWorked,
	A.AdjustmentId,
	A.DateOfIssuance,
	A.Amount as AdjustmentAmount,
	A.Description as AdjustmentDescription
FROM
	AspNetUsers as U
	INNER JOIN CompanyUser as CU ON
		CU.UserId = U.Id
	INNER JOIN Company as C ON
		C.CompanyId = CU.CompanyId
	INNER JOIN Employee as E ON
		E.CompanyId = C.CompanyId
	LEFT JOIN Work as W ON
		W.EmployeeId = E.EmployeeId
	LEFT JOIN Adjustment as A ON
		A.EmployeeId = E.EmployeeId AND
		A.DateOfIssuance = W.DateWorked
WHERE
	U.Email = 'hlkplk@gmail.com'


SELECT * FROM Deduction
SElECT * FROM Employee WHERE EmployeeId <=4