﻿using Microsoft.AspNet.Identity.EntityFramework;
using Raido.WageJournal.DataAccess.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Raido.WageJournal.DataAccess.DataContext
{
    public class WageDbContext : IdentityDbContext<ApplicationUser>
    {
        public WageDbContext(string connectionString)
            : base(connectionString)
        {
        }

        public WageDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static WageDbContext Create()
        {
            return new WageDbContext();
        }

        public virtual DbSet<Entities.CompanyUser> CompanyUsers { get; set; }
        public virtual DbSet<Entities.Company> Companies { get; set; }
        public virtual DbSet<Entities.Employee> Employees { get; set; }
        public virtual DbSet<Entities.Policy> Policies { get; set; }
        public virtual DbSet<Entities.Work> Work { get; set; }
        public virtual DbSet<Entities.Adjustment> Adjustments { get; set; }
        public virtual DbSet<Entities.Deduction> Deductions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Do not pluralize the db tables
            modelBuilder.Conventions.Remove<System.Data.Entity.ModelConfiguration.Conventions.PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}
