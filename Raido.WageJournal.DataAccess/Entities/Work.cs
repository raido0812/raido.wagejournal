﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.DataAccess.Entities
{
    public class Work : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkId { get; set; }

        [Required]
        [Display(Name = "HoursWorkedDisplayText", ResourceType = typeof(Localization.Work))]
        public decimal HoursWorked { get; set; }

        [Required]
        [Display(Name = "DateWorkedDisplayText", ResourceType = typeof(Localization.Work))]
        [Index("UX_Employee_DateWorked", IsUnique = true, Order = 2)]
        public DateTime DateWorked { get; set; }

        [Required]
        [Index("UX_Employee_DateWorked", IsUnique = true, Order = 1)]
        public int EmployeeId { get; set; }

        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }

        public Work()
            : base()
        { }
    }
}
