﻿using Raido.WageJournal.Contracts.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.DataAccess.Entities
{
    public class Policy : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PolicyId { get; set; }

        public PolicyType PolicyType { get; set; }

        [MaxLength(255, ErrorMessageResourceName = "SettingValueMaxLengthErrorMessage", ErrorMessageResourceType = typeof(Localization.Policy))]
        public string Setting { get; set; }

        public decimal Value { get; set; }
        
        [Required]
        public int CompanyId { get; set; }

        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }

        public Policy()
            : base()
        { }
    }
}
