﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.DataAccess.Entities
{
    public class Company : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CompanyId { get; set; }

        [MaxLength(255, ErrorMessageResourceName = "TradeNameMaxLengthErrorMessage", ErrorMessageResourceType = typeof(Localization.Company))]
        [Required]
        public string TradeName { get; set; }

        [MaxLength(500, ErrorMessageResourceName = "FullNameMaxLengthErrorMessage", ErrorMessageResourceType = typeof(Localization.Company))]
        public string FullName { get; set; }

        public virtual IList<CompanyUser> CompanyUsers { get; set; }

        public virtual IList<Policy> Policies { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors", Justification = "Virtual is for EF Code First")]
        public Company()
            : base()
        {
            this.CompanyUsers = new List<CompanyUser>();
            this.Policies = new List<Policy>();
        }
    }
}
