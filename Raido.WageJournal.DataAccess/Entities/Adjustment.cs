﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.DataAccess.Entities
{
    public class Adjustment : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AdjustmentId { get; set; }

        [MaxLength(100, ErrorMessageResourceName = "DescriptionMaxLengthErrorMessage", ErrorMessageResourceType = typeof(Localization.Adjustment))]
        [Required]
        [Display(Name = "AdjustmentDescriptionDisplayText", ResourceType = typeof(Localization.Adjustment))]
        public string Description { get; set; }

        [Required]
        [Display(Name = "AdjustmentAmountDisplayText", ResourceType = typeof(Localization.Adjustment))]
        public decimal Amount { get; set; }

        [Required]
        public DateTime DateOfIssuance { get; set; }

        [Required]
        public int EmployeeId { get; set; }

        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }

        public Adjustment()
            : base()
        { }
    }
}
