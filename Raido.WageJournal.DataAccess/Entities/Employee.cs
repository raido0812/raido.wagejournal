﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.DataAccess.Entities
{
    public class Employee : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmployeeId { get; set; }
        
        [MaxLength(255, ErrorMessageResourceName = "EmployeeNameMaxLengthErrorMessage", ErrorMessageResourceType = typeof(Localization.Employee))]
        [Required]
        public string Name { get; set; }

        [MaxLength(255, ErrorMessageResourceName = "EmailMaxLengthErrorMessage", ErrorMessageResourceType = typeof(Localization.Employee))]
        public string Email { get; set; }

        [MaxLength(50, ErrorMessageResourceName = "PhoneNumberMaxLengthErrorMessage", ErrorMessageResourceType = typeof(Localization.Employee))]
        public string PhoneNumber { get; set; }

        [MaxLength(50, ErrorMessageResourceName = "EmployeeNumberMaxLengthErrorMessage", ErrorMessageResourceType = typeof(Localization.Employee))]
        public string EmployeeNumber { get; set; }

        [Required]
        public decimal HourlyRate { get; set; }

        public DateTime? EmploymentStartDate { get; set; }

        public DateTime? EmploymentEndDate { get; set; }

        [Required]
        public int CompanyId { get; set; }

        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }

        public virtual IList<Adjustment> Adjustments { get; set; }

        public virtual IList<Work> Work { get; set; }

        public virtual IList<Deduction> Deductions { get; set; }

        public Employee()
            : base()
        { }
    }
}
