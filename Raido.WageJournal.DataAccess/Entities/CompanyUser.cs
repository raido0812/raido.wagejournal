﻿using Raido.WageJournal.DataAccess.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.DataAccess.Entities
{
    public class CompanyUser : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CompanyUserId { get; set; }
        public int CompanyId { get; set; }
        public string UserId { get; set; }
        public bool IsCurrentDisplayed { get; set; }

        public CompanyUser() : base() { }
        
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
    }
}
