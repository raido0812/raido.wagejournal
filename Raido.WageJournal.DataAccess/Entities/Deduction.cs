﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.DataAccess.Entities
{
    public class Deduction : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DeductionId { get; set; }

        [MaxLength(100, ErrorMessageResourceName = "DescriptionMaxLengthErrorMessage", ErrorMessageResourceType = typeof(Localization.Deduction))]
        [Required]
        public string Description { get; set; }

        [Required]
        public decimal Amount { get; set; }

        [Required]
        public DateTime EffectiveFrom { get; set; }

        public DateTime? EffectiveTo { get; set; }

        [MaxLength(20, ErrorMessageResourceName = "CronMaxLengthErrorMessage", ErrorMessageResourceType = typeof(Localization.Deduction))]
        public string Cron { get; set; }

        [Required]
        public int EmployeeId { get; set; }

        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }

        public Deduction()
            : base()
        { }
    }
}
