﻿using System.Security.Claims;

namespace Raido.WageJournal.DataAccess.Security
{
    public interface ICustomPrincipalProvider
    {
        ClaimsPrincipal GetCurrentUser();
        string GetCurrentUserId();
    }
}
