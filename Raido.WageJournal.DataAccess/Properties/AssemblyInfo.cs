﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Raido.WageJournal.DataAccess")]
[assembly: AssemblyProduct("Raido.WageJournal.DataAccess")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("8440950d-4b22-4e23-a561-1ee746379644")]
[assembly: InternalsVisibleTo("Raido.WageJournal.UnitTest")]
