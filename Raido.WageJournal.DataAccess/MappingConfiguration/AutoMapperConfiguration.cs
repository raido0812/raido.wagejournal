﻿using AutoMapper;
using DataTransferObjects = Raido.WageJournal.Contracts.DataTransferObjects;
namespace Raido.WageJournal.DataAccess.MappingConfiguration
{
    public static class AutoMapperConfiguration
    {
        public static void RegisterAllMappings()
        {
            Mapper.Initialize(config =>
            {
                config.CreateMap<Entities.Company, DataTransferObjects.Company>();
                config.CreateMap<Entities.Policy, DataTransferObjects.Policy>();
                config.CreateMap<Entities.CompanyUser, DataTransferObjects.CompanyUser>();
                config.CreateMap<Entities.Adjustment, DataTransferObjects.Adjustment>()
                    .ForMember(model => model.EmployeeIdentifier, opt => opt.MapFrom(entity => entity.Employee.Identifier))
                    .ForMember(model => model.CompanyIdentifier, opt => opt.MapFrom(entity => entity.Employee.Company.Identifier));
                config.CreateMap<Entities.Work, DataTransferObjects.Work>()
                    .ForMember(model => model.EmployeeIdentifier, opt => opt.MapFrom(entity => entity.Employee.Identifier))
                    .ForMember(model => model.CompanyIdentifier, opt => opt.MapFrom(entity => entity.Employee.Company.Identifier))
                    .ForMember(model => model.EmployeeName, opt => opt.MapFrom(entity => entity.Employee.Name))
                    .ForMember(model => model.EmployeeNumber, opt => opt.MapFrom(entity => entity.Employee.EmployeeNumber));

                config.CreateMap<Entities.Deduction, DataTransferObjects.Deduction>()
                    .ForMember(model => model.EmployeeIdentifier, opt => opt.MapFrom(entity => entity.Employee.Identifier))
                    .ForMember(model => model.CompanyIdentifier, opt => opt.MapFrom(entity => entity.Employee.Company.Identifier));
                config.CreateMap<Entities.Employee, DataTransferObjects.Employee>()
                    .ForMember(model => model.CompanyIdentifier, opt => opt.MapFrom(entity => entity.Company.Identifier));
            });
        }
    }
}
