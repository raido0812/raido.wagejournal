namespace Raido.WageJournal.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LimitDateWorkedToBeUniquePerEmployee : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Work", new[] { "EmployeeId" });
            CreateIndex("dbo.Work", new[] { "EmployeeId", "DateWorked" }, unique: true, name: "UX_Employee_DateWorked");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Work", "UX_Employee_DateWorked");
            CreateIndex("dbo.Work", "EmployeeId");
        }
    }
}
