﻿using System;
using System.Runtime.Caching;
using System.Security.Claims;
using System.Threading.Tasks;
using Raido.WageJournal.DataAccess.DataContext;
using Raido.WageJournal.DataAccess.Entities;
using Raido.WageJournal.DataAccess.Security;

namespace Raido.WageJournal.DataAccess.Repositories
{
    internal abstract class RepositoryBase : IDisposable
    {
        private ICustomPrincipalProvider _principalProvider;
        private WageDbContext _wageDbContext;
        private string _currentUserID;
        private ObjectCache _objectCache;

        internal ClaimsPrincipal CurrentUser
        {
            get
            {
                return this._principalProvider.GetCurrentUser();
            }
        }

        internal string CurrentUserID
        {
            get
            {
                if (string.IsNullOrEmpty(this._currentUserID))
                {
                    this._currentUserID = this._principalProvider.GetCurrentUserId();
                }

                return this._currentUserID;
            }
        }

        internal WageDbContext DbContext { get { return this._wageDbContext; } }

        internal ObjectCache Cache { get { return this._objectCache; } }

        internal int DefaultCacheExpiryMinutes { get { return 5; } }

        internal string DefaultCacheRegion { get { return null; } }

        internal RepositoryBase(ICustomPrincipalProvider principalProvider, ObjectCache cache)
        {
            this._principalProvider = principalProvider;
            this._wageDbContext = new WageDbContext(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            this._objectCache = cache;
        }

        internal RepositoryBase(WageDbContext wageDbContext, ICustomPrincipalProvider principalProvider, ObjectCache cache)
        {
            this._principalProvider = principalProvider;
            this._wageDbContext = wageDbContext;
            this._objectCache = cache;
        }

        protected async Task<T> GetFromCacheIfAvailable<T>(Func<Task<T>> getFromDataStore, string cacheKey)
        {
            return await this.GetFromCacheIfAvailable(getFromDataStore, cacheKey, this.DefaultCacheRegion, this.DefaultCacheExpiryMinutes);
        }

        protected async Task<T> GetFromCacheIfAvailable<T>(Func<Task<T>> getFromDataStore, string cacheKey, string cacheRegion)
        {
            return await this.GetFromCacheIfAvailable(getFromDataStore, cacheKey, cacheRegion, this.DefaultCacheExpiryMinutes);
        }

        protected async Task<T> GetFromCacheIfAvailable<T>(Func<Task<T>> getFromDataStore, string cacheKey, string cacheRegion, int cacheExpirationMinutes)
        {
            T result = default(T);
            if (this.Cache != null &&
                this.Cache.Contains(cacheKey, cacheRegion))
            {
                result = (T)this.Cache.GetCacheItem(cacheKey, cacheRegion).Value;
            }
            else
            {
                result = await getFromDataStore();

                // cache adjustments for the company
                if (this.Cache != null)
                {
                    this.Cache.Add(
                        key: cacheKey,
                        value: result,
                        absoluteExpiration: DateTimeOffset.Now.AddMinutes(this.DefaultCacheExpiryMinutes),
                        regionName: this.DefaultCacheRegion);
                }
            }

            return result;
        }

        protected async Task DeleteAndRemoveFromCache<T>(Func<Task<T>> entityFindingLogic, string cacheKey) where T : EntityBase
        {
            await this.DeleteAndRemoveFromCache(entityFindingLogic, cacheKey, this.DefaultCacheRegion);
        }

        protected async Task DeleteAndRemoveFromCache<T>(Func<Task<T>> entityFindingLogic, string cacheKey, string cacheRegion) where T : EntityBase
        {
            var entity = await entityFindingLogic();

            if (entity != null)
            {
                entity.IsDeleted = true;
                entity.DeletedDate = DateTime.UtcNow;
                await this.DbContext.SaveChangesAsync();

                if (this.Cache != null && this.Cache.Contains(cacheKey, cacheRegion))
                {
                    this.Cache.Remove(cacheKey, cacheRegion);
                }
            }
        }

        public void Dispose()
        {
            if (this._wageDbContext != null)
            {
                this._wageDbContext.Dispose();
                this._wageDbContext = null;
            }
        }
    }
}
