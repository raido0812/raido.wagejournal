﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using Raido.WageJournal.Contracts.SearchFilters;
using Raido.WageJournal.DataAccess.DataContext;
using Raido.WageJournal.DataAccess.Repositories.Interfaces;
using Raido.WageJournal.DataAccess.Security;
using DataTransferObjects = Raido.WageJournal.Contracts.DataTransferObjects;

namespace Raido.WageJournal.DataAccess.Repositories
{
    internal class PolicyRepository : RepositoryBase, IPolicyRepository
    {
        private const string CacheKeyPrefix = "PoliciesForCompany";

        public PolicyRepository(ICustomPrincipalProvider principalProvider, ObjectCache cache)
            : base(principalProvider, cache)
        {
        }

        public PolicyRepository(WageDbContext wageDbContext, ICustomPrincipalProvider principalProvider, ObjectCache cache)
            : base(wageDbContext, principalProvider, cache)
        {
        }

        public async Task<DataTransferObjects.Policy> GetPolicyAsync(Guid policyIdentifier, Guid companyIdentifier)
        {
            var policies = await this.GetPoliciesAsync(new PolicySearchOptions { PolicyIdentifier = policyIdentifier }, companyIdentifier);
            return policies.FirstOrDefault();
        }

        public async Task<IList<DataTransferObjects.Policy>> GetPoliciesAsync(PolicySearchOptions policySearchOptions, Guid companyIdentifier)
        {
            var cacheKey = CacheKeyPrefix + companyIdentifier.ToString();

            IList<DataTransferObjects.Policy> policiesForCompany = await this.GetFromCacheIfAvailable(
                async () =>
                {
                    var policies = await (from pol in this.DbContext.Policies
                                          let company = pol.Company
                                          let users = company.CompanyUsers
                                          where
                                              pol.Company.Identifier == companyIdentifier &&
                                              !pol.IsDeleted &&
                                              users.Any(u => u.UserId == this.CurrentUserID)
                                          select
                                              pol)
                                        .ProjectTo<DataTransferObjects.Policy>()
                                        .ToListAsync();

                    foreach (var policy in policies)
                    {
                        if (policy.Value != default(decimal))
                        {
                            switch (policy.PolicyType)
                            {
                                case Contracts.Enumerations.PolicyType.UnemploymentInsuranceFundPercentage:
                                    policy.Value = policy.Value * 100;
                                    break;
                                default:
                                    policy.Value = policy.Value;
                                    break;
                            }
                        }
                    }

                    return policies;
                },
                cacheKey);

            if (policySearchOptions != null && policySearchOptions.PolicyIdentifier.HasValue)
            {
                return policiesForCompany.Where(pol => pol.Identifier == (policySearchOptions.PolicyIdentifier ?? pol.Identifier)).ToList();
            }

            return policiesForCompany;
        }

        public async Task<DataTransferObjects.Policy> SavePolicyAsync(DataTransferObjects.Policy policy, Guid companyIdentifier)
        {
            if (policy == null)
            {
                throw new ArgumentNullException("policy");
            }

            Entities.Company companyEntity = this.DbContext.Companies.Where(c => c.Identifier == companyIdentifier).Single();
            Entities.Policy policyEntity = this.DbContext.Policies
                .Where(p =>
                    (p.Identifier == policy.Identifier || p.PolicyId == policy.PolicyId) &&
                    p.Company.Identifier == companyIdentifier).FirstOrDefault();

            if (policyEntity == null)
            {
                policyEntity = new Entities.Policy();
                policyEntity.Identifier = policy.Identifier;
                policy.IsActive = true;
                this.DbContext.Policies.Add(policyEntity);
            }
            else
            {
                policyEntity.IsActive = policy.IsActive;
            }

            policyEntity.ModifiedDate = DateTime.UtcNow;
            policyEntity.CompanyId = companyEntity.CompanyId;
            policyEntity.PolicyType = policy.PolicyType;
            policyEntity.Setting = policy.Setting;

            switch (policy.PolicyType)
            {
                case Contracts.Enumerations.PolicyType.UnemploymentInsuranceFundPercentage:
                    policyEntity.Value = policy.Value / 100;
                    break;
                default:
                    policyEntity.Value = policy.Value;
                    break;
            }

            await this.DbContext.SaveChangesAsync();

            policy.Identifier = policyEntity.Identifier;
            policy.PolicyId = policyEntity.PolicyId;
            policy.CompanyId = policyEntity.CompanyId;
            policy.IsActive = policyEntity.IsActive;
            policy.IsDeleted = policyEntity.IsDeleted;
            policy.ModifiedDate = policyEntity.ModifiedDate;
            policy.DeletedDate = policyEntity.DeletedDate;

            var cacheKey = CacheKeyPrefix + companyIdentifier.ToString();
            if (this.Cache != null && this.Cache.Contains(cacheKey, this.DefaultCacheRegion))
            {
                this.Cache.Remove(cacheKey, this.DefaultCacheRegion);
            }

            return policy;
        }

        public async Task<bool> DeletePolicyAsync(Guid policyIdentifier, Guid companyIdentifier)
        {
            if (policyIdentifier == Guid.Empty)
            {
                throw new ArgumentNullException("policyIdentifier");
            }

            if (companyIdentifier == Guid.Empty)
            {
                throw new ArgumentNullException("companyIdentifier");
            }

            var cacheKey = CacheKeyPrefix + companyIdentifier.ToString();
            await this.DeleteAndRemoveFromCache(async () => await this.DbContext.Policies
                .Where(p =>
                    p.Identifier == policyIdentifier &&
                    p.Company.Identifier == companyIdentifier).FirstOrDefaultAsync(),
                cacheKey);

            return true;
        }
    }
}
