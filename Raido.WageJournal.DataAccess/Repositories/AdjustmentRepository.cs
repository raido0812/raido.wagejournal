﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using Raido.WageJournal.Contracts.SearchFilters;
using Raido.WageJournal.DataAccess.DataContext;
using Raido.WageJournal.DataAccess.Repositories.Interfaces;
using Raido.WageJournal.DataAccess.Security;
using DataTransferObjects = Raido.WageJournal.Contracts.DataTransferObjects;
namespace Raido.WageJournal.DataAccess.Repositories
{
    internal class AdjustmentRepository : RepositoryBase, IAdjustmentRepository
    {
        #region Fields

        private const string EmployeeAdjustmentCacheKeyPrefix = "AdjustmentsForCompany";

        #endregion Fields

        #region Constructors
        public AdjustmentRepository(ICustomPrincipalProvider principalProvider, ObjectCache cache)
            : base(principalProvider, cache)
        {
        }

        public AdjustmentRepository(WageDbContext wageDbContext, ICustomPrincipalProvider principalProvider, ObjectCache cache)
            : base(wageDbContext, principalProvider, cache)
        {
        }
        #endregion Constructors

        #region Methods

        public async Task<DataTransferObjects.Adjustment> GetAdjustmentAsync(Guid adjustmentIdentifier, Guid employeeIdentifier, Guid companyIdentifier)
        {
            var results = await this.GetAdjustmentsAsync(new AdjustmentSearchOptions { AdjustmentIdentifier = adjustmentIdentifier, EmployeeIdentifier = employeeIdentifier, }, companyIdentifier);
            return results.FirstOrDefault();
        }

        public async Task<IList<DataTransferObjects.Adjustment>> GetAdjustmentsAsync(AdjustmentSearchOptions adjustmentSearchOptions, Guid companyIdentifier)
        {
            var cacheKey = EmployeeAdjustmentCacheKeyPrefix + companyIdentifier.ToString();
            IList<DataTransferObjects.Adjustment> adjustmentForCompany = null;

            adjustmentForCompany = await this.GetFromCacheIfAvailable(
                async () =>
                {
                    return await (from adjustment in this.DbContext.Adjustments
                                  let emp = adjustment.Employee
                                  let company = emp.Company
                                  let users = company.CompanyUsers
                                  where
                                      company.Identifier == companyIdentifier &&
                                      !emp.IsDeleted &&
                                      !adjustment.IsDeleted &&
                                      users.Any(u => u.UserId == this.CurrentUserID)
                                  select
                                      adjustment)
                            .ProjectTo<DataTransferObjects.Adjustment>()
                            .OrderByDescending(adjustment => adjustment.DateOfIssuance)
                            .ToListAsync();
                },
                cacheKey);

            if (adjustmentSearchOptions != null)
            {
                return adjustmentForCompany
                    .Where(adjustment =>
                            adjustment.Identifier == (adjustmentSearchOptions.AdjustmentIdentifier ?? adjustment.Identifier) &&
                            adjustment.EmployeeIdentifier == (adjustmentSearchOptions.EmployeeIdentifier ?? adjustment.EmployeeIdentifier) &&
                            (adjustment.DateOfIssuance ?? DateTime.Today) >= (adjustmentSearchOptions.FromDate ?? adjustment.DateOfIssuance ?? DateTime.Today) &&
                            (adjustment.DateOfIssuance ?? DateTime.Today) <= (adjustmentSearchOptions.ToDate ?? adjustment.DateOfIssuance ?? DateTime.Today))
                    .OrderByDescending(adjustment => adjustment.DateOfIssuance)
                    .ToList();
            }

            return adjustmentForCompany;
        }

        public async Task<DataTransferObjects.Adjustment> SaveAdjustmentAsync(DataTransferObjects.Adjustment adjustment, Guid employeeIdentifier, Guid companyIdentifier)
        {
            if (adjustment == null)
            {
                throw new ArgumentNullException("adjustment");
            }

            Entities.Employee employeeEntity = await this.DbContext.Employees.Where(e => e.Identifier == employeeIdentifier).SingleAsync();
            Entities.Adjustment adjustmentEntity = await this.DbContext.Adjustments
                .Where(i =>
                    i.Identifier == adjustment.Identifier || i.AdjustmentId == adjustment.AdjustmentId &&
                    i.Employee.Identifier == employeeIdentifier &&
                    i.Employee.Company.Identifier == companyIdentifier).FirstOrDefaultAsync();

            if (adjustmentEntity == null)
            {
                adjustmentEntity = new Entities.Adjustment();
                adjustmentEntity.Identifier = adjustment.Identifier;
                adjustment.IsActive = true;
                this.DbContext.Adjustments.Add(adjustmentEntity);
            }
            else
            {
                adjustmentEntity.IsActive = adjustment.IsActive;
            }

            adjustmentEntity.ModifiedDate = DateTime.UtcNow;
            adjustmentEntity.Amount = adjustment.Amount ?? 0;
            if (adjustment.DateOfIssuance.HasValue)
            {
                adjustmentEntity.DateOfIssuance = adjustment.DateOfIssuance.Value;
            }

            adjustmentEntity.Description = adjustment.Description;
            adjustmentEntity.EmployeeId = employeeEntity.EmployeeId;
            adjustmentEntity.Employee = employeeEntity;

            await this.DbContext.SaveChangesAsync();
            adjustment.Identifier = adjustmentEntity.Identifier;
            adjustment.AdjustmentId = adjustmentEntity.AdjustmentId;
            adjustment.IsActive = adjustmentEntity.IsActive;
            adjustment.IsDeleted = adjustmentEntity.IsDeleted;
            adjustment.ModifiedDate = adjustmentEntity.ModifiedDate;
            adjustment.DeletedDate = adjustmentEntity.DeletedDate;

            var cacheKey = EmployeeAdjustmentCacheKeyPrefix + companyIdentifier.ToString();
            if (this.Cache != null && this.Cache.Contains(cacheKey, this.DefaultCacheRegion))
            {
                this.Cache.Remove(cacheKey, this.DefaultCacheRegion);
            }

            return adjustment;
        }

        public async Task DeleteAdjustmentAsync(Guid adjustmentIdentifier, Guid employeeIdentifier, Guid companyIdentifier)
        {
            if (adjustmentIdentifier == Guid.Empty)
            {
                throw new ArgumentNullException("adjustmentIdentifier");
            }

            var companyEmployeeAdjustmentsCacheKey = EmployeeAdjustmentCacheKeyPrefix + companyIdentifier.ToString();

            await this.DeleteAndRemoveFromCache(async () => await this.DbContext.Adjustments
                .Where(i =>
                    i.Identifier == adjustmentIdentifier &&
                    i.Employee.Identifier == employeeIdentifier &&
                    i.Employee.Company.Identifier == companyIdentifier).FirstOrDefaultAsync(),
                companyEmployeeAdjustmentsCacheKey);
        }

        #endregion Methods
    }
}
