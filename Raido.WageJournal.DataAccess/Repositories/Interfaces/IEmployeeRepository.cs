﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Raido.WageJournal.Contracts.SearchFilters;
using DataTransferObjects = Raido.WageJournal.Contracts.DataTransferObjects;

namespace Raido.WageJournal.DataAccess.Repositories.Interfaces
{
    public interface IEmployeeRepository
    {
        Task<DataTransferObjects.Employee> GetEmployeeAsync(Guid employeeIdentifier, Guid companyIdentifier);

        Task<IList<DataTransferObjects.Employee>> GetEmployeesAsync(EmployeeSearchOptions employeeSearchOptions, Guid companyIdentifier);

        Task<DataTransferObjects.Employee> SaveEmployeeAsync(DataTransferObjects.Employee employee, Guid companyIdentifier);

        Task DeleteEmployeeAsync(Guid employeeIdentifier, Guid companyIdentifier);
    }
}
