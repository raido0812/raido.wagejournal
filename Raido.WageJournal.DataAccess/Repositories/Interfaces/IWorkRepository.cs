﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Raido.WageJournal.Contracts.SearchFilters;
using DataTransferObjects = Raido.WageJournal.Contracts.DataTransferObjects;
namespace Raido.WageJournal.DataAccess.Repositories.Interfaces
{
    public interface IWorkRepository
    {
        Task<DataTransferObjects.Work> GetWorkAsync(Guid workIdentifier, Guid employeeIdentifier, Guid companyIdentifier);

        Task<IList<DataTransferObjects.Work>> GetWorkAsync(WorkSearchOptions workSearchOptions, Guid companyIdentifier);

        Task<DataTransferObjects.Work> SaveWorkAsync(DataTransferObjects.Work Work, Guid employeeIdentifier, Guid companyIdentifier);

        Task DeleteWorkAsync(Guid workIdentifier, Guid employeeIdentifier, Guid companyIdentifier);
    }
}
