﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Raido.WageJournal.Contracts.SearchFilters;
using Raido.WageJournal.Contracts.DataTransferObjects;
namespace Raido.WageJournal.DataAccess.Repositories.Interfaces
{
    public interface IAdjustmentRepository
    {
        Task<Adjustment> GetAdjustmentAsync(Guid adjustmentIdentifier, Guid employeeIdentifier, Guid companyIdentifier);

        Task<IList<Adjustment>> GetAdjustmentsAsync(AdjustmentSearchOptions adjustmentSearchOptions, Guid companyIdentifier);

        Task<Adjustment> SaveAdjustmentAsync(Adjustment adjustment, Guid employeeIdentifier, Guid companyIdentifier);

        Task DeleteAdjustmentAsync(Guid adjustmentIdentifier, Guid employeeIdentifier, Guid companyIdentifier);
    }
}
