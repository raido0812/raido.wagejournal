﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raido.WageJournal.Contracts.SearchFilters;
using DataTransferObjects = Raido.WageJournal.Contracts.DataTransferObjects;
namespace Raido.WageJournal.DataAccess.Repositories.Interfaces
{
    public interface ICompanyRepository
    {
        Task<Guid> AddCompanyForNewUserAsync(string userId, string companyName);

        Task<Guid> AddCompanyForCurrentUserAsync(Guid companyIdentifier, string companyName);

        Task<DataTransferObjects.Company> GetCompanyForCurrentUserAsync(Guid companyIdentifier);

        Task<IList<DataTransferObjects.Company>> GetCompaniesForCurrentUserAsync();

        Task<IList<DataTransferObjects.Company>> GetCompaniesAsync(CompanySearchOptions companySearchOptions, string userId);

        Task<Guid> GetLastViewedCompanyIdentifierAsync();
        
        Task UpdateLastViewedCompanyIdentifierAsync(Guid identifier);

        Task<bool> CompanyExistsAsync(Guid identifier);

        Task<DataTransferObjects.Company> SaveCompanyAsync(DataTransferObjects.Company companyDetails);
    }
}
