﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Raido.WageJournal.Contracts.SearchFilters;
using DataTransferObjects = Raido.WageJournal.Contracts.DataTransferObjects;
namespace Raido.WageJournal.DataAccess.Repositories.Interfaces
{
    public interface IDeductionRepository
    {
        Task<DataTransferObjects.Deduction> GetDeductionAsync(Guid deductionIdentifier, Guid employeeIdentifier, Guid companyIdentifier);

        Task<IList<DataTransferObjects.Deduction>> GetDeductionsAsync(DeductionSearchOptions deductionSearchOptions, Guid companyIdentifier);

        Task<DataTransferObjects.Deduction> SaveDeductionAsync(DataTransferObjects.Deduction deduction, Guid employeeIdentifier, Guid companyIdentifier);

        Task DeleteDeductionAsync(Guid deductionIdentifier, Guid employeeIdentifier, Guid companyIdentifier);
    }
}
