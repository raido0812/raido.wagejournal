﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Raido.WageJournal.Contracts.SearchFilters;
using DataTransferObjects = Raido.WageJournal.Contracts.DataTransferObjects;

namespace Raido.WageJournal.DataAccess.Repositories.Interfaces
{
    public interface IPolicyRepository
    {
        Task<DataTransferObjects.Policy> GetPolicyAsync(Guid policyIdentifier, Guid companyIdentifier);
        Task<IList<DataTransferObjects.Policy>> GetPoliciesAsync(PolicySearchOptions policySearchOptions, Guid companyIdentifier);
        Task<DataTransferObjects.Policy> SavePolicyAsync(DataTransferObjects.Policy policy, Guid companyIdentifier);
        Task<bool> DeletePolicyAsync(Guid policyIdentifier, Guid companyIdentifier);
    }
}
