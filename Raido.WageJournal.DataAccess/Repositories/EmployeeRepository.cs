﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using Raido.WageJournal.Contracts.SearchFilters;
using Raido.WageJournal.DataAccess.DataContext;
using Raido.WageJournal.DataAccess.Repositories.Interfaces;
using Raido.WageJournal.DataAccess.Security;
using DataTransferObjects = Raido.WageJournal.Contracts.DataTransferObjects;

namespace Raido.WageJournal.DataAccess.Repositories
{
    internal class EmployeeRepository : RepositoryBase, IEmployeeRepository
    {
        #region Fields

        private const string CacheKeyPrefix = "EmployeesForCompany";

        #endregion Fields

        #region Constructor

        public EmployeeRepository(ICustomPrincipalProvider principalProvider, ObjectCache cache)
            : base(principalProvider, cache)
        {
        }

        public EmployeeRepository(WageDbContext wageDbContext, ICustomPrincipalProvider principalProvider, ObjectCache cache)
            : base(wageDbContext, principalProvider, cache)
        {
        }

        #endregion Constructor

        #region Methods

        public async Task<DataTransferObjects.Employee> GetEmployeeAsync(Guid employeeIdentifier, Guid companyIdentifier)
        {
            var employees = await this.GetEmployeesAsync(new EmployeeSearchOptions { EmployeeIdentifier = employeeIdentifier }, companyIdentifier);
            return employees.FirstOrDefault();
        }

        public async Task<IList<DataTransferObjects.Employee>> GetEmployeesAsync(EmployeeSearchOptions employeeSearchOptions, Guid companyIdentifier)
        {
            var cacheKey = CacheKeyPrefix + companyIdentifier.ToString();
            IList<DataTransferObjects.Employee> employeesForCompany = await this.GetFromCacheIfAvailable(
                async () => await (from emp in this.DbContext.Employees
                                   let company = emp.Company
                                   let users = company.CompanyUsers
                                   where
                                       emp.Company.Identifier == companyIdentifier &&
                                       !emp.IsDeleted &&
                                       users.Any(u => u.UserId == this.CurrentUserID)
                                   select
                                       emp)
                        .ProjectTo<DataTransferObjects.Employee>()
                        .ToListAsync(),
                cacheKey);
            int temp = 0;
            if (employeeSearchOptions != null)
            {
                return employeesForCompany
                    .Where(emp => emp.Identifier == (employeeSearchOptions.EmployeeIdentifier ?? emp.Identifier))
                    .OrderBy(emp =>int.TryParse(emp.EmployeeNumber, out temp) ? int.Parse(emp.EmployeeNumber) : 0)
                    .ThenBy(emp => emp.Name)
                    .ToList();
            }

            return employeesForCompany
                .OrderBy(emp => int.TryParse(emp.EmployeeNumber, out temp) ? int.Parse(emp.EmployeeNumber) : 0)
                .ThenBy(emp => emp.Name)
                .ToList();
        }

        public async Task<DataTransferObjects.Employee> SaveEmployeeAsync(DataTransferObjects.Employee employee, Guid companyIdentifier)
        {
            if (employee == null)
            {
                throw new ArgumentNullException("employee");
            }

            Entities.Company companyEntity = await this.DbContext.Companies.Where(c => c.Identifier == companyIdentifier).SingleAsync();
            Entities.Employee employeeEntity = await this.DbContext.Employees
                .Where(e =>
                    e.Identifier == employee.Identifier || e.EmployeeId == employee.EmployeeId &&
                    e.Company.Identifier == companyIdentifier).FirstOrDefaultAsync();

            if (employeeEntity == null)
            {
                employeeEntity = new Entities.Employee();
                employeeEntity.Identifier = employee.Identifier;
                employee.IsActive = true;
                if (await this.DbContext.Employees.AnyAsync())
                {
                    employee.EmployeeNumber = ((await this.DbContext.Employees.Select(e => e.EmployeeId).MaxAsync()) + 1).ToString();
                }
                else
                {
                    employee.EmployeeNumber = "1";
                }
                this.DbContext.Employees.Add(employeeEntity);
            }
            else
            {
                employeeEntity.IsActive = employee.IsActive;
            }

            employeeEntity.ModifiedDate = DateTime.UtcNow;
            employeeEntity.Email = employee.Email;
            employeeEntity.CompanyId = companyEntity.CompanyId;
            employeeEntity.Name = employee.Name;
            employeeEntity.EmployeeNumber = employee.EmployeeNumber;
            employeeEntity.EmploymentStartDate = employee.EmploymentStartDate;
            employeeEntity.EmploymentEndDate = employee.EmploymentEndDate;
            employeeEntity.PhoneNumber = employee.PhoneNumber;
            if (employee.HourlyRate.HasValue)
            {
                employeeEntity.HourlyRate = employee.HourlyRate.Value;
            }

            await this.DbContext.SaveChangesAsync();

            employee.Identifier = employeeEntity.Identifier;
            employee.EmployeeId = employeeEntity.EmployeeId;
            employee.IsActive = employeeEntity.IsActive;
            employee.IsDeleted = employeeEntity.IsDeleted;
            employee.ModifiedDate = employeeEntity.ModifiedDate;
            employee.DeletedDate = employeeEntity.DeletedDate;

            var cacheKey = CacheKeyPrefix + companyIdentifier.ToString();
            if (this.Cache != null && this.Cache.Contains(cacheKey, this.DefaultCacheRegion))
            {
                this.Cache.Remove(cacheKey, this.DefaultCacheRegion);
            }

            return employee;
        }

        public async Task DeleteEmployeeAsync(Guid employeeIdentifier, Guid companyIdentifier)
        {
            if (employeeIdentifier == Guid.Empty)
            {
                throw new ArgumentNullException("employeeIdentifier");
            }

            var cacheKey = CacheKeyPrefix + companyIdentifier.ToString();
            await this.DeleteAndRemoveFromCache(async () => await this.DbContext.Employees
                .Where(e =>
                    e.Identifier == employeeIdentifier &&
                    e.Company.Identifier == companyIdentifier).FirstOrDefaultAsync(),
                cacheKey);
        }

        #endregion Methods
    }
}
