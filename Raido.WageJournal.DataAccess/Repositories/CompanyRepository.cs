﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using Raido.WageJournal.Contracts.SearchFilters;
using Raido.WageJournal.DataAccess.DataContext;
using Raido.WageJournal.DataAccess.Repositories.Interfaces;
using Raido.WageJournal.DataAccess.Security;
using DataTransferObjects = Raido.WageJournal.Contracts.DataTransferObjects;

namespace Raido.WageJournal.DataAccess.Repositories
{
    internal class CompanyRepository : RepositoryBase, ICompanyRepository
    {
        #region Fields

        private const string CacheKeyPrefix = "CompaniesForUser";

        #endregion Fields

        #region Constructors
        public CompanyRepository(ICustomPrincipalProvider principalProvider, ObjectCache cache)
            : base(principalProvider, cache)
        {
        }

        public CompanyRepository(WageDbContext wageDbContext, ICustomPrincipalProvider principalProvider, ObjectCache cache)
            : base(wageDbContext, principalProvider, cache)
        {
        }
        #endregion Constructors

        #region Methods

        public async Task<Guid> AddCompanyForNewUserAsync(string userId, string companyName)
        {
            ApplicationUser newUser = await this.DbContext.Users.FirstOrDefaultAsync(u => u.Id == userId);
            Entities.Company newCompany = new Entities.Company();
            newCompany.Identifier = Guid.NewGuid();
            newCompany.FullName = companyName;
            newCompany.IsActive = true;
            newCompany.IsDeleted = false;
            newCompany.ModifiedDate = DateTime.UtcNow;
            newCompany.TradeName = companyName;

            Entities.CompanyUser companyUser = new Entities.CompanyUser();
            companyUser.Identifier = Guid.NewGuid();
            companyUser.IsActive = true;
            companyUser.IsDeleted = false;
            companyUser.ModifiedDate = DateTime.UtcNow;
            companyUser.User = newUser;
            companyUser.IsCurrentDisplayed = true;

            newCompany.CompanyUsers.Add(companyUser);

            this.DbContext.Companies.Add(newCompany);
            await this.DbContext.SaveChangesAsync();

            var cacheKey = CacheKeyPrefix + this.CurrentUserID;
            if (this.Cache != null && this.Cache.Contains(cacheKey, this.DefaultCacheRegion))
            {
                this.Cache.Remove(cacheKey, this.DefaultCacheRegion);
            }

            return newCompany.Identifier;
        }

        public async Task<Guid> AddCompanyForCurrentUserAsync(Guid companyIdentifier, string companyName)
        {
            if (companyIdentifier == Guid.Empty)
            {
                throw new ArgumentNullException("companyIdentifier");
            }

            ApplicationUser currentUser = await this.DbContext.Users.FirstOrDefaultAsync(u => u.Id == this.CurrentUserID);
            Entities.Company newCompany = await this.DbContext.Companies.FirstOrDefaultAsync(c => c.Identifier == companyIdentifier);

            if (newCompany == null)
            {
                newCompany = new Entities.Company();
                this.DbContext.Companies.Add(newCompany);
                newCompany.Identifier = companyIdentifier;
            }

            newCompany.FullName = companyName;
            newCompany.IsActive = true;
            newCompany.IsDeleted = false;
            newCompany.ModifiedDate = DateTime.UtcNow;
            newCompany.TradeName = companyName;

            newCompany.CompanyUsers = newCompany.CompanyUsers ?? new List<Entities.CompanyUser>();
            if (!newCompany.CompanyUsers.Any(cu => cu.UserId == currentUser.Id))
            {
                Entities.CompanyUser companyUser = new Entities.CompanyUser();
                companyUser.Identifier = Guid.NewGuid();
                companyUser.IsActive = true;
                companyUser.IsDeleted = false;
                companyUser.ModifiedDate = DateTime.UtcNow;
                companyUser.UserId = currentUser.Id;
                companyUser.User = currentUser;
                companyUser.IsCurrentDisplayed = true;

                newCompany.CompanyUsers.Add(companyUser);
            }

            var errors = this.DbContext.GetValidationErrors();
            await this.DbContext.SaveChangesAsync();

            var cacheKey = CacheKeyPrefix + this.CurrentUserID;
            if (this.Cache != null && this.Cache.Contains(cacheKey, this.DefaultCacheRegion))
            {
                this.Cache.Remove(cacheKey, this.DefaultCacheRegion);
            }

            return newCompany.Identifier;
        }

        public async Task<DataTransferObjects.Company> GetCompanyForCurrentUserAsync(Guid companyIdentifier)
        {
            var companies = await this.GetCompaniesAsync(new CompanySearchOptions { CompanyIdentifier = companyIdentifier }, this.CurrentUserID);
            return companies.FirstOrDefault();
        }

        public async Task<IList<DataTransferObjects.Company>> GetCompaniesForCurrentUserAsync()
        {
            return await this.GetCompaniesAsync(null, this.CurrentUserID);
        }

        public async Task<IList<DataTransferObjects.Company>> GetCompaniesAsync(CompanySearchOptions companySearchOptions, string userId)
        {
            IList<DataTransferObjects.Company> companiesForUser = null;
            var cacheKey = CacheKeyPrefix + this.CurrentUserID;

            var companyIds = await this.DbContext.CompanyUsers
                .Where(item => item.UserId == userId)
                .Select(cu => cu.CompanyId)
                .ToListAsync();

            companiesForUser = await this.GetFromCacheIfAvailable(
                async () =>
                {
                    return await (from item in this.DbContext.Companies
                                  where
                                      companyIds.Contains(item.CompanyId) &&
                                      !item.IsDeleted
                                  select
                                      item)
                                  .ProjectTo<DataTransferObjects.Company>()
                                  .ToListAsync();
                },
                cacheKey);

            if (companySearchOptions != null && companySearchOptions.CompanyIdentifier.HasValue)
            {
                return companiesForUser
                    .Where(item => item.Identifier == (companySearchOptions.CompanyIdentifier ?? item.Identifier))
                    .ToList();
            }

            return companiesForUser;
        }

        public async Task<Guid> GetLastViewedCompanyIdentifierAsync()
        {
            var cacheKey = CacheKeyPrefix + this.CurrentUserID + "LastViewed";
            Guid lastViewedCompanyIdentifier = await this.GetFromCacheIfAvailable(
                async () =>
                {
                    return await this.DbContext.CompanyUsers
                            .Where(cu => cu.UserId == this.CurrentUserID)
                            .OrderBy(cu => cu.IsCurrentDisplayed ? 0 : 1)
                            .Select(cu => cu.Company.Identifier)
                            .FirstOrDefaultAsync();
                },
                cacheKey);

            return lastViewedCompanyIdentifier;
        }

        public async Task UpdateLastViewedCompanyIdentifierAsync(Guid identifier)
        {
            int selectedCompanyId = await this.DbContext.Companies
                .Where(c => c.Identifier == identifier)
                .Select(c => c.CompanyId)
                .FirstOrDefaultAsync();

            var companyUser = await this.DbContext.CompanyUsers
               .Where(cu => cu.UserId == this.CurrentUserID)
               .ToListAsync();

            if (companyUser != null && companyUser.Count > 0)
            {
                foreach (var companyForUser in companyUser)
                {
                    companyForUser.IsCurrentDisplayed = companyForUser.CompanyId == selectedCompanyId;
                }
            }

            this.DbContext.SaveChanges();

            var cacheKey = CacheKeyPrefix + this.CurrentUserID + "LastViewed";
            if (this.Cache != null && this.Cache.Contains(cacheKey, this.DefaultCacheRegion))
            {
                this.Cache.Remove(cacheKey, this.DefaultCacheRegion);
            }
        }

        public async Task<bool> CompanyExistsAsync(Guid identifier)
        {
            return await this.DbContext.CompanyUsers
                    .AnyAsync(cu => cu.UserId == this.CurrentUserID && cu.Company.Identifier == identifier);
        }

        public async Task<DataTransferObjects.Company> SaveCompanyAsync(DataTransferObjects.Company companyDetails)
        {
            var companyEntity = await this.DbContext.Companies.Where(c => c.Identifier == companyDetails.Identifier || c.CompanyId == companyDetails.CompanyId).FirstOrDefaultAsync();
            if (companyEntity == null)
            {
                companyEntity = new Entities.Company();
                companyEntity.Identifier = companyDetails.Identifier;
                companyDetails.IsActive = true;
                this.DbContext.Companies.Add(companyEntity);
            }
            else
            {
                companyDetails.IsActive = companyEntity.IsActive;
            }

            companyEntity.ModifiedDate = DateTime.UtcNow;
            companyEntity.TradeName = companyDetails.TradeName;
            companyEntity.FullName = companyDetails.FullName;

            await this.DbContext.SaveChangesAsync();
            companyDetails.Identifier = companyEntity.Identifier;
            companyDetails.CompanyId = companyEntity.CompanyId;
            companyDetails.IsActive = companyEntity.IsActive;
            companyDetails.IsDeleted = companyEntity.IsDeleted;
            companyDetails.ModifiedDate = companyEntity.ModifiedDate;
            companyDetails.DeletedDate = companyEntity.DeletedDate;

            var cacheKey = CacheKeyPrefix + this.CurrentUserID;
            if (this.Cache !=null && this.Cache.Contains(cacheKey, this.DefaultCacheRegion))
            {
                this.Cache.Remove(cacheKey, this.DefaultCacheRegion);
            }

            return companyDetails;
        }

        #endregion Methods
    }
}
