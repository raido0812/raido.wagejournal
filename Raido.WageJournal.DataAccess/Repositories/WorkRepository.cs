﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using Raido.WageJournal.Contracts.SearchFilters;
using Raido.WageJournal.DataAccess.DataContext;
using Raido.WageJournal.DataAccess.Repositories.Interfaces;
using Raido.WageJournal.DataAccess.Security;
using DataTransferObjects = Raido.WageJournal.Contracts.DataTransferObjects;

namespace Raido.WageJournal.DataAccess.Repositories
{
    internal class WorkRepository : RepositoryBase, IWorkRepository
    {
        #region Fields

        private const string CompanyWorkCacheKeyPrefix = "WorkForCompany";

        #endregion Fields

        #region Constructor

        public WorkRepository(ICustomPrincipalProvider principalProvider, ObjectCache cache)
            : base(principalProvider, cache)
        {
        }

        public WorkRepository(WageDbContext wageDbContext, ICustomPrincipalProvider principalProvider, ObjectCache cache)
            : base(wageDbContext, principalProvider, cache)
        {
        }

        #endregion Constructor

        #region Methods

        public async Task<DataTransferObjects.Work> GetWorkAsync(Guid workIdentifier, Guid employeeIdentifier, Guid companyIdentifier)
        {
            var results = await this.GetWorkAsync(new WorkSearchOptions { WorkIdentifier = workIdentifier, EmployeeIdentifier = employeeIdentifier }, companyIdentifier);
            return results.FirstOrDefault();
        }

        public async Task<IList<DataTransferObjects.Work>> GetWorkAsync(WorkSearchOptions workSearchOptions, Guid companyIdentifier)
        {
            var cacheKey = CompanyWorkCacheKeyPrefix + companyIdentifier.ToString();
            IList<DataTransferObjects.Work> workForCompany = await this.GetFromCacheIfAvailable(
                async () => await (from work in this.DbContext.Work
                                   let emp = work.Employee
                                   let company = emp.Company
                                   let users = company.CompanyUsers
                                   where
                                       emp.Company.Identifier == companyIdentifier &&
                                       !emp.IsDeleted &&
                                       !work.IsDeleted &&
                                       users.Any(u => u.UserId == this.CurrentUserID)
                                   select
                                       work)
                        .ProjectTo<DataTransferObjects.Work>()
                        .ToListAsync(),
                cacheKey);

            int temp = 0;
            if (workSearchOptions != null)
            {
                return workForCompany
                    .Where(work =>
                            work.Identifier == (workSearchOptions.WorkIdentifier ?? work.Identifier) &&
                            work.EmployeeIdentifier == (workSearchOptions.EmployeeIdentifier ?? work.EmployeeIdentifier) &&
                            work.DateWorked >= (workSearchOptions.PeriodStart ?? work.DateWorked) &&
                            work.DateWorked <= (workSearchOptions.PeriodEnd ?? work.DateWorked))
                    .OrderBy(work => int.TryParse(work.EmployeeNumber, out temp) ? int.Parse(work.EmployeeNumber) : 0)
                    .ThenBy(work => work.EmployeeName)
                    .ThenBy(work => work.DateWorked)
                    .ToList();
            }

            return workForCompany
               .OrderBy(work => int.TryParse(work.EmployeeNumber, out temp) ? int.Parse(work.EmployeeNumber) : 0)
               .ThenBy(emp => emp.EmployeeName)
               .ThenBy(work => work.DateWorked)
               .ToList();
        }

        public async Task<DataTransferObjects.Work> SaveWorkAsync(DataTransferObjects.Work work, Guid employeeIdentifier, Guid companyIdentifier)
        {
            if (work == null)
            {
                throw new ArgumentNullException("work");
            }

            Entities.Employee employeeEntity = await this.DbContext.Employees.Where(e => e.Identifier == employeeIdentifier).SingleAsync();
            Entities.Work workEntity = await this.DbContext.Work
                .Where(w =>
                    (
                        w.Identifier == work.Identifier || 
                        w.WorkId == work.WorkId ||
                        w.DateWorked == work.DateWorked // we only ever allow 1 entry per day, so if there's already one for employee, use existing
                    ) &&
                    w.EmployeeId == employeeEntity.EmployeeId &&
                    w.Employee.Company.Identifier == companyIdentifier).FirstOrDefaultAsync();

            if (workEntity == null)
            {
                workEntity = new Entities.Work();
                workEntity.Identifier = work.Identifier;
                work.IsActive = true;
                this.DbContext.Work.Add(workEntity);
            }
            else
            {
                workEntity.IsActive = work.IsActive;
            }

            workEntity.ModifiedDate = DateTime.UtcNow;
            workEntity.DateWorked = work.DateWorked;
            workEntity.EmployeeId = employeeEntity.EmployeeId;
            workEntity.Employee = employeeEntity;
            workEntity.HoursWorked = work.HoursWorked ?? 0;

            await this.DbContext.SaveChangesAsync();

            work.Identifier = workEntity.Identifier;
            work.WorkId = workEntity.WorkId;
            work.EmployeeId = workEntity.EmployeeId;
            work.IsActive = workEntity.IsActive;
            work.IsDeleted = workEntity.IsDeleted;
            work.ModifiedDate = workEntity.ModifiedDate;
            work.DeletedDate = workEntity.DeletedDate;

            var cacheKey = CompanyWorkCacheKeyPrefix + companyIdentifier.ToString();
            if (this.Cache != null && this.Cache.Contains(cacheKey, this.DefaultCacheRegion))
            {
                this.Cache.Remove(cacheKey, this.DefaultCacheRegion);
            }

            return work;
        }

        public async Task DeleteWorkAsync(Guid workIdentifier, Guid employeeIdentifier, Guid companyIdentifier)
        {
            if (workIdentifier == Guid.Empty)
            {
                throw new ArgumentNullException("workIdentifier");
            }

            var cacheKey = CompanyWorkCacheKeyPrefix + companyIdentifier.ToString();
            await this.DeleteAndRemoveFromCache(
                async () => await this.DbContext.Work
                .Where(e =>
                    e.Identifier == workIdentifier &&
                    e.Employee.Identifier == employeeIdentifier &&
                    e.Employee.Company.Identifier == companyIdentifier).FirstOrDefaultAsync(),
                cacheKey);
        }

        #endregion Methods
    }
}
