﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using Raido.WageJournal.Contracts.SearchFilters;
using Raido.WageJournal.DataAccess.DataContext;
using Raido.WageJournal.DataAccess.Repositories.Interfaces;
using Raido.WageJournal.DataAccess.Security;
using DataTransferObjects = Raido.WageJournal.Contracts.DataTransferObjects;
namespace Raido.WageJournal.DataAccess.Repositories
{
    internal class DeductionRepository : RepositoryBase, IDeductionRepository
    {
        #region Fields

        private const string EmployeeDeductionCacheKeyPrefix = "DeductionsForEmployee";

        #endregion Fields

        #region Constructors
        public DeductionRepository(ICustomPrincipalProvider principalProvider, ObjectCache cache)
            : base(principalProvider, cache)
        {
        }

        public DeductionRepository(WageDbContext wageDbContext, ICustomPrincipalProvider principalProvider, ObjectCache cache)
            : base(wageDbContext, principalProvider, cache)
        {
        }
        #endregion Constructors

        #region Methods

        public async Task<DataTransferObjects.Deduction> GetDeductionAsync(Guid deductionIdentifier, Guid employeeIdentifier, Guid companyIdentifier)
        {
            var results = await this.GetDeductionsAsync(new DeductionSearchOptions { DeductionIdentifier = deductionIdentifier, EmployeeIdentifier = employeeIdentifier }, companyIdentifier);
            return results.FirstOrDefault();
        }

        public async Task<IList<DataTransferObjects.Deduction>> GetDeductionsAsync(DeductionSearchOptions deductionSearchOptions, Guid companyIdentifier)
        {
            var cacheKey = EmployeeDeductionCacheKeyPrefix + companyIdentifier.ToString();
            IList<DataTransferObjects.Deduction> deductionForEmployee = await this.GetFromCacheIfAvailable(
                async () =>
                {
                    return await (from deduction in this.DbContext.Deductions
                                  let emp = deduction.Employee
                                  let company = emp.Company
                                  let users = company.CompanyUsers
                                  where
                                      company.Identifier == companyIdentifier &&
                                      !emp.IsDeleted &&
                                      !deduction.IsDeleted &&
                                      users.Any(u => u.UserId == this.CurrentUserID)
                                  select
                                      deduction)
                            .ProjectTo<DataTransferObjects.Deduction>()
                            .ToListAsync();
                },
                cacheKey);

            if (deductionSearchOptions != null)
            {
                return deductionForEmployee
                    .Where(deduction =>
                            deduction.Identifier == (deductionSearchOptions.DeductionIdentifier ?? deduction.Identifier) &&
                            deduction.EmployeeIdentifier == (deductionSearchOptions.EmployeeIdentifier ?? deduction.EmployeeIdentifier))
                    .OrderByDescending(deduction => deduction.EffectiveFrom)
                    .ToList();
            }

            return deductionForEmployee
                .OrderByDescending(deduction => deduction.EffectiveFrom)
                .ToList();
        }

        public async Task<DataTransferObjects.Deduction> SaveDeductionAsync(DataTransferObjects.Deduction deduction, Guid employeeIdentifier, Guid companyIdentifier)
        {
            if (deduction == null)
            {
                throw new ArgumentNullException("deduction");
            }

            Entities.Company companyEntity = await this.DbContext.Companies.Where(c => c.Identifier == companyIdentifier).SingleAsync();
            Entities.Employee employeeEntity = await this.DbContext.Employees.Where(e => e.Identifier == employeeIdentifier).SingleAsync();
            Entities.Deduction deductionEntity = await this.DbContext.Deductions
                .Where(d =>
                    d.Identifier == deduction.Identifier || d.DeductionId == deduction.DeductionId &&
                    d.Employee.Identifier == employeeIdentifier &&
                    d.Employee.Company.Identifier == companyIdentifier).FirstOrDefaultAsync();

            if (deductionEntity == null)
            {
                deductionEntity = new Entities.Deduction();
                deductionEntity.Identifier = deduction.Identifier;
                deduction.IsActive = true;
                this.DbContext.Deductions.Add(deductionEntity);
            }
            else
            {
                deductionEntity.IsActive = deduction.IsActive;
            }

            deductionEntity.ModifiedDate = DateTime.UtcNow;
            deductionEntity.Amount = deduction.Amount;
            deductionEntity.Cron = deduction.Cron;
            deductionEntity.Description = deduction.Description;
            if (deduction.EffectiveFrom.HasValue)
            {
                deductionEntity.EffectiveFrom = deduction.EffectiveFrom.Value;
            }

            if (deduction.EffectiveTo.HasValue)
            {
                deductionEntity.EffectiveTo = deduction.EffectiveTo.Value;
            }

            deductionEntity.EmployeeId = employeeEntity.EmployeeId;
            deductionEntity.Employee = employeeEntity;

            await this.DbContext.SaveChangesAsync();
            deduction.Identifier = deductionEntity.Identifier;
            deduction.DeductionId = deductionEntity.DeductionId;
            deduction.IsActive = deductionEntity.IsActive;
            deduction.IsDeleted = deductionEntity.IsDeleted;
            deduction.ModifiedDate = deductionEntity.ModifiedDate;
            deduction.DeletedDate = deductionEntity.DeletedDate;

            var cacheKey = EmployeeDeductionCacheKeyPrefix + companyIdentifier.ToString();
            if (this.Cache != null && this.Cache.Contains(cacheKey, this.DefaultCacheRegion))
            {
                this.Cache.Remove(cacheKey, this.DefaultCacheRegion);
            }

            return deduction;
        }

        public async Task DeleteDeductionAsync(Guid deductionIdentifier, Guid employeeIdentifier, Guid companyIdentifier)
        {
            if (deductionIdentifier == Guid.Empty)
            {
                throw new ArgumentNullException("deductionIdentifier");
            }

            var cacheKey = EmployeeDeductionCacheKeyPrefix + companyIdentifier.ToString();
            await this.DeleteAndRemoveFromCache(async () => await this.DbContext.Deductions
                .Where(d =>
                    d.Identifier == deductionIdentifier &&
                    d.Employee.Identifier == employeeIdentifier &&
                    d.Employee.Company.Identifier == companyIdentifier).FirstOrDefaultAsync(),
                cacheKey);
        }

        #endregion Methods
    }
}
