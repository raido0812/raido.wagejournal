﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Contracts.Enumerations
{
    public enum CultureCodes : byte
    {
        EnglishUK = 0,

        EnglishUS = 1,

        ChineseTW = 2
    }
}
