﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Contracts.Enumerations
{
    public enum SortDirection : byte
    {
        Ascending = 0,
        Descending = 1
    }
}
