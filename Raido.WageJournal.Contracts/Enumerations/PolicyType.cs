﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Raido.WageJournal.Contracts.Enumerations
{
    public enum PolicyType : byte
    {
        NotSet = 0,

        SaturdayOvertimeMultiplier = 1,

        SundayOvertimeMultiplier = 2,

        WorkDayHours = 3,

        UnemploymentInsuranceFundPercentage = 4,

        AttendanceBonusAmount = 5
    }
}
