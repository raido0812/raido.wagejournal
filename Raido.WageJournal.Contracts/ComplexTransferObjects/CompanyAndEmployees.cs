﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Contracts.ComplexTransferObjects
{
    public class CompanyAndEmployees
    {
        public IList<DataTransferObjects.Employee> Employees { get; set; }
        public DataTransferObjects.Company Company { get; set; }
    }
}
