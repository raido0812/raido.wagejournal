﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Contracts.ComplexTransferObjects
{
    public class CompanyAndSettings
    {
        public DataTransferObjects.Company Company { get; set; }
        public DataTransferObjects.Policy SaturdayOvertimeMultiplier { get; set; }
        public DataTransferObjects.Policy SundayOvertimeMultiplier { get; set; }
        public DataTransferObjects.Policy UnemploymentInsuranceFundPercentage { get; set; }
        public DataTransferObjects.Policy WorkDayHours { get; set; }
        public DataTransferObjects.Policy AttendanceBonusAmount { get; set; }
    }
}
