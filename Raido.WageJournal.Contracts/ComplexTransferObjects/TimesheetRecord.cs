﻿using System.Collections.Generic;
using Raido.WageJournal.Contracts.DataTransferObjects;

namespace Raido.WageJournal.Contracts.ComplexTransferObjects
{
    public class TimesheetRecord
    {
        public TimesheetMetaInfo TimesheetMetaInfo { get; set; }
        public IEnumerable<TimesheetLogEntry> TimesheetLogEntries { get; set; }
    }
}
