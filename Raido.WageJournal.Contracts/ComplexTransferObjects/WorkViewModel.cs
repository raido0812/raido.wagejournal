﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Contracts.ComplexTransferObjects
{
    public class CompanyAndEmployeeWork
    {
        public Guid CompanyIdentifier { get; set; }
        public DataTransferObjects.Policy SaturdayOvertimeMultiplier { get; set; }
        public DataTransferObjects.Policy SundayOvertimeMultiplier { get; set; }
        public IList<EmployeeWorkDay> EmployeeWork { get; set; }
    }
}
