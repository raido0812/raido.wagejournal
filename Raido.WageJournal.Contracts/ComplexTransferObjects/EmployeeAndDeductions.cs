﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raido.WageJournal.Contracts.DataTransferObjects;
namespace Raido.WageJournal.Contracts.ComplexTransferObjects
{
    public class EmployeeAndDeductions
    {
        public IList<Deduction> Deductions { get; set; }
        public Employee Employee { get; set; }
    }
}
