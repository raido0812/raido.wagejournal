﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Contracts.ComplexTransferObjects
{
    public class EmployeeWorkDay
    {
        public DateTime Day { get; set; }
        public DataTransferObjects.Policy SaturdayOvertimeMultiplier { get; set; }
        public DataTransferObjects.Policy SundayOvertimeMultiplier { get; set; }
        public DataTransferObjects.Work Work { get; set; }
        public DataTransferObjects.Adjustment Adjustment { get; set; }
        public DataTransferObjects.Employee Employee { get; set; }
    }
}
