﻿using System;

namespace Raido.WageJournal.Contracts.SearchFilters
{
    public class DeductionSearchOptions
    {
        public Guid? DeductionIdentifier { get; set; }
        public Guid? EmployeeIdentifier { get; set; }
    }
}
