﻿using System;

namespace Raido.WageJournal.Contracts.SearchFilters
{
    public class WorkSearchOptions
    {
        public Guid? WorkIdentifier { get; set; }
        public Guid? EmployeeIdentifier { get; set; }
        public DateTime? PeriodStart { get; set; }
        public DateTime? PeriodEnd { get; set; }
    }
}
