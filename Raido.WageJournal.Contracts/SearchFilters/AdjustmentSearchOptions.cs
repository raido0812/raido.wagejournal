﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Contracts.SearchFilters
{
    public class AdjustmentSearchOptions
    {
        public Guid? AdjustmentIdentifier { get; set; }
        public Guid? EmployeeIdentifier { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
