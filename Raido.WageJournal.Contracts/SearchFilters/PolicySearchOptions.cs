﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Contracts.SearchFilters
{
    public class PolicySearchOptions
    {
        public Guid? PolicyIdentifier { get; set; }
    }
}
