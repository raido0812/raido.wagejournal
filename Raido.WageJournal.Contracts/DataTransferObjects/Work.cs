﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Contracts.DataTransferObjects
{
    public class Work : DtoBase
    {
        public int WorkId { get; set; }

        [Display(Name = "HoursWorkedDisplayText", ResourceType = typeof(Localization.Work))]
        public decimal? HoursWorked { get; set; }

        [Required]
        [Display(Name = "DateWorkedDisplayText", ResourceType = typeof(Localization.Work))]
        public DateTime DateWorked { get; set; }
        
        [Required]
        public int EmployeeId { get; set; }

        public string EmployeeNumber { get; set; }

        public string EmployeeName { get; set; }

        public Guid EmployeeIdentifier { get; set; }

        public Guid CompanyIdentifier { get; set; }

        public Work()
            : base()
        { }
    }
}
