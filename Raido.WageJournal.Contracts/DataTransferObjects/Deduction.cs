﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Contracts.DataTransferObjects
{
    public class Deduction : DtoBase
    {
        public int DeductionId { get; set; }

        [MaxLength(100, ErrorMessageResourceName = "DescriptionMaxLengthErrorMessage", ErrorMessageResourceType = typeof(Localization.Deduction))]
        [Required]
        [Display(ResourceType = typeof(Localization.Deduction), Name = "DescriptionDisplayText")]
        public string Description { get; set; }

        [Required]
        [Display(ResourceType = typeof(Localization.Deduction), Name = "AmountDisplayText")]
        public decimal Amount { get; set; }

        [Required]
        [Display(ResourceType = typeof(Localization.Deduction), Name = "EffectiveFromDisplayText")]
        public DateTime? EffectiveFrom { get; set; }

        [Display(ResourceType = typeof(Localization.Deduction), Name = "EffectiveToDisplayText")]
        public DateTime? EffectiveTo { get; set; }

        [MaxLength(20, ErrorMessageResourceName = "CronMaxLengthErrorMessage", ErrorMessageResourceType = typeof(Localization.Deduction))]
        [Required]
        [Display(ResourceType = typeof(Localization.Deduction), Name = "CronDisplayText")]
        public string Cron { get; set; }
        
        public Guid EmployeeIdentifier { get; set; }

        public Guid CompanyIdentifier { get; set; }

        public Deduction()
            : base()
        {
            this.Cron = Raido.WageJournal.Contracts.SelectListOptions.Cron.CronOptions.Where(kv => kv.Key == "WeeklyFriday").Select(kv => kv.Value).FirstOrDefault();
        }
    }
}
