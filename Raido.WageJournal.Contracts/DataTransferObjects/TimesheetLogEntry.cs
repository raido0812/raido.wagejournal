﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Contracts.DataTransferObjects
{
    public class TimesheetLogEntry
    {
        public DateTime? Date { get; set; }
        public TimeSpan? EndTime { get; set; }
        public TimeSpan? StartTime { get; set; }
        public decimal? HourDifference => EndTime.HasValue && StartTime.HasValue
            ? (decimal)Math.Round((EndTime.Value - StartTime.Value).TotalMinutes / 60.0, 2, MidpointRounding.AwayFromZero)
            : 0m;
        public string DateDisplay => Date.HasValue ? Date.Value.ToShortDateString() : string.Empty;
        public string DateISO8601Display => Date.HasValue ? Date.Value.ToString("yyyy-MM-dd") : string.Empty;

        public string WorkTimeRangeDisplay =>
           (StartTime.HasValue ? DateTime.Today.Add(StartTime.Value).ToString("hh:mm") : "???")
           + " ~ " +
           (EndTime.HasValue ? DateTime.Today.Add(EndTime.Value).ToString("hh:mm") : "???");
    }
}
