﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Contracts.DataTransferObjects
{
    public class CompanyUser : DtoBase
    {
        public int CompanyId { get; set; }
        public string UserId { get; set; }
        public bool IsCurrentDisplayed { get; set; }

        public CompanyUser() : base() { }
    }
}
