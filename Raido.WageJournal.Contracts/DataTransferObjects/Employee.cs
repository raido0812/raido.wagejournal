﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Contracts.DataTransferObjects
{
    public class Employee : DtoBase
    {
        public int EmployeeId { get; set; }

        [MaxLength(255, ErrorMessageResourceName = "EmployeeNameMaxLengthErrorMessage", ErrorMessageResourceType = typeof(Localization.Employee))]
        [Required]
        [Display(ResourceType = typeof(Localization.Employee), Name = "NameDisplayText")]
        public string Name { get; set; }

        [Display(ResourceType = typeof(Localization.Employee), Name = "NameDisplayText")]
        public string EmployeeDisplayName => Name +
            (!string.IsNullOrEmpty(EmployeeNumber)
                ? " (#" + EmployeeNumber + ")"
                : string.Empty);

        [MaxLength(255, ErrorMessageResourceName = "EmailMaxLengthErrorMessage", ErrorMessageResourceType = typeof(Localization.Employee))]
        [Display(ResourceType = typeof(Localization.Employee), Name = "EmailDisplayText")]
        public string Email { get; set; }

        [MaxLength(50, ErrorMessageResourceName = "PhoneNumberMaxLengthErrorMessage", ErrorMessageResourceType = typeof(Localization.Employee))]
        [Display(ResourceType = typeof(Localization.Employee), Name = "PhoneDisplayText")]
        public string PhoneNumber { get; set; }

        [MaxLength(50, ErrorMessageResourceName = "EmployeeNumberMaxLengthErrorMessage", ErrorMessageResourceType = typeof(Localization.Employee))]
        [Display(ResourceType = typeof(Localization.Employee), Name = "EmployeeNumberDisplayText")]
        public string EmployeeNumber { get; set; }

        [Display(ResourceType = typeof(Localization.Employee), Name = "EmployeeHourlyRateDisplayText")]
        public decimal? HourlyRate { get; set; }

        [Display(ResourceType = typeof(Localization.Employee), Name = "EmploymentStartDateDisplayText")]
        public DateTime? EmploymentStartDate { get; set; }

        [Display(ResourceType = typeof(Localization.Employee), Name = "EmploymentEndDateDisplayText")]
        public DateTime? EmploymentEndDate { get; set; }

        [Required]
        public Guid CompanyIdentifier { get; set; }

        public Employee()
            : base()
        { }
    }
}
