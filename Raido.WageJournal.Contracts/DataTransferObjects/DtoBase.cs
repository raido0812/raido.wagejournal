﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Contracts.DataTransferObjects
{
    public class DtoBase
    {
        public Guid Identifier { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public DateTime? DeletedDate { get; set; }

        [Display(Name = "IsActiveDisplayText", ResourceType = typeof(Localization.ModelBase))]
        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        public DtoBase()
        {
            this.Identifier = Guid.NewGuid();
            this.IsActive = true;
            this.IsDeleted = false;
            this.ModifiedDate = DateTime.UtcNow;
        }
    }
}
