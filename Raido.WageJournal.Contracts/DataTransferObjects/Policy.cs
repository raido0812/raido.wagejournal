﻿using Raido.WageJournal.Contracts.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Contracts.DataTransferObjects
{
    public class Policy : DtoBase
    {
        public int PolicyId { get; set; }

        public PolicyType PolicyType { get; set; }

        [MaxLength(255, ErrorMessageResourceName = "SettingValueMaxLengthErrorMessage", ErrorMessageResourceType = typeof(Localization.Policy))]
        [Display(Name = "SettingDisplayText", ResourceType = typeof(Localization.Policy))]
        public string Setting { get; set; }

        [Display(Name = "ValueDisplayText", ResourceType = typeof(Localization.Policy))]
        public decimal Value { get; set; }
        
        [Required]
        public int CompanyId { get; set; }
        
        public Policy()
            : base()
        { }
    }
}
