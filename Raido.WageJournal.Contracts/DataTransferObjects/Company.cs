﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Contracts.DataTransferObjects
{
    public class Company : DtoBase
    {
        public int CompanyId { get; set; }

        [MaxLength(255, ErrorMessageResourceName = "TradeNameMaxLengthErrorMessage", ErrorMessageResourceType = typeof(Localization.Company))]
        [Required(ErrorMessageResourceName = "TradeNameRequiredErrorMessage", ErrorMessageResourceType = typeof(Localization.Company))]
        [Display(Name = "TradeNameDisplayText", ResourceType = typeof(Localization.Company))]
        public string TradeName { get; set; }

        [MaxLength(500, ErrorMessageResourceName = "FullNameMaxLengthErrorMessage", ErrorMessageResourceType = typeof(Localization.Company))]
        [Required(ErrorMessageResourceName = "FullNameRequiredErrorMessage", ErrorMessageResourceType = typeof(Localization.Company))]
        [Display(Name = "FullNameDisplayText", ResourceType = typeof(Localization.Company))]
        public string FullName { get; set; }

        public Company()
            : base()
        { }
    }
}
