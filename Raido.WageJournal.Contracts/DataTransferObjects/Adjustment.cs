﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Contracts.DataTransferObjects
{
    public class Adjustment : DtoBase
    {
        public int AdjustmentId { get; set; }

        [MaxLength(100, ErrorMessageResourceName = "DescriptionMaxLengthErrorMessage", ErrorMessageResourceType = typeof(Localization.Adjustment))]
        [Required]
        [Display(ResourceType = typeof(Localization.Adjustment), Name = "AdjustmentDescriptionDisplayText")]
        public string Description { get; set; }

        [Required]
        [Display(ResourceType = typeof(Localization.Adjustment), Name = "AdjustmentAmountDisplayText")]
        public decimal? Amount { get; set; }

        [Required]
        [Display(ResourceType = typeof(Localization.Adjustment), Name = "AdjustmentDateDisplayText")]
        public DateTime? DateOfIssuance { get; set; }
        
        public Guid EmployeeIdentifier { get; set; }

        public Guid CompanyIdentifier { get; set; }

        public Adjustment()
            : base()
        {
        }
    }
}
