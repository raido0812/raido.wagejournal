﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Contracts.SelectListOptions
{
    public static class Cron
    {
        public static IList<KeyValuePair<string, string>> CronOptions = new List<KeyValuePair<string, string>>() { 
            new KeyValuePair<string, string>("WeeklySunday", "0 0 * * SUN"),
            new KeyValuePair<string, string>("WeeklyMonday", "0 0 * * MON"),
            new KeyValuePair<string, string>("WeeklyTuesday", "0 0 * * TUE"),
            new KeyValuePair<string, string>("WeeklyWednesday", "0 0 * * WED"),
            new KeyValuePair<string, string>("WeeklyThursday", "0 0 * * THU"),
            new KeyValuePair<string, string>("WeeklyFriday", "0 0 * * FRI"),
            new KeyValuePair<string, string>("WeeklySaturday", "0 0 * * SAT"),
            new KeyValuePair<string, string>("MonthlyFirstDay", "0 0 1 * *"),
            new KeyValuePair<string, string>("MonthlyFifteenthDay", "0 0 15 * *"),
            new KeyValuePair<string, string>("MonthlyTwentyEighthDay", "0 0 28 * *")
        };
    }
}
