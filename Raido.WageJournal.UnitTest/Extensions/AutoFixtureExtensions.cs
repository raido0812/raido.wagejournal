﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ploeh.AutoFixture.Dsl;
using Raido.WageJournal.DataAccess.Entities;

namespace Raido.WageJournal.UnitTest.Extensions
{
    internal static class AutoFixtureExtensions
    {
        internal static IPostprocessComposer<T>  ActiveEntity<T>(this IPostprocessComposer<T> postprocessComposer) where T : EntityBase
        {
            return postprocessComposer.With(t => t.IsActive, true);
        }

        internal static IPostprocessComposer<T> InActiveEntity<T>(this IPostprocessComposer<T> postprocessComposer) where T : EntityBase
        {
            return postprocessComposer.With(t => t.IsActive, false);
        }

        internal static IPostprocessComposer<T> DeletedEntity<T>(this IPostprocessComposer<T> postprocessComposer) where T : EntityBase
        {
            return postprocessComposer.With(t => t.IsDeleted, true);
        }

        internal static IPostprocessComposer<T> NonDeletedEntity<T>(this IPostprocessComposer<T> postprocessComposer) where T : EntityBase
        {
            return postprocessComposer.With(t => t.IsDeleted, false);
        }
    }
}
