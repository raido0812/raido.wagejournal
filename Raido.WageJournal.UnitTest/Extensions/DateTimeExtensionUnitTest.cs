﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Raido.WageJournal.Extensions;

namespace Raido.WageJournal.UnitTest.Extensions
{
    [TestClass]
    public class DateTimeExtensionUnitTest
    {
        [TestMethod]
        [TestCategory("Extensions")]
        public void DaysCountOneWeekShouldHaveOneDayEach()
        {
            DateTime startDate = new DateTime (2012,2,27);
            DateTime endDate = new DateTime(2012, 3, 4);

            var result = startDate.DaysCountUntil(endDate);
            Assert.AreEqual(result[DayOfWeek.Monday], 1,"Monday should have 1 count");
            Assert.AreEqual(result[DayOfWeek.Tuesday], 1, "Tuesday should have 1 count");
            Assert.AreEqual(result[DayOfWeek.Wednesday], 1, "Wednesday should have 1 count");
            Assert.AreEqual(result[DayOfWeek.Thursday], 1, "Thursday should have 1 count");
            Assert.AreEqual(result[DayOfWeek.Friday], 1, "Friday should have 1 count");
            Assert.AreEqual(result[DayOfWeek.Saturday], 1, "Saturday should have 1 count");
            Assert.AreEqual(result[DayOfWeek.Sunday], 1, "Sunday should have 1 count");
        }

        [TestMethod]
        [TestCategory("Extensions")]
        public void DaysCountStartWeekToMidWeekShouldHaveTwoDaysOnRepeated()
        {
            DateTime startDate = new DateTime(2012, 2, 27);
            DateTime endDate = new DateTime(2012, 3, 6);

            var result = startDate.DaysCountUntil(endDate);
            Assert.AreEqual(result[DayOfWeek.Monday], 2, "Monday should have 2 count");
            Assert.AreEqual(result[DayOfWeek.Tuesday], 2, "Tuesday should have 2 count");
            Assert.AreEqual(result[DayOfWeek.Wednesday], 1, "Wednesday should have 1 count");
            Assert.AreEqual(result[DayOfWeek.Thursday], 1, "Thursday should have 1 count");
            Assert.AreEqual(result[DayOfWeek.Friday], 1, "Friday should have 1 count");
            Assert.AreEqual(result[DayOfWeek.Saturday], 1, "Saturday should have 1 count");
            Assert.AreEqual(result[DayOfWeek.Sunday], 1, "Sunday should have 1 count");
        }

        [TestMethod]
        [TestCategory("Extensions")]
        public void DaysCountMidWeekToEndWeekShouldHaveThreeDaysOnRepeated()
        {
            DateTime startDate = new DateTime(2015, 8, 19);
            DateTime endDate = new DateTime(2015, 9, 5);

            var result = startDate.DaysCountUntil(endDate);
            Assert.AreEqual(result[DayOfWeek.Monday], 2, "Monday should have 2 count");
            Assert.AreEqual(result[DayOfWeek.Tuesday], 2, "Tuesday should have 2 count");
            Assert.AreEqual(result[DayOfWeek.Wednesday], 3, "Wednesday should have 3 count");
            Assert.AreEqual(result[DayOfWeek.Thursday], 3, "Thursday should have 3 count");
            Assert.AreEqual(result[DayOfWeek.Friday], 3, "Friday should have 3 count");
            Assert.AreEqual(result[DayOfWeek.Saturday], 3, "Saturday should have 3 count");
            Assert.AreEqual(result[DayOfWeek.Sunday], 2, "Sunday should have 2 count");
        }
    }
}
