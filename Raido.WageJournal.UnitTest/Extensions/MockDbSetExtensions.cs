﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Moq;
using Raido.WageJournal.Contracts.Enumerations;
using Raido.WageJournal.Extensions;
namespace Raido.WageJournal.UnitTest.Extensions
{
    public static class MockDbSetExtensions
    {
        internal static void SetupDBInsert<T>(this Mock<DbSet<T>> fakeDbSet, IList<T> fakeDBItems) where T : class
        {
            fakeDbSet.Setup(f =>
                f.Add(It.IsAny<T>()))
                    .Callback((T objectToInsert) =>
                    {
                        Type objectType = objectToInsert.GetType();
                        string typeName = objectType.Name;
                        var property = objectType.GetProperty(typeName + "Id");
                        if (property != null)
                        {
                            int newValue = 1;
                            var itemWithMaxId = fakeDBItems.AsQueryable().OrderBy(typeName + "Id", SortDirection.Descending).FirstOrDefault();
                            if (itemWithMaxId != null)
                            {
                                newValue = (int)property.GetValue(itemWithMaxId);
                                newValue++;
                            }

                            property.SetValue(objectToInsert, newValue);
                        }

                        fakeDBItems.Add(objectToInsert);
                    });
        }
    }
}
