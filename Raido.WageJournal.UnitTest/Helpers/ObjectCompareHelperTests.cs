﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Raido.WageJournal.UnitTest.Helpers
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class ObjectCompareHelperTests
    {
        #region AreObjectPropertiesEqual EnumSpecific

        private enum a : byte
        {
            NotSet = 0,
            TestA = 1,
            TestB = 2
        }
        private enum b : byte
        {
            NotSet = 0,
            TestA = 1,
            TestB = 2
        }

        private enum c : byte
        {
            NotSet = 0,
            TestC = 1,
            TestD = 2
        }

        private enum d : byte
        {
            NotSet = 0,
            TestA = 3,
            TestB = 4
        }

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void AreEnumsPropertiesEqualWhenNameAndValueEqual()
        {
            object object1 = new { val = a.TestA };
            object object2 = new { val = b.TestA };

            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(object1, object2));
        }

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void AreEnumsPropertiesNotEqualWhenNameNotEqual()
        {
            object object1 = new { val = a.TestA };
            object object2 = new { val = c.TestC };

            Assert.IsFalse(ObjectCompareHelper.AreObjectPropertiesEqual(object1, object2));
        }

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void AreEnumsPropertiesNotEqualWhenValueNotEqual()
        {
            object object1 = new { val = a.TestA };
            object object2 = new { val = d.TestA };

            Assert.IsFalse(ObjectCompareHelper.AreObjectPropertiesEqual(object1, object2));
        }

        #endregion AreObjectPropertiesEqual EnumSpecific
        
        #region AreObjectPropertiesEqual nullable vs non-nullable compare

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void ArePropertiesEqualWhenNameEqualButOneOfTypeIsNullableInt()
        {
            var a = new { val1 = (int?)25 };
            var b = new { val1 = 25 };
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(a,b));
        }

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void ArePropertiesEqualWhenNameEqualButOneOfTypeIsNullableDecimal()
        {
            var a = new { val1 = 25m };
            var b = new { val1 = (decimal?)25 };
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(a, b));
        }

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void ArePropertiesEqualWhenNameEqualButOneOfTypeIsNullableDecimalAndNoValue()
        {
            var a = new { val1 = 25m };
            var b = new { val1 = (decimal?)null };
            Assert.IsFalse(ObjectCompareHelper.AreObjectPropertiesEqual(a, b));
        }

        #endregion

        #region AreObjectPropertiesEqual

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void AreEmptyObjectsPropertiesNotEqual()
        {
            object object1 = new { };
            object object2 = new { };

            Assert.IsFalse(ObjectCompareHelper.AreObjectPropertiesEqual(object1, object2));
        }

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void AreObjectsWithoutSharedPropertiesNotEqual()
        {
            object object1 = new { Id = 1 };
            object object2 = new { Name = "name" };

            Assert.IsFalse(ObjectCompareHelper.AreObjectPropertiesEqual(object1, object2));
        }

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void AreObjectsWithoutSharedPropertiesThatAreNotIgnoredNotEqual()
        {
            object object1 = new { Id = 1, number = 2 };
            object object2 = new { Name = "name", number = 3 };

            Assert.IsFalse(ObjectCompareHelper.AreObjectPropertiesEqual(object1, object2));
        }

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void AreNullObjectsNotEqual()
        {
            object object1 = new { Id = 1 };
            object object2 = new { Name = "name" };
            object object3 = null;

            Assert.IsFalse(ObjectCompareHelper.AreObjectPropertiesEqual(object1, object3));
            Assert.IsFalse(ObjectCompareHelper.AreObjectPropertiesEqual(object2, object3));
        }

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void AreObjectsWithPrimativePropertiesAndEqualValuesEqual()
        {
            object object1 = new
            {
                i = 1,
                s = "string",
                dec = (decimal)2.2,
                d = (double)3.3,
                b = false,
                c = 'a',
                by = (byte)1,
                date = DateTime.Parse("2015/12/25")
            };

            object object2 = new
            {
                i = 1,
                s = "string",
                dec = (decimal)2.2,
                d = (double)3.3,
                b = false,
                c = 'a',
                by = (byte)1,
                date = DateTime.Parse("2015/12/25")
            };

            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(object1, object2));
        }

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void AreObjectsWithPrimativePropertiesAndDifferentValuesNotEqual()
        {
            object object1 = new
            {
                i = 6,
                s = "new string",
                dec = (decimal)5.5,
                d = (double)6.7,
                b = true,
                c = 'b',
                by = (byte)0,
                date = DateTime.Parse("2015/12/31")
            };

            object object2 = new
            {
                i = 1,
                s = "string",
                dec = (decimal)2.2,
                d = (double)3.3,
                b = false,
                c = 'a',
                by = (byte)1,
                date = DateTime.Parse("2015/12/25")
            };

            Assert.IsFalse(ObjectCompareHelper.AreObjectPropertiesEqual(object1, object2));
        }

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void AreObjectsWithEnumarablesOfEqualValuesEqual()
        {
            object object1 = new
            {
                i = new List<int>() { 1, 2, 3, 4 },
                ignoredProperty = "ignored"
            };

            object object2 = new
            {
                i = new List<int>() { 1, 2, 3, 4 },
                otherIgnoredProperty = "also ignored"
            };

            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(object1, object2));
        }

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void AreObjectsWithEnumarablesOfDifferentValuesNotEqual()
        {
            object object1 = new
            {
                i = new List<int>() { 1, 2, 3, 4 },
                ignoredProperty = "ignored"
            };

            object object2 = new
            {
                i = new List<int>() { 1, 3, 4 },
                otherIgnoredProperty = "also ignored"
            };

            Assert.IsFalse(ObjectCompareHelper.AreObjectPropertiesEqual(object1, object2));
        }

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void AreObjectsWithEnumarablesOfDifferentlyOrderedValuesNotEqual()
        {
            object object1 = new
            {
                i = new List<int>() { 1, 2, 3, 4 },
                ignoredProperty = "ignored"
            };

            object object2 = new
            {
                i = new List<int>() { 4, 2, 1, 3 },
                otherIgnoredProperty = "also ignored"
            };

            Assert.IsFalse(ObjectCompareHelper.AreObjectPropertiesEqual(object1, object2));
        }

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void AreObjectsWithEnumarablesOfDifferentlyTypesNotEqual()
        {
            object object1 = new
            {
                i = new List<int>() { 1, 2, 3, 4 },
                ignoredProperty = "ignored"
            };

            object object2 = new
            {
                i = new List<double>() { 4.2, 2.4, 1.3, 3.4 },
                otherIgnoredProperty = "also ignored"
            };

            Assert.IsFalse(ObjectCompareHelper.AreObjectPropertiesEqual(object1, object2));
        }

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void AreObjectsWithEmptyEnumarablesEqual()
        {
            object object1 = new
            {
                i = new List<int>(),
                ignoredProperty = "ignored"
            };

            object object2 = new
            {
                i = new List<int>(),
                otherIgnoredProperty = "also ignored"
            };

            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(object1, object2));
        }

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void AreObjectsWithEqualChildObjectsEqual()
        {
            object object1 = new
            {
                child = new { i = 1 },
                ignoredProperty = "ignored"
            };

            object object2 = new
            {
                child = new { i = 1 },
                otherIgnoredProperty = "also ignored"
            };

            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(object1, object2));
        }

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void AreObjectsWithDifferentChildObjectsNotEqual()
        {
            object object1 = new
            {
                child = new { i = 1 },
                ignoredProperty = "ignored"
            };

            object object2 = new
            {
                child = new { i = 2 },
                otherIgnoredProperty = "also ignored"
            };

            Assert.IsFalse(ObjectCompareHelper.AreObjectPropertiesEqual(object1, object2));
        }

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void AreObjectsWithChildObjectsWithDifferentPropertiesNotEqual()
        {
            object object1 = new
            {
                child = new { i = 1 },
                child2 = new { x = 2 },
                ignoredProperty = "ignored"
            };

            object object2 = new
            {
                child = new { s = "string" },
                child2 = new { x = 2 },
                otherIgnoredProperty = "also ignored"
            };

            Assert.IsFalse(ObjectCompareHelper.AreObjectPropertiesEqual(object1, object2));
        }

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void AreObjectsWithIdenticallyNamedButDifferentlyTypedPropertiesNotEqual()
        {
            object object1 = new
            {
                i = 1,
                s = string.Empty,
                ignoredProperty = "ignored"
            };

            object object2 = new
            {
                i = string.Empty,
                s = string.Empty,
                otherIgnoredProperty = "also ignored"
            };

            Assert.IsFalse(ObjectCompareHelper.AreObjectPropertiesEqual(object1, object2));
        }


        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void AreObjectsIgnoreListPropertiesIgnored()
        {
            object object1 = new
            {
                i = 1,
                s = string.Empty,
                z = 'b',
                ignoredProperty = "ignored"
            };

            object object2 = new
            {
                i = 1,
                s = "different",
                z = 'c',
                otherIgnoredProperty = "also ignored"
            };

            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(object1, object2, ignoreList: new string[] { "s", "z" }));
        }

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void AreObjectsWithAllPropertiesIgnoredNotEqual()
        {
            object object1 = new
            {
                s = string.Empty,
                z = 'b',
                ignoredProperty = "ignored"
            };

            object object2 = new
            {
                s = "different",
                z = 'c',
                otherIgnoredProperty = "also ignored"
            };

            Assert.IsFalse(ObjectCompareHelper.AreObjectPropertiesEqual(object1, object2, ignoreList: new string[] { "s", "z" }));
        }

        [TestMethod]
        [TestCategory("Unit Test Helpers")]
        public void AreObjectsPropertiesWithSubstringIgnoreListItemsNotIgnored()
        {
            object object1 = new
            {
                matchingProperty = 1,
                objectProperty = 1,
                ignoredProperty = "ignored"
            };

            object object2 = new
            {
                matchingProperty = 1,
                objectProperty = 2,
                otherIgnoredProperty = "also ignored"
            };

            Assert.IsFalse(ObjectCompareHelper.AreObjectPropertiesEqual(object1, object2, ignoreList: new string[] { "obj", "p" }));
        }

        #endregion
    }
}
