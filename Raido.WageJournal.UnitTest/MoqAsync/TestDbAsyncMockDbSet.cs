﻿/* https://msdn.microsoft.com/en-us/data/dn314429.aspx?f=255&MSPPError=-2147217396 */
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;

namespace Raido.WageJournal.UnitTest.MoqAsync
{
    internal static class TestDbAsyncMockDbSet
    {
        internal static void SetupWithQueryable<T>(this Mock<DbSet<T>> fakeDbSet, IQueryable<T> fakeDBList) where T : class
        {
            fakeDbSet.As<IDbAsyncEnumerable<T>>()
                    .Setup(m => m.GetAsyncEnumerator())
                    .Returns(new TestDbAsyncEnumerator<T>(fakeDBList.GetEnumerator()));

            fakeDbSet.As<IQueryable<T>>()
                .Setup(m => m.Provider)
                .Returns(new TestDbAsyncQueryProvider<T>(fakeDBList.Provider));

            fakeDbSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(fakeDBList.Expression);
            fakeDbSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(fakeDBList.ElementType);
            fakeDbSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(fakeDBList.GetEnumerator());
        }
    }
}
