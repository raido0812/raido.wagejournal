﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NCrontab;
using Raido.WageJournal.Contracts.SelectListOptions;

namespace Raido.WageJournal.UnitTest
{
    [TestClass]
    public class CronOptionTests
    {
        [TestMethod]
        public void EnsureAllCronOptionsValid()
        {
            foreach (var cron in Cron.CronOptions)
            {
                Assert.IsNotNull(CrontabSchedule.Parse(cron.Value));
            }
        }
    }
}
