﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Raido.WageJournal.Contracts.ComplexTransferObjects;
using Raido.WageJournal.Logic.Services;

namespace Raido.WageJournal.UnitTest.Services
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class TimesheetImporterTests
    {
        [TestMethod]
        [TestCategory("XmlImporter")]
        public async Task TimesheetEntryShouldContain26Employees()
        {
            var timeSheetImporter = new TimeSheetImporter();
            IEnumerable<TimesheetRecord> results;
            using (var stream = typeof(TimesheetImporterTests).Assembly.GetManifestResourceStream($"Raido.WageJournal.UnitTest.SampleFiles.TimeCardMachineExcelXml.xml"))
            {
                results = await timeSheetImporter.ExtractTimesheetRecords(stream);
            }

            Assert.AreEqual(26, results.Count());
        }

        [TestMethod]
        [TestCategory("XmlImporter")]
        public async Task TimesheetEntryShouldContainEmployeeNumber()
        {
            var timeSheetImporter = new TimeSheetImporter();
            IEnumerable<TimesheetRecord> results;
            using (var stream = typeof(TimesheetImporterTests).Assembly.GetManifestResourceStream($"Raido.WageJournal.UnitTest.SampleFiles.TimeCardMachineExcelXml.xml"))
            {
                results = await timeSheetImporter.ExtractTimesheetRecords(stream);
            }

            Assert.AreEqual("00003", results.Skip(1).Take(1).Single().TimesheetMetaInfo.EmployeeNumber);
        }

        [TestMethod]
        [TestCategory("XmlImporter")]
        public async Task TimesheetEntryShouldContainWorkDayEntries()
        {
            var timeSheetImporter = new TimeSheetImporter();
            IEnumerable<TimesheetRecord> results;
            using (var stream = typeof(TimesheetImporterTests).Assembly.GetManifestResourceStream($"Raido.WageJournal.UnitTest.SampleFiles.TimeCardMachineExcelXml.xml"))
            {
                results = await timeSheetImporter.ExtractTimesheetRecords(stream);
            }

            var employee0003 = results.Skip(1).First();
            var logEntries = employee0003.TimesheetLogEntries.ToList();
            Assert.AreEqual(7, logEntries.Count);

            Assert.AreEqual(new DateTime(2014, 02, 11, 0, 0, 0, DateTimeKind.Utc), logEntries[0].Date.Value.ToUniversalTime());
            Assert.AreEqual(17, logEntries[0].EndTime.Value.Hours);
            Assert.AreEqual(3, logEntries[0].EndTime.Value.Minutes);

            Assert.AreEqual(new DateTime(2014, 02, 12, 0, 0, 0, DateTimeKind.Utc), logEntries[1].Date.Value.ToUniversalTime());
            Assert.AreEqual(17, logEntries[1].EndTime.Value.Hours);
            Assert.AreEqual(1, logEntries[1].EndTime.Value.Minutes);

            Assert.AreEqual(new DateTime(2014, 02, 13, 0, 0, 0, DateTimeKind.Utc), logEntries[2].Date.Value.ToUniversalTime());
            Assert.AreEqual(6, logEntries[2].StartTime.Value.Hours);
            Assert.AreEqual(46, logEntries[2].StartTime.Value.Minutes);
            Assert.AreEqual(17, logEntries[2].EndTime.Value.Hours);
            Assert.AreEqual(1, logEntries[2].EndTime.Value.Minutes);

            Assert.AreEqual(new DateTime(2014, 02, 14, 0, 0, 0, DateTimeKind.Utc), logEntries[3].Date.Value.ToUniversalTime());
            Assert.AreEqual(6, logEntries[3].StartTime.Value.Hours);
            Assert.AreEqual(57, logEntries[3].StartTime.Value.Minutes);
            Assert.AreEqual(15, logEntries[3].EndTime.Value.Hours);
            Assert.AreEqual(3, logEntries[3].EndTime.Value.Minutes);

            Assert.AreEqual(new DateTime(2014, 02, 17, 0, 0, 0, DateTimeKind.Utc), logEntries[4].Date.Value.ToUniversalTime());
            Assert.AreEqual(6, logEntries[4].StartTime.Value.Hours);
            Assert.AreEqual(53, logEntries[4].StartTime.Value.Minutes);
            Assert.AreEqual(17, logEntries[4].EndTime.Value.Hours);
            Assert.AreEqual(59, logEntries[4].EndTime.Value.Minutes);

            Assert.AreEqual(new DateTime(2014, 02, 18, 0, 0, 0, DateTimeKind.Utc), logEntries[5].Date.Value.ToUniversalTime());
            Assert.AreEqual(6, logEntries[5].StartTime.Value.Hours);
            Assert.AreEqual(54, logEntries[5].StartTime.Value.Minutes);
            Assert.AreEqual(17, logEntries[5].EndTime.Value.Hours);
            Assert.AreEqual(3, logEntries[5].EndTime.Value.Minutes);

            Assert.AreEqual(new DateTime(2014, 02, 19, 0, 0, 0, DateTimeKind.Utc), logEntries[6].Date.Value.ToUniversalTime());
            Assert.AreEqual(6, logEntries[6].StartTime.Value.Hours);
            Assert.AreEqual(58, logEntries[6].StartTime.Value.Minutes);
            Assert.AreEqual(17, logEntries[6].EndTime.Value.Hours);
            Assert.AreEqual(2, logEntries[6].EndTime.Value.Minutes);
        }
    }
}
