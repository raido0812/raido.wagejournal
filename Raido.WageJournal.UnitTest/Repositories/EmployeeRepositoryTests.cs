﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ploeh.AutoFixture;
using Raido.WageJournal.DataAccess.Security;
using Raido.WageJournal.DataAccess.DataContext;
using Raido.WageJournal.DataAccess.MappingConfiguration;
using Raido.WageJournal.DataAccess.Repositories;
using Raido.WageJournal.UnitTest.Extensions;
using Raido.WageJournal.UnitTest.MoqAsync;
using Entities = Raido.WageJournal.DataAccess.Entities;
using Raido.WageJournal.UnitTest.Helpers;
using DataTransferObjects = Raido.WageJournal.Contracts.DataTransferObjects;

namespace Raido.WageJournal.UnitTest.Repositories
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class EmployeeRepositoryTests
    {
        #region Get
        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetEmployeeShouldReturnNullWhenGuidDoesntExist()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Employee> inMemoryDbEmployees = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work)
                .CreateMany()
                .Where(a => a.Identifier != guidThatDoesntExist)
                .ToList();

            var fakeDbEmployees = new Mock<DbSet<Entities.Employee>>();
            fakeDbEmployees.SetupWithQueryable(inMemoryDbEmployees.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Employees).Returns(fakeDbEmployees.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var employeeRepository = new EmployeeRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var employee = await employeeRepository.GetEmployeeAsync(guidThatDoesntExist, company.Identifier);

            Assert.IsNull(employee);
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetEmployeeSimpleCaseWhereAllDataExists()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(ce => ce.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();


            IList<Entities.Employee> inMemoryDbEmployees = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work)
                .CreateMany()
                .ToList();

            var firstItemGuid = inMemoryDbEmployees[0].Identifier;

            var fakeDbEmployees = new Mock<DbSet<Entities.Employee>>();
            fakeDbEmployees.SetupWithQueryable(inMemoryDbEmployees.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Employees).Returns(fakeDbEmployees.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var employeeRepository = new EmployeeRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var employee = await employeeRepository.GetEmployeeAsync(firstItemGuid, company.Identifier);
            Assert.IsNotNull(employee);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(employee, inMemoryDbEmployees[0]));
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetEmployeeWhereIncorrectCompany()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(ce => ce.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            IList<Entities.Employee> inMemoryDbEmployees = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Deductions)
                .Without(e => e.Adjustments)
                .Without(e => e.Work)
                .CreateMany()
                .ToList();

            var firstItemGuid = inMemoryDbEmployees[0].Identifier;

            var fakeDbEmployees = new Mock<DbSet<Entities.Employee>>();
            fakeDbEmployees.SetupWithQueryable(inMemoryDbEmployees.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Employees).Returns(fakeDbEmployees.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var employeeRepository = new EmployeeRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var employee = await employeeRepository.GetEmployeeAsync(firstItemGuid, Guid.NewGuid());
            Assert.IsNull(employee);
        }

        #endregion Get

        #region Get List

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetEmployeeListShouldReturnMatchingEmployeeIdentifier()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();


            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Employee> inMemoryDbEmployees = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Deductions)
                .Without(e => e.Adjustments)
                .Without(e => e.Work)
                .CreateMany()
                .Where(a => a.Identifier != guidThatDoesntExist)
                .ToList();

            var fakeDbEmployees = new Mock<DbSet<Entities.Employee>>();
            fakeDbEmployees.SetupWithQueryable(inMemoryDbEmployees.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Employees).Returns(fakeDbEmployees.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var employeeRepository = new EmployeeRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var employees = await employeeRepository.GetEmployeesAsync(new Contracts.SearchFilters.EmployeeSearchOptions { EmployeeIdentifier = Guid.NewGuid() }, company.Identifier);
            Assert.AreEqual(0, employees.Count);
        }

        #endregion Get List

        #region Save

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task SaveEmployeeShouldInsertAllFields()
        {
            var fixture = new Fixture();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(c => c.CompanyUsers)
                .Without(c => c.Policies).Create();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Employee> inMemoryDbEmployees = new List<Entities.Employee> ();
            IList<Entities.Company> inMemoryDbCompanies = new List<Entities.Company> { company };

            var fakeDbEmployees = new Mock<DbSet<Entities.Employee>>();
            fakeDbEmployees.SetupWithQueryable(inMemoryDbEmployees.AsQueryable());
            fakeDbEmployees.SetupDBInsert(inMemoryDbEmployees);

            var fakeDbCompanies = new Mock<DbSet<Entities.Company>>();
            fakeDbCompanies.SetupWithQueryable(inMemoryDbCompanies.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Employees).Returns(fakeDbEmployees.Object);
            fakeDataContext.Setup(c => c.Companies).Returns(fakeDbCompanies.Object);
            fakeDataContext.Setup(c => c.SaveChanges()).Returns(1);
            fakeDataContext.Setup(c => c.SaveChangesAsync()).ReturnsAsync(1);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var employeeRepository = new EmployeeRepository(fakeDataContext.Object, null, null);

            var saveData = fixture.Create<DataTransferObjects.Employee>();

            var saveResult = await employeeRepository.SaveEmployeeAsync(saveData, company.Identifier);

            Assert.AreEqual(1, inMemoryDbEmployees.Count);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(saveData, inMemoryDbEmployees[0]));
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task SaveEmployeeWithMatchingIdentifierShouldUpdateAllFields()
        {
            var fixture = new Fixture();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(c => c.CompanyUsers)
                .Without(c => c.Policies).Create();

            Guid employeeGuid = Guid.NewGuid();
            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Identifier, employeeGuid)
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Deductions)
                .Without(e => e.Adjustments)
                .Without(e => e.Work)
                .Create();

            IList<Entities.Employee> inMemoryDbEmployees = new List<Entities.Employee> { employee };
            IList<Entities.Company> inMemoryDbCompanies = new List<Entities.Company> { company };

            var fakeDbEmployees = new Mock<DbSet<Entities.Employee>>();
            fakeDbEmployees.SetupWithQueryable(inMemoryDbEmployees.AsQueryable());
            fakeDbEmployees.SetupDBInsert(inMemoryDbEmployees);

            var fakeDbCompanies = new Mock<DbSet<Entities.Company>>();
            fakeDbCompanies.SetupWithQueryable(inMemoryDbCompanies.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Employees).Returns(fakeDbEmployees.Object);
            fakeDataContext.Setup(c => c.Companies).Returns(fakeDbCompanies.Object);
            fakeDataContext.Setup(c => c.SaveChanges()).Returns(1);
            fakeDataContext.Setup(c => c.SaveChangesAsync()).ReturnsAsync(1);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var employeeRepository = new EmployeeRepository(fakeDataContext.Object, null, null);

            var saveData = fixture.Build<DataTransferObjects.Employee>().With(a => a.Identifier, employeeGuid).Create();

            var saveResult = await employeeRepository.SaveEmployeeAsync(saveData, company.Identifier);

            Assert.AreEqual(1, inMemoryDbEmployees.Count);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(saveData, inMemoryDbEmployees[0]));
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task SaveEmployeeWithMatchingIdShouldUpdateAllFields()
        {
            var fixture = new Fixture();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(c => c.CompanyUsers)
                .Without(c => c.Policies).Create();

            int employeeId = 321584;
            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.EmployeeId, employeeId)
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Deductions)
                .Without(e => e.Adjustments)
                .Without(e => e.Work)
                .Create();


            IList<Entities.Employee> inMemoryDbEmployees = new List<Entities.Employee> { employee };
            IList<Entities.Company> inMemoryDbCompanies = new List<Entities.Company> { company };

            var fakeDbEmployees = new Mock<DbSet<Entities.Employee>>();
            fakeDbEmployees.SetupWithQueryable(inMemoryDbEmployees.AsQueryable());
            fakeDbEmployees.SetupDBInsert(inMemoryDbEmployees);

            var fakeDbCompanies = new Mock<DbSet<Entities.Company>>();
            fakeDbCompanies.SetupWithQueryable(inMemoryDbCompanies.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Employees).Returns(fakeDbEmployees.Object);
            fakeDataContext.Setup(c => c.Companies).Returns(fakeDbCompanies.Object);
            fakeDataContext.Setup(c => c.SaveChanges()).Returns(1);
            fakeDataContext.Setup(c => c.SaveChangesAsync()).ReturnsAsync(1);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var employeeRepository = new EmployeeRepository(fakeDataContext.Object, null, null);

            var saveData = fixture.Build<DataTransferObjects.Employee>().With(a => a.EmployeeId, employeeId).Create();

            var saveResult = await employeeRepository.SaveEmployeeAsync(saveData, company.Identifier);

            Assert.AreEqual(1, inMemoryDbEmployees.Count);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(saveData, inMemoryDbEmployees[0]));
        }

        #endregion Save

        #region Delete

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task DeleteEmployeeWhereIdentifierDoesntExistsDoesNothing()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Employee> inMemoryDbEmployees = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Deductions)
                .Without(e => e.Adjustments)
                .Without(e => e.Work)
                .CreateMany()
                .Where(a => a.Identifier != guidThatDoesntExist)
                .ToList();
            int NumberEmployees = inMemoryDbEmployees.Count;
            var fakeDbEmployees = new Mock<DbSet<Entities.Employee>>();
            fakeDbEmployees.SetupWithQueryable(inMemoryDbEmployees.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Employees).Returns(fakeDbEmployees.Object);

            var employeeRepository = new EmployeeRepository(fakeDataContext.Object, null, null);
            await employeeRepository.DeleteEmployeeAsync(guidThatDoesntExist, company.Identifier);
            Assert.AreEqual(NumberEmployees, inMemoryDbEmployees.Where(a => !a.IsDeleted).Count());
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task DeleteEmployeeWhereIdentifierDoesExistsSetsToIsDeletedWithADeletedDate()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            IList<Entities.Employee> inMemoryDbEmployees = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Deductions)
                .Without(e => e.Adjustments)
                .Without(e => e.Work)
                .CreateMany()
                .ToList();
            int NumberEmployees = inMemoryDbEmployees.Count;
            var fakeDbEmployees = new Mock<DbSet<Entities.Employee>>();
            fakeDbEmployees.SetupWithQueryable(inMemoryDbEmployees.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Employees).Returns(fakeDbEmployees.Object);

            var employeeRepository = new EmployeeRepository(fakeDataContext.Object, null, null);
            await employeeRepository.DeleteEmployeeAsync(inMemoryDbEmployees[0].Identifier, company.Identifier);
            Assert.AreEqual(NumberEmployees - 1, inMemoryDbEmployees.Where(a => !a.IsDeleted).Count());
            Assert.IsTrue(inMemoryDbEmployees[0].IsDeleted);
            Assert.IsTrue((DateTime.UtcNow - inMemoryDbEmployees[0].DeletedDate.Value).Milliseconds < 1000);
        }

        #endregion Delete
    }
}
