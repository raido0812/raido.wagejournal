﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ploeh.AutoFixture;
using Raido.WageJournal.DataAccess.Security;
using Raido.WageJournal.DataAccess.DataContext;
using Raido.WageJournal.DataAccess.MappingConfiguration;
using Raido.WageJournal.DataAccess.Repositories;
using Raido.WageJournal.UnitTest.Extensions;
using Raido.WageJournal.UnitTest.MoqAsync;
using Entities = Raido.WageJournal.DataAccess.Entities;
using Raido.WageJournal.UnitTest.Helpers;
using DataTransferObjects = Raido.WageJournal.Contracts.DataTransferObjects;

namespace Raido.WageJournal.UnitTest.Repositories
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class PolicyRepositoryTests
    {
        #region Get
        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetPolicyShouldReturnNullWhenGuidDoesntExist()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Policy> inMemoryDbPolicies = fixture.Build<Entities.Policy>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .CreateMany()
                .Where(a => a.Identifier != guidThatDoesntExist)
                .ToList();

            var fakeDbPolicies = new Mock<DbSet<Entities.Policy>>();
            fakeDbPolicies.SetupWithQueryable(inMemoryDbPolicies.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Policies).Returns(fakeDbPolicies.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var policyRepository = new PolicyRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var policy = await policyRepository.GetPolicyAsync(guidThatDoesntExist, company.Identifier);

            Assert.IsNull(policy);
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetPolicySimpleCaseWhereAllDataExists()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(ce => ce.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();


            IList<Entities.Policy> inMemoryDbPolicies = fixture.Build<Entities.Policy>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .CreateMany()
                .ToList();

            var firstItemGuid = inMemoryDbPolicies[0].Identifier;

            var fakeDbPolicies = new Mock<DbSet<Entities.Policy>>();
            fakeDbPolicies.SetupWithQueryable(inMemoryDbPolicies.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Policies).Returns(fakeDbPolicies.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var policyRepository = new PolicyRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var policy = await policyRepository.GetPolicyAsync(firstItemGuid, company.Identifier);
            Assert.IsNotNull(policy);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(policy, inMemoryDbPolicies[0]));
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetPolicyWhereIncorrectCompany()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(ce => ce.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            IList<Entities.Policy> inMemoryDbPolicies = fixture.Build<Entities.Policy>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .CreateMany()
                .ToList();

            var firstItemGuid = inMemoryDbPolicies[0].Identifier;

            var fakeDbPolicies = new Mock<DbSet<Entities.Policy>>();
            fakeDbPolicies.SetupWithQueryable(inMemoryDbPolicies.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Policies).Returns(fakeDbPolicies.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var policyRepository = new PolicyRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var policy = await policyRepository.GetPolicyAsync(firstItemGuid, Guid.NewGuid());
            Assert.IsNull(policy);
        }

        #endregion Get

        #region Get List

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetPolicyListShouldReturnMatchingPolicyIdentifier()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();


            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Policy> inMemoryDbPolicies = fixture.Build<Entities.Policy>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .CreateMany()
                .Where(a => a.Identifier != guidThatDoesntExist)
                .ToList();

            var fakeDbPolicies = new Mock<DbSet<Entities.Policy>>();
            fakeDbPolicies.SetupWithQueryable(inMemoryDbPolicies.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Policies).Returns(fakeDbPolicies.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var policyRepository = new PolicyRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var policys = await policyRepository.GetPoliciesAsync(new Contracts.SearchFilters.PolicySearchOptions { PolicyIdentifier = Guid.NewGuid() }, company.Identifier);
            Assert.AreEqual(0, policys.Count);
        }

        #endregion Get List

        #region Save

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task SavePolicyShouldInsertAllFields()
        {
            var fixture = new Fixture();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(c => c.CompanyUsers)
                .Without(c => c.Policies).Create();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Policy> inMemoryDbPolicies = new List<Entities.Policy>();
            IList<Entities.Company> inMemoryDbCompanies = new List<Entities.Company> { company };

            var fakeDbPolicies = new Mock<DbSet<Entities.Policy>>();
            fakeDbPolicies.SetupWithQueryable(inMemoryDbPolicies.AsQueryable());
            fakeDbPolicies.SetupDBInsert(inMemoryDbPolicies);

            var fakeDbCompanies = new Mock<DbSet<Entities.Company>>();
            fakeDbCompanies.SetupWithQueryable(inMemoryDbCompanies.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Policies).Returns(fakeDbPolicies.Object);
            fakeDataContext.Setup(c => c.Companies).Returns(fakeDbCompanies.Object);
            fakeDataContext.Setup(c => c.SaveChanges()).Returns(1);
            fakeDataContext.Setup(c => c.SaveChangesAsync()).ReturnsAsync(1);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var policyRepository = new PolicyRepository(fakeDataContext.Object, null, null);

            var saveData = fixture.Create<DataTransferObjects.Policy>();

            var saveResult = await policyRepository.SavePolicyAsync(saveData, company.Identifier);

            Assert.AreEqual(1, inMemoryDbPolicies.Count);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(saveData, inMemoryDbPolicies[0], new string[] { "Value" }));

            // percentages are stored in decimal. ie. 50% = 0.5
            Assert.AreEqual(saveData.Value / (saveData.PolicyType == Contracts.Enumerations.PolicyType.UnemploymentInsuranceFundPercentage ? 100 : 1), inMemoryDbPolicies[0].Value);
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task SavePolicyWithMatchingIdentifierShouldUpdateAllFields()
        {
            var fixture = new Fixture();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(c => c.CompanyUsers)
                .Without(c => c.Policies).Create();

            Guid policyGuid = Guid.NewGuid();
            Entities.Policy policy = fixture.Build<Entities.Policy>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Identifier, policyGuid)
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Create();

            IList<Entities.Policy> inMemoryDbPolicies = new List<Entities.Policy> { policy };
            IList<Entities.Company> inMemoryDbCompanies = new List<Entities.Company> { company };

            var fakeDbPolicies = new Mock<DbSet<Entities.Policy>>();
            fakeDbPolicies.SetupWithQueryable(inMemoryDbPolicies.AsQueryable());
            fakeDbPolicies.SetupDBInsert(inMemoryDbPolicies);

            var fakeDbCompanies = new Mock<DbSet<Entities.Company>>();
            fakeDbCompanies.SetupWithQueryable(inMemoryDbCompanies.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Policies).Returns(fakeDbPolicies.Object);
            fakeDataContext.Setup(c => c.Companies).Returns(fakeDbCompanies.Object);
            fakeDataContext.Setup(c => c.SaveChanges()).Returns(1);
            fakeDataContext.Setup(c => c.SaveChangesAsync()).ReturnsAsync(1);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var policyRepository = new PolicyRepository(fakeDataContext.Object, null, null);

            var saveData = fixture.Build<DataTransferObjects.Policy>().With(a => a.Identifier, policyGuid).Create();

            var saveResult = await policyRepository.SavePolicyAsync(saveData, company.Identifier);

            Assert.AreEqual(1, inMemoryDbPolicies.Count);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(saveData, inMemoryDbPolicies[0]));
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task SavePolicyWithMatchingIdShouldUpdateAllFields()
        {
            var fixture = new Fixture();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(c => c.CompanyUsers)
                .Without(c => c.Policies).Create();

            int policyId = 321584;
            Entities.Policy policy = fixture.Build<Entities.Policy>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.PolicyId, policyId)
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Create();


            IList<Entities.Policy> inMemoryDbPolicies = new List<Entities.Policy> { policy };
            IList<Entities.Company> inMemoryDbCompanies = new List<Entities.Company> { company };

            var fakeDbPolicies = new Mock<DbSet<Entities.Policy>>();
            fakeDbPolicies.SetupWithQueryable(inMemoryDbPolicies.AsQueryable());
            fakeDbPolicies.SetupDBInsert(inMemoryDbPolicies);

            var fakeDbCompanies = new Mock<DbSet<Entities.Company>>();
            fakeDbCompanies.SetupWithQueryable(inMemoryDbCompanies.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Policies).Returns(fakeDbPolicies.Object);
            fakeDataContext.Setup(c => c.Companies).Returns(fakeDbCompanies.Object);
            fakeDataContext.Setup(c => c.SaveChanges()).Returns(1);
            fakeDataContext.Setup(c => c.SaveChangesAsync()).ReturnsAsync(1);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var policyRepository = new PolicyRepository(fakeDataContext.Object, null, null);

            var saveData = fixture.Build<DataTransferObjects.Policy>().With(a => a.PolicyId, policyId).Create();

            var saveResult = await policyRepository.SavePolicyAsync(saveData, company.Identifier);

            Assert.AreEqual(1, inMemoryDbPolicies.Count);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(saveData, inMemoryDbPolicies[0]));
        }

        #endregion Save

        #region Delete

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task DeletePolicyWhereIdentifierDoesntExistsDoesNothing()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Policy> inMemoryDbPolicies = fixture.Build<Entities.Policy>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .CreateMany()
                .Where(a => a.Identifier != guidThatDoesntExist)
                .ToList();
            int NumberPolicies = inMemoryDbPolicies.Count;
            var fakeDbPolicies = new Mock<DbSet<Entities.Policy>>();
            fakeDbPolicies.SetupWithQueryable(inMemoryDbPolicies.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Policies).Returns(fakeDbPolicies.Object);

            var policyRepository = new PolicyRepository(fakeDataContext.Object, null, null);
            await policyRepository.DeletePolicyAsync(guidThatDoesntExist, company.Identifier);
            Assert.AreEqual(NumberPolicies, inMemoryDbPolicies.Where(a => !a.IsDeleted).Count());
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task DeletePolicyWhereIdentifierDoesExistsSetsToIsDeletedWithADeletedDate()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            IList<Entities.Policy> inMemoryDbPolicies = fixture.Build<Entities.Policy>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .CreateMany()
                .ToList();
            int NumberPolicies = inMemoryDbPolicies.Count;
            var fakeDbPolicies = new Mock<DbSet<Entities.Policy>>();
            fakeDbPolicies.SetupWithQueryable(inMemoryDbPolicies.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Policies).Returns(fakeDbPolicies.Object);

            var policyRepository = new PolicyRepository(fakeDataContext.Object, null, null);
            await policyRepository.DeletePolicyAsync(inMemoryDbPolicies[0].Identifier, company.Identifier);
            Assert.AreEqual(NumberPolicies - 1, inMemoryDbPolicies.Where(a => !a.IsDeleted).Count());
            Assert.IsTrue(inMemoryDbPolicies[0].IsDeleted);
            Assert.IsTrue((DateTime.UtcNow - inMemoryDbPolicies[0].DeletedDate.Value).Milliseconds < 1000);
        }

        #endregion Delete
    }
}
