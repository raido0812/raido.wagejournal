﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ploeh.AutoFixture;
using Raido.WageJournal.DataAccess.Security;
using Raido.WageJournal.DataAccess.DataContext;
using Raido.WageJournal.DataAccess.MappingConfiguration;
using Raido.WageJournal.DataAccess.Repositories;
using Raido.WageJournal.UnitTest.Extensions;
using Raido.WageJournal.UnitTest.MoqAsync;
using Entities = Raido.WageJournal.DataAccess.Entities;
using Raido.WageJournal.UnitTest.Helpers;
using DataTransferObjects = Raido.WageJournal.Contracts.DataTransferObjects;

namespace Raido.WageJournal.UnitTest.Repositories
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class WorkRepositoryTests
    {
        #region Get
        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetWorkShouldReturnNullWhenGuidDoesntExist()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Work> inMemoryDbWork = fixture.Build<Entities.Work>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .Where(a => a.Identifier != guidThatDoesntExist)
                .ToList();

            var fakeDbWork = new Mock<DbSet<Entities.Work>>();
            fakeDbWork.SetupWithQueryable(inMemoryDbWork.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Work).Returns(fakeDbWork.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var workRepository = new WorkRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var work = await workRepository.GetWorkAsync(guidThatDoesntExist, employee.Identifier, company.Identifier);

            Assert.IsNull(work);
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetWorkSimpleCaseWhereAllDataExists()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(ce => ce.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            IList<Entities.Work> inMemoryDbWork = fixture.Build<Entities.Work>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .ToList();

            var firstItemGuid = inMemoryDbWork[0].Identifier;

            var fakeDbWork = new Mock<DbSet<Entities.Work>>();
            fakeDbWork.SetupWithQueryable(inMemoryDbWork.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Work).Returns(fakeDbWork.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var workRepository = new WorkRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var work = await workRepository.GetWorkAsync(firstItemGuid, employee.Identifier, company.Identifier);
            Assert.IsNotNull(work);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(work, inMemoryDbWork[0]));
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetWorkWhereIncorrectCompany()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(ce => ce.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            IList<Entities.Work> inMemoryDbWork = fixture.Build<Entities.Work>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .ToList();

            var firstItemGuid = inMemoryDbWork[0].Identifier;

            var fakeDbWork = new Mock<DbSet<Entities.Work>>();
            fakeDbWork.SetupWithQueryable(inMemoryDbWork.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Work).Returns(fakeDbWork.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var workRepository = new WorkRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var work = await workRepository.GetWorkAsync(firstItemGuid, employee.Identifier, Guid.NewGuid());
            Assert.IsNull(work);
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetWorkWhereIncorrectEmployee()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(ce => ce.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            IList<Entities.Work> inMemoryDbWork = fixture.Build<Entities.Work>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .ToList();

            var firstItemGuid = inMemoryDbWork[0].Identifier;

            var fakeDbWork = new Mock<DbSet<Entities.Work>>();
            fakeDbWork.SetupWithQueryable(inMemoryDbWork.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Work).Returns(fakeDbWork.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var workRepository = new WorkRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var work = await workRepository.GetWorkAsync(firstItemGuid, Guid.NewGuid(), company.Identifier);
            Assert.IsNull(work);
        }
        #endregion Get

        #region Get List

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetWorkListShouldReturnMatchingEmployeeIdentifier()
        {

            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Work> inMemoryDbWork = fixture.Build<Entities.Work>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .Where(a => a.Identifier != guidThatDoesntExist)
                .ToList();

            var fakeDbWork = new Mock<DbSet<Entities.Work>>();
            fakeDbWork.SetupWithQueryable(inMemoryDbWork.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Work).Returns(fakeDbWork.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var workRepository = new WorkRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var work = await workRepository.GetWorkAsync(new Contracts.SearchFilters.WorkSearchOptions { EmployeeIdentifier = Guid.NewGuid() }, company.Identifier);
            Assert.AreEqual(0, work.Count);
            work = await workRepository.GetWorkAsync(new Contracts.SearchFilters.WorkSearchOptions { EmployeeIdentifier = employee.Identifier }, company.Identifier);
            Assert.AreEqual(inMemoryDbWork.Count, work.Count);
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetWorkListShouldReturnMatchingPeriodStartResults()
        {

            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Work> inMemoryDbWork = fixture.Build<Entities.Work>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .Where(a => a.Identifier != guidThatDoesntExist)
                .ToList();

            var fakeDbWork = new Mock<DbSet<Entities.Work>>();
            fakeDbWork.SetupWithQueryable(inMemoryDbWork.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Work).Returns(fakeDbWork.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var workRepository = new WorkRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);

            var maxDate = inMemoryDbWork.Max(a => a.DateWorked);
            var minDate = inMemoryDbWork.Min(a => a.DateWorked);
            var work = await workRepository.GetWorkAsync(new Contracts.SearchFilters.WorkSearchOptions { PeriodStart = maxDate }, company.Identifier);
            Assert.AreEqual(inMemoryDbWork.Where(a => a.DateWorked >= maxDate).Count(), work.Count);
            work = await workRepository.GetWorkAsync(new Contracts.SearchFilters.WorkSearchOptions { PeriodStart = minDate }, company.Identifier);
            Assert.AreEqual(inMemoryDbWork.Where(a => a.DateWorked >= minDate).Count(), work.Count);
            work = await workRepository.GetWorkAsync(new Contracts.SearchFilters.WorkSearchOptions { PeriodStart = maxDate.AddDays(1) }, company.Identifier);
            Assert.AreEqual(0, work.Count);
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetWorkListShouldReturnMatchingPeriodEndResults()
        {

            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Work> inMemoryDbWork = fixture.Build<Entities.Work>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .Where(a => a.Identifier != guidThatDoesntExist)
                .ToList();

            var fakeDbWork = new Mock<DbSet<Entities.Work>>();
            fakeDbWork.SetupWithQueryable(inMemoryDbWork.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Work).Returns(fakeDbWork.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var workRepository = new WorkRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);

            var maxDate = inMemoryDbWork.Max(a => a.DateWorked);
            var minDate = inMemoryDbWork.Min(a => a.DateWorked);
            var work = await workRepository.GetWorkAsync(new Contracts.SearchFilters.WorkSearchOptions { PeriodEnd = maxDate }, company.Identifier);
            Assert.AreEqual(inMemoryDbWork.Where(a => a.DateWorked <= maxDate).Count(), work.Count);
            work = await workRepository.GetWorkAsync(new Contracts.SearchFilters.WorkSearchOptions { PeriodEnd = minDate }, company.Identifier);
            Assert.AreEqual(inMemoryDbWork.Where(a => a.DateWorked <= minDate).Count(), work.Count);
            work = await workRepository.GetWorkAsync(new Contracts.SearchFilters.WorkSearchOptions { PeriodEnd = minDate.AddDays(-1) }, company.Identifier);
            Assert.AreEqual(0, work.Count);
        }
        #endregion Get List

        #region Save

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task SaveWorkShouldInsertAllFields()
        {
            var fixture = new Fixture();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(c => c.CompanyUsers)
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Work> inMemoryDbWork = new List<Entities.Work>();
            IList<Entities.Employee> inMemoryDbEmployees = new List<Entities.Employee> { employee };

            var fakeDbWork = new Mock<DbSet<Entities.Work>>();
            fakeDbWork.SetupWithQueryable(inMemoryDbWork.AsQueryable());
            fakeDbWork.SetupDBInsert(inMemoryDbWork);

            var fakeDbEmployees = new Mock<DbSet<Entities.Employee>>();
            fakeDbEmployees.SetupWithQueryable(inMemoryDbEmployees.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Work).Returns(fakeDbWork.Object);
            fakeDataContext.Setup(c => c.Employees).Returns(fakeDbEmployees.Object);
            fakeDataContext.Setup(c => c.SaveChanges()).Returns(1);
            fakeDataContext.Setup(c => c.SaveChangesAsync()).ReturnsAsync(1);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var workRepository = new WorkRepository(fakeDataContext.Object, null, null);

            var saveData = fixture.Create<DataTransferObjects.Work>();

            var saveResult = await workRepository.SaveWorkAsync(saveData, employee.Identifier, company.Identifier);

            Assert.AreEqual(1, inMemoryDbWork.Count);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(saveData, inMemoryDbWork[0]));
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task SaveWorkWithMatchingIdentifierShouldUpdateAllFields()
        {
            var fixture = new Fixture();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(c => c.CompanyUsers)
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            Guid workGuid = Guid.NewGuid();
            Entities.Work work = fixture.Build<Entities.Work>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.Identifier, workGuid)
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .Create();

            IList<Entities.Work> inMemoryDbWork = new List<Entities.Work> { work };
            IList<Entities.Employee> inMemoryDbEmployees = new List<Entities.Employee> { employee };

            var fakeDbWork = new Mock<DbSet<Entities.Work>>();
            fakeDbWork.SetupWithQueryable(inMemoryDbWork.AsQueryable());
            fakeDbWork.SetupDBInsert(inMemoryDbWork);

            var fakeDbEmployees = new Mock<DbSet<Entities.Employee>>();
            fakeDbEmployees.SetupWithQueryable(inMemoryDbEmployees.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Work).Returns(fakeDbWork.Object);
            fakeDataContext.Setup(c => c.Employees).Returns(fakeDbEmployees.Object);
            fakeDataContext.Setup(c => c.SaveChanges()).Returns(1);
            fakeDataContext.Setup(c => c.SaveChangesAsync()).ReturnsAsync(1);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var workRepository = new WorkRepository(fakeDataContext.Object, null, null);

            var saveData = fixture.Build<DataTransferObjects.Work>().With(a => a.Identifier, workGuid).Create();

            var saveResult = await workRepository.SaveWorkAsync(saveData, employee.Identifier, company.Identifier);

            Assert.AreEqual(1, inMemoryDbWork.Count);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(saveData, inMemoryDbWork[0]));
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task SaveWorkWithMatchingIdShouldUpdateAllFields()
        {
            var fixture = new Fixture();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(c => c.CompanyUsers)
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            int workId = 99485;
            Entities.Work work = fixture.Build<Entities.Work>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.WorkId, workId)
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .Create();

            IList<Entities.Work> inMemoryDbWork = new List<Entities.Work> { work };
            IList<Entities.Employee> inMemoryDbEmployees = new List<Entities.Employee> { employee };

            var fakeDbWork = new Mock<DbSet<Entities.Work>>();
            fakeDbWork.SetupWithQueryable(inMemoryDbWork.AsQueryable());
            fakeDbWork.SetupDBInsert(inMemoryDbWork);

            var fakeDbEmployees = new Mock<DbSet<Entities.Employee>>();
            fakeDbEmployees.SetupWithQueryable(inMemoryDbEmployees.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Work).Returns(fakeDbWork.Object);
            fakeDataContext.Setup(c => c.Employees).Returns(fakeDbEmployees.Object);
            fakeDataContext.Setup(c => c.SaveChanges()).Returns(1);
            fakeDataContext.Setup(c => c.SaveChangesAsync()).ReturnsAsync(1);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var workRepository = new WorkRepository(fakeDataContext.Object, null, null);

            var saveData = fixture.Build<DataTransferObjects.Work>().With(a => a.WorkId, workId).Create();

            var saveResult = await workRepository.SaveWorkAsync(saveData, employee.Identifier, company.Identifier);

            Assert.AreEqual(1, inMemoryDbWork.Count);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(saveData, inMemoryDbWork[0]));
        }

        #endregion Save

        #region Delete

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task DeleteWorkWhereIdentifierDoesntExistsDoesNothing()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Work> inMemoryDbWork = fixture.Build<Entities.Work>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .Where(a => a.Identifier != guidThatDoesntExist)
                .ToList();
            int NumberWork = inMemoryDbWork.Count;
            var fakeDbWork = new Mock<DbSet<Entities.Work>>();
            fakeDbWork.SetupWithQueryable(inMemoryDbWork.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Work).Returns(fakeDbWork.Object);

            var workRepository = new WorkRepository(fakeDataContext.Object, null, null);
            await workRepository.DeleteWorkAsync(guidThatDoesntExist, employee.Identifier, company.Identifier);
            Assert.AreEqual(NumberWork, inMemoryDbWork.Where(a => !a.IsDeleted).Count());
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task DeleteWorkWhereIdentifierDoesExistsSetsToIsDeletedWithADeletedDate()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            IList<Entities.Work> inMemoryDbWork = fixture.Build<Entities.Work>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .ToList();
            int NumberWork = inMemoryDbWork.Count;
            var fakeDbWork = new Mock<DbSet<Entities.Work>>();
            fakeDbWork.SetupWithQueryable(inMemoryDbWork.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Work).Returns(fakeDbWork.Object);

            var workRepository = new WorkRepository(fakeDataContext.Object, null, null);
            await workRepository.DeleteWorkAsync(inMemoryDbWork[0].Identifier, employee.Identifier, company.Identifier);
            Assert.AreEqual(NumberWork - 1, inMemoryDbWork.Where(a => !a.IsDeleted).Count());
            Assert.IsTrue(inMemoryDbWork[0].IsDeleted);
            Assert.IsTrue((DateTime.UtcNow - inMemoryDbWork[0].DeletedDate.Value).Milliseconds < 1000);
        }

        #endregion Delete
    }
}
