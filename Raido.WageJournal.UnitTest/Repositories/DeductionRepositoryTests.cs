﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ploeh.AutoFixture;
using Raido.WageJournal.DataAccess.Security;
using Raido.WageJournal.DataAccess.DataContext;
using Raido.WageJournal.DataAccess.MappingConfiguration;
using Raido.WageJournal.DataAccess.Repositories;
using Raido.WageJournal.UnitTest.Extensions;
using Raido.WageJournal.UnitTest.MoqAsync;
using Entities = Raido.WageJournal.DataAccess.Entities;
using Raido.WageJournal.UnitTest.Helpers;
using DataTransferObjects = Raido.WageJournal.Contracts.DataTransferObjects;

namespace Raido.WageJournal.UnitTest.Repositories
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class DeductionRepositoryTests
    {
        #region Get
        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetDeductionShouldReturnNullWhenGuidDoesntExist()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Deductions)
                .Without(e => e.Adjustments)
                .Without(e => e.Work).Create();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Deduction> inMemoryDbDeductions = fixture.Build<Entities.Deduction>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .Where(a => a.Identifier != guidThatDoesntExist)
                .ToList();

            var fakeDbDeductions = new Mock<DbSet<Entities.Deduction>>();
            fakeDbDeductions.SetupWithQueryable(inMemoryDbDeductions.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Deductions).Returns(fakeDbDeductions.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var deductionRepository = new DeductionRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var deduction = await deductionRepository.GetDeductionAsync(guidThatDoesntExist, employee.Identifier, company.Identifier);

            Assert.IsNull(deduction);
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetDeductionSimpleCaseWhereAllDataExists()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(ce => ce.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Deductions)
                .Without(e => e.Adjustments)
                .Without(e => e.Work).Create();

            IList<Entities.Deduction> inMemoryDbDeductions = fixture.Build<Entities.Deduction>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .ToList();

            var firstItemGuid = inMemoryDbDeductions[0].Identifier;

            var fakeDbDeductions = new Mock<DbSet<Entities.Deduction>>();
            fakeDbDeductions.SetupWithQueryable(inMemoryDbDeductions.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Deductions).Returns(fakeDbDeductions.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var deductionRepository = new DeductionRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var deduction = await deductionRepository.GetDeductionAsync(firstItemGuid, employee.Identifier, company.Identifier);
            Assert.IsNotNull(deduction);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(deduction, inMemoryDbDeductions[0]));
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetDeductionWhereIncorrectCompany()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(ce => ce.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Deductions)
                .Without(e => e.Adjustments)
                .Without(e => e.Work).Create();

            IList<Entities.Deduction> inMemoryDbDeductions = fixture.Build<Entities.Deduction>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .ToList();

            var firstItemGuid = inMemoryDbDeductions[0].Identifier;

            var fakeDbDeductions = new Mock<DbSet<Entities.Deduction>>();
            fakeDbDeductions.SetupWithQueryable(inMemoryDbDeductions.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Deductions).Returns(fakeDbDeductions.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var deductionRepository = new DeductionRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var deduction = await deductionRepository.GetDeductionAsync(firstItemGuid, employee.Identifier, Guid.NewGuid());
            Assert.IsNull(deduction);
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetDeductionWhereIncorrectEmployee()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(ce => ce.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Deductions)
                .Without(e => e.Adjustments)
                .Without(e => e.Work).Create();

            IList<Entities.Deduction> inMemoryDbDeductions = fixture.Build<Entities.Deduction>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .ToList();

            var firstItemGuid = inMemoryDbDeductions[0].Identifier;

            var fakeDbDeductions = new Mock<DbSet<Entities.Deduction>>();
            fakeDbDeductions.SetupWithQueryable(inMemoryDbDeductions.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Deductions).Returns(fakeDbDeductions.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var deductionRepository = new DeductionRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var deduction = await deductionRepository.GetDeductionAsync(firstItemGuid, Guid.NewGuid(), company.Identifier);
            Assert.IsNull(deduction);
        }
        #endregion Get

        #region Get List

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetDeductionListShouldReturnMatchingEmployeeIdentifier()
        {

            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Deductions)
                .Without(e => e.Adjustments)
                .Without(e => e.Work).Create();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Deduction> inMemoryDbDeductions = fixture.Build<Entities.Deduction>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .Where(a => a.Identifier != guidThatDoesntExist)
                .ToList();

            var fakeDbDeductions = new Mock<DbSet<Entities.Deduction>>();
            fakeDbDeductions.SetupWithQueryable(inMemoryDbDeductions.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Deductions).Returns(fakeDbDeductions.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var deductionRepository = new DeductionRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var deductions = await deductionRepository.GetDeductionsAsync(new Contracts.SearchFilters.DeductionSearchOptions { EmployeeIdentifier = Guid.NewGuid() }, company.Identifier);
            Assert.AreEqual(0, deductions.Count);
            deductions = await deductionRepository.GetDeductionsAsync(new Contracts.SearchFilters.DeductionSearchOptions { EmployeeIdentifier = employee.Identifier }, company.Identifier);
            Assert.AreEqual(inMemoryDbDeductions.Count, deductions.Count);
        }

        #endregion Get List

        #region Save

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task SaveDeductionShouldInsertAllFields()
        {
            var fixture = new Fixture();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(c => c.CompanyUsers)
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Deductions)
                .Without(e => e.Adjustments)
                .Without(e => e.Work).Create();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Deduction> inMemoryDbDeductions = new List<Entities.Deduction>();
            IList<Entities.Employee> inMemoryDbEmployees = new List<Entities.Employee> { employee };
            IList<Entities.Company> inMemoryDbCompanies = new List<Entities.Company> { company };

            var fakeDbDeductions = new Mock<DbSet<Entities.Deduction>>();
            fakeDbDeductions.SetupWithQueryable(inMemoryDbDeductions.AsQueryable());
            fakeDbDeductions.SetupDBInsert(inMemoryDbDeductions);

            var fakeDbEmployees = new Mock<DbSet<Entities.Employee>>();
            fakeDbEmployees.SetupWithQueryable(inMemoryDbEmployees.AsQueryable());

            var fakeDbCompanies = new Mock<DbSet<Entities.Company>>();
            fakeDbCompanies.SetupWithQueryable(inMemoryDbCompanies.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Deductions).Returns(fakeDbDeductions.Object);
            fakeDataContext.Setup(c => c.Employees).Returns(fakeDbEmployees.Object);
            fakeDataContext.Setup(c => c.Companies).Returns(fakeDbCompanies.Object);
            fakeDataContext.Setup(c => c.SaveChanges()).Returns(1);
            fakeDataContext.Setup(c => c.SaveChangesAsync()).ReturnsAsync(1);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var deductionRepository = new DeductionRepository(fakeDataContext.Object, null, null);

            var saveData = fixture.Create<DataTransferObjects.Deduction>();

            var saveResult = await deductionRepository.SaveDeductionAsync(saveData, employee.Identifier, company.Identifier);

            Assert.AreEqual(1, inMemoryDbDeductions.Count);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(saveData, inMemoryDbDeductions[0]));
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task SaveDeductionWithMatchingIdentifierShouldUpdateAllFields()
        {
            var fixture = new Fixture();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(c => c.CompanyUsers)
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Deductions)
                .Without(e => e.Adjustments)
                .Without(e => e.Work).Create();

            Guid deductionGuid = Guid.NewGuid();
            Entities.Deduction deduction = fixture.Build<Entities.Deduction>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.Identifier, deductionGuid)
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .Create();

            IList<Entities.Deduction> inMemoryDbDeductions = new List<Entities.Deduction> { deduction };
            IList<Entities.Employee> inMemoryDbEmployees = new List<Entities.Employee> { employee };
            IList<Entities.Company> inMemoryDbCompanies = new List<Entities.Company> { company };

            var fakeDbDeductions = new Mock<DbSet<Entities.Deduction>>();
            fakeDbDeductions.SetupWithQueryable(inMemoryDbDeductions.AsQueryable());
            fakeDbDeductions.SetupDBInsert(inMemoryDbDeductions);

            var fakeDbEmployees = new Mock<DbSet<Entities.Employee>>();
            fakeDbEmployees.SetupWithQueryable(inMemoryDbEmployees.AsQueryable());

            var fakeDbCompanies = new Mock<DbSet<Entities.Company>>();
            fakeDbCompanies.SetupWithQueryable(inMemoryDbCompanies.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Deductions).Returns(fakeDbDeductions.Object);
            fakeDataContext.Setup(c => c.Employees).Returns(fakeDbEmployees.Object);
            fakeDataContext.Setup(c => c.Companies).Returns(fakeDbCompanies.Object);
            fakeDataContext.Setup(c => c.SaveChanges()).Returns(1);
            fakeDataContext.Setup(c => c.SaveChangesAsync()).ReturnsAsync(1);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var deductionRepository = new DeductionRepository(fakeDataContext.Object, null, null);

            var saveData = fixture.Build<DataTransferObjects.Deduction>().With(a => a.Identifier, deductionGuid).Create();

            var saveResult = await deductionRepository.SaveDeductionAsync(saveData, employee.Identifier, company.Identifier);

            Assert.AreEqual(1, inMemoryDbDeductions.Count);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(saveData, inMemoryDbDeductions[0]));
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task SaveDeductionWithMatchingIdShouldUpdateAllFields()
        {
            var fixture = new Fixture();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(c => c.CompanyUsers)
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Deductions)
                .Without(e => e.Adjustments)
                .Without(e => e.Work).Create();

            int deductionId = 99485;
            Entities.Deduction deduction = fixture.Build<Entities.Deduction>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.DeductionId, deductionId)
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .Create();

            IList<Entities.Deduction> inMemoryDbDeductions = new List<Entities.Deduction> { deduction };
            IList<Entities.Employee> inMemoryDbEmployees = new List<Entities.Employee> { employee };
            IList<Entities.Company> inMemoryDbCompanies = new List<Entities.Company> { company };

            var fakeDbDeductions = new Mock<DbSet<Entities.Deduction>>();
            fakeDbDeductions.SetupWithQueryable(inMemoryDbDeductions.AsQueryable());
            fakeDbDeductions.SetupDBInsert(inMemoryDbDeductions);

            var fakeDbEmployees = new Mock<DbSet<Entities.Employee>>();
            fakeDbEmployees.SetupWithQueryable(inMemoryDbEmployees.AsQueryable());

            var fakeDbCompanies = new Mock<DbSet<Entities.Company>>();
            fakeDbCompanies.SetupWithQueryable(inMemoryDbCompanies.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Deductions).Returns(fakeDbDeductions.Object);
            fakeDataContext.Setup(c => c.Employees).Returns(fakeDbEmployees.Object);
            fakeDataContext.Setup(c => c.Companies).Returns(fakeDbCompanies.Object);
            fakeDataContext.Setup(c => c.SaveChanges()).Returns(1);
            fakeDataContext.Setup(c => c.SaveChangesAsync()).ReturnsAsync(1);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var deductionRepository = new DeductionRepository(fakeDataContext.Object, null, null);

            var saveData = fixture.Build<DataTransferObjects.Deduction>().With(a => a.DeductionId, deductionId).Create();

            var saveResult = await deductionRepository.SaveDeductionAsync(saveData, employee.Identifier, company.Identifier);

            Assert.AreEqual(1, inMemoryDbDeductions.Count);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(saveData, inMemoryDbDeductions[0]));
        }

        #endregion Save

        #region Delete

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task DeleteDeductionWhereIdentifierDoesntExistsDoesNothing()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Deductions)
                .Without(e => e.Adjustments)
                .Without(e => e.Work).Create();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Deduction> inMemoryDbDeductions = fixture.Build<Entities.Deduction>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .Where(a => a.Identifier != guidThatDoesntExist)
                .ToList();
            int NumberDeductions = inMemoryDbDeductions.Count;
            var fakeDbDeductions = new Mock<DbSet<Entities.Deduction>>();
            fakeDbDeductions.SetupWithQueryable(inMemoryDbDeductions.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Deductions).Returns(fakeDbDeductions.Object);

            var deductionRepository = new DeductionRepository(fakeDataContext.Object, null, null);
            await deductionRepository.DeleteDeductionAsync(guidThatDoesntExist, employee.Identifier, company.Identifier);
            Assert.AreEqual(NumberDeductions, inMemoryDbDeductions.Where(a => !a.IsDeleted).Count());
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task DeleteDeductionWhereIdentifierDoesExistsSetsToIsDeletedWithADeletedDate()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Deductions)
                .Without(e => e.Adjustments)
                .Without(e => e.Work).Create();

            IList<Entities.Deduction> inMemoryDbDeductions = fixture.Build<Entities.Deduction>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .ToList();
            int NumberDeductions = inMemoryDbDeductions.Count;
            var fakeDbDeductions = new Mock<DbSet<Entities.Deduction>>();
            fakeDbDeductions.SetupWithQueryable(inMemoryDbDeductions.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Deductions).Returns(fakeDbDeductions.Object);

            var deductionRepository = new DeductionRepository(fakeDataContext.Object, null, null);
            await deductionRepository.DeleteDeductionAsync(inMemoryDbDeductions[0].Identifier, employee.Identifier, company.Identifier);
            Assert.AreEqual(NumberDeductions - 1, inMemoryDbDeductions.Where(a => !a.IsDeleted).Count());
            Assert.IsTrue(inMemoryDbDeductions[0].IsDeleted);
            Assert.IsTrue((DateTime.UtcNow - inMemoryDbDeductions[0].DeletedDate.Value).Milliseconds < 1000);
        }

        #endregion Delete
    }
}
