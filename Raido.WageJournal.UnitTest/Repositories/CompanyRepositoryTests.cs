﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ploeh.AutoFixture;
using Raido.WageJournal.DataAccess.Security;
using Raido.WageJournal.DataAccess.DataContext;
using Raido.WageJournal.DataAccess.MappingConfiguration;
using Raido.WageJournal.DataAccess.Repositories;
using Raido.WageJournal.UnitTest.Extensions;
using Raido.WageJournal.UnitTest.MoqAsync;
using Entities = Raido.WageJournal.DataAccess.Entities;
using Raido.WageJournal.UnitTest.Helpers;
using DataTransferObjects = Raido.WageJournal.Contracts.DataTransferObjects;
namespace Raido.WageJournal.UnitTest.Repositories
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class CompanyRepositoryTests
    {
        #region Get
        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetCompanyShouldReturnNullWhenGuidDoesntExist()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            IList<Entities.CompanyUser> inMemoryDbCompanyUsers = new List<Entities.CompanyUser> { companyUser };
            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Company> inMemoryDbCompanies = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, inMemoryDbCompanyUsers)
                .Without(c => c.Policies)
                .CreateMany()
                .Where(a => a.Identifier != guidThatDoesntExist)
                .ToList();

            var fakeDbCompanies = new Mock<DbSet<Entities.Company>>();
            fakeDbCompanies.SetupWithQueryable(inMemoryDbCompanies.AsQueryable());

            var fakeDbCompanyUsers = new Mock<DbSet<Entities.CompanyUser>>();
            fakeDbCompanyUsers.SetupWithQueryable(inMemoryDbCompanyUsers.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Companies).Returns(fakeDbCompanies.Object);
            fakeDataContext.Setup(c => c.CompanyUsers).Returns(fakeDbCompanyUsers.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var companyRepository = new CompanyRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var company = await companyRepository.GetCompanyForCurrentUserAsync(guidThatDoesntExist);

            Assert.IsNull(company);
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetCompanySimpleCaseWhereAllDataExists()
        {
            var fixture = new Fixture();

            IList<Entities.Company> inMemoryDbCompanies = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(c => c.CompanyUsers)
                .Without(c => c.Policies)
                .CreateMany()
                .ToList();

            string userId = Guid.NewGuid().ToString();
            IList<Entities.CompanyUser> inMemoryDbCompanyUsers = new List<Entities.CompanyUser>();
            foreach (var company in inMemoryDbCompanies)
            {
                Entities.CompanyUser companyUser = new Entities.CompanyUser() { Company = company, CompanyId = company.CompanyId, UserId = userId };
                company.CompanyUsers = new List<Entities.CompanyUser>() { companyUser };
                inMemoryDbCompanyUsers.Add(companyUser);
            }

            var firstItemGuid = inMemoryDbCompanies[0].Identifier;

            var fakeDbCompanies = new Mock<DbSet<Entities.Company>>();
            fakeDbCompanies.SetupWithQueryable(inMemoryDbCompanies.AsQueryable());

            var fakeDbCompanyUsers = new Mock<DbSet<Entities.CompanyUser>>();
            fakeDbCompanyUsers.SetupWithQueryable(inMemoryDbCompanyUsers.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Companies).Returns(fakeDbCompanies.Object);
            fakeDataContext.Setup(c => c.CompanyUsers).Returns(fakeDbCompanyUsers.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(userId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var companyRepository = new CompanyRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var companyData = await companyRepository.GetCompanyForCurrentUserAsync(firstItemGuid);
            Assert.IsNotNull(companyData);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(companyData, inMemoryDbCompanies[0]));
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetCompanyWhereIncorrectUser()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(ce => ce.Company).Create();

            IList<Entities.CompanyUser> inMemoryDbCompanyUsers = new List<Entities.CompanyUser> { companyUser };
            IList<Entities.Company> inMemoryDbCompanies = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, inMemoryDbCompanyUsers)
                .Without(c => c.Policies)
                .CreateMany()
                .ToList();

            var firstItemGuid = inMemoryDbCompanies[0].Identifier;

            var fakeDbCompanies = new Mock<DbSet<Entities.Company>>();
            fakeDbCompanies.SetupWithQueryable(inMemoryDbCompanies.AsQueryable());

            var fakeDbCompanyUsers = new Mock<DbSet<Entities.CompanyUser>>();
            fakeDbCompanyUsers.SetupWithQueryable(inMemoryDbCompanyUsers.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Companies).Returns(fakeDbCompanies.Object);
            fakeDataContext.Setup(c => c.CompanyUsers).Returns(fakeDbCompanyUsers.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId + 1);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var companyRepository = new CompanyRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var company = await companyRepository.GetCompanyForCurrentUserAsync(firstItemGuid);
            Assert.IsNull(company);
        }

        #endregion Get

        #region Get List

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetCompanyListShouldReturnAllCompaniesForUser()
        {
            var fixture = new Fixture();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Company> inMemoryDbCompanies = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(c => c.CompanyUsers)
                .Without(c => c.Policies)
                .CreateMany()
                .Where(a => a.Identifier != guidThatDoesntExist)
                .ToList();

            string userId = Guid.NewGuid().ToString();
            IList<Entities.CompanyUser> inMemoryDbCompanyUsers = new List<Entities.CompanyUser>();
            foreach (var company in inMemoryDbCompanies)
            {
                Entities.CompanyUser companyUser = new Entities.CompanyUser() { Company = company, CompanyId = company.CompanyId, UserId = userId };
                company.CompanyUsers = new List<Entities.CompanyUser>() { companyUser };
                inMemoryDbCompanyUsers.Add(companyUser);
            }

            var fakeDbCompanies = new Mock<DbSet<Entities.Company>>();
            fakeDbCompanies.SetupWithQueryable(inMemoryDbCompanies.AsQueryable());

            var fakeDbCompanyUsers = new Mock<DbSet<Entities.CompanyUser>>();
            fakeDbCompanyUsers.SetupWithQueryable(inMemoryDbCompanyUsers.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Companies).Returns(fakeDbCompanies.Object);
            fakeDataContext.Setup(c => c.CompanyUsers).Returns(fakeDbCompanyUsers.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(userId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var companyRepository = new CompanyRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var companys = await companyRepository.GetCompaniesAsync(null, userId);
            Assert.AreEqual(inMemoryDbCompanies.Count, companys.Count);
        }

        #endregion Get List

        #region Save

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task SaveCompanyShouldInsertAllFields()
        {
            var fixture = new Fixture();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(c => c.CompanyUsers)
                .Without(c => c.Policies).Create();

            string userId = Guid.NewGuid().ToString();
            IList<Entities.CompanyUser> inMemoryDbCompanyUsers = new List<Entities.CompanyUser>();
            Entities.CompanyUser companyUser = new Entities.CompanyUser() { Company = company, CompanyId = company.CompanyId, UserId = userId };
            company.CompanyUsers = new List<Entities.CompanyUser>() { companyUser };
            inMemoryDbCompanyUsers.Add(companyUser);

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Company> inMemoryDbCompanies = new List<Entities.Company>();

            var fakeDbCompanies = new Mock<DbSet<Entities.Company>>();
            fakeDbCompanies.SetupWithQueryable(inMemoryDbCompanies.AsQueryable());
            fakeDbCompanies.SetupDBInsert(inMemoryDbCompanies);

            var fakeDbCompanyUsers = new Mock<DbSet<Entities.CompanyUser>>();
            fakeDbCompanyUsers.SetupWithQueryable(inMemoryDbCompanyUsers.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Companies).Returns(fakeDbCompanies.Object);
            fakeDataContext.Setup(c => c.CompanyUsers).Returns(fakeDbCompanyUsers.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(userId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var companyRepository = new CompanyRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);

            var saveData = fixture.Create<DataTransferObjects.Company>();

            var saveResult = await companyRepository.SaveCompanyAsync(saveData);

            Assert.AreEqual(1, inMemoryDbCompanies.Count);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(saveData, inMemoryDbCompanies[0]));
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task SaveCompanyWithMatchingIdentifierShouldUpdateAllFields()
        {
            var fixture = new Fixture();
            var companyGuid = Guid.NewGuid();
            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.Identifier, companyGuid)
                .Without(c => c.CompanyUsers)
                .Without(c => c.Policies).Create();

            string userId = Guid.NewGuid().ToString();
            IList<Entities.CompanyUser> inMemoryDbCompanyUsers = new List<Entities.CompanyUser>();
            Entities.CompanyUser companyUser = new Entities.CompanyUser() { Company = company, CompanyId = company.CompanyId, UserId = userId };
            company.CompanyUsers = new List<Entities.CompanyUser>() { companyUser };
            inMemoryDbCompanyUsers.Add(companyUser);

            IList<Entities.Company> inMemoryDbCompanies = new List<Entities.Company> { company };

            var fakeDbCompanies = new Mock<DbSet<Entities.Company>>();
            fakeDbCompanies.SetupWithQueryable(inMemoryDbCompanies.AsQueryable());
            fakeDbCompanies.SetupDBInsert(inMemoryDbCompanies);

            var fakeDbCompanyUsers = new Mock<DbSet<Entities.CompanyUser>>();
            fakeDbCompanyUsers.SetupWithQueryable(inMemoryDbCompanyUsers.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Companies).Returns(fakeDbCompanies.Object);
            fakeDataContext.Setup(c => c.CompanyUsers).Returns(fakeDbCompanyUsers.Object);
            fakeDataContext.Setup(c => c.SaveChanges()).Returns(1);
            fakeDataContext.Setup(c => c.SaveChangesAsync()).ReturnsAsync(1);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(Guid.NewGuid().ToString());

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var companyRepository = new CompanyRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);

            var saveData = fixture.Build<DataTransferObjects.Company>().With(a => a.Identifier, companyGuid).Create();

            var saveResult = await companyRepository.SaveCompanyAsync(saveData);

            Assert.AreEqual(1, inMemoryDbCompanies.Count);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(saveData, inMemoryDbCompanies[0]));
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task SaveCompanyWithMatchingIdShouldUpdateAllFields()
        {
            var fixture = new Fixture();
            var companyId = 98456;
            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyId, companyId)
                .Without(c => c.CompanyUsers)
                .Without(c => c.Policies).Create();

            string userId = Guid.NewGuid().ToString();
            IList<Entities.CompanyUser> inMemoryDbCompanyUsers = new List<Entities.CompanyUser>();
            Entities.CompanyUser companyUser = new Entities.CompanyUser() { Company = company, CompanyId = company.CompanyId, UserId = userId };
            company.CompanyUsers = new List<Entities.CompanyUser>() { companyUser };
            inMemoryDbCompanyUsers.Add(companyUser);

            IList<Entities.Company> inMemoryDbCompanies = new List<Entities.Company> { company };

            var fakeDbCompanies = new Mock<DbSet<Entities.Company>>();
            fakeDbCompanies.SetupWithQueryable(inMemoryDbCompanies.AsQueryable());
            fakeDbCompanies.SetupDBInsert(inMemoryDbCompanies);

            var fakeDbCompanyUsers = new Mock<DbSet<Entities.CompanyUser>>();
            fakeDbCompanyUsers.SetupWithQueryable(inMemoryDbCompanyUsers.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Companies).Returns(fakeDbCompanies.Object);
            fakeDataContext.Setup(c => c.CompanyUsers).Returns(fakeDbCompanyUsers.Object);
            fakeDataContext.Setup(c => c.SaveChanges()).Returns(1);
            fakeDataContext.Setup(c => c.SaveChangesAsync()).ReturnsAsync(1);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(Guid.NewGuid().ToString());

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var companyRepository = new CompanyRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);

            var saveData = fixture.Build<DataTransferObjects.Company>().With(a => a.CompanyId, companyId).Create();

            var saveResult = await companyRepository.SaveCompanyAsync(saveData);

            Assert.AreEqual(1, inMemoryDbCompanies.Count);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(saveData, inMemoryDbCompanies[0]));
        }

        #endregion Save
    }
}
