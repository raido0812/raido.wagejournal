﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ploeh.AutoFixture;
using Raido.WageJournal.DataAccess.Security;
using Raido.WageJournal.DataAccess.DataContext;
using Raido.WageJournal.DataAccess.MappingConfiguration;
using Raido.WageJournal.DataAccess.Repositories;
using Raido.WageJournal.UnitTest.Extensions;
using Raido.WageJournal.UnitTest.MoqAsync;
using Entities = Raido.WageJournal.DataAccess.Entities;
using Raido.WageJournal.UnitTest.Helpers;
using DataTransferObjects = Raido.WageJournal.Contracts.DataTransferObjects;

namespace Raido.WageJournal.UnitTest.Repositories
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class AdjustmentRepositoryTests
    {
        #region Get
        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetAdjustmentShouldReturnNullWhenGuidDoesntExist()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Adjustment> inMemoryDbAdjustments = fixture.Build<Entities.Adjustment>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .Where(a => a.Identifier != guidThatDoesntExist)
                .ToList();

            var fakeDbAdjustments = new Mock<DbSet<Entities.Adjustment>>();
            fakeDbAdjustments.SetupWithQueryable(inMemoryDbAdjustments.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Adjustments).Returns(fakeDbAdjustments.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var adjustmentRepository = new AdjustmentRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var adjustment = await adjustmentRepository.GetAdjustmentAsync(guidThatDoesntExist, employee.Identifier, company.Identifier);

            Assert.IsNull(adjustment);
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetAdjustmentSimpleCaseWhereAllDataExists()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(ce => ce.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            IList<Entities.Adjustment> inMemoryDbAdjustments = fixture.Build<Entities.Adjustment>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .ToList();

            var firstItemGuid = inMemoryDbAdjustments[0].Identifier;

            var fakeDbAdjustments = new Mock<DbSet<Entities.Adjustment>>();
            fakeDbAdjustments.SetupWithQueryable(inMemoryDbAdjustments.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Adjustments).Returns(fakeDbAdjustments.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var adjustmentRepository = new AdjustmentRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var adjustment = await adjustmentRepository.GetAdjustmentAsync(firstItemGuid, employee.Identifier, company.Identifier);
            Assert.IsNotNull(adjustment);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(adjustment, inMemoryDbAdjustments[0]));
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetAdjustmentWhereIncorrectCompany()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(ce => ce.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            IList<Entities.Adjustment> inMemoryDbAdjustments = fixture.Build<Entities.Adjustment>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .ToList();

            var firstItemGuid = inMemoryDbAdjustments[0].Identifier;

            var fakeDbAdjustments = new Mock<DbSet<Entities.Adjustment>>();
            fakeDbAdjustments.SetupWithQueryable(inMemoryDbAdjustments.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Adjustments).Returns(fakeDbAdjustments.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var adjustmentRepository = new AdjustmentRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var adjustment = await adjustmentRepository.GetAdjustmentAsync(firstItemGuid, employee.Identifier, Guid.NewGuid());
            Assert.IsNull(adjustment);
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetAdjustmentWhereIncorrectEmployee()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(ce => ce.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            IList<Entities.Adjustment> inMemoryDbAdjustments = fixture.Build<Entities.Adjustment>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .ToList();

            var firstItemGuid = inMemoryDbAdjustments[0].Identifier;

            var fakeDbAdjustments = new Mock<DbSet<Entities.Adjustment>>();
            fakeDbAdjustments.SetupWithQueryable(inMemoryDbAdjustments.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Adjustments).Returns(fakeDbAdjustments.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var adjustmentRepository = new AdjustmentRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var adjustment = await adjustmentRepository.GetAdjustmentAsync(firstItemGuid, Guid.NewGuid(), company.Identifier);
            Assert.IsNull(adjustment);
        }
        #endregion Get

        #region Get List

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetAdjustmentListShouldReturnMatchingEmployeeIdentifier()
        {

            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Adjustment> inMemoryDbAdjustments = fixture.Build<Entities.Adjustment>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .Where(a => a.Identifier != guidThatDoesntExist)
                .ToList();

            var fakeDbAdjustments = new Mock<DbSet<Entities.Adjustment>>();
            fakeDbAdjustments.SetupWithQueryable(inMemoryDbAdjustments.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Adjustments).Returns(fakeDbAdjustments.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var adjustmentRepository = new AdjustmentRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);
            var adjustments = await adjustmentRepository.GetAdjustmentsAsync(new Contracts.SearchFilters.AdjustmentSearchOptions { EmployeeIdentifier = Guid.NewGuid() }, company.Identifier);
            Assert.AreEqual(0, adjustments.Count);
            adjustments = await adjustmentRepository.GetAdjustmentsAsync(new Contracts.SearchFilters.AdjustmentSearchOptions { EmployeeIdentifier = employee.Identifier }, company.Identifier);
            Assert.AreEqual(inMemoryDbAdjustments.Count, adjustments.Count);
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetAdjustmentListShouldReturnMatchingFromDateResults()
        {

            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Adjustment> inMemoryDbAdjustments = fixture.Build<Entities.Adjustment>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .Where(a => a.Identifier != guidThatDoesntExist)
                .ToList();

            var fakeDbAdjustments = new Mock<DbSet<Entities.Adjustment>>();
            fakeDbAdjustments.SetupWithQueryable(inMemoryDbAdjustments.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Adjustments).Returns(fakeDbAdjustments.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var adjustmentRepository = new AdjustmentRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);

            var maxDate = inMemoryDbAdjustments.Max(a => a.DateOfIssuance);
            var minDate = inMemoryDbAdjustments.Min(a => a.DateOfIssuance);
            var adjustments = await adjustmentRepository.GetAdjustmentsAsync(new Contracts.SearchFilters.AdjustmentSearchOptions { FromDate = maxDate }, company.Identifier);
            Assert.AreEqual(inMemoryDbAdjustments.Where(a => a.DateOfIssuance >= maxDate).Count(), adjustments.Count);
            adjustments = await adjustmentRepository.GetAdjustmentsAsync(new Contracts.SearchFilters.AdjustmentSearchOptions { FromDate = minDate }, company.Identifier);
            Assert.AreEqual(inMemoryDbAdjustments.Where(a => a.DateOfIssuance >= minDate).Count(), adjustments.Count);
            adjustments = await adjustmentRepository.GetAdjustmentsAsync(new Contracts.SearchFilters.AdjustmentSearchOptions { FromDate = maxDate.AddDays(1) }, company.Identifier);
            Assert.AreEqual(0, adjustments.Count);
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task GetAdjustmentListShouldReturnMatchingToDateResults()
        {

            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Adjustment> inMemoryDbAdjustments = fixture.Build<Entities.Adjustment>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .Where(a => a.Identifier != guidThatDoesntExist)
                .ToList();

            var fakeDbAdjustments = new Mock<DbSet<Entities.Adjustment>>();
            fakeDbAdjustments.SetupWithQueryable(inMemoryDbAdjustments.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Adjustments).Returns(fakeDbAdjustments.Object);

            var fakePrincipalProvider = new Mock<ICustomPrincipalProvider>();
            fakePrincipalProvider.Setup(pp => pp.GetCurrentUserId()).Returns(companyUser.UserId);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var adjustmentRepository = new AdjustmentRepository(fakeDataContext.Object, fakePrincipalProvider.Object, null);

            var maxDate = inMemoryDbAdjustments.Max(a => a.DateOfIssuance);
            var minDate = inMemoryDbAdjustments.Min(a => a.DateOfIssuance);
            var adjustments = await adjustmentRepository.GetAdjustmentsAsync(new Contracts.SearchFilters.AdjustmentSearchOptions { ToDate = maxDate }, company.Identifier);
            Assert.AreEqual(inMemoryDbAdjustments.Where(a => a.DateOfIssuance <= maxDate).Count(), adjustments.Count);
            adjustments = await adjustmentRepository.GetAdjustmentsAsync(new Contracts.SearchFilters.AdjustmentSearchOptions { ToDate = minDate }, company.Identifier);
            Assert.AreEqual(inMemoryDbAdjustments.Where(a => a.DateOfIssuance <= minDate).Count(), adjustments.Count);
            adjustments = await adjustmentRepository.GetAdjustmentsAsync(new Contracts.SearchFilters.AdjustmentSearchOptions { ToDate = minDate.AddDays(-1) }, company.Identifier);
            Assert.AreEqual(0, adjustments.Count);
        }
        #endregion Get List

        #region Save

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task SaveAdjustmentShouldInsertAllFields()
        {
            var fixture = new Fixture();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(c => c.CompanyUsers)
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Adjustment> inMemoryDbAdjustments = new List<Entities.Adjustment>();
            IList<Entities.Employee> inMemoryDbEmployees = new List<Entities.Employee> { employee };

            var fakeDbAdjustments = new Mock<DbSet<Entities.Adjustment>>();
            fakeDbAdjustments.SetupWithQueryable(inMemoryDbAdjustments.AsQueryable());
            fakeDbAdjustments.SetupDBInsert(inMemoryDbAdjustments);

            var fakeDbEmployees = new Mock<DbSet<Entities.Employee>>();
            fakeDbEmployees.SetupWithQueryable(inMemoryDbEmployees.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Adjustments).Returns(fakeDbAdjustments.Object);
            fakeDataContext.Setup(c => c.Employees).Returns(fakeDbEmployees.Object);
            fakeDataContext.Setup(c => c.SaveChanges()).Returns(1);
            fakeDataContext.Setup(c => c.SaveChangesAsync()).ReturnsAsync(1);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var adjustmentRepository = new AdjustmentRepository(fakeDataContext.Object, null, null);

            var saveData = fixture.Create<DataTransferObjects.Adjustment>();

            var saveResult = await adjustmentRepository.SaveAdjustmentAsync(saveData, employee.Identifier, company.Identifier);

            Assert.AreEqual(1, inMemoryDbAdjustments.Count);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(saveData, inMemoryDbAdjustments[0]));
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task SaveAdjustmentWithMatchingIdentifierShouldUpdateAllFields()
        {
            var fixture = new Fixture();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(c => c.CompanyUsers)
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            Guid adjustmentGuid = Guid.NewGuid();
            Entities.Adjustment adjustment = fixture.Build<Entities.Adjustment>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.Identifier, adjustmentGuid)
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .Create();

            IList<Entities.Adjustment> inMemoryDbAdjustments = new List<Entities.Adjustment> { adjustment };
            IList<Entities.Employee> inMemoryDbEmployees = new List<Entities.Employee> { employee };

            var fakeDbAdjustments = new Mock<DbSet<Entities.Adjustment>>();
            fakeDbAdjustments.SetupWithQueryable(inMemoryDbAdjustments.AsQueryable());
            fakeDbAdjustments.SetupDBInsert(inMemoryDbAdjustments);

            var fakeDbEmployees = new Mock<DbSet<Entities.Employee>>();
            fakeDbEmployees.SetupWithQueryable(inMemoryDbEmployees.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Adjustments).Returns(fakeDbAdjustments.Object);
            fakeDataContext.Setup(c => c.Employees).Returns(fakeDbEmployees.Object);
            fakeDataContext.Setup(c => c.SaveChanges()).Returns(1);
            fakeDataContext.Setup(c => c.SaveChangesAsync()).ReturnsAsync(1);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var adjustmentRepository = new AdjustmentRepository(fakeDataContext.Object, null, null);

            var saveData = fixture.Build<DataTransferObjects.Adjustment>().With(a => a.Identifier, adjustmentGuid).Create();

            var saveResult = await adjustmentRepository.SaveAdjustmentAsync(saveData, employee.Identifier, company.Identifier);

            Assert.AreEqual(1, inMemoryDbAdjustments.Count);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(saveData, inMemoryDbAdjustments[0]));
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task SaveAdjustmentWithMatchingIdShouldUpdateAllFields()
        {
            var fixture = new Fixture();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(c => c.CompanyUsers)
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            int adjustmentId = 99485;
            Entities.Adjustment adjustment = fixture.Build<Entities.Adjustment>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.AdjustmentId, adjustmentId)
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .Create();

            IList<Entities.Adjustment> inMemoryDbAdjustments = new List<Entities.Adjustment> { adjustment };
            IList<Entities.Employee> inMemoryDbEmployees = new List<Entities.Employee> { employee };

            var fakeDbAdjustments = new Mock<DbSet<Entities.Adjustment>>();
            fakeDbAdjustments.SetupWithQueryable(inMemoryDbAdjustments.AsQueryable());
            fakeDbAdjustments.SetupDBInsert(inMemoryDbAdjustments);

            var fakeDbEmployees = new Mock<DbSet<Entities.Employee>>();
            fakeDbEmployees.SetupWithQueryable(inMemoryDbEmployees.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Adjustments).Returns(fakeDbAdjustments.Object);
            fakeDataContext.Setup(c => c.Employees).Returns(fakeDbEmployees.Object);
            fakeDataContext.Setup(c => c.SaveChanges()).Returns(1);
            fakeDataContext.Setup(c => c.SaveChangesAsync()).ReturnsAsync(1);

            // ensures mappings are registered for automapper
            AutoMapperConfiguration.RegisterAllMappings();
            var adjustmentRepository = new AdjustmentRepository(fakeDataContext.Object, null, null);

            var saveData = fixture.Build<DataTransferObjects.Adjustment>().With(a => a.AdjustmentId, adjustmentId).Create();

            var saveResult = await adjustmentRepository.SaveAdjustmentAsync(saveData, employee.Identifier, company.Identifier);

            Assert.AreEqual(1, inMemoryDbAdjustments.Count);
            Assert.IsTrue(ObjectCompareHelper.AreObjectPropertiesEqual(saveData, inMemoryDbAdjustments[0]));
        }

        #endregion Save

        #region Delete

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task DeleteAdjustmentWhereIdentifierDoesntExistsDoesNothing()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            Guid guidThatDoesntExist = Guid.NewGuid();
            IList<Entities.Adjustment> inMemoryDbAdjustments = fixture.Build<Entities.Adjustment>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .Where(a => a.Identifier != guidThatDoesntExist)
                .ToList();
            int NumberAdjustments = inMemoryDbAdjustments.Count;
            var fakeDbAdjustments = new Mock<DbSet<Entities.Adjustment>>();
            fakeDbAdjustments.SetupWithQueryable(inMemoryDbAdjustments.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Adjustments).Returns(fakeDbAdjustments.Object);

            var adjustmentRepository = new AdjustmentRepository(fakeDataContext.Object, null, null);
            await adjustmentRepository.DeleteAdjustmentAsync(guidThatDoesntExist, employee.Identifier, company.Identifier);
            Assert.AreEqual(NumberAdjustments, inMemoryDbAdjustments.Where(a => !a.IsDeleted).Count());
        }

        [TestMethod]
        [TestCategory("Repositories")]
        public async Task DeleteAdjustmentWhereIdentifierDoesExistsSetsToIsDeletedWithADeletedDate()
        {
            var fixture = new Fixture();

            Entities.CompanyUser companyUser = fixture.Build<Entities.CompanyUser>()
                .ActiveEntity()
                .NonDeletedEntity()
                .Without(cu => cu.Company).Create();

            Entities.Company company = fixture.Build<Entities.Company>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(c => c.CompanyUsers, new List<Entities.CompanyUser> { companyUser })
                .Without(c => c.Policies).Create();

            Entities.Employee employee = fixture.Build<Entities.Employee>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(e => e.Company, company)
                .With(e => e.CompanyId, company.CompanyId)
                .Without(e => e.Adjustments)
                .Without(e => e.Deductions)
                .Without(e => e.Work).Create();

            IList<Entities.Adjustment> inMemoryDbAdjustments = fixture.Build<Entities.Adjustment>()
                .ActiveEntity()
                .NonDeletedEntity()
                .With(a => a.EmployeeId, employee.EmployeeId)
                .With(a => a.Employee, employee)
                .CreateMany()
                .ToList();
            int NumberAdjustments = inMemoryDbAdjustments.Count;
            var fakeDbAdjustments = new Mock<DbSet<Entities.Adjustment>>();
            fakeDbAdjustments.SetupWithQueryable(inMemoryDbAdjustments.AsQueryable());

            var fakeDataContext = new Mock<WageDbContext>();
            fakeDataContext.Setup(c => c.Adjustments).Returns(fakeDbAdjustments.Object);

            var adjustmentRepository = new AdjustmentRepository(fakeDataContext.Object, null, null);
            await adjustmentRepository.DeleteAdjustmentAsync(inMemoryDbAdjustments[0].Identifier, employee.Identifier, company.Identifier);
            Assert.AreEqual(NumberAdjustments - 1, inMemoryDbAdjustments.Where(a => !a.IsDeleted).Count());
            Assert.IsTrue(inMemoryDbAdjustments[0].IsDeleted);
            Assert.IsTrue((DateTime.UtcNow - inMemoryDbAdjustments[0].DeletedDate.Value).Milliseconds < 1000);
        }

        #endregion Delete
    }
}
