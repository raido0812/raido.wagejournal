﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Extensions
{
    public static class DateTimeExtensions
    {
        public static IDictionary<DayOfWeek, int> DaysCountUntil(this DateTime startDate, DateTime endDate)
        {
            // plus one is because start and end date is both inclusive
            int totalDaysForPeriod = (int)Math.Floor(endDate.Subtract(startDate).TotalDays) + 1;
            int numberOfWeeks = (int)Math.Floor(totalDaysForPeriod / 7.0m);
            int fullWeekRemainder = totalDaysForPeriod % 7;

            IDictionary<DayOfWeek, int> daysCount = new Dictionary<DayOfWeek, int>()
                { 
                    {DayOfWeek.Monday,0},
                    {DayOfWeek.Tuesday,0},
                    {DayOfWeek.Wednesday,0},
                    {DayOfWeek.Thursday,0},
                    {DayOfWeek.Friday,0},
                    {DayOfWeek.Saturday,0},
                    {DayOfWeek.Sunday,0}
                };

            if (numberOfWeeks > 0)
            {
                daysCount[(DayOfWeek)Enum.Parse(typeof(DayOfWeek), (((int)startDate.DayOfWeek + 0) % 7).ToString())] += numberOfWeeks;
                daysCount[(DayOfWeek)Enum.Parse(typeof(DayOfWeek), (((int)startDate.DayOfWeek + 1) % 7).ToString())] += numberOfWeeks;
                daysCount[(DayOfWeek)Enum.Parse(typeof(DayOfWeek), (((int)startDate.DayOfWeek + 2) % 7).ToString())] += numberOfWeeks;
                daysCount[(DayOfWeek)Enum.Parse(typeof(DayOfWeek), (((int)startDate.DayOfWeek + 3) % 7).ToString())] += numberOfWeeks;
                daysCount[(DayOfWeek)Enum.Parse(typeof(DayOfWeek), (((int)startDate.DayOfWeek + 4) % 7).ToString())] += numberOfWeeks;
                daysCount[(DayOfWeek)Enum.Parse(typeof(DayOfWeek), (((int)startDate.DayOfWeek + 5) % 7).ToString())] += numberOfWeeks;
                daysCount[(DayOfWeek)Enum.Parse(typeof(DayOfWeek), (((int)startDate.DayOfWeek + 6) % 7).ToString())] += numberOfWeeks;
            }

            if (fullWeekRemainder > 0)
            {
                DateTime afterWeeks = startDate.AddDays(numberOfWeeks * 7);
                for (int i = 0; i < fullWeekRemainder; i++)
                {
                    daysCount[(DayOfWeek)Enum.Parse(typeof(DayOfWeek), (((int)afterWeeks.DayOfWeek + i) % 7).ToString())] += 1;
                }
            }
            
            return daysCount;
        }
    }
}
