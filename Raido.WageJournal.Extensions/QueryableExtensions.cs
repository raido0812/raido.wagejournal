﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Raido.WageJournal.Contracts.Enumerations;

namespace Raido.WageJournal.Extensions
{
    public static class QueryableExtensions
    {
        public static IQueryable<T> OrderBy<T>(this IQueryable<T> source, string columnName, SortDirection direction)
        {
            string directionMethodToCall = direction == SortDirection.Ascending ? "OrderBy" : "OrderByDescending";

            // Get the property to use
            var type = typeof(T);
            var property = type.GetProperty(columnName);

            // Create the lamba expression
            var parameter = Expression.Parameter(type, "p");
            var propertyAccess = Expression.MakeMemberAccess(parameter, property);
            var orderByExp = Expression.Lambda(propertyAccess, parameter);

            // Find the method which is to be called
            MethodCallExpression resultExp = Expression.Call(
                typeof(Queryable),
                directionMethodToCall,
                new Type[] { type, property.PropertyType },
                source.Expression,
                Expression.Quote(orderByExp));

            // Make magic
            return source.Provider.CreateQuery<T>(resultExp);
        }
    }
}
