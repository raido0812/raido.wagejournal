﻿
namespace Raido.WageJournal.Extensions
{
    public static class StringExtensions
    {
        #region Convert to bool
        public static bool AsBoolean(this string value)
        {
            return value.AsBoolean(default(bool));
        }

        public static bool AsBoolean(this string value, bool defaultValue)
        {
            bool temp = defaultValue;
            if (string.IsNullOrWhiteSpace(value) || !bool.TryParse(value, out temp))
            {
                return defaultValue;
            }

            return temp;
        }

        public static bool? AsNullableBoolean(this string value)
        {
            bool temp = false;
            if (string.IsNullOrWhiteSpace(value) || !bool.TryParse(value, out temp))
            {
                return null;
            }

            return temp;
        }
        #endregion

        #region Convert to decimal
        public static decimal AsDecimal(this string value)
        {
            return value.AsDecimal(default(decimal));
        }

        public static decimal AsDecimal(this string value, decimal defaultValue)
        {
            decimal temp = defaultValue;
            if (string.IsNullOrWhiteSpace(value) || !decimal.TryParse(value, out temp))
            {
                return defaultValue;
            }

            return temp;
        }

        public static decimal? AsNullableDecimal(this string value)
        {
            decimal temp = 0;
            if (string.IsNullOrWhiteSpace(value) || !decimal.TryParse(value, out temp))
            {
                return null;
            }

            return temp;
        }
        #endregion
    }
}
