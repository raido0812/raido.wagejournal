﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Extensions
{
    public static class BooleanExtensions
    {
        public static string AsYesNoText(this bool condition)
        {
            return condition.AsYesNoText(true);
        }

        public static string AsYesNoText(this bool condition, bool localize)
        {
            if (localize)
            {
                Localization.SharedText.Culture = System.Threading.Thread.CurrentThread.CurrentCulture;
            }
            else
            {
                Localization.SharedText.Culture = new System.Globalization.CultureInfo("en-us");
            }

            return condition ? Localization.SharedText.Yes : Localization.SharedText.No;
        }
    }
}
