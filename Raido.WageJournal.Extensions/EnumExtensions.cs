﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace Raido.WageJournal.Extensions
{
    public static class EnumExtensions
    {
        public static string GetDisplayName(this Enum e, Type enumDisplayTextResource)
        {
            var rm = new ResourceManager(enumDisplayTextResource);
            var resourceDisplayName = rm.GetString(e.GetType().Name + "_" + e);

            return string.IsNullOrWhiteSpace(resourceDisplayName) ? string.Format("[[{0}]]", e) : resourceDisplayName;
        }
    }
}
